import http from "../http-common";

class ProductDataService {
  getAll() {
    return http.get("/products");
  }

  get(id) {
    return http.get(`/products/${id}`);
  }

  getCart() {
    return http.get(`/cart`);
  }

  create(data) {
    return http.post("/products", data);
  }

  createToCart(data) {
    return http.post("/cart", data);
  }

  update(id, data) {
    return http.put(`/products/${id}`, data);
  }

  delete(id) {
    return http.delete(`/products/${id}`);
  }

  deleteCart(id) {
    return http.delete(`/cart/${id}`);
  }

  deleteAll() {
    return http.delete("/products/");
  }

  findByName(name) {
    return http.get(`/products/?name=${name}`);
  }
}

export default new ProductDataService();
