const os = require('os');

module.exports = {
  /**
   * This function returns true if the platform on which app is running is BigSur
   */
  isBigSurPlatform: function () {
    if (os.platform() !== 'darwin') {
      return false;
    }

    // Darwin version release for bigSur OS is '20'. Refer this https://en.wikipedia.org/wiki/MacOS_version_history#Releases
    return os.release().startsWith('20');
  }
};
