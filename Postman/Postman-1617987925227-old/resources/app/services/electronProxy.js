var HttpProxy = require('http-proxy'),
    path = require('path'),
    http = require('http'),
    https = require('https'),
    net = require('net'),
    fs = require('fs'),
    urlParser = require('url');

// This was commented out because this set the property on the entire process
// including requests made through the Postman UI
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

var regex_hostport = /^([^:]+)(:([0-9]+))?$/,
  proxyErrorMessage = 'You can find more information in the logs (View > Developer > View Logs in Finder/Explorer)',
  proxyGenericErrorMessage = `Postman\'s proxy was unable to forward your request correctly. ${proxyErrorMessage}`,
  proxyDNSFailedErrorMessage = `DNS Lookup failed. ${proxyErrorMessage}`,
  proxyConnectionRefusedErrorMessage = `No connection could be made. ${proxyErrorMessage}`;


// Port used to start the https-only server
// This is randomized in the startProxy method
var SECURE_PORT = 8443;

/**
 * @description It returns the host and port from the host string
 * @return {Array}
 */
function getHostPortFromString (hostString, defaultPort) {
  var host = hostString;
  var port = defaultPort;

  var result = regex_hostport.exec(hostString);
  if (result != null) {
    host = result[1];
    if (result[2] != null) {
      port = result[3];
    }
  }

  return ([host, port]);
}


exports.electronProxy = {
  proxy: null,
  proxy_s: null,
  proxyServer: null,
  proxyServer_s: null,
  lastHttpsUrl: null,
  startProxy: function (port, proxyClosedCallback, capturedRequestCallback, proxyStartErrorCallback, proxyStartSuccessCallback) {
    try {
      // set new secure port to proxy https requests
      // This is the range of private port numbers
      // that other apps are unlikely to have taken up
      SECURE_PORT = 49152 + parseInt(Math.random() * 16383);

      var oldThis = this;
      this.proxy = HttpProxy.createProxyServer({
        secure: false,
        ssl: {
          key: fs.readFileSync(path.resolve(__dirname, '../certificates/proxy.key'), 'utf8'),
          cert: fs.readFileSync(path.resolve(__dirname, '../certificates/proxy.crt'), 'utf8')
        }
      });
      this.proxy_s = HttpProxy.createProxyServer({
        secure: false,
        ssl: {
          key: fs.readFileSync(path.resolve(__dirname, '../certificates/proxy.key'), 'utf8'),
          cert: fs.readFileSync(path.resolve(__dirname, '../certificates/proxy.crt'), 'utf8')
        }
      });

      this.proxy.on('error', function (err, req, res) {
        if (err.code === 'ECONNREFUSED') {
          res.end(proxyConnectionRefusedErrorMessage);
        }
        else if (err.code === 'ENOTFOUND') {
          res.end(proxyDNSFailedErrorMessage);
        }
        else {
          res.end(proxyGenericErrorMessage);
        }
        pm.logger.error('electronProxy - Error proxying http', err);
      });

      this.proxy.on('close', function (e) {
        pm.logger.info('electronProxy - Shut down http proxy');
        proxyClosedCallback();
      });

      this.proxy_s.on('error', function (err, req, res) {
        if (err.code === 'ECONNREFUSED') {
          res.end(proxyConnectionRefusedErrorMessage);
        }
        else if (err.code === 'ENOTFOUND') {
          res.end(proxyDNSFailedErrorMessage);
        }
        else {
          res.end(proxyGenericErrorMessage);
        }
        pm.logger.error('electronProxy - Error proxying https', err);
      });

      this.proxy_s.on('close', function (e) {
        pm.logger.info('electronProxy - Shut down https proxy');
        proxyClosedCallback();
      });

      this.proxyServer = http.createServer(function (req, res) {
        var urlParts = urlParser.parse(req.url, true);
        var target1 = urlParts.protocol + '//' + urlParts.host;
        try {
          var queryData = '';
          if (req.method === 'POST' || req.method === 'PUT') {
            req.on('data', function (data) {
              queryData += data;
              if (queryData.length > 1e6) {
                queryData = '';
              }
              capturedRequestCallback(req.url, req.method, req.headers, queryData);
            });
          } else {
            capturedRequestCallback(req.url, req.method, req.headers);
          }
          oldThis.proxy.web(req, res, { target: target1 });
        }
        catch (e) {
          pm.logger.error('electronProxy - Error proxying', e);
        }
      });

      this.proxyServer.on('error', (e) => {
        this.proxyServer.close();
        if (e.code === 'EADDRINUSE') {
          proxyStartErrorCallback('One of the ports the proxy server requires ' +
            '(' + port + ') is already in use. Please choose a different port ' +
            'before restarting the proxy server.');
        }
      });

      try {
        pm.logger.info('Attempting to start proxy server on port: ' + port + '...');
        this.proxyServer.listen(port);
        if (this.proxyServer.listening) {
          // proxy server is ready to listen
          proxyStartSuccessCallback();
        }
      }
      catch (e) {
        pm.logger.error('electronProxy - Error in starting proxy', e);
      }

      this.proxyServer.addListener(
        'connect',
        function (request, socketRequest, bodyhead) {
          var url = request.url;
          oldThis.lastHttpsUrl = url;
          var httpVersion = request.httpVersion;
          var hostport = getHostPortFromString(url, 443);
          hostport[0] = 'localhost';
          hostport[1] = SECURE_PORT;
          var proxySocket = new net.Socket();

          proxySocket.connect(
            parseInt(hostport[1]), hostport[0],
            function () {
              proxySocket.write(bodyhead);
              socketRequest.write('HTTP/' + httpVersion + ' 200 Connection established\r\n\r\n');
            }
          );

          proxySocket.on('data', function (chunk) {
            socketRequest.write(chunk);
          });

          proxySocket.on('end', function () {
            socketRequest.end();
          });

          socketRequest.on('data', function (chunk) {
            proxySocket.write(chunk);
          });

          socketRequest.on('end', function () {
            proxySocket.end();
          });

          proxySocket.on('error', function (err) {
            socketRequest.write('HTTP/' + httpVersion + '500Connectionerror\r\n\r\n');
            socketRequest.end();
          });

          socketRequest.on('error', function (err) {
            proxySocket.end();
          });
        }
      );


      this.proxyServer_s = https.createServer(
        {
          key: fs.readFileSync(path.resolve(__dirname, '../certificates/key.pem'), 'utf8'),
          cert: fs.readFileSync(path.resolve(__dirname, '../certificates/cert.pem'), 'utf8')
        }, function (req, res) {
          if (req.url[0] == '/') {
            req.url = 'https://' + req.socket.servername + req.url;
          }
          var urlParts = urlParser.parse(req.url, true);
          var target1 = urlParts.protocol + '//' + urlParts.host;
          try {
            var queryData = '';
            if (req.method === 'POST' || req.method === 'PUT') {
              req.on('data', function (data) {
                queryData += data;
                if (queryData.length > 1e6) {
                  queryData = '';
                }
                capturedRequestCallback(req.url, req.method, req.headers, queryData);
              });
            } else {
              capturedRequestCallback(req.url, req.method, req.headers);
            }
            oldThis.proxy_s.web(req, res, { target: target1 });
          }
          catch (e) {
            pm.logger.error('electronProxy - Error in https proxy: ', e);
          }
        }
      );

      this.proxyServer_s.on('error', (e) => {
        this.proxyServer_s.close();
        if (e.code === 'EADDRINUSE') {
          // This is going to be very rare
          // will only occur when a randomly chosen port is in-use
          proxyStartErrorCallback('One of the ports the proxy server internally requires ' +
            ' (' + SECURE_PORT + ') is already in use. Try restarting the proxy server.');
        }
      });

      var a = this.proxyServer_s.listen(SECURE_PORT, function (err) {
        if (err) { pm.logger.error('electronProxy - Error creating https proxy ' + err); }
      });


      return 0;
    }
    catch (e) {
      pm.logger.error('electronProxy - Error listening to Port: ', e);
      return -1;
    }
  },

  stopProxy: function () {
    var proxy = this.proxy;
    var proxy_s = this.proxy_s;
    var proxyServer = this.proxyServer;
    var proxyServer_s = this.proxyServer_s;
    if (proxy) { proxy.close(); }
    if (proxy_s) { proxy_s.close(); }
    if (proxyServer) { proxyServer.close(); }
    if (proxyServer_s) { proxyServer_s.close(); }
  }
};
