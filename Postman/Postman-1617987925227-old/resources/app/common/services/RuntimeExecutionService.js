// ###### WARNING: DO NOT REQUIRE NON-ISOMORPHIC LIBRARIES HERE
let _ = require('lodash'),
  async = require('async'),

  EventProcessor = require('./RuntimeEventProcessor'),

  // ###### WARNING: DO NOT REQUIRE NON-ISOMORPHIC LIBRARIES HERE

  // Character detection to node encoding map
  CHARDET_BUFF_MAP = {
    ASCII: 'ascii',
    'UTF-8': 'utf8',
    'UTF-16LE': 'utf16le',
    'ISO-8859-1': 'latin1'
  },

  detectEncoding = (buff) => CHARDET_BUFF_MAP[chardet.detect(buff)],

  isBrowser = false,


  // TODO: Implement these using a dependency injection model like awilix
  fs,
  os,
  app,
  path,
  dialog,
  chardet,
  session,
  PostmanFs,
  CookieJar,
  dryRunRequest,
  getSystemProxy,
  SerializedError,
  collectionRunner,
  sanitizeFilename,
  postmanCollectionSdk,

  defaultWorkingDir,

  activeRuns = {};

/**
 * Helper function to get the file extension given a mime-type
 * @param {String} mimeType
 */
function __getProbableExtension (mimeType) {
  var mimeExtensions = [
    {
      typeSubstring: 'text',
      extension: '.txt'
    },
    {
      typeSubstring: 'json',
      extension: '.json'
    },
    {
      typeSubstring: 'javascript',
      extension: '.js'
    },
    {
      typeSubstring: 'pdf',
      extension: '.pdf'
    },
    {
      typeSubstring: 'png',
      extension: '.png'
    },
    {
      typeSubstring: 'jpg',
      extension: '.jpg'
    },
    {
      typeSubstring: 'jpeg',
      extension: '.jpg'
    },
    {
      typeSubstring: 'gif',
      extension: '.gif'
    },
    {
      typeSubstring: 'excel',
      extension: '.xls'
    },
    {
      typeSubstring: 'zip',
      extension: '.zip'
    },
    {
      typeSubstring: 'compressed',
      extension: '.zip'
    },
    {
      typeSubstring: 'audio/wav',
      extension: '.wav'
    },
    {
      typeSubstring: 'tiff',
      extension: '.tiff'
    },
    {
      typeSubstring: 'shockwave',
      extension: '.swf'
    },
    {
      typeSubstring: 'powerpoint',
      extension: '.ppt'
    },
    {
      typeSubstring: 'mpeg',
      extension: '.mpg'
    },
    {
      typeSubstring: 'quicktime',
      extension: '.mov'
    },
    {
      typeSubstring: 'html',
      extension: '.html'
    },
    {
      typeSubstring: 'css',
      extension: '.css'
    }
  ];

  for (var i = 0; i < mimeExtensions.length; i++) {
    if (mimeType.indexOf(mimeExtensions[i].typeSubstring) > -1) {
      return mimeExtensions[i].extension;
    }
  }

  return '';
}


// #region API Functions

/**
 * @private
 */
function trackRun (executionId, run, processor) {
  // Set the processor as the runs processor
  run.processor = processor;

  activeRuns[executionId] = run;
}

/**
 * @private
 */
function addAborter (executionId, aborter) {
  activeRuns[executionId] && (activeRuns[executionId].aborter = aborter);
}

/**
 * @private
 */
function removeAborter (executionId) {
  activeRuns[executionId] && (delete activeRuns[executionId].aborter);
}

/**
 * @private
 * Sanitizes options to be sent to runtime. Mostly converting objects into SDK instances.
 *
 * @param {Object} rawOptions
 */
function sanitizeRunOptions (rawOptions) {
  if (!rawOptions) {
    return;
  }

  if (rawOptions.useSystemProxy && !!getSystemProxy) {
    rawOptions.systemProxy = getSystemProxy;
  }

  if (rawOptions.proxies) {
    rawOptions.proxies = new postmanCollectionSdk.ProxyConfigList({}, rawOptions.proxies);
  }

  rawOptions.certificates = new postmanCollectionSdk.CertificateList({}, rawOptions.certificates);
}

/**
 * @private
 * Save the given stream to a file the user chooses
 *
 * @param {Object} contentInfo
 * @param {Buffer} stream
 */
function saveStreamToFile (contentInfo, stream, cb) {
  let name;

  // sdkResponse override
  if (contentInfo && contentInfo.fileName) {
    name = contentInfo.fileName || '';
  }

  if (_.isEmpty(name)) {
    name = 'response';
    name = (contentInfo && contentInfo.mimeType) ? `${name}${__getProbableExtension(contentInfo.mimeType)}` : name;
  }

  // TODO: Implement browser specific logic here
  if (isBrowser) {
    return;
  }

  // WARNING: All usages below require native dependencies hence be careful in browser environment

  name = sanitizeFilename(name, { replacement: '-' });

  dialog.showSaveDialog({
    title: 'Select path to save file',
    defaultPath: name // Default filename to be used
  }).then((result) => {
    if (result.canceled) {
      // If the request was cancelled then don't do anything, not even call the callback;
      return cb(null, false);
    }

    fs.writeFile(result.filePath, stream, (err) => {
      return cb(err, true);
    });
  })
  .catch(cb);
}

/**
 * Abort and cleanup an existing collection run
 *
 * @param {String} executionId - The execution id to terminate or dispose
 * @param {Function} emit - emitter to call to denote the execution has terminated/disposed
 */
function disposeRun (executionId, emit = _.noop) {
  if (!executionId || !activeRuns[executionId]) {
    return;
  }

  const run = activeRuns[executionId];

  run.host && run.host.dispose && run.host.dispose();

  // dispose the reference
  activeRuns[executionId] = null;

  emit({ id: executionId, event: '__dispose__' });
}

/**
 * @public
 * Stops and disposes an existing collection run
 *
 * @param {String} executionId - The execution id to terminate or dispose
 * @param {Function} emit - emitter to call to denote the execution has terminated/disposed
 */
function stopRun (executionId, emit) {
  if (!executionId || !activeRuns[executionId]) {
    return;
  }

  const run = activeRuns[executionId];

  run.aborter && run.aborter.abort();
  run.abort();

  // Force call the stop event as postman-runtime will no longer call any event
  run.processor.call('abort', [null]);

  disposeRun(executionId, emit);
}

/**
 * @public
 * Pause the current collection run
 *
 * @param {String} executionId
 */
function pauseRun (executionId) {
  if (!executionId || !activeRuns[executionId]) {
    return;
  }

  activeRuns[executionId].pause();
}

/**
 * @public
 * Resume the paused current collection run
 *
 * @param {String} executionId
 */
function resumeRun (executionId) {
  if (!executionId || !activeRuns[executionId]) {
    return;
  }

  activeRuns[executionId].resume();
}

/**
 * @public
 * Start a collection run with the given collection and variables
 *
 * @param {Object} info
 * @param {Object} collection
 * @param {Object} variables
 * @param {Object} options
 */
function startRun (info, collection, variables, options = {}, cookieManager, emit) {
  const sdkCollection = new postmanCollectionSdk.Collection(collection),

    // Create an event processor instance and handling runtime events
    eventProccessor = new EventProcessor(info.schema, (event, data, refs) => {
      emit({ id: info.id, event, data, refs });
    });

  let cookieJar;

  // We have the CookieManager
  if (cookieManager) {
    cookieJar = new CookieJar(cookieManager, {
      programmaticAccess: options.cookieConfiguration,
      readFromDB: !_.get(info, 'cookie.runtimeWithEmptyJar', false),
      writeToDB: !_.get(info, 'cookie.disableRealtimeWrite', false),
      onCookieAccessDenied: (domain) => {
        let message = `Unable to access "${domain}" cookie store.` +
          ' Try whitelisting the domain in "Manage Cookies" screen.' +
          ' View more detailed instructions in the Learning Center: https://go.pstmn.io/docs-cookies';

        emit({
          name: 'log',
          namespace: 'console',
          data: {
            id: info.id,
            cursor: {},
            level: 'warn',
            messages: [message]
          }
        });
      }
    });

    _.set(options, ['requester', 'cookieJar'], cookieJar);
  }

  // fileResolver defined and we have PostmanFs
  if (options.fileResolver && !!PostmanFs) {
    let { workingDir, insecureFileRead, fileWhitelist } = options.fileResolver;

    _.set(options, 'fileResolver', new PostmanFs(workingDir || defaultWorkingDir, insecureFileRead, fileWhitelist));
  }

  // sanitize
  sanitizeRunOptions(options);

  // add variables
  variables.environment && (options.environment = new postmanCollectionSdk.VariableScope(variables.environment));
  variables.globals && (options.globals = new postmanCollectionSdk.VariableScope(variables.globals));

  collectionRunner.run(sdkCollection, options, function (err, run) {
    if (err) {
      pm.logger.error('RuntimeExecutionService~startRun - Error in starting the run', err);

      this.emit({ id: info.id, event: '__dispose__', error: true });
      return;
    }

    trackRun(info.id, run, eventProccessor);

    // Intercept beforeRequest and response and done events
    eventProccessor.intercept('beforeRequest', (_, __, ___, ____, aborter) => {
      addAborter(info.id, aborter);
    });

    eventProccessor.intercept('response', (err, _, response) => {
      removeAborter(info.id);

      // If the response is present, and download is set then
      if (!err && response && info.download) {
        saveStreamToFile(response.contentInfo(), response.stream, (err, success) => {
          // If there is an error success then emit the download event
          (err || success) && eventProccessor.call('download', [err]);
        });
      }
    });

    eventProccessor.intercept('done', () => {
      if (info.cookie && info.cookie.saveAfterRun && cookieJar && typeof cookieJar.updateStore === 'function') {
        // TODO - DECIDE on what to do with cookies when there was an error in done
        cookieJar.updateStore(() => {
          disposeRun(info.id, emit);
        });

        return;
      }
      disposeRun(info.id, emit);
    });

    // Note: .handlers should be called after all interceptors have been attached
    // else events not part of schema will be ignored
    run.start(eventProccessor.handlers());
  });
}

/**
 * @public
 * LivePreview: Calling runtime's `dryRunRequest` to get previewed request
 *
 * @param {Request} request
 * @param {Object} info
 * @param {Object} options
 * @param {Function} cb
 */
function previewRequest (request, options = {}, cookieManager, cb) {
  let requestToPreview = new postmanCollectionSdk.Request(request),
    dryRunOptions = _.pick(options, ['implicitCacheControl', 'implicitTraceHeader', 'protocolProfileBehavior']),
    disableCookies = dryRunOptions.protocolProfileBehavior && dryRunOptions.protocolProfileBehavior.disableCookies;

  !disableCookies && CookieJar &&
    (dryRunOptions.cookieJar = new CookieJar(cookieManager, { readFromDB: true, writeToDB: false }));

  try {
    dryRunRequest(requestToPreview, dryRunOptions, (err, previewedRequest) => {
      cb(err, previewedRequest && previewedRequest.toJSON());
    });
  }
  catch (e) {
    pm.logger.error('RuntimeExecutionService~previewRequest.dryRunRequest', e);
  }
}

/**
 * Checks if the given path is within the working directory
 */
function isInWorkingDir (workingDir, path) {
  return Boolean((new PostmanFs(workingDir))._resolve(path, []));
}

/**
 * Create Temporary File
 * @param name
 * @param content
 */
function createTemporaryFile (name, content, cb) {
  const basePath = app.getPath('temp'),
    tempFilePath = path.join(basePath, name);

  async.waterfall([
    // Attempt to clear the file if it already exists.
    // Note: We ignore the error here
    (next) => fs.unlink(tempFilePath, () => next()),

    // Write the contents of the temp directory
    (next) => fs.writeFile(tempFilePath, content, next)
  ], (err) => {
    cb(err && new SerializedError(err), tempFilePath);
  });
}

/**
 * Read file from filesystem
 * @param {String} id
 * @param {String} path
 */
function readFile (path, cb) {
  fs.readFile(path, (err, content) => {
    if (!err) {
      try {
        // From here on we will try detect the encoding and convert the buffer accordingly
        content = content.toString(detectEncoding(content));
      } catch (e) {
        err = new Error('Failed to detect encoding of the file content');
      }
    }

    return cb(err && new SerializedError(err), content);
  });
}

/**
 * Check if the given path has the required file-system access
 * @param {String} id
 * @param {String} path
 * @param {Boolean} writable
 */
function accessFile (path, writeable, cb) {
  // If no path is given then there is nothing to check
  if (!path) {
    return cb();
  }

  const perm = writeable ? (fs.constants.R_OK | fs.constants.W_OK) : (fs.constants.R_OK);

  fs.access(path, perm, (err) => {
    cb(err && new SerializedError(err));
  });
}

/**
 * Clear all cookies for the given partition id
 *
 * @param {String} partitionId Cookie partition id
 * @param {Function} cb
 */
function clearAllElectronCookies (partitionId, cb) {
  if (!partitionId) {
    return cb();
  }

  const partition = session.fromPartition(partitionId);

  // NOTE: This will clear all the cookies from this partition
  partition.clearStorageData({ storages: ['cookies'] })
    .then(() => cb && cb())
    .catch((err) => cb && cb(new SerializedError(err)));
}

// #endregion

// We have two places that this is going to be used main process in electron and the we agent, hence we expose two API's here
module.exports = function () {
  /* native-ignore:start */ // IMPORTANT: Do not remove this comment. Used by webpack to ignore this section

  const postmanRuntime = require('postman-runtime');

  fs = require('fs');
  os = require('os');
  path = require('path');
  chardet = require('chardet');
  app = require('electron').app;
  dialog = require('electron').dialog;
  session = require('electron').session;
  sanitizeFilename = require('sanitize-filename');
  postmanCollectionSdk = require('postman-collection');
  SerializedError = require('serialised-error');

  // These are present in the main directory
  CookieJar = require('./CookieJar');
  getSystemProxy = require('../../utils/getSystemProxy');

  PostmanFs = require('../utils/postmanFs'); // This is within the common folder

  collectionRunner = new postmanRuntime.Runner();
  dryRunRequest = postmanRuntime.Requester && postmanRuntime.Requester.dryRun;

  defaultWorkingDir = path.join(os.homedir(), 'Postman', 'files');

  isBrowser = false;


  // WARNING: Be very contingent of what is being exposed here. Some API's need native dependency that may not be available for the web
  // TODO: This needs a better api in-terms of DEPENDENCY INJECTION. Can be worked out alter to prevent bugs
  return {
    startRun,
    stopRun,
    pauseRun,
    resumeRun,
    previewRequest,

    // Only native
    isInWorkingDir,
    createTemporaryFile,
    readFile,
    accessFile,
    defaultWorkingDir,
    saveStreamToFile,
    clearAllElectronCookies
  };

  /* native-ignore:end */ // IMPORTANT: Do not remove this comment. Used by webpack to ignore this section
};


module.exports.Browser = async function () {
  const postmanRuntime = await import(/* webpackChunkName: "postman-runtime" */ 'postman-runtime/dist');

  chardet = require('chardet');
  postmanCollectionSdk = require('postman-collection');
  SerializedError = require('serialised-error');
  CookieJar = require('./CookieJar');

  collectionRunner = new postmanRuntime.Runner();
  dryRunRequest = postmanRuntime.Requester && postmanRuntime.Requester.dryRun;

  isBrowser = true;


  return {
    startRun,
    stopRun,
    pauseRun,
    resumeRun,

    previewRequest,
    saveStreamToFile
  };
};
