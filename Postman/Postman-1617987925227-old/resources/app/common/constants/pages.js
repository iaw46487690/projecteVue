const SCRATCHPAD = 'scratchpad';
const OPEN_WORKSPACE_IDENTIFIER = 'workspace.open';

module.exports = {
  OPEN_WORKSPACE_IDENTIFIER,
  SCRATCHPAD
};
