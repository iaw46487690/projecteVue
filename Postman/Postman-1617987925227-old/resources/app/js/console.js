/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		2: 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "" + ({"9":"controllers/SideBarMockController","10":"components/SidebarMockListing","11":"CommonLazyChunk","12":"components/MockInfoContextView","13":"postman-converters","14":"monaco-editor","15":"postman-code-generators","16":"controllers/MonitorSidebarController","17":"containers/MonitorSidebarContainer","18":"controllers/MonitorContextBarController","19":"containers/MonitorActivityLogs","20":"controllers/MonitorActivityDetailsController","21":"containers/MonitorActivityInfo","22":"contextbar/FolderInfoCBView/FolderInfoCBView","23":"contextbar/FolderInfoCBController","24":"sidebar/CollectionSidebarView","25":"sidebar/CollectionSidebarController","26":"contextbar/CollectionInfoCBView/CollectionInfoCBView","27":"contextbar/CollectionInfoCBController","28":"contextbar/GlobalsInfoCBView/GlobalsInfoCBView","29":"sidebar/EnvironmentSidebarView","30":"sidebar/EnvironmentSidebarController","31":"contextbar/EnvironmentInfoCBView/EnvironmentInfoCBView","32":"contextbar/EnvironmentInfoCBController","33":"contextbar/ExampleInfoCBView/ExampleInfoCBView","34":"contextbar/RequestInfoCBView/RequestInfoCBView","35":"js/containers/history/HistorySidebarContainer","36":"HistorySidebarController","37":"pull-request/components/PullRequestMeta","38":"changelog/components/CollectionChangelog","39":"fork/ForkListing","40":"components/DocumentationContextBarView","41":"controllers/DocumentationContextBarController","42":"components/api-sidebar/APISidebarContainer/APISidebarContainer","43":"controllers/APISidebarController","44":"components/api-editor/api-context-bar/APIInfoContextBarView/APIInfoContextBarView","45":"components/api-editor/api-context-bar/CommentsContextBarView/CommentsContextBarView","46":"swagger1-to-postman","47":"swagger2-to-postmanv2","48":"@postman/dhc-to-postman","49":"raml1-to-postman","50":"raml-to-postman","51":"@postman/runscope-to-postman","52":"curl-to-postmanv2","53":"@postman/wadl-to-postman","54":"graphql-to-postman","55":"components/CodeCBView","56":"controllers/CodeCBController","57":"controllers/CommentsContextBarController","58":"StatusBarContainer","59":"WorkspaceBrowser","60":"containers/SchemaChangelogContainer","63":"jsonMode","64":"html","65":"htmlMode","66":"xml","67":"javascript","68":"tsMode","69":"markdown","70":"yaml","71":"graphql","72":"csharp","73":"cpp","74":"fsharp","75":"powershell","76":"go","77":"java","78":"objective-c","79":"php","80":"python","81":"ruby","82":"swift"}[chunkId]||chunkId) + ".js"
/******/ 	}
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.name = 'ChunkLoadError';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "../js/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([6757,4,8]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ 15062:
/***/ (function(module, exports) {

module.exports = require("child_process");

/***/ }),

/***/ 2957:
/***/ (function(module, exports) {

module.exports = require("os");

/***/ }),

/***/ 320:
/***/ (function(module, exports) {

module.exports = require("electron");

/***/ }),

/***/ 4245:
/***/ (function(module, exports) {

module.exports = require("zlib");

/***/ }),

/***/ 435:
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),

/***/ 450:
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ 6757:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(6758);


/***/ }),

/***/ 6758:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _electron_ElectronService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(319);
/* harmony import */ var _styles_console_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6759);
/* harmony import */ var _styles_console_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_console_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _init__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6760);
/* harmony import */ var _controllers_theme_ThemeManager__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2560);
/* harmony import */ var _utils_TelemetryHelpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6614);
/* harmony import */ var _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1834);
/* harmony import */ var _runtime_components_console_Console__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5415);
/* harmony import */ var _components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3488);
/* harmony import */ var _shared_ThemeContext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6615);















if (false) {} else
if (window.SDK_PLATFORM !== 'browser') {
  window.onbeforeunload = () => {
    return false;
  };
}

/**
   * Shims requestIdleCallback in case a browser doesn't support it.
   */
window.requestIdleCallback = window.requestIdleCallback ||
function (cb) {
  return setTimeout(function () {
    var start = Date.now();
    cb({
      didTimeout: false,
      timeRemaining: function () {
        return Math.max(0, 50 - (Date.now() - start));
      } });

  }, 1);
};

window.cancelIdleCallback = window.cancelIdleCallback ||
function (id) {
  clearTimeout(id);
};

/**
    * Hides the placeholder loading state
    */
function hideLoadingState() {
  let loader = document.getElementsByClassName('pm-loader')[0];
  loader && loader.classList.add('is-hidden');
}

const rootEl = document.getElementsByClassName('app-root')[0];

_init__WEBPACK_IMPORTED_MODULE_4__["default"].init(err => {
  // We hide the loading state just before the point when we are rendering the console. Earlier,
  // this was happening while initializing ThemeDomDelegator. This was causing an issue where there
  // was a blank white screen rendered after the loader was hidden and before the console got
  // rendered. This was resulting in a jarring experience while loading the console.
  hideLoadingState();

  if (err) {
    Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["render"])(react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_9__["default"], { showError: true }), rootEl);
    return;
  }

  Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["render"])(
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_9__["default"], null,
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_shared_ThemeContext__WEBPACK_IMPORTED_MODULE_10__["Theme"], null,
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_runtime_components_console_Console__WEBPACK_IMPORTED_MODULE_8__["default"], {
        currentTheme: _controllers_theme_ThemeManager__WEBPACK_IMPORTED_MODULE_5__["default"].getCurrentTheme(),
        showBlobInModal: true,
        withDropdownRoot: true,
        showHeader: true,
        showFooter: true,
        isWindow: true }))),



  rootEl,
  () => {
    let loadTime = Object(_utils_TelemetryHelpers__WEBPACK_IMPORTED_MODULE_6__["getWindowLoadTime"])();

    // TODO: Finalize if these are the events we are going to emit and what the final console events going to be
    _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_7__["default"].addEvent('app_performance_metric', 'console_window_loaded', null, null, { load_time: loadTime });

    Object(_electron_ElectronService__WEBPACK_IMPORTED_MODULE_2__["getLaunchParams"])().
    then(([_w, params]) => {
      if (params.triggerSource === 'menuAction') {
        _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_7__["default"].addEventV2({
          category: 'console',
          action: 'open_window',
          label: 'menubar' });

      }
    });
  });

});

/***/ }),

/***/ 6759:
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 6760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var async_series__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(18);
/* harmony import */ var async_series__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(async_series__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _boot_bootAppModels__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6419);
/* harmony import */ var _boot_booted__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6509);
/* harmony import */ var _boot_bootThemeManager__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6510);
/* harmony import */ var _boot_bootConfig__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(66);
/* harmony import */ var _boot_bootConsole__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6761);
/* harmony import */ var _boot_bootCrashReporter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6428);
/* harmony import */ var _boot_bootLogger__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5895);
/* harmony import */ var _boot_bootMessaging__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5986);
/* harmony import */ var _boot_bootSettings__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(6423);
/* harmony import */ var _boot_bootTelemetry__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6426);
/* harmony import */ var _boot_bootWLModels__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(5991);
/* harmony import */ var _boot_bootSession__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(6471);
/* harmony import */ var _utils_DisableProcessThrottling__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(6527);

















// Initialize the global window.pm.* API
window.pm = window.pm || {};

pm.init = done => {
  async_series__WEBPACK_IMPORTED_MODULE_0___default()([
  // Configure the window
  _boot_bootConfig__WEBPACK_IMPORTED_MODULE_4__["Config"].init({ process: 'console', ui: true }),

  // Initialize the pm.logger interface
  _boot_bootLogger__WEBPACK_IMPORTED_MODULE_7__["default"],

  // Initialize the pm.mediator event bus interface
  _boot_bootMessaging__WEBPACK_IMPORTED_MODULE_8__["default"],

  // Initialize sentry crash reporter
  _boot_bootCrashReporter__WEBPACK_IMPORTED_MODULE_6__["default"],

  // Initialize analytics interface
  _boot_bootTelemetry__WEBPACK_IMPORTED_MODULE_10__["default"],

  // Initialize the pm.settings interface
  _boot_bootSettings__WEBPACK_IMPORTED_MODULE_9__["default"],

  // Initialize the waterline indexDB models
  _boot_bootWLModels__WEBPACK_IMPORTED_MODULE_11__["default"],

  // Initialize pm.app and pm.appWindow interfaces
  _boot_bootAppModels__WEBPACK_IMPORTED_MODULE_1__["default"],

  // Initialize the theme manager
  _boot_bootThemeManager__WEBPACK_IMPORTED_MODULE_3__["default"],

  // Initialize the console window
  _boot_bootConsole__WEBPACK_IMPORTED_MODULE_5__["default"],

  // Initialize pm.window for menuManager shortcuts
  _boot_bootSession__WEBPACK_IMPORTED_MODULE_12__["bootSession"]],
  err => {
    Object(_boot_booted__WEBPACK_IMPORTED_MODULE_2__["default"])(err);
    if (err) {
      pm.logger.error('Error in console boot sequence', err);
    }

    // Disabling throttling for this process. This is to prevent electron from
    // throttling actions for this process even if it is running in the background
    Object(_utils_DisableProcessThrottling__WEBPACK_IMPORTED_MODULE_13__["default"])();

    done && done(err);
  });
};

/* harmony default export */ __webpack_exports__["default"] = (pm);

/***/ }),

/***/ 6761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var async_series__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(18);
/* harmony import */ var async_series__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(async_series__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _bootShortcuts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6475);
/* harmony import */ var _bootStore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6473);
/* harmony import */ var _controllers_GenericContextMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6762);
/* harmony import */ var _controllers_UIZoom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6478);
/* harmony import */ var _models_ToastManager__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6465);
/* harmony import */ var _models_Toasts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6469);










/**
                                                * Boot the console window with stores
                                                * @param {*} cb
                                                */
function bootConsole(cb) {
  async_series__WEBPACK_IMPORTED_MODULE_0___default()([
  _bootStore__WEBPACK_IMPORTED_MODULE_2__["default"],
  _bootShortcuts__WEBPACK_IMPORTED_MODULE_1__["default"]],
  () => {
    _.assign(window.pm, {
      uiZoom: new _controllers_UIZoom__WEBPACK_IMPORTED_MODULE_4__["default"](),
      toasts: _models_Toasts__WEBPACK_IMPORTED_MODULE_6__,
      toastManager: new _models_ToastManager__WEBPACK_IMPORTED_MODULE_5__["default"](),

      // Disabling electron context menu in browser
      contextMenuManager: window.SDK_PLATFORM === 'browser' ? {} : new _controllers_GenericContextMenu__WEBPACK_IMPORTED_MODULE_3__["default"]() });


    pm.logger.info('Console~boot - Success');

    return cb && cb();
  });
}

/* harmony default export */ __webpack_exports__["default"] = (bootConsole);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GenericContextMenu; });
/* harmony import */ var _electron_ElectronService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(319);
let

GenericContextMenu = class GenericContextMenu {
  constructor() {
    this.Remote = __webpack_require__(320).remote;
    this.Menu = this.Remote.Menu;
    this.MenuItem = this.Remote.MenuItem;
    this.currentSelection = '';

    /**
                                 * On right click in any input field, text gets automatically selected even if there was no explicit selection made before.
                                 * The text which gets automatically selected on right click, depends on the cursor position.
                                 */
    window.addEventListener('contextmenu', e => {
      e.preventDefault();
      this.currentSelection = window.getSelection().toString();
      let menu = this.buildMenu(e);

      // Empty menu fix for windows native app
      if (menu && _.size(menu.items)) {
        menu.popup(Object(_electron_ElectronService__WEBPACK_IMPORTED_MODULE_0__["getCurrentWindow"])());
      }
    }, false);
  }

  buildMenu(e) {
    let menu = new this.Menu(),
    isInput = _.get(e, 'target.nodeName') === 'INPUT' || _.get(e, 'target.nodeName') === 'TEXTAREA',
    selectedText = this.currentSelection;

    // Resetting values so that context menu is not triggered on right clicking anywhere on the screen
    this.currentSelection = '';

    if (!_.isEmpty(selectedText) || isInput) {
      this.buildGenericMenu(menu);
    }
    return menu;
  }

  buildGenericMenu(menu) {

    menu.append(new this.MenuItem({
      label: 'Undo',
      role: 'undo' }));

    menu.append(new this.MenuItem({
      label: 'Redo',
      role: 'redo' }));

    menu.append(new this.MenuItem({ type: 'separator' }));
    menu.append(new this.MenuItem({
      label: 'Cut',
      role: 'cut' }));

    menu.append(new this.MenuItem({
      label: 'Copy',
      role: 'copy' }));

    menu.append(new this.MenuItem({
      label: 'Paste',
      role: 'paste' }));

    menu.append(new this.MenuItem({
      label: 'Select All',
      role: 'selectall' }));

    menu.append(new this.MenuItem({ type: 'separator' }));
  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

/******/ });