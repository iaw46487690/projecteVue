(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[39],{

/***/ 15040:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ForkListing; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(82);
/* harmony import */ var querystring__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(455);
/* harmony import */ var querystring__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(querystring__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1835);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3505);
/* harmony import */ var _ForkListBody__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15041);
/* harmony import */ var _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(303);









const ForkList = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div`
  overflow-y: auto;
`;

const forkListConfig = {
  length: 12,
  sortBy: 'createdAt',
  sortType: 'desc' };


/**
                       * Fork Listing
                       *
                       * @param {Object} props
                       */
function ForkListing(props) {
  const [forks, setForks] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]);
  const [forkInfo, setForkInfo] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({});
  const [loading, setLoading] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  const [isEmpty, setIsEmpty] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [error, setError] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  const syncStatusStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore');
  const collection = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CollectionStore').find(props.contextData.id);
  const isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected;
  const isSignedOut = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore').isLoggedIn;
  const isVisitor = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('PermissionStore').can('addHistory', 'workspace', Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id);
  const queryObject = {
    populateUsers: true,
    pageSize: forkListConfig.length,
    sortBy: forkListConfig.sortBy,
    sortType: forkListConfig.sortType };

  const fetchForks = () => {
    setLoading(true);
    setError(null);

    syncStatusStore.
    onSyncAvailable({ timeout: 5000 }).
    then(() => _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_6__["default"].request(`/collection/${collection.uid}/fork-list?${querystring__WEBPACK_IMPORTED_MODULE_2___default.a.stringify(queryObject)}`)).
    then(res => {
      setForks(_.get(res, 'body.data.forks'));
      setForkInfo({
        publicForks: _.get(res, 'body.data.publicForkCount'),
        privateForks: _.get(res, 'body.data.privateForkCount'),
        hiddenForks: _.get(res, 'body.data.hiddenForkCount'),
        totalForks: _.get(res, 'body.data.totalForkCount') });

      setIsEmpty(!(_.get(res, 'body.data.forks') || []).length);
      setLoading(false);
    }).
    catch(error => {
      setError(error);
      setLoading(false);

      pm.logger.error('Unable to fetch fork list', error);
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    fetchForks();
  }, []);

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ForkList, null,
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_4__["ContextBarViewHeader"], {
        title: 'Forks',
        onClose: props.onClose }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ForkListBody__WEBPACK_IMPORTED_MODULE_5__["default"], {
        loading: loading,
        isOffline: isOffline,
        isEmpty: isEmpty,
        isForbidden: isVisitor || isSignedOut,
        error: error,
        forks: forks,
        fetchForks: fetchForks,
        forkInfo: forkInfo,
        collection: collection })));



}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15041:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ForkListBody; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(82);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment_timezone__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2666);
/* harmony import */ var moment_timezone__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment_timezone__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _common_TabLoader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4992);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4913);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3043);
/* harmony import */ var _common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4997);
/* harmony import */ var _common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4998);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(69);
/* harmony import */ var _js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(318);
/* harmony import */ var _appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3044);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3016);















const ListItemWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div`
  display: inline-block;
`;

const ListItemAvatar = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["default"])(_common__WEBPACK_IMPORTED_MODULE_5__["CustomTooltip"])`
  float: right;
  margin-top: var(--spacing-s);
  margin-right: var(--spacing-xxl);
`;

const HelperText = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div`
  font-size: var(--text-size-m);
  color: var(--content-color-primary);
  font-weight: var(--text-weight-medium);
  margin-bottom: var(--spacing-m);
`;

const LinkText = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].span`
  color: var(--content-color-info);
  font-size: var(--text-size-m);
  margin-top: var(--spacing-l);
  cursor: pointer;
  padding-bottom: var(--spacing-xl);

  &:hover {
    text-decoration: underline
  }
`;

const ExternalLinkIcon = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["default"])(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Icon"])`
  position: relative;
  top: 2px;
`;

/**
    * Fork List Body
    *
    * @param {Object} props - React props
    */
function ForkListBody(props) {
  const FORK_LEARNING_CENTER_LINK = 'https://learning.postman.com/docs/collaborating-in-postman/version-control-for-collections/#forking-a-collection';
  const EmptyStateAction =
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', null,
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, 'Learn more about '),
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkText, { onClick: () => Object(_js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_10__["openExternalLink"])(FORK_LEARNING_CENTER_LINK, '_blank') },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, 'Forks '),
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ExternalLinkIcon, {
        name: 'icon-action-open-stroke-small',
        color: 'base-color-info',
        size: 'small' })));




  const RetryAction =
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_12__["Button"], {
      type: 'primary',
      size: 'small',
      onClick: props.fetchForks }, 'Retry');





  if (props.loading) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabLoader__WEBPACK_IMPORTED_MODULE_4__["default"], null);
  } else
  if (pm.isScratchPad) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
        type: 'context-bar',
        icon: 'icon-action-fork-stroke',
        title: 'Forks not available in scratchpad',
        message: 'Check to make sure you\'re online, then switch to a workspace to view forks.' }));


  } else
  if (props.isOffline) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
        showAction: true,
        type: 'context-bar',
        icon: 'icon-state-offline-stroke',
        title: 'Check your connection',
        message: 'Get online to view your list of forks.',
        action: RetryAction }));


  } else
  if (props.isForbidden || props.error) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
        type: 'context-bar',
        icon: 'icon-state-error-stroke',
        title: 'Unable to load list of forks',
        message: props.isForbidden ? 'You cannot view the list of forks for this collection.' : _.get(props, 'error.error.message') || 'Try refetching forks' }));


  } else
  if (props.isEmpty) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
        showAction: true,
        type: 'context-bar',
        icon: 'icon-action-fork-stroke',
        title: 'No forks created yet',
        message: 'All forks created from this collection will appear here.',
        action: EmptyStateAction }));


  }

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarDescription"], null,
        _.get(props, 'forkInfo.totalForks'), ' total forks of ', _.get(props, 'collection.name'), '.',
        _.get(props, 'forkInfo.hiddenForks') > 0 ? ` ${_.get(props, 'forkInfo.hiddenForks')} of those aren't in this list because they're in workspaces you don't have access to.` : ''),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarContainer"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HelperText, null, 'Recently created:'),
        (props.forks || []).map(fork =>
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { key: fork.modelId },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ListItemWrapper, null,
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_11__["default"], {
                to: fork.modelId ? `${pm.artemisUrl}/collection/${fork.modelId}` : '',
                disabled: !fork.modelId },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarListItem"], null,
                fork.forkName)),


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarSubtext"], null,
              `Created on: ${moment_timezone__WEBPACK_IMPORTED_MODULE_3___default()(fork.createdAt).format('DD MMM, YYYY')}`)),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ListItemAvatar, {
              align: 'bottom',
              body: _.get(fork, 'createdBy.name') },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_6__["default"], {
              size: 'medium',
              userId: _.get(fork, 'createdBy.id'),
              customPic: _.get(fork, 'createdBy.profilePicUrl') })))),




        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkText, { onClick: () => _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__["default"].transitionTo('build.forks', { cid: _.get(props, 'collection.uid') }) },
          _.get(props, 'forkInfo.totalForks') > (props.forks || []).length ? 'View all forks' : 'View details'))));



}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);