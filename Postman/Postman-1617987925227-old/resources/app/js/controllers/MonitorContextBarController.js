(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[18],{

/***/ 14959:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorContextBarController; });
/* harmony import */ var _stores_domain_MonitorActivitiesStore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(14960);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(69);

let

MonitorContextBarController = class MonitorContextBarController {
  didCreate() {
    const monitorIdParams = _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_1__["default"].getCurrentRouteParams('build.monitor');
    const monitorPaths = monitorIdParams.monitorPath.split('~');
    const monitorId = monitorPaths.pop();

    this.monitorActivityStore = new _stores_domain_MonitorActivitiesStore__WEBPACK_IMPORTED_MODULE_0__["default"]();
    this.monitorActivityStore.setActiveMonitorId(monitorId);
  }

  didActivate() {
    const monitorIdParams = _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_1__["default"].getCurrentRouteParams('build.monitor');
    const monitorPaths = monitorIdParams.monitorPath.split('~');
    const monitorId = monitorPaths.pop();

    if (!this.monitorActivityStore.isOffline) {
      this.monitorActivityStore.getActivitiesByMonitor({ monitorId, page: 1 }, true);
    }

    this.monitorActivityStore.setActiveMonitorId(monitorId);
  }};

/***/ }),

/***/ 14960:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivitiesStore; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1835);
/* harmony import */ var _MasterMonitorStore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4702);
var _dec, _desc, _value, _class, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8;function _initDefineProp(target, property, descriptor, context) {if (!descriptor) return;Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 });}function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}function _initializerWarningHelper(descriptor, context) {throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');}

let

MonitorActivitiesStore = (_dec =




























































mobx__WEBPACK_IMPORTED_MODULE_0__["action"].bound, (_class = class MonitorActivitiesStore {constructor() {_initDefineProp(this, 'loading', _descriptor, this);_initDefineProp(this, 'loadingMore', _descriptor2, this);_initDefineProp(this, 'loaded', _descriptor3, this);_initDefineProp(this, 'error', _descriptor4, this);_initDefineProp(this, 'nextPage', _descriptor5, this);_initDefineProp(this, 'activeMonitorId', _descriptor6, this);_initDefineProp(this, 'moreLoading', _descriptor7, this);_initDefineProp(this, 'isOpen', _descriptor8, this);this.masterMonitorStore = new _MasterMonitorStore__WEBPACK_IMPORTED_MODULE_2__["default"]();Object(mobx__WEBPACK_IMPORTED_MODULE_0__["reaction"])(() => {return this.masterMonitorStore.isOffline;}, async isOffline => {if (!isOffline && this.activeMonitorId) {await this.getActivitiesByMonitor({ monitorId: this.activeMonitorId, page: 1 }, false);}});} // Get offline status from master monitor store
  // @computed to ensure isOffline will never have a stale value
  get isOffline() {return this.masterMonitorStore.isOffline;}get values() {return this.masterMonitorStore.getJobTemplateActivities(this.activeMonitorId).map(activity => {return { action: activity.action, actor: activity.actor, context: activity.context, createdAt: activity.createdAt, deletedAt: activity.deletedAt, entityId: activity.entityId, id: activity.id, team: activity.team, timestamp: activity.timestamp, entityType: activity.entityType };});}setIsOpen(state) {this.isOpen = state;}update(payload) {_.has(payload, 'loading') && (this.loading = payload.loading);_.has(payload, 'loaded') && (this.loaded = payload.loaded);_.has(payload, 'loadingMore') && (this.loadingMore = payload.loadingMore);_.has(payload, 'error') && (this.error = payload.error);_.has(payload, 'nextPage') && (this.nextPage = payload.nextPage);}async getActivitiesByMonitor(activityLogsParams, checkReload) {const { monitorId, members, page } = activityLogsParams;

    if (checkReload) {
      this.masterMonitorStore.clearJobTemplateActivities(monitorId);
    }

    this.update({ loading: true, loaded: false, error: null });

    try {
      // wait for the sync to be available before fetching the activities for the monitor.
      // This is necessary especially when the context bar for activity log is un-collapsed by default when rendered.
      await Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('SyncStatusStore').onSyncAvailable();

      const response = await this.masterMonitorStore.loadMonitorActivities({ monitorId, members, page }),
      meta = _.get(response, 'body.meta', {});

      this.update({
        loading: false,
        loaded: true,
        nextPage: meta.nextUrl ? parseInt(meta.nextUrl.split('page=')[1], 10) : null });

    }
    catch (e) {
      this.update({ loading: false, error: e });
    }
  }


  setActiveMonitorId(monitorId) {
    this.activeMonitorId = monitorId;
  }


  setMoreLoading(val) {
    this.moreLoading = val;
  }}, (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'loading', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'loadingMore', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor3 = _applyDecoratedDescriptor(_class.prototype, 'loaded', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor4 = _applyDecoratedDescriptor(_class.prototype, 'error', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return null;} }), _descriptor5 = _applyDecoratedDescriptor(_class.prototype, 'nextPage', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return null;} }), _descriptor6 = _applyDecoratedDescriptor(_class.prototype, 'activeMonitorId', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return undefined;} }), _descriptor7 = _applyDecoratedDescriptor(_class.prototype, 'moreLoading', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor8 = _applyDecoratedDescriptor(_class.prototype, 'isOpen', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _applyDecoratedDescriptor(_class.prototype, 'isOffline', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'isOffline'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'values', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'values'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setIsOpen', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setIsOpen'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'update', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'update'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'getActivitiesByMonitor', [_dec], Object.getOwnPropertyDescriptor(_class.prototype, 'getActivitiesByMonitor'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setActiveMonitorId', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setActiveMonitorId'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setMoreLoading', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setMoreLoading'), _class.prototype)), _class));
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);