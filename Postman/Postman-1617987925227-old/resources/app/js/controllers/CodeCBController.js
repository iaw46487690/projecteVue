(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[56],{

/***/ 15661:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CodegenController; });
/* harmony import */ var _js_services_CodeGeneratorExperimental__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4297);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1835);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(74);
var _desc, _value, _class;function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}

let

CodegenController = (_class = class CodegenController {


  get editor() {
    let activeEditor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceSessionStore').activeEditor,
    editor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('EditorStore').find(activeEditor);

    return editor && editor.model;
  }

  didCreate() {
    var activeEditor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceSessionStore').activeEditor,
    editorStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('EditorStore').find(activeEditor),
    editorId = editorStore.id;

    this.editorId = editorId;
    this.getLanguageList();
  }

  getLanguageList() {
    let languageTree,
    displayName,
    languageList = [],
    labelEditorMap = {};

    _js_services_CodeGeneratorExperimental__WEBPACK_IMPORTED_MODULE_0__["default"].getLanguages().
    then(languages => {
      languageTree = languages;
      if (languageTree && languageTree.length) {
        languageTree.forEach(language => {
          if (!language.variants || !language.variants.length) {
            return;
          }
          if (language.variants.length > 1) {
            language.variants.forEach(variant => {
              displayName = language.label + ' - ' + variant.key;
              languageList.push(
              {
                id: JSON.stringify({
                  language: language.key,
                  variant: variant.key }),

                name: displayName });

              labelEditorMap[`${language.key}_${variant.key}`] = {
                displayName: displayName,
                editorMode: language.syntax_mode };

            });
          } else
          {
            displayName = language.label.toLowerCase() !== language.variants[0].key.toLowerCase() ? language.label + ' - ' + language.variants[0].key : language.label;
            languageList.push(
            {
              id: JSON.stringify({
                language: language.key,
                variant: language.variants[0].key }),

              name: displayName });

            labelEditorMap[`${language.key}_${language.variants[0].key}`] = {
              displayName: displayName,
              editorMode: language.syntax_mode };

          }
        });
        this.languageList = languageList;
        this.labelEditorMap = labelEditorMap;
      }
    });

  }}, (_applyDecoratedDescriptor(_class.prototype, 'editor', [mobx__WEBPACK_IMPORTED_MODULE_2__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'editor'), _class.prototype)), _class);

/***/ })

}]);