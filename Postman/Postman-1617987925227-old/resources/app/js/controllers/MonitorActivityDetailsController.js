(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[20],{

/***/ 14968:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityDetailsController; });
/* harmony import */ var _stores_domain_MonitorActivityDetailsStore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(14969);
let

MonitorActivityDetailsController = class MonitorActivityDetailsController {
  didCreate() {
    this.monitorActivityDetailsStore = new _stores_domain_MonitorActivityDetailsStore__WEBPACK_IMPORTED_MODULE_0__["default"]();
  }

  didActivate() {
    this.monitorActivityDetailsStore.setIsOpen(true);
  }

  didDeactivate() {
    this.monitorActivityDetailsStore.setIsOpen(false);
  }};

/***/ }),

/***/ 14969:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityDetailsStore; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _MasterMonitorStore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4702);
var _desc, _value, _class, _descriptor, _descriptor2;function _initDefineProp(target, property, descriptor, context) {if (!descriptor) return;Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 });}function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}function _initializerWarningHelper(descriptor, context) {throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');}
let

MonitorActivityDetailsStore = (_class = class MonitorActivityDetailsStore {
  constructor() {_initDefineProp(this, 'isOpen', _descriptor, this);_initDefineProp(this, 'error', _descriptor2, this);
    this.masterMonitorStore = new _MasterMonitorStore__WEBPACK_IMPORTED_MODULE_1__["default"]();
  }





  get isOffline() {
    return this.masterMonitorStore.isOffline;
  }


  setIsOpen(val) {
    this.isOpen = val;
  }}, (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'isOpen', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'error', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return null;} }), _applyDecoratedDescriptor(_class.prototype, 'isOffline', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'isOffline'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setIsOpen', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setIsOpen'), _class.prototype)), _class);

/***/ })

}]);