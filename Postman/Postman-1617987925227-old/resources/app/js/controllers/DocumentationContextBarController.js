(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[41],{

/***/ 15046:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DocumentationContextBarController; });
/* harmony import */ var _utils_navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15047);
/* harmony import */ var _stores_DocumentationContextBarStore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15048);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1856);


let

DocumentationContextBarController = class DocumentationContextBarController {constructor() {this.
    entityId = undefined;this.
    entityUid = undefined;this.
    entityType = undefined;this.
    owner = undefined;this.
    store = undefined;}


  didCreate() {
    this.createStore();
  }

  createStore() {
    let { type, uid } = Object(_utils_navigation__WEBPACK_IMPORTED_MODULE_0__["getEntityFromUrl"])(),
    { modelId: entityId, owner } = Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_2__["decomposeUID"])(uid);

    this.entityType = type;
    this.entityUid = uid;
    this.entityId = entityId;
    this.owner = owner;

    this.store = new _stores_DocumentationContextBarStore__WEBPACK_IMPORTED_MODULE_1__["default"](this.entityType, this.entityUid);
  }

  reset() {
    if (this.store) {
      this.store.cleanUp();
    }
    this.createStore();
  }

  willDestroy() {
    if (this.store) {
      this.store.cleanUp();
      this.store = undefined;
    }

    this.codegen = undefined;
  }};

/***/ }),

/***/ 15047:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEntityFromUrl", function() { return getEntityFromUrl; });
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(69);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3990);



/**
                                                                       * Helper function for getting the entity information from the url. This method
                                                                       * is a short term hack till the time we are blocked by client foundation for getting
                                                                       * the routeParams and queryParams in the context bar controllers
                                                                       *
                                                                       * TODO: Remove this function once ContextBarController also has
                                                                       * didCreate and didQueryParamsChange lifecycle method
                                                                       *
                                                                       * @typedef {Object} Entity
                                                                       * @property {String} type - type of the entity such as collection, folder, request
                                                                       * @property {String} uid - uid of the entity
                                                                       *
                                                                       * @returns {Entity | null} - object containing the entity if current url matches
                                                                       */
function getEntityFromUrl() {
  const currentRoute = _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_0__["default"].currentURL.get();
  const matches = _constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_CONTEXT_BAR_URL_PATTERN"].exec(currentRoute);

  // @TODO I'm very iffy about the usage of a regexp like this on a construct we don't control (element URL). Replace
  // this with something more reliable when possible.
  if (matches) {
    return { type: matches[1], uid: matches[2] };
  }

  return null;
}

/***/ }),

/***/ 15048:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DocumentationContextBarStore; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1856);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3990);
/* harmony import */ var _DocumentationMasterStore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5098);
var _desc, _value, _class, _class2, _temp;function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}


let

DocumentationContextBarStore = (_class = (_temp = _class2 = class DocumentationContextBarStore {






  /**
                                                                                                 * Create a new BaseDocumentationStore
                                                                                                 *
                                                                                                 * @param {String} entityType - The type of the entity
                                                                                                 * @param {String} entityUid - The uid of the entity in form of (ownerId-entityId)
                                                                                                 */
  constructor(entityType, entityUid) {
    this.entityType = entityType;
    this.entityUid = entityUid;
    this.parentCollection = undefined;

    this.masterStore = _DocumentationMasterStore__WEBPACK_IMPORTED_MODULE_3__["default"].getInstance();

    let fetchMethod = {
      [_constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_ENTITY"].COLLECTION]: 'fetchCollection',
      [_constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_ENTITY"].FOLDER]: 'fetchFolder',
      [_constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_ENTITY"].REQUEST]: 'fetchRequest' };

    this.masterStore[fetchMethod[this.entityType]]({
      id: this.entityId,
      ownerId: this.owner,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_LATEST_VERSION_STRING"] });

  }

  get entityId() {
    let { modelId: id } = Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_1__["decomposeUID"])(this.entityUid);
    return id;
  }

  get owner() {
    let { owner } = Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_1__["decomposeUID"])(this.entityUid);
    return owner;
  }


  get data() {
    let { data } = this.masterStore[
    DocumentationContextBarStore.masterStoreGetMethod[this.entityType]](
    {
      id: this.entityId,
      ownerId: this.owner,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_LATEST_VERSION_STRING"] });

    return data;
  }


  get loading() {
    let { status } = this.masterStore[
    DocumentationContextBarStore.masterStoreGetMethod[this.entityType]](
    {
      id: this.entityId,
      ownerId: this.owner,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_LATEST_VERSION_STRING"] });

    return status === _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_STATUS"].LOADING;
  }


  get error() {
    let { status } = this.masterStore[
    DocumentationContextBarStore.masterStoreGetMethod[this.entityType]](
    {
      id: this.entityId,
      ownerId: this.owner,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_LATEST_VERSION_STRING"] });


    switch (status) {
      case _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_STATUS"].DATA_NOT_AVAILABLE_OFFLINE:{
          return {
            title: `Unable to load ${this.entityType} data`,
            message: 'Check if you\'re connected to the internet.' };

        }

      case _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_STATUS"].DATA_NOT_FOUND:{
          return {
            title: 'Couldn\'t find documentation',
            message: `We could not find the documentation for this ${this.entityType}. The collection might have been moved or deleted.` };

        }

      default:{
          return undefined;
        }}

  }

  get isOffline() {
    return this.masterStore.isOffline;
  }

  get isEditable() {
    return this.masterStore.isEditable;
  }

  updateName(name, type = this.entityType, id = this.entityId) {
    this.masterStore.updateName(type, { id }, name);
  }

  updateDescription(name, type = this.entityType, id = this.entityId) {
    this.masterStore.updateDescription(type, { id }, name);
  }

  fetchParentCollectionData(collectionId) {
    this.masterStore.fetchCollection({
      id: collectionId,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_LATEST_VERSION_STRING"] });

  }

  getParentCollectionData(collectionId) {
    let { data } = this.masterStore.getCollection({
      id: collectionId,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_LATEST_VERSION_STRING"] });

    return data;
  }}, _class2.masterStoreGetMethod = { [_constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_ENTITY"].COLLECTION]: 'getCollection', [_constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_ENTITY"].FOLDER]: 'getFolder', [_constants__WEBPACK_IMPORTED_MODULE_2__["DOCUMENTATION_ENTITY"].REQUEST]: 'getRequest' }, _temp), (_applyDecoratedDescriptor(_class.prototype, 'data', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'data'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'loading', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'loading'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'error', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'error'), _class.prototype)), _class);

/***/ })

}]);