(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[57],{

/***/ 15662:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CommentsContextBarController; });
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1835);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(74);
var _desc, _value, _class, _descriptor, _descriptor2, _descriptor3;function _initDefineProp(target, property, descriptor, context) {if (!descriptor) return;Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 });}function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}function _initializerWarningHelper(descriptor, context) {throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');}
let

CommentsContextBarController = (_class = class CommentsContextBarController {constructor() {_initDefineProp(this, 'loaded', _descriptor, this);_initDefineProp(this, 'loading', _descriptor2, this);_initDefineProp(this, 'loadingError', _descriptor3, this);}





  setLoaded(loaded) {
    this.loaded = loaded;
  }


  setLoading(loading) {
    this.loading = loading;
  }


  setLoadingError(loadingError) {
    this.loadingError = loadingError;
  }


  get editor() {
    let activeEditor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_0__["getStore"])('ActiveWorkspaceSessionStore').activeEditor,
    editor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_0__["getStore"])('EditorStore').find(activeEditor);

    return editor && editor.model;
  }


  get annotationOrder() {
    if (!this.editor) {
      return;
    }

    const allComments = this.editor.getAdditionalData('comments'),
    filteredComments = _.filter(allComments || [], comment => comment.anchor && comment.anchor.startsWith(`request/${this.editor.requestId}`)) || [],
    comments = _.sortBy(filteredComments, 'createdAt'),
    annotationOrder = _.reduce(comments.reverse(), (result, comment) => {
      if (!result.includes(comment.annotationId)) {
        result.push(comment.annotationId);
      }

      return result;
    }, []);

    return annotationOrder;
  }}, (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'loaded', [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'loading', [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor3 = _applyDecoratedDescriptor(_class.prototype, 'loadingError', [mobx__WEBPACK_IMPORTED_MODULE_1__["observable"]], { enumerable: true, initializer: function () {return null;} }), _applyDecoratedDescriptor(_class.prototype, 'setLoaded', [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setLoaded'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setLoading', [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setLoading'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setLoadingError', [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setLoadingError'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'editor', [mobx__WEBPACK_IMPORTED_MODULE_1__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'editor'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'annotationOrder', [mobx__WEBPACK_IMPORTED_MODULE_1__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'annotationOrder'), _class.prototype)), _class);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);