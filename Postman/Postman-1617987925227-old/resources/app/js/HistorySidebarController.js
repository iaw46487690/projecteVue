(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[36],{

/***/ 15031:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistorySidebarController; });
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1835);
/* harmony import */ var _services_SyncService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15032);

let

HistorySidebarController = class HistorySidebarController {
  async didCreate() {
    // Search query is reset when the controller is created.
    // this ensures that the filter on the data is cleared every time either when the workspace is switched
    // or when the sidebar component is mounted.
    // e.g, when the page loads or when the user switches back from the home page.
    this.store = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_0__["getStore"])('HistorySidebarStore');
    this.store.setSearchQuery('');

    if (!pm.isScratchpad) {
      await Object(_services_SyncService__WEBPACK_IMPORTED_MODULE_1__["fetchMoreRunsInActiveWorkspace"])();
    }
  }

  beforeDestroy() {
    this.store = null;
  }};

/***/ }),

/***/ 15032:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMoreRunsInActiveWorkspace", function() { return fetchMoreRunsInActiveWorkspace; });
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(306);
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(async__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1975);
/* harmony import */ var _controllers_RunnerRunController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2211);
/* harmony import */ var _js_modules_services_SyncRemoteFetcherService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1847);
/* harmony import */ var _js_modules_sync_helpers_create_changeset__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2186);
/* harmony import */ var _js_models_sync_services_SyncIncomingHandler__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2411);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1835);
/* eslint-disable import/prefer-default-export */









const MODEL_COLLECTION_RUN = 'collectionrun',
ACTION_IMPORT = 'import',
ACTION_FIND = 'find',
COLLECTION_RUN_FIRST_PULL_COUNT = 20,
DEFAULT_LAST_REVISION_ID = 0;

/**
                               * Gets the active workspace from workspace session
                               */
async function getActiveWorkspace() {
  return _js_modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].getActiveSession().then(session => {
    if (!session) {
      pm.logger.error('WorkspaceSession not found in DB');

      return Promise.reject(Error('Runtime~getActiveWorkspace: WorkspaceSession not found in db'));
    }

    return session.workspace;
  });
}

/**
   * Fetches collection runs from sync server for a specified workspace.
   * New runs are added to app datastore and not returned from this function.
   * Ensure that this is called only once sync socket is available
   *
   * See @fetchMoreRunsInWorkspace for an example
   *
   * @param {Object} options
   * @param {String} options.workspace  Id of the workspace from which collection runs are to be fetched
   * @param {Number} options.count  Maximum number of collection runs to be fetched
   * @param {Number} options.sinceLastRevisionId  Fetch only collection runs beyond this revision id
  */
function fetchMoreRemoteCollectionRuns({ workspace, count, sinceLastRevisionId }) {
  return new Promise((resolve, reject) => {
    async__WEBPACK_IMPORTED_MODULE_0___default.a.waterfall([
    // Fetch runs from remote source
    next => {
      _js_modules_services_SyncRemoteFetcherService__WEBPACK_IMPORTED_MODULE_3__["default"].remoteFetch(MODEL_COLLECTION_RUN, ACTION_FIND, {
        workspace,
        count,
        since_id: sinceLastRevisionId }).

      then(runs => next(null, runs)).
      catch(err => next(err));
    },

    // Filter out existing runs
    (runs, next) => {
      const runIds = runs.map(run => run.id),
      existingRuns = _controllers_RunnerRunController__WEBPACK_IMPORTED_MODULE_2__["default"].getAll({ id: runIds });

      existingRuns.
      then(results => {
        const existingRunIds = results.map(run => run.id),
        newIds = runIds.filter(id => existingRunIds.indexOf(id) === -1),
        newRuns = runs.filter(run => newIds.indexOf(run.id) !== -1);

        next(null, newRuns);
      }).
      catch(err => next(err));
    },

    // Process the runs as changesets
    // This ensures that partial runs are also handled appropriately
    (runs, next) => {
      const changesets = runs.map(run => Object(_js_modules_sync_helpers_create_changeset__WEBPACK_IMPORTED_MODULE_4__["default"])(
      MODEL_COLLECTION_RUN,
      ACTION_IMPORT,
      {
        instance: run,
        modelId: run.id,
        owner: run.owner },

      { partial: true }));


      next(null, changesets);
    },

    // Process the changesets individually
    // This will also take care of persisting runs to store
    (changesets, next) => {
      const promises = changesets.map(changeset => Object(_js_models_sync_services_SyncIncomingHandler__WEBPACK_IMPORTED_MODULE_5__["processIncomingChangeset"])(changeset).catch(
      e => {
        pm.logger.warn(
        'RunnerRunStore~fetchMoreCollectionRuns: Could not create collection run',
        e);

      }));


      return Promise.all(promises).
      then(() => next(null, changesets)).
      catch(err => next(err));
    }],
    err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

/**
   * Fetches collection runs for active workspace
   *
   * @param {Number} count Maximum number of runs to fetch
   * @param {Number} sinceLastRevisionId  Fetch only collection runs beyond this revision id
   */
function fetchMoreRunsInActiveWorkspace(
count = COLLECTION_RUN_FIRST_PULL_COUNT,
sinceLastRevisionId = DEFAULT_LAST_REVISION_ID)
{
  return Promise.resolve(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('SyncStatusStore')).
  then(store => store.onSyncAvailable()).
  then(() => getActiveWorkspace()).
  then(workspace => fetchMoreRemoteCollectionRuns({ workspace, count, sinceLastRevisionId }));
}

/***/ })

}]);