(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[40],{

/***/ 15042:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DocumentationContextBarView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(10);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _SkeletonLoader_ContextBarSkeletonLoader_ContextBarSkeletonLoader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15043);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1835);
/* harmony import */ var _js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3488);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2705);
/* harmony import */ var _js_utils_authUtil__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2992);
/* harmony import */ var _runtime_api_CollectionInterface__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2692);
/* harmony import */ var _Request_Request__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5083);
/* harmony import */ var _Collection_Collection__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(5023);
/* harmony import */ var _Folder_Folder__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(5081);
/* harmony import */ var _ContextBar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(5027);
/* harmony import */ var _utils_collection__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(5013);
/* harmony import */ var _utils_sanitization__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(5079);
/* harmony import */ var _services_DocumentationService__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(5097);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(3990);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(1834);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(5011);
/* harmony import */ var _DocumentationError__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(5015);
/* harmony import */ var _hocs_withErrorHandler__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(15044);
/* harmony import */ var _hocs_withContextBarTitle__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(15045);
/* harmony import */ var _DocumentationIntersectionObserver__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(5018);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;























let




DocumentationContextBarView = Object(_hocs_withContextBarTitle__WEBPACK_IMPORTED_MODULE_21__["default"])(_class = Object(_hocs_withErrorHandler__WEBPACK_IMPORTED_MODULE_20__["default"])(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class DocumentationContextBarView extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {constructor(...args) {var _temp;return _temp = super(...args), this.
    state = {
      showFooter: true,
      collectionData: undefined }, this.


    unsubscribe = null, this.


































    fetchParentCollectionData = async collectionId => {
      this.props.controller.store.fetchParentCollectionData(collectionId);
    }, this.

    openCollection = () => {
      let { contextData, controller } = this.props,
      { id: entityId, type: entityType } = contextData,
      { entityUid } = controller;

      _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_17__["default"].addEventV2({
        category: _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ANALYTICS"].CATEGORY,
        action: 'open_tab',
        label: Object(_utils_utils__WEBPACK_IMPORTED_MODULE_18__["isPublicWorkspace"])() ? _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ANALYTICS"].LABEL.PUBLIC_CONTEXTBAR : _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ANALYTICS"].LABEL.PRIVATE_CONTEXTBAR,
        entityType,
        entityId: entityType === _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].COLLECTION ? entityUid : entityId });

    }, this.

    requestEditAccess = () => {
      let collectionId = this.props.contextData.parentCollectionUid || this.props.contextData.uid;

      Object(_runtime_api_CollectionInterface__WEBPACK_IMPORTED_MODULE_8__["collectionActions"])(collectionId, _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_6__["ACTION_REQUEST_ACCESS"]);
    }, this.

    handleParentEntityClick = (parentType, parentId) => {
      let { controller } = this.props,
      { owner } = controller;
      Object(_utils_utils__WEBPACK_IMPORTED_MODULE_18__["openEntity"])(parentType, `${owner}-${parentId}`);
    }, this.

    handleEditModeChange = (entityType, entityId, editMode) => {
      return this.setState({ showFooter: !editMode });
    }, this.

    handleUpdateName = (entityType, entityId, title) => {
      this.props.controller.store.updateName(title);
    }, this.

    handleUpdateDescription = (entityType, entityId, description) => {
      this.props.controller.store.updateDescription(description);
    }, this.
















    renderEntity = ({
      data,
      id,
      type,
      collectionUid,
      rawData,
      parentCollectionData,
      rootRef,
      isEditable }) =>
    {
      if (!data) {
        return null;
      }

      let entityBody,
      inheritedAuth;

      if (parentCollectionData) {
        inheritedAuth = Object(_js_utils_authUtil__WEBPACK_IMPORTED_MODULE_7__["getInheritedAuth"])(parentCollectionData, data.id, type);
      }

      // Sanitize the data. At the time of writing, this takes care of entities with
      // an empty name.
      Object(_utils_sanitization__WEBPACK_IMPORTED_MODULE_14__["sanitizeEntity"])(data, type);

      if (type === _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].COLLECTION) {
        entityBody =
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Collection_Collection__WEBPACK_IMPORTED_MODULE_10__["default"], {
          entityData: data,
          rawData: rawData,
          onClose: this.props.onClose,
          source: _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ORIGIN"].CONTEXT_VIEW,
          onRequestAccess: this.requestEditAccess,
          editable: isEditable,
          updateName: this.handleUpdateName,
          updateDescription: this.handleUpdateDescription,
          inheritedAuth: inheritedAuth,
          onParentEntityClick: this.handleParentEntityClick,
          onEditModeChange: this.handleEditModeChange });


      } else
      if (type === _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].FOLDER) {
        entityBody =
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Folder_Folder__WEBPACK_IMPORTED_MODULE_11__["default"], {
          entityData: data,
          rawData: rawData,
          onClose: this.props.onClose,
          source: _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ORIGIN"].CONTEXT_VIEW,
          onRequestAccess: this.requestEditAccess,
          editable: isEditable,
          updateName: this.handleUpdateName,
          updateDescription: this.handleUpdateDescription,
          inheritedAuth: inheritedAuth,
          onParentEntityClick: this.handleParentEntityClick,
          onEditModeChange: this.handleEditModeChange });


      } else
      if (type === _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].REQUEST) {
        entityBody =
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Request_Request__WEBPACK_IMPORTED_MODULE_9__["default"], _extends({},
        this.props, {
          controller: this.props.controller,
          entityData: data,
          rawData: rawData,
          onClose: this.props.onClose,
          source: _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ORIGIN"].CONTEXT_VIEW,
          onRequestAccess: this.requestEditAccess,
          editable: isEditable,
          updateName: this.handleUpdateName,
          updateDescription: this.handleUpdateDescription,
          inheritedAuth: inheritedAuth,
          onParentEntityClick: this.handleParentEntityClick,
          onEditModeChange: this.handleEditModeChange }));


      }

      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { ref: rootRef, className: 'documentation-context-view__entity-body-container' },
            entityBody),

          this.state.showFooter ?
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContextBar__WEBPACK_IMPORTED_MODULE_12__["ContextBarFooter"], {
            id: id,
            type: type,
            collectionUid: collectionUid,
            onOpenCollection: this.openCollection }) :
          null));


    }, _temp;}componentDidMount() {let { contextData, controller } = this.props,{ owner, entityUid } = controller,{ type: entityType, id: entityId } = contextData,collectionId = entityType === _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].COLLECTION ? entityId : contextData.parentCollectionId;if (entityType !== _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].COLLECTION) {// Fetch collection data for getting collection variables
      this.fetchParentCollectionData(collectionId, owner);}_js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_17__["default"].addEventV2({ category: _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ANALYTICS"].CATEGORY, action: 'view', label: Object(_utils_utils__WEBPACK_IMPORTED_MODULE_18__["isPublicWorkspace"])() ? _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ANALYTICS"].LABEL.PUBLIC_CONTEXTBAR : _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ANALYTICS"].LABEL.PRIVATE_CONTEXTBAR, entityType, entityId: entityType === _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].COLLECTION ? entityUid : entityId });}componentDidUpdate(prevProps) {let { id: entityId } = this.props.contextData,{ id: prevEntityId } = prevProps.contextData; // if a request is openened in a non-fixed tab and the user opens another request
    // the context bar not created again, it is simply updated,
    // so we have to reset the store and force update the component
    if (prevEntityId !== entityId) {this.props.controller.reset();this.forceUpdate();}} /**
                                                                                         * Render an entity of any type: collection, folder, request.
                                                                                         *
                                                                                         * @param  {Object} data - The data to be rendered within the component, with resolved variables/placeholders
                                                                                         * @param  {String} id - UUID of the selected entity
                                                                                         * @param  {String} type - The element type. One of collection, folder, or request
                                                                                         * @param  {String} collectionUid - collectionUid of the parent collection of the selected entity
                                                                                         * @param  {Object} [rawData] - Data with unresolved variables/placeholders. Necessary if inline editing is to be
                                                                                         *                              enabled.
                                                                                         * @param  {Object} parentCollectionData - Parent collection object of the selected entity.
                                                                                         *                                         Required for inherited auth resolution.
                                                                                         * @param  {Object} rootRef - Ref to the root element
                                                                                         * @param  {Boolean} isEditable - Flag indicating if the entity is editable by the user
                                                                                         * @return {JSX|null} - The rendered element-specific output, or null.
                                                                                         */render() {let { contextData, controller, editorId } = this.props;let editorStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('EditorStore').find(editorId); // Forcefully un-mounting the documentation context bar
    // Otherwise the hash fragment conflicts if same documentation opened in 2 different tabs
    // @todo - properly unmount the parent tab itself when tab is not active
    if (!editorStore.isActive) {return null;}let { id, type, collectionUid, parentCollectionUid } = contextData,{ store } = controller,{ loading, data: rawData, error, isEditable } = store,entityData = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.cloneDeep(rawData),parentCollectionData = type === _constants__WEBPACK_IMPORTED_MODULE_16__["DOCUMENTATION_ENTITY"].COLLECTION ? rawData : store.getParentCollectionData(contextData.parentCollectionId),activeEnvironment = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('ActiveEnvironmentStore');

    // Get collectionUid from parentCollectionUid for folder/request
    collectionUid = collectionUid || parentCollectionUid;

    if (typeof id === 'undefined') {
      return null;
    }

    let resolvedEntityData;

    /**
                             * If parent collection data is available then only resolve collection variables
                             * till then, we can only resolve the environment variables and render the documentation
                             */
    if (parentCollectionData) {
      resolvedEntityData = Object(_utils_collection__WEBPACK_IMPORTED_MODULE_13__["resolveVariables"])(
      entityData,
      activeEnvironment.enabledValues,
      parentCollectionData.variables);

    } else {
      resolvedEntityData = Object(_utils_collection__WEBPACK_IMPORTED_MODULE_13__["resolveVariables"])(
      entityData,
      activeEnvironment.enabledValues);

    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
        loading ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SkeletonLoader_ContextBarSkeletonLoader_ContextBarSkeletonLoader__WEBPACK_IMPORTED_MODULE_3__["default"], null) :
        error ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocumentationError__WEBPACK_IMPORTED_MODULE_19__["default"], { title: error.title, message: error.message }) :

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_5__["default"], {
            message: `There was an error while fetching the documentation for this ${type}. If this problem persists, contact us at help@postman.com`

            // error should only shown only when there is an explict error, other wise we can show
            // empty screen (render null)
            , showError: error },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocumentationIntersectionObserver__WEBPACK_IMPORTED_MODULE_22__["default"], null,
            ({ rootRef }) =>
            resolvedEntityData ?
            this.renderEntity({
              data: resolvedEntityData,
              id,
              type,
              collectionUid,
              rawData,
              parentCollectionData,
              rootRef,
              isEditable }) :

            null))));







  }}) || _class) || _class) || _class;

/***/ }),

/***/ 15043:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ContextBarSkeletonLoader; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SkeletonUnit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5021);



/**
                                                * Return skeleton loader for Context Bar View
                                                */
function ContextBarSkeletonLoader() {

  const contextBarSkeletonProps = [
  { height: 16, width: 293, top: 60, left: 60, borderRadius: 3 },
  { height: 16, width: 211, top: 88, left: 60, borderRadius: 3 },
  { height: 16, width: 144, top: 124, left: 60, borderRadius: 3 },
  { height: 16, width: 20, top: 156, left: 60, borderRadius: 3 },
  { height: 16, width: 20, top: 186, left: 60, borderRadius: 3 },
  { height: 16, width: 116, top: 156, left: 88, borderRadius: 3 },
  { height: 16, width: 116, top: 186, left: 88, borderRadius: 3 }];


  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'documentation-context-bar-loader' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'documentation-context-bar-body' },

        _.map(contextBarSkeletonProps, (contextBarSkeletonProp, index) => {
          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SkeletonUnit__WEBPACK_IMPORTED_MODULE_1__["default"], {
              key: index,
              height: contextBarSkeletonProp.height,
              width: contextBarSkeletonProp.width,
              top: contextBarSkeletonProp.top,
              left: contextBarSkeletonProp.left,
              borderRadius: contextBarSkeletonProp.borderRadius }));


        }))));




}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15044:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return withErrorHandler; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3488);



/**
                                                                           * Higher Order Component to wrap an existic component
                                                                           */
function withErrorHandler(WrappingComponent) {let
  WrappedComponent = class WrappedComponent extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
    render() {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_1__["default"], null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(WrappingComponent, this.props)));


    }};


  WrappedComponent.displayName = `withErrorHandler(${WrappingComponent.displayName || WrappingComponent.name})`;

  return WrappedComponent;
}

/***/ }),

/***/ 15045:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return withContextBarTitle; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1834);
/* harmony import */ var _components_DocumentationEntityTitle__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5024);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3990);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5011);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};





/**
                                                     * Higher Order Component to wrap the component with context bar title. If the
                                                     * underlying component throws an error, this the entity title would be visible
                                                     *
                                                     * @param {React.Component} WrappingComponent - The component to be wrapped
                                                     */
function withContextBarTitle(WrappingComponent) {let
  WrappedComponent = class WrappedComponent extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {constructor(...args) {var _temp;return _temp = super(...args), this.
      handleContextBarClose = () => {
        let { contextData, controller } = this.props,
        { entityUid } = controller,
        { type: entityType, id: entityId } = contextData;

        _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_1__["default"].addEventV2({
          category: _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ANALYTICS"].CATEGORY,
          action: 'close',
          label: Object(_utils_utils__WEBPACK_IMPORTED_MODULE_4__["isPublicWorkspace"])() ? _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ANALYTICS"].LABEL.PUBLIC_CONTEXTBAR : _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ANALYTICS"].LABEL.PRIVATE_CONTEXTBAR,
          entityType,
          entityId: entityType === _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ENTITY"].COLLECTION ? entityUid : entityId });


        this.props.onClose();
      }, _temp;}

    render() {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: 'documentation-context-view' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_DocumentationEntityTitle__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({},
          this.props, {
            onClose: this.handleContextBarClose,
            source: _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ORIGIN"].CONTEXT_VIEW })),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(WrappingComponent, this.props)));


    }};


  WrappedComponent.displayName = `withContextBarTitle(${WrappingComponent.displayName || WrappingComponent.name})`;

  return WrappedComponent;
}

/***/ })

}]);