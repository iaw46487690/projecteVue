(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ 6765:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SidebarMockListing; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4886);
/* harmony import */ var react_window__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3299);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(74);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1835);
/* harmony import */ var _SidebarMockListItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6766);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3016);
/* harmony import */ var _services_MockNavigationService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2718);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(69);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6770);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6772);
/* harmony import */ var _appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(5455);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(6773);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3011);
/* harmony import */ var _constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(2719);
/* harmony import */ var _constants_Permissions__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(4462);
/* harmony import */ var _services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(4463);
/* harmony import */ var _components_MockOffline__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(4460);
/* harmony import */ var _js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(310);
var _class;






















const MIN_ROW_HEIGHT = 28,
OVERSCAN_COUNT = 10;let


SidebarMockListing = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class SidebarMockListing extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    if (!window.analytics.sentFirstLoadMetrics) {
      window.analytics.sendAnalytics(
      'workspace_page',
      'workspace_page_sidebar_mock_server_listing_initial_render_time');

    }
    super(props);

    this.store = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('RequesterSidebarStore');

    this.listItemRefs = {};

    this.handleCreate = this.handleCreate.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleRetry = this.handleRetry.bind(this);
    this.successHandler = this.successHandler.bind(this);
    this.handleRename = this.handleRename.bind(this);
    this.focusNext = this.focusNext.bind(this);
    this.focusPrev = this.focusPrev.bind(this);
    this.handleDeleteShortcut = this.handleDeleteShortcut.bind(this);
    this.handleRenameShortcut = this.handleRenameShortcut.bind(this);
    this.scrollToItemById = this.scrollToItemById.bind(this);
    this.resetFocusedItem = this.resetFocusedItem.bind(this);

    this.getListContainer = this.getListContainer.bind(this);
    this.getSidebarEmptyState = this.getSidebarEmptyState.bind(this);

    this.mockStore = _.get(this.props, 'controller.mockListStore');

    // Reaction for moving to the specified list item on focus shift
    this.focusReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_4__["reaction"])(() => {
      return this.mockStore.focusedItem || this.mockStore.activeItem;
    },
    itemToFocus => {
      if (!itemToFocus) {
        return;
      }

      // `requestIdleCallback` is used because mounting and rendering of new List items is of
      // higher priority and must be done before scrolling.
      // Also, this helps in waiting for the initial loading and mounting when Mock Sidebar is mounted,
      // before the initial scroll.
      requestIdleCallback(this.scrollToItemById.bind(this, itemToFocus), { timeout: 5000 });
    },
    { fireImmediately: true });
  }

  componentWillUnmount() {
    this.focusReactionDisposer && this.focusReactionDisposer();
  }

  resetFocusedItem() {
    this.mockStore.resetFocusedItem();
  }

  handleRetry() {
    this.mockStore.reload();
  }

  scrollToItemById(itemId) {
    if (!itemId) {
      return;
    }

    const itemIndex = _.findIndex(_.get(this.mockStore, 'filteredItems'), visibleItem => visibleItem.id === itemId);

    // Item not found
    if (itemIndex < 0) {
      return;
    }

    this.listRef && this.listRef.scrollToItem(itemIndex);
  }

  successHandler() {
    this.mockStore.hydrate();
  }

  handleRename(data) {
    this.mockStore.rename(data);
  }

  handleCreate() {
    _services_MockNavigationService__WEBPACK_IMPORTED_MODULE_8__["default"].transitionTo('build.mockCreate', {}, { from: 'sidebar' });
  }

  handleSearchChange(query) {
    this.mockStore.setSearchQuery(query);
  }

  componentDidMount() {
    pm.mediator.on('refreshSidebarMockListing', this.successHandler);
  }

  componentWillUnmount() {
    pm.mediator.off('refreshSidebarMockListing', this.successHandler);
  }

  createMock() {
    _services_MockNavigationService__WEBPACK_IMPORTED_MODULE_8__["default"].transitionTo('build.mockCreate', {}, { from: 'sidebar_empty_listing' });
  }

  getFilteredMocks() {
    const filteredMocks = this.mockStore.filteredItems;

    return filteredMocks;
  }

  handleDeleteShortcut() {
    const permissions = _.get(this.mockStore, 'permissions', {}),
    mock = this.mockStore.filteredItems[this.mockStore.focusedItemIndex] || this.mockStore.filteredItems[this.mockStore.activeItemIndex],
    deleteMockEnabled = Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_17__["hasPermission"])(permissions, mock.id, _constants_Permissions__WEBPACK_IMPORTED_MODULE_16__["DELETE_MOCK"]);

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('SyncStatusStore').isSocketConnected) {
      pm.toasts.error(_constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["OFFLINE_ERROR"], {
        title: 'You\'re Offline' });

    } else
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').isLoggedIn) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'mock',
        subtitle: _constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["NOT_SIGNED_IN_ERROR"],
        origin: 'shortcut' });

    } else
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ActiveWorkspaceStore').isMember) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    } else
    if (mock && deleteMockEnabled) {
      pm.mediator.trigger(
      'showDeleteMockModal',
      mock,
      null,
      { origin: 'shortcut' });

    } else
    {
      pm.toasts.error(_constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["PERMISSION_ERROR"]);
    }
  }

  handleRenameShortcut() {
    const permissions = _.get(this.mockStore, 'permissions', {}),
    mock = this.mockStore.filteredItems[this.mockStore.focusedItemIndex] || this.mockStore.filteredItems[this.mockStore.activeItemIndex],
    updateMockEnabled = Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_17__["hasPermission"])(permissions, mock.id, _constants_Permissions__WEBPACK_IMPORTED_MODULE_16__["EDIT_MOCK"]);

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('SyncStatusStore').isSocketConnected) {
      pm.toasts.error(_constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["OFFLINE_ERROR"], {
        title: 'You\'re Offline' });

    } else
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').isLoggedIn) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'mock',
        subtitle: _constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["NOT_SIGNED_IN_ERROR"],
        origin: 'shortcut' });

    } else
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ActiveWorkspaceStore').isMember) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    } else
    if (!_.get(mock, 'active')) {
      pm.toasts.error(_constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["INACTIVE_MOCK"]);
    } else
    if (mock && updateMockEnabled) {
      _.invoke(this.listItemRefs[mock.id], 'handleEditName');
    } else
    {
      pm.toasts.error(_constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["PERMISSION_ERROR"]);
    }
  }

  getKeyMapHandlers() {
    return {
      nextItem: pm.shortcuts.handle('nextItem', this.focusNext),
      prevItem: pm.shortcuts.handle('prevItem', this.focusPrev),
      delete: pm.shortcuts.handle('delete', this.handleDeleteShortcut),
      rename: pm.shortcuts.handle('rename', this.handleRenameShortcut) };

  }

  focusNext(e) {
    e && e.preventDefault();

    this.mockStore.focusNext();
  }

  focusPrev(e) {
    e && e.preventDefault();

    this.mockStore.focusPrev();
  }

  getListItem({ index, style }) {
    const filteredMocks = this.getFilteredMocks(),
    mock = filteredMocks[index] || {},
    isSelected = _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__["default"].isActive('build.mock', { mockId: mock.id }) ||
    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__["default"].isActive('build.mock', { mockId: mock.id }, { ctx: 'info' }) ||
    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__["default"].isActive('build.mockEdit', { mockId: mock.id }),
    permissions = _.get(this.mockStore, 'permissions', {});

    // Set the activeItem in mock list store every time a mock is selected
    if (isSelected) {
      this.mockStore.setActiveItem(mock.id);
    } else
    {
      // Clear the activeItem in mock list store every time an active mock tab is closed
      this.mockStore.activeItem === mock.id && this.mockStore.resetActiveItem();
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_14__["default"], { identifier: mock.id },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { style: style },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SidebarMockListItem__WEBPACK_IMPORTED_MODULE_6__["default"], {
            ref: ref => {this.listItemRefs[mock.id] = ref;},
            key: mock.id,
            mock: mock,
            onRename: this.handleRename,
            isSelected: this.mockStore.focusedItem === mock.id || isSelected,
            permissions: permissions,
            onResetFocusedItem: this.resetFocusedItem }))));




  }

  getTooltip() {
    const canAddMock = this.canAddMockToWorkspace();

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('SyncStatusStore').isSocketConnected) {
      return _constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["OFFLINE_ERROR"];
    } else
    if (canAddMock) {
      return 'Create new Mock Server';
    } else
    {
      return _constants_MockErrors__WEBPACK_IMPORTED_MODULE_15__["PERMISSION_ERROR"];
    }
  }

  /**
     * Helper function to check whether user can add mock to workspace
     */
  canAddMockToWorkspace() {
    const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('PermissionStore'),
    workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ActiveWorkspaceStore').id,
    canAddMock = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').isLoggedIn ||
    permissionStore.can('addMock', 'workspace', workspaceId);

    return canAddMock;
  }

  getSidebarEmptyState() {
    const canAddMock = this.canAddMockToWorkspace(),
    isOnline = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('SyncStatusStore').isSocketConnected;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_13__["default"], {
        icon: 'icon-entity-mock-stroke',
        title: 'You don\u2019t have any mock servers.',
        message: 'Mock servers let you simulate endpoints and their corresponding responses in a collection without actually setting up a back end.',
        action: {
          label: 'Create new Mock Server',
          handler: this.createMock,
          tooltip: this.getTooltip() },

        hasPermissions: isOnline && canAddMock }));


  }

  getListContainer() {
    const mocks = this.mockStore.values,
    filteredMocks = this.getFilteredMocks(),
    searchQuery = this.mockStore.searchQuery,
    fetchingMocks = this.mockStore.isHydrating;

    if (fetchingMocks) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_12__["default"], null));

    }

    if (mocks.length === 0 && !searchQuery) {
      return this.getSidebarEmptyState();
    }

    if (filteredMocks.length === 0) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_11__["default"], { searchQuery: searchQuery, icon: 'icon-entity-mock-stroke' });
    } else

    {
      const lister = data =>
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_1__["Observer"], null,
        this.getListItem.bind(this, data));



      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_2__["default"], null,
          ({ height, width }) =>
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_window__WEBPACK_IMPORTED_MODULE_3__["FixedSizeList"], {
              height: height,
              width: width,
              itemCount: filteredMocks.length,
              ref: ref => {this.listRef = ref;},
              itemSize: MIN_ROW_HEIGHT,
              overscanCount: OVERSCAN_COUNT },

            lister)));




    }
  }

  render() {
    const errorFetchingMocks = this.mockStore.errorFetchingMocks,
    searchQuery = this.mockStore.searchQuery,
    isOffline = this.mockStore.isOffline,
    isSocketConnected = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('SyncStatusStore').isSocketConnected;

    if (isOffline) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_MockOffline__WEBPACK_IMPORTED_MODULE_18__["default"], { origin: 'sidebar' }));

    }

    if (errorFetchingMocks) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__error__wrapper' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__error' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__error__illustration' }),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__error__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__error__content__header' }, 'Something went wrong'),


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__error__content__sub-header' }, 'There was an unexpected error loading mock servers. Please try again.')),



            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_7__["Button"], {
                className: 'btn-small sidebar-mock-listing__error__retry-button',
                type: 'primary',
                onClick: this.handleRetry }, 'Try Again'))));






    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_10__["default"], {
          createNewConfig: {
            tooltip: this.getTooltip(),
            disabled: !this.canAddMockToWorkspace() || !isSocketConnected,
            onCreate: this.handleCreate,
            xPathIdentifier: 'addMock' },

          onSearch: this.handleSearchChange,
          searchQuery: searchQuery }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_19__["default"], { handlers: this.getKeyMapHandlers() },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-mock-listing__container__list' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_14__["default"], { identifier: 'mock' },

              this.getListContainer())))));






  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6766:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SidebarMockListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(80);
/* harmony import */ var _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2982);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1856);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1835);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3016);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3055);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(69);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6767);
/* harmony import */ var _MockStatusIcons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(6768);
/* harmony import */ var _constants_MockErrors__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2719);
/* harmony import */ var _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(4462);
/* harmony import */ var _services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(4463);
/* harmony import */ var _js_modules_services_ManageRolesModalService__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(2700);
/* harmony import */ var _InactiveMockIndicator__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(6769);
/* harmony import */ var _js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(3594);
var _class;

















let


SidebarMockListItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class SidebarMockListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.containerRef = this.containerRef.bind(this);
    this.handleEditName = this.handleEditName.bind(this);
    this.handleCopyUrl = this.handleCopyUrl.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.getMenuItems = this.getMenuItems.bind(this);
    this.getRightAlignedContainer = this.getRightAlignedContainer.bind(this);
    this.getActions = this.getActions.bind(this);
    this.getStatusIndicators = this.getStatusIndicators.bind(this);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);
    this.handleRenameSubmit = this.handleRenameSubmit.bind(this);
  }

  handleDropdownActionSelect(action) {
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').isLoggedIn) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'mock',
        subtitle: _constants_MockErrors__WEBPACK_IMPORTED_MODULE_12__["NOT_SIGNED_IN_ERROR"],
        origin: 'sidebar_mock_listing' });

    }

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('ActiveWorkspaceStore').isMember) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    }

    switch (action) {
      case 'delete':
        this.handleDelete();

        return;
      case 'rename':
        this.handleEditName();

        return;
      case 'manage-roles':
        Object(_js_modules_services_ManageRolesModalService__WEBPACK_IMPORTED_MODULE_15__["manageRolesOnMock"])(_.get(this.props, 'mock.id'), { origin: 'mock_sidebar' });

        return;}

  }

  containerRef(ele) {
    this.listItem = ele;
  }

  getMenuItemIconClasses(label) {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({ 'dropdown-menu-item-icon': true }, 'menu-icon--' + label);
  }

  getStatusIndicators() {
    const mockId = _.get(this.props, 'mock.id'),
    workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('ActiveWorkspaceStore'),
    permissions = this.props.permissions || {},
    deleteMockEnabled = Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__["hasPermission"])(permissions, mockId, _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__["DELETE_MOCK"]),
    updateMockEnabled = Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__["hasPermission"])(permissions, mockId, _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__["EDIT_MOCK"]);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_MockStatusIcons__WEBPACK_IMPORTED_MODULE_11__["default"], {
        isPublic: workspace.visibilityStatus === 'public',
        isShared: workspace.type === 'team',
        isEditable: updateMockEnabled && deleteMockEnabled }));


  }

  handleDelete() {
    pm.mediator.trigger(
    'showDeleteMockModal',
    this.props.mock,
    null,
    { origin: 'sidebar' });

  }

  getMockUrl() {
    return _.get(this.props, 'mock.url', '');
  }

  handleCopyUrl() {
    _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_4__["default"].copy(this.getMockUrl());

    pm.toasts.success('Mock URL copied');
  }

  getActions() {
    const currentUser = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore'),
    mockId = _.get(this.props, 'mock.id'),
    permissions = this.props.permissions || {},
    isOnline = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('SyncStatusStore').isSocketConnected,
    deleteMockEnabled = isOnline && (!currentUser.isLoggedIn ||
    Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__["hasPermission"])(permissions, mockId, _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__["DELETE_MOCK"])),
    updateMockEnabled = isOnline && _.get(this.props, 'mock.active') && (!currentUser.isLoggedIn ||
    Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__["hasPermission"])(permissions, mockId, _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__["EDIT_MOCK"])),
    manageRolesEnabled = isOnline && _.get(this.props, 'mock.active') && (!currentUser.isLoggedIn ||
    Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__["hasPermission"])(permissions, mockId, _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__["UPDATE_MOCK_ROLES"]) && !!currentUser.team);

    return [
    {
      type: 'rename',
      label: 'Rename',
      isEnabled: updateMockEnabled,
      xpathLabel: 'rename',
      shortcut: 'rename' },

    {
      type: 'manage-roles',
      label: 'Manage Roles',
      isEnabled: manageRolesEnabled,
      xpathLabel: 'manageRoles' },

    {
      type: 'delete',
      label: 'Delete',
      isEnabled: deleteMockEnabled,
      xpathLabel: 'delete',
      shortcut: 'delete' }];


  }

  getDisabledText(isDisabled, actionType) {
    if (isDisabled) {
      let defaultMessage = _constants_MockErrors__WEBPACK_IMPORTED_MODULE_12__["PERMISSION_ERROR"],
      manageRolesMessage = _constants_MockErrors__WEBPACK_IMPORTED_MODULE_12__["NO_TEAM"];

      if (!_.get(this.props, 'mock.active')) {
        if (actionType === 'delete') {
          const deleteMockEnabled = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').isLoggedIn || Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__["hasPermission"])(_.get(this.props, 'mockStore.permissions', {}), _.get(this.props, 'mock.id'), _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__["DELETE_MOCK"]);

          if (!deleteMockEnabled) {
            return defaultMessage;
          }
        } else {
          return _constants_MockErrors__WEBPACK_IMPORTED_MODULE_12__["INACTIVE_MOCK"];
        }
      }

      if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('SyncStatusStore').isSocketConnected) {
        return _constants_MockErrors__WEBPACK_IMPORTED_MODULE_12__["OFFLINE_ERROR"];
      }

      switch (actionType) {
        case 'manage-roles':
          const permissions = this.props.permissions || {},
          mockId = _.get(this.props, 'mock.id'),
          manageRolesPermission = Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_14__["hasPermission"])(permissions, mockId, _constants_Permissions__WEBPACK_IMPORTED_MODULE_13__["UPDATE_MOCK_ROLES"]);

          if (manageRolesPermission && !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').team) {
            return manageRolesMessage;
          }

          return defaultMessage;

        default:
          return defaultMessage;}

    }
  }

  handleEditName() {
    _.invoke(this.listItem, 'editText');
  }

  handleRenameSubmit(value) {
    if (_.get(this.props, 'mock.name') === value) {
      return;
    }

    const editPayload = {
      id: _.get(this.props, 'mock.id'),
      name: value };


    this.props.onRename(editPayload);
  }

  getRouteConfig() {
    return {
      routeIdentifier: 'build.mock',
      routeParams: {
        mockId: _.get(this.props, 'mock.id') } };


  }

  getMenuItems() {
    return _.chain(this.getActions()).
    map(action => {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
            key: action.type,
            refKey: action.type,
            disabled: !action.isEnabled,
            disabledText: this.getDisabledText(!action.isEnabled, action.type) },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'mock-action-item' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, action.label),

            action.shortcut &&
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-shortcut' }, Object(_js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_17__["getShortcutByName"])(action.shortcut)))));




    }).value();
  }

  getRightAlignedContainer(isHovered, isMoreActionsDropdownOpen) {
    const classNames = classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'sidebar-mock-list-item__actions': true,
      hovered: isHovered });


    if (!isHovered && !isMoreActionsDropdownOpen) {
      if (_.get(this.props, 'mock.active')) {
        return null;
      }

      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'inactive-mock-indicator__sidebar_fixed' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_InactiveMockIndicator__WEBPACK_IMPORTED_MODULE_16__["default"], null)));


    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,

        !_.get(this.props, 'mock.active') &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_InactiveMockIndicator__WEBPACK_IMPORTED_MODULE_16__["default"], null),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_7__["Button"], {
            className: 'sidebar-action-btn',
            tooltip: 'Copy Mock URL',
            type: 'icon',
            onClick: this.handleCopyUrl },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
            name: 'icon-action-copy-stroke',
            className: 'pm-icon pm-icon-normal' }))));





  }

  getActionsMenuItems() {
    const menuItems = this.getMenuItems();
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__["DropdownMenu"], {
          className: 'mocks-dropdown-menu',
          'align-right': true },

        menuItems));


  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_10__["default"], {
        ref: this.containerRef,
        text: _.get(this.props, 'mock.name', ''),
        isSelected: this.props.isSelected,
        onClick: this.props.onResetFocusedItem,
        onRename: this.handleRenameSubmit,
        rightMetaComponent: this.getRightAlignedContainer,
        moreActions: this.getActionsMenuItems(),
        statusIndicators: this.getStatusIndicators,
        onActionsDropdownSelect: this.handleDropdownActionSelect,
        routeConfig: this.getRouteConfig() }));


  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6768:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MockStatusIcons; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);



/**
                                         * Function to render the mock status icons
                                         *
                                         * @param {string} props.isPublic
                                         * @param {string} props.isShared
                                         * @param {object} props.isEditable
                                         */
function MockStatusIcons(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,

      props.isShared &&
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
        name: 'icon-descriptive-team-stroke',
        color: 'content-color-tertiary',
        className: 'mock-status-icon',
        size: 'small',
        title: 'Shared with team' }),



      !props.isEditable &&
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
        name: 'icon-state-locked-stroke',
        color: 'content-color-tertiary',
        className: 'mock-status-icon',
        size: 'small',
        title: 'Read only' }),



      props.isPublic &&
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
        name: 'icon-state-published-stroke',
        color: 'content-color-tertiary',
        className: 'mock-status-icon',
        size: 'small',
        title: 'Public' })));




}

/***/ }),

/***/ 6769:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_base_Tooltips__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3017);



const DEFAULT_TOOLTIP_PLACEMENT = 'bottom';let

InactiveMockIndicator = class InactiveMockIndicator extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltipVisible: false };


    this.mouseEnter = this.mouseEnter.bind(this);
    this.mouseLeave = this.mouseLeave.bind(this);
  }

  mouseEnter() {
    this.setState({ tooltipVisible: true });
  }

  mouseLeave() {
    this.setState({ tooltipVisible: false });
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: 'inactive-mock-indicator',
          ref: 'bullet',
          onMouseEnter: this.mouseEnter,
          onMouseLeave: this.mouseLeave },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Tooltips__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {
            target: this.refs.bullet,
            show: this.state.tooltipVisible,
            delay: 0,
            placement: DEFAULT_TOOLTIP_PLACEMENT },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Tooltips__WEBPACK_IMPORTED_MODULE_1__["TooltipBody"], null, 'This mock server has been ', react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('strong', null, 'deactivated'), '.'))));



  }};


/* harmony default export */ __webpack_exports__["default"] = (InactiveMockIndicator);

/***/ })

}]);