(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ 6774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MockInfoContextView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(297);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(80);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(74);
/* harmony import */ var _js_components_activity_feed_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6775);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3505);
/* harmony import */ var _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2982);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3016);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3043);
/* harmony import */ var _js_components_base_Tooltips__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3017);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(69);
/* harmony import */ var _version_control_common_ForkLabel__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(4815);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3037);
/* harmony import */ var _services_CollectionService__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(6778);
/* harmony import */ var _js_modules_services_APIService__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(2196);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(1835);
/* harmony import */ var _components_MockOffline__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(4460);
var _class;




















// Duration for which we will show the success state after performing the Copy action
const SHOW_COPY_SUCCESS_DURATION = 3000;let


MockInfoContextView = Object(mobx_react__WEBPACK_IMPORTED_MODULE_4__["observer"])(_class = class MockInfoContextView extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      idCopySuccess: false,
      urlCopySuccess: false,
      isUserTooltipVisible: false,
      collection: null,
      fetchingCollection: true,
      errorFetchingCollection: false,
      isOffline: false };


    this.tooltipRef = react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef();

    this.handleCopyId = this.handleCopyId.bind(this);
    this.handleCopyUrl = this.handleCopyUrl.bind(this);
    this.handleCollectionClick = this.handleCollectionClick.bind(this);
    this.handleEnvironmentClick = this.handleEnvironmentClick.bind(this);
    this.getUserIcon = this.getUserIcon.bind(this);
    this.getUserName = this.getUserName.bind(this);
    this.showUserTooltip = this.showUserTooltip.bind(this);
    this.hideUserTooltip = this.hideUserTooltip.bind(this);
    this.fetchCollection = this.fetchCollection.bind(this);
  }

  componentDidMount() {
    this.syncStatusStoreReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_5__["reaction"])(() => Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_17__["getStore"])('SyncStatusStore').isSocketConnected, isSocketConnected => {
      !isSocketConnected && (
      this.state.fetchingCollection || this.state.errorFetchingCollection) &&
      this.setState({ isOffline: true });

      isSocketConnected &&
      this.setState({ isOffline: false }, () => {
        this.fetchCollection();
      });
    }, {
      fireImmediately: true });

  }

  componentWillUnmount() {
    clearTimeout(this.clearIdCopySuccessTimeout);
    clearTimeout(this.clearUrlCopySuccessTimeout);

    this.syncStatusStoreReactionDisposer && this.syncStatusStoreReactionDisposer();
  }

  fetchCollection() {
    const collection = Object(mobx__WEBPACK_IMPORTED_MODULE_5__["toJS"])(_.get(this.props, 'contextData.collection'));

    if (!_.get(this.props, 'contextData.active') || _.get(collection, '_isAccessible') === false) {
      this.setState({ fetchingCollection: false, errorFetchingCollection: false });

      return;
    }

    this.setState({ fetchingCollection: true, errorFetchingCollection: false });

    Object(_services_CollectionService__WEBPACK_IMPORTED_MODULE_15__["fetchCollection"])(_.get(this.props, 'contextData.collection.id')).
    then(collection => {
      this.setState({ collection: collection, fetchingCollection: false, errorFetchingCollection: false });
    }).
    catch(err => {
      this.setState({ collection: null, fetchingCollection: false, errorFetchingCollection: true });
    });
  }

  handleCopyId() {
    if (this.state.idCopySuccess) {
      return;
    }

    _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_8__["default"].copy(_.get(this.props, 'contextData.id', ''));

    this.setState(
    { idCopySuccess: true },
    () => {
      this.clearIdCopySuccessTimeout = setTimeout(
      () => this.setState({ idCopySuccess: false }),
      SHOW_COPY_SUCCESS_DURATION);

    });

  }

  handleCopyUrl() {
    if (this.state.urlCopySuccess) {
      return;
    }

    _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_8__["default"].copy(_.get(this.props, 'contextData.url', ''));

    this.setState(
    { urlCopySuccess: true },
    () => {
      this.clearUrlCopySuccessTimeout = setTimeout(
      () => this.setState({ urlCopySuccess: false }),
      SHOW_COPY_SUCCESS_DURATION);

    });

  }

  handleCollectionClick() {
    const collectionId = _.get(this.props, 'contextData.collection.id', '');

    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_12__["default"].transitionTo('build.collection', { cid: collectionId });
  }

  handleEnvironmentClick() {
    const environmentId = _.get(this.props, 'contextData.environment.id', '');

    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_12__["default"].transitionTo('build.environment', { eid: environmentId });
  }

  handleRequestAccess(entityType, entityId, type) {
    Object(_js_modules_services_APIService__WEBPACK_IMPORTED_MODULE_16__["openAuthenticatedRoute"])(`${pm.dashboardUrl}/request-access?entityType=${entityType}&entityId=${entityId}&type=${type}`);
  }

  getIcon(isCopied) {
    if (isCopied) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-state-success-stroke',
          className: 'mock-info-context-view__entity__content__button__success' }));


    } else
    {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-action-copy-stroke',
          className: 'mock-info-context-view__entity__content__button__copy' }));


    }
  }

  getUserIcon(user = {}) {
    if (user.isAccessible) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_activity_feed_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_6__["ProfilePic"], { id: user.id });
    }

    // Private/Anonymous User
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_10__["default"], {
        size: 'small',
        userId: user.id,
        customPic: user.profilePicUrl }));


  }

  getUserName(user = {}) {
    if (user.isAccessible) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_activity_feed_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_6__["User"], {
          id: user.id,
          name: user.name || user.username }));


    }

    // Private/Anonymous User
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', {
            className: 'activity-item-user',
            ref: this.tooltipRef,
            onMouseEnter: this.showUserTooltip,
            onMouseLeave: this.hideUserTooltip },

          user.name),

        this.tooltipRef.current && !user.isPublic &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Tooltips__WEBPACK_IMPORTED_MODULE_11__["Tooltip"], {
            immediate: true,
            show: this.state.isUserTooltipVisible,
            target: this.tooltipRef.current,
            placement: 'bottom' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Tooltips__WEBPACK_IMPORTED_MODULE_11__["TooltipBody"], null, 'This user profile is private.'))));






  }

  showUserTooltip() {
    this.setState({ isUserTooltipVisible: true });
  }

  hideUserTooltip() {
    this.setState({ isUserTooltipVisible: false });
  }

  render() {
    const mock = _.get(this.props, 'contextData', {}),
    createdAt = moment__WEBPACK_IMPORTED_MODULE_2___default()(mock.createdAt).format('DD MMM YYYY, h:mm A');

    if (!this.state.isOffline && this.state.fetchingCollection) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view-loading' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_14__["default"], null)));


    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_7__["ContextBarViewHeader"], {
          title: this.props.title,
          onClose: this.props.onClose }),


        this.state.isOffline && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_MockOffline__WEBPACK_IMPORTED_MODULE_18__["default"], { origin: 'context-bar' }),


        !this.state.isOffline && this.state.errorFetchingCollection &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__error__wrapper' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__error' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__error__illustration' }),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__error__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__error__content__header' }, 'Something went wrong'),


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__error__content__sub-header' }, 'There was an unexpected error. Please try again.')),



            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_9__["Button"], {
                className: 'btn-small mock-info-context-view__error__retry-button',
                type: 'primary',
                onClick: this.fetchCollection }, 'Try Again'))),








        !(this.state.isOffline || this.state.errorFetchingCollection) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__label' }, 'ID'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'mock-info-context-view__entity__content__id',
                  title: mock.id },

                mock.id),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_9__["Button"], {
                  className: 'mock-info-context-view__entity__content__button',
                  tooltip: this.state.idCopySuccess ? 'Copied' : 'Copy Mock ID',
                  type: 'icon',
                  onClick: this.handleCopyId },

                this.getIcon(this.state.idCopySuccess)))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__label' }, 'CREATED BY'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content' },
              mock.createdBy && this.getUserIcon(mock.createdBy),
              mock.createdBy && this.getUserName(mock.createdBy))),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__label' }, 'CREATED ON'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content' },
              createdAt)),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__label' }, 'MOCK SERVER URL'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'mock-info-context-view__entity__content__url',
                  title: mock.url },

                mock.url),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_9__["Button"], {
                  className: 'mock-info-context-view__entity__content__button',
                  tooltip: this.state.urlCopySuccess ? 'Copied' : 'Copy Mock URL',
                  type: 'icon',
                  onClick: this.handleCopyUrl },

                this.getIcon(this.state.urlCopySuccess)))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__label' }, 'COLLECTION'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content' },

              _.get(mock, 'collection._isDeleted') ?

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null, 'The associated collection has been deleted') :




              _.get(mock, 'collection._isAccessible') === false &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null, 'Collection inaccessible',

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                    className: 'mock-info-context-view__entity__content__request-access',
                    onClick: () => this.handleRequestAccess('collection', _.get(mock, 'collection.id'), 'share_entity') }, 'Request access')),







              !_.get(mock, 'collection._isDeleted') && _.get(mock, 'collection._isAccessible') &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'mock-info-context-view__entity__content__collection',
                  title: _.get(mock, 'collection.name'),
                  onClick: this.handleCollectionClick },

                _.get(mock, 'collection.name')),



              _.get(this.state, 'collection.meta.forkedFrom') &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content__collection-fork-label' },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_version_control_common_ForkLabel__WEBPACK_IMPORTED_MODULE_13__["default"], {
                  destination: {
                    id: _.get(this.state, 'collection.meta.forkedFrom.id'),
                    name: _.get(this.state, 'collection.meta.forkedFrom.name') },

                  label: _.get(this.state, 'collection.meta.forkedFrom.forkName'),
                  size: 'large' })),




              !_.get(mock, 'collection._isDeleted') && _.get(mock, 'collection._isAccessible') &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  title: _.get(mock, 'collection.versionName'),
                  className: classnames__WEBPACK_IMPORTED_MODULE_3___default()({
                    'mock-info-context-view__entity__content__collection-version-tag': true,
                    'mock-info-context-view__entity__content__collection-version-tag__current':
                    _.isEqual(_.get(mock, 'collection.versionTag'), 'latest') }) },


                _.get(mock, 'collection.versionName')))),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__label' }, 'ENVIRONMENT'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content' },
              _.get(mock.environment, 'name') ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'mock-info-context-view__entity__content__environment',
                  title: _.get(mock, 'environment.name'),
                  onClick: this.handleEnvironmentClick },

                _.get(mock, 'environment.name')) :

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'mock-info-context-view__entity__content__environment-empty' },
                _.get(mock, 'environment._isDeleted') &&
                'The associated environment has been deleted',


                _.get(mock, 'environment._isAccessible') === false &&
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null, 'Environment inaccessible',

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                      className: 'mock-info-context-view__entity__content__request-access',
                      onClick: () => this.handleRequestAccess('environment', _.get(mock, 'environment.id'), 'share_entity') }, 'Request access')),






                !_.get(mock, 'environment') && 'No environment'))))));










  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6778:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchCollection", function() { return fetchCollection; });
/* harmony import */ var _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(303);


/**
                                                                                            * Fetch collection information
                                                                                            *
                                                                                            * @param {Object} id - id of the collection to be fetched
                                                                                            *
                                                                                            * @returns {Promise}
                                                                                            */
function fetchCollection(id) {
  if (!id) {
    return Promise.reject(new Error('CollectionService~fetchCollection: id not provided'));
  }

  return _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_0__["default"].request(`/collection/${id}`, {
    method: 'get' }).

  then(({ body }) => {
    return Promise.resolve(body);
  }).
  catch(({ error }) => {
    return Promise.reject(error);
  });
}

/***/ })

}]);