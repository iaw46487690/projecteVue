(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[21],{

/***/ 14970:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityInfo; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(297);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3016);
/* harmony import */ var _components_common_UserInfo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(14964);
/* harmony import */ var _appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3044);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3505);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1835);
/* harmony import */ var _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4701);
/* harmony import */ var _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2982);
/* harmony import */ var _components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(14966);
/* harmony import */ var _integrations_components_context_bar_IntegrationContextBarList__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(14971);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;













// Duration for which we will show the success state after performing the Copy action
const SHOW_COPY_SUCCESS_DURATION = 3000;let


MonitorActivityInfo = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class MonitorActivityInfo extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.monitorPermissionStore = new _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_9__["default"]();
    this.showPrvtUserTooltip = this.showPrvtUserTooltip.bind(this);
    this.hidePrvtUserTooltip = this.hidePrvtUserTooltip.bind(this);
    this.state = {
      showPrvtUserTooltip: false,
      idCopySuccess: false };

  }

  componentWillUnmount() {
    clearTimeout(this.clearIdCopySuccessTimeout);
  }

  showPrvtUserTooltip() {
    this.setState({ showPrvtUserTooltip: true });
  }

  hidePrvtUserTooltip() {
    this.setState({ showPrvtUserTooltip: false });
  }

  fetchCollectionName(id) {
    // Reg Ex- Remove the userID from the collection id. Find the first "-" and remove characters behind the dash
    if (id) {
      const collection = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('ActiveWorkspaceStore').collections.find(_ => id.replace(/^.+?(\-)/, '') == _.id);

      if (collection === undefined) {
        return null;
      } else {
        return collection && collection.name;
      }
    } else {
      return null;
    }
  }

  fetchEnvironmentName(id) {
    // Reg Ex- Remove the userID from the environment id. Find the first "-" and remove characters behind the dash
    if (id) {
      const environments = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('ActiveWorkspaceStore').environments.find(_ => id.replace(/^.+?(\-)/, '') == _.id);

      if (environments === undefined) {
        return null;
      } else {
        return environments && environments.name;
      }
    } else {
      return null;
    }
  }

  fetchUserName(userId, data) {
    let teams = data.reduce((obj, item) => {
      obj[item.id] = item;
      return obj;
    }, {});
    let userInfo = _.get(teams, userId, {});
    return userInfo.name;
  }

  canViewCreatorInfo() {
    // contextData here refers to the MonitorConfigurationStore
    return this.monitorPermissionStore.can('viewCreatorInfo', this.props.contextData.id);
  }

  copyMonitorId(id) {
    if (this.state.idCopySuccess) {
      return;
    }

    _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_10__["default"].copy(id);

    this.setState(
    { idCopySuccess: true },
    () => {
      this.clearIdCopySuccessTimeout = setTimeout(
      () => this.setState({ idCopySuccess: false }),
      SHOW_COPY_SUCCESS_DURATION);

    });

  }

  fetchCopyIcon(statusCheck) {
    if (statusCheck) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-state-success-stroke',
          className: 'monitor-info-context-view__entity__content__toggle__success' }));


    } else
    {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-action-copy-stroke',
          className: 'monitor-info-context-view__entity__content__toggle__copy' }));


    }
  }

  render() {
    const { isOffline } = this.props.controller.monitorActivityDetailsStore;

    this.monitorInfo = _.get(this.props, 'contextData', {});

    const createdAt = moment__WEBPACK_IMPORTED_MODULE_3___default()(this.monitorInfo.createdAt).format('DD MMM YYYY, h:mm A');

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_7__["ContextBarViewHeader"], {
          title: this.props.title,
          onClose: this.props.onClose }),


        isOffline && typeof this.props.contextData.createdBy === 'number' ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_11__["default"], null) :

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'ID'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'monitor-info-context-view__entity__content__id' },

                this.monitorInfo.id),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                  className: 'monitor-info-context-view__entity__content__toggle',
                  tooltip: this.state.idCopySuccess ? 'Copied' : 'Copy Monitor ID',
                  type: 'icon',
                  onClick: () => this.copyMonitorId(this.monitorInfo.id) },

                this.fetchCopyIcon(this.state.idCopySuccess)))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'CREATED BY'),
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  disabled: isOffline,
                  className: isOffline && 'monitor-info-userinfo-offline' },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_UserInfo__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({},
                this.monitorInfo.creatorInfo, {
                  containerClass: 'monitor-info-context-view__entity__content' }))))),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'CREATED ON'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },
              createdAt)),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'COLLECTION'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },

              this.fetchCollectionName(this.monitorInfo.collection) ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_6__["default"], { to: isOffline ? '' : { routeIdentifier: 'build.collection',
                    routeParams: { cid: this.monitorInfo.collection } },
                  className: 'monitor-info-context-view__entity__content__collection' },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                    type: 'tertiary',
                    className: 'monitor-info-btn',
                    disabled: isOffline,
                    tooltip: isOffline && 'You can take this action once you are back online' },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-info-context-view__entity__icon' },
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
                      name: 'icon-entity-collection-stroke',
                      color: 'content-color-primary',
                      size: 'small' })),


                  this.fetchCollectionName(this.monitorInfo.collection))) :


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-info-context-view__entity__content__collection-empty' }, 'No collection'))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'ENVIRONMENT'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },

              this.fetchEnvironmentName(this.monitorInfo.environment) ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_6__["default"], { to: isOffline ? '' : {
                    routeIdentifier: 'build.environment', routeParams: { eid: this.monitorInfo.environment } },
                  className: 'monitor-info-context-view__entity__content__environment' },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                    type: 'tertiary',
                    className: 'monitor-info-btn',
                    disabled: isOffline,
                    tooltip: isOffline && 'You can take this action once you are back online' },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-info-context-view__entity__icon' },
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
                      name: 'icon-entity-environment-stroke',
                      color: 'content-color-primary',
                      size: 'small' })),


                  this.fetchEnvironmentName(this.monitorInfo.environment))) :


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-info-context-view__entity__content__environment-empty' }, 'No environment'))),



          _.includes(['dev', 'beta', 'stage', 'canary', 'prod'], window.RELEASE_CHANNEL) &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_integrations_components_context_bar_IntegrationContextBarList__WEBPACK_IMPORTED_MODULE_12__["IntegrationContextBarList"], {
            entityId: this.monitorInfo.id,
            entityType: 'monitor' }))));







  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);