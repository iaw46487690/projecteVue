(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[19],{

/***/ 14961:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityLogs; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3505);
/* harmony import */ var _components_activity_logs_MonitorActivityFeed__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(14962);
/* harmony import */ var _components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(14966);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1835);
/* harmony import */ var _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4701);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(69);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3016);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(102);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _utils_offlineText__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4700);
/* harmony import */ var _components_activity_logs_MonitorActivityFilter__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(14967);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;












const DEBOUNCE_TIMEOUT = 600;let


MonitorActivityLogs = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class MonitorActivityLogs extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);this.

























    handleSearch = _.debounce(filter => {
      const monitorIdParams = _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_7__["default"].getCurrentRouteParams('build.monitor');
      const monitorPaths = monitorIdParams.monitorPath.split('~');
      const monitorId = monitorPaths.pop();

      this.setState({ isFilterApplied: true, filter });
      this.props.controller.monitorActivityStore.getActivitiesByMonitor({
        monitorId,
        members: Object.keys(filter.members),
        page: 1 },
      true);
      this.props.controller.monitorActivityStore.setActiveMonitorId(monitorId);
    }, DEBOUNCE_TIMEOUT);this.state = { isFilterApplied: false, filter: { members: {} } };this.handleLoadMore = this.handleLoadMore.bind(this);this.canViewActivityLogs = this.canViewActivityLogs.bind(this);this.handleRefresh = this.handleRefresh.bind(this);this.handleSearch = this.handleSearch.bind(this);this.getWorkspaceUsers = this.getWorkspaceUsers.bind(this);this.handleFilterSelect = this.handleFilterSelect.bind(this);this.monitorPermissionStore = new _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_6__["default"]();}handleLoadMore() {this.props.controller.monitorActivityStore.getActivitiesByMonitor({ monitorId: this.props.controller.monitorActivityStore.activeMonitorId, members: Object.keys(this.state.filter.members), page: this.props.controller.monitorActivityStore.nextPage }, false);this.props.controller.monitorActivityStore.setMoreLoading(true);}

  canViewActivityLogs() {
    // contextData here refers to the MonitorConfigurationStore
    return this.monitorPermissionStore.can('viewActivityLogs', this.props.contextData.id);
  }

  handleRefresh() {
    const monitorIdParams = _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_7__["default"].getCurrentRouteParams('build.monitor');
    const monitorPaths = monitorIdParams.monitorPath.split('~');
    const monitorId = monitorPaths.pop();

    this.setState({
      isFilterApplied: false,
      filter: { members: {} } });


    this.props.controller.monitorActivityStore.getActivitiesByMonitor({ monitorId, page: 1 }, true);
    this.props.controller.monitorActivityStore.setActiveMonitorId(monitorId);
  }

  handleFilterSelect(id, users, isSelected) {
    let updatedSelectedMembers = {};

    if (isSelected) {
      updatedSelectedMembers = _extends({},
      this.state.filter.members, {
        [id]: _.find(users, { id }) });

    } else {
      updatedSelectedMembers = _.omitBy(this.state.filter.members, { id });
    }

    this.setState({
      isFilterApplied: true,
      filter: {
        members: updatedSelectedMembers } },

    () => {
      this.handleSearch(this.state.filter);
    });
  }

  getWorkspaceUsers() {
    const members = _.get(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ActiveWorkspaceStore'), 'members');
    const users = {};

    _.forEach(members, member => {
      let memberDetails = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').teamMembers.get(member.id);

      if (!_.isEmpty(memberDetails)) {
        users[member.id] = _.omit(_extends({}, memberDetails), ['role']);
      }
    });

    return users;
  }

  render() {
    let workspaceUsers = this.getWorkspaceUsers();

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'contextCustomScroll' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_2__["ContextBarViewHeader"], {
            title: this.props.title,
            onClose: this.props.onClose },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_8__["Button"], {
              onClick: this.handleRefresh,
              className: 'pm-activity-section__refresh__btn',
              tooltip:
              this.props.controller.monitorActivityStore.isOffline ? _utils_offlineText__WEBPACK_IMPORTED_MODULE_10__["default"].isOffline :
              _utils_offlineText__WEBPACK_IMPORTED_MODULE_10__["default"].isOnline,

              disabled: this.props.controller.monitorActivityStore.isOffline ||
              this.props.controller.monitorActivityStore.loading },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9___default.a, {
              name: 'icon-action-refresh-stroke',
              color: 'content-color-primary',
              size: 'large' })),


          Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').isLoggedIn && workspaceUsers &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_activity_logs_MonitorActivityFilter__WEBPACK_IMPORTED_MODULE_11__["default"], {
            filtersApplied: this.state.filter,
            loadingUsers: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').isHydrating || !_.size(workspaceUsers),
            handleRefresh: this.handleRefresh,
            handleSearch: this.handleSearch,
            isFilterApplied: this.state.isFilterApplied,
            workspaceUsers: workspaceUsers,
            handleSelect: this.handleFilterSelect,
            disabled: this.props.controller.monitorActivityStore.isOffline,
            tooltip: this.props.controller.monitorActivityStore.isOffline ?
            _utils_offlineText__WEBPACK_IMPORTED_MODULE_10__["default"].isOffline : 'Filter activity logs' })),




        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-activity' },

          this.props.controller.monitorActivityStore.isOffline && !this.props.controller.monitorActivityStore.values.length ?
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_4__["default"], null) :

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_activity_logs_MonitorActivityFeed__WEBPACK_IMPORTED_MODULE_3__["default"], {
            isFilterApplied: this.state.isFilterApplied,
            activities: this.props.controller.monitorActivityStore.values,
            teamUsers: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('TeamStore').allTeamMembers,
            nextPage: this.props.controller.monitorActivityStore.nextPage,
            loading: this.props.controller.monitorActivityStore.loading,
            loaded: this.props.controller.monitorActivityStore.loaded,
            loadingMore: this.props.controller.monitorActivityStore.loadingMore,
            handleLoadMore: this.handleLoadMore,
            listLoading: this.props.controller.monitorActivityStore.listLoading,
            moreLoading: this.props.controller.monitorActivityStore.moreLoading,
            canViewActivityLogs: this.canViewActivityLogs(),
            isOffline: this.props.controller.monitorActivityStore.isOffline }))));





  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14962:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadMore", function() { return LoadMore; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTeamUsers", function() { return fetchTeamUsers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityFeed; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(10);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3016);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3037);
/* harmony import */ var _MonitorActivityItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(14963);
/* harmony import */ var _NoMonitorActivity__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(14965);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1835);
/* harmony import */ var _js_components_empty_states_SignInModal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4694);
/* harmony import */ var _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3785);
/* harmony import */ var _utils_disabled_messages__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4681);
/* harmony import */ var _utils_offlineText__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4700);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};












const MONTH_NAME = ['January', 'February', 'March', 'April', 'May', 'June',
'July', 'August', 'September', 'October', 'November', 'December'];

function formatDateAsMoment(datetime) {
  const time = new Date(datetime),
  monthName = MONTH_NAME[time.getMonth()],
  date = `${time.getDate()},`,
  year = time.getFullYear(),
  formattedDate = [monthName, date, year].join(' '),
  relativeDays = getRelativeDays(datetime);

  if (relativeDays === 0) {
    return 'Today';
  } else
  if (relativeDays === -1) {
    return 'Yesterday';
  }

  return formattedDate;
}

function getRelativeDays(datetime) {
  const time = new Date(datetime),
  dateWithoutTime = new Date(time.toDateString()),
  currentTime = new Date(),
  currentDateWithoutTime = new Date(currentTime.toDateString()),
  diffTime = Math.abs(dateWithoutTime - currentDateWithoutTime),
  diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

  if (dateWithoutTime < currentDateWithoutTime) {
    return -diffDays;
  }

  return diffDays;
}

/**
   * getActivitySections
   * This function groups the activity feed based on the date the action was performed
   *
   * @param {Array} activityFeed  Array containing all the activity in descending order
   * @returns {Array} Array containing activity grouped based on the date it happened
   *
   */
function getActivitySections(activityFeed) {
  if (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activityFeed)) {
    return [];
  }

  let prevGroupDate = '',
  sections = [];

  activityFeed.forEach(feed => {
    let currentGroupDate = new Date(feed.timestamp).toDateString(),
    dateSection = formatDateAsMoment(feed.timestamp);

    if (currentGroupDate !== prevGroupDate) {
      prevGroupDate = currentGroupDate;
      sections.push({
        name: dateSection,
        items: [feed] });

    } else
    {
      sections[sections.length - 1].items.push(feed);
    }
  });

  return sections;
}

/**
   * Load more activity link for monitor activity
   *
   * @param {Object} props - react props
   * @param {Function} props.onClick - function to call on link click
   * @returns {JSX} div with load more link
   */
function LoadMore(props) {

  let content;

  if (!props.show) {
    return null;
  }

  if (props.loading) {
    content = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__["default"], { className: 'loader-activity-logs' });
  } else
  if (!props.nextPage) {
    content = 'The end! You\'ve seen all the activity for this monitor.';
  } else
  {
    content =
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        className: 'loader-link',
        onClick: props.onClick,
        disabled: props.isOffline,
        tooltip: props.isOffline && _utils_offlineText__WEBPACK_IMPORTED_MODULE_10__["default"].isOffline }, 'Load More');




  }

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-activity__load-more' }, content);
}

function fetchTeamUsers(data) {
  let result = Object.assign({}, ...Object.entries(_extends({}, data)).map(([a, b]) => ({ [Number(b.id)]: b })));
  return result;
}

function MonitorActivityFeed(props) {
  const { activities, loading, nextPage, moreLoading } = props,
  groupedActivities = getActivitySections(activities);

  function handleSignIn() {
    _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_8__["default"].initiateLogin({ isSignup: false });
  }

  function handleSignUp() {
    _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_8__["default"].initiateLogin({ isSignup: true });
  }

  if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').isLoggedIn) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-logs-public-view' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_empty_states_SignInModal__WEBPACK_IMPORTED_MODULE_7__["default"], {
          title: 'Create an account',
          subtitle: 'You need an account to continue exploring Postman.',
          onSignIn: handleSignIn,
          onCreateAccount: handleSignUp })));



  } else
  if (!props.canViewActivityLogs) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-logs-public-view' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'empty-state-activity-logs' }),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-logs-public-view__entity' },
          Object(_utils_disabled_messages__WEBPACK_IMPORTED_MODULE_9__["getMessagesList"])().CONSOLE_DISABLED_MSG)));



  } else
  if (loading && !moreLoading) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__["default"], { className: 'loader-activity-logs' }));

  } else
  if (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activities) && loading) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__["default"], { className: 'loader-activity-logs' }));

  } else
  if (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activities) && !loading) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_NoMonitorActivity__WEBPACK_IMPORTED_MODULE_5__["default"], { isFilterApplied: props.isFilterApplied }));

  }

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-activity-feed' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-activity-section-container' },

        lodash__WEBPACK_IMPORTED_MODULE_1___default.a.map(groupedActivities, subActivities => {
          let currentFeedUserId = '',
          showUserIcon = false;

          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: 'pm-activity-section',
                key: subActivities.name },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-monitor-activity-section__date' },
                subActivities.name),


              lodash__WEBPACK_IMPORTED_MODULE_1___default.a.map(subActivities.items, activity => {
                showUserIcon = currentFeedUserId !== activity.actor.id;
                currentFeedUserId = activity.actor.id;

                return (
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_MonitorActivityItem__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    activity: activity,
                    key: activity.id,
                    showUserIcon: showUserIcon,
                    isOffline: props.isOffline }));


              })));



        })),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LoadMore, {
        loading: loading,
        nextPage: nextPage,
        show: !lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activities),
        onClick: props.handleLoadMore,
        isOffline: props.isOffline })));



}

/***/ }),

/***/ 14963:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(296);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_date_helper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _utils_selectors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4685);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3043);
/* harmony import */ var _components_common_UserInfo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(14964);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};







const SYSTEM_ACTOR_ID = 0;

function getTimeString(dateString) {
  let dateObj = new Date(dateString);

  return dateObj.toLocaleString('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true });

}

// eslint-disable-next-line require-jsdoc
function MonitorActivityItem(props) {
  const { activity, showUserIcon } = props,
  relativeDays = _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default.a.getFormattedDate(activity.timestamp);
  let formattedTime;

  // Get relative time if activity is made on the same day
  if (relativeDays === 0) {
    formattedTime = _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default.a.getFormattedTime(activity.timestamp);
  } else
  {
    formattedTime = getTimeString(activity.timestamp);
  }

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()({ 'pm-activity-list-item': true, 'show-user': showUserIcon, 'activity-logs-offline': props.isOffline }) },

      showUserIcon &&
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-monitor-activity-list-item__icon' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_4__["default"], { type: 'user', customPic: activity.actor.profilePicUrl, userId: activity.actor.id, size: 'large' })),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', { className: 'pm-activity-list-item__time' },
        formattedTime),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-activity-list-item__text' },
        activity.actor.id !== SYSTEM_ACTOR_ID &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_UserInfo__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({},
        activity.actor, {
          showAvatar: false,
          allowAlias: true })),


        activity.actor.id !== SYSTEM_ACTOR_ID ? Object(_utils_selectors__WEBPACK_IMPORTED_MODULE_3__["getUserVisibleAction"])(activity.action) : Object(_utils_selectors__WEBPACK_IMPORTED_MODULE_3__["getUserVisibleSystemAction"])(activity.action, activity.context))));



}

/***/ }),

/***/ 14965:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NoMonitorActivity; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _job_information_EmptyState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4677);



/**
                                                         * Empty state for no activity in monitor
                                                         */
function NoMonitorActivity(props) {
  const EMPTY_STATE = {
    text: 'Looks like this monitor doesn\'t have any activity',
    subText: 'The activity feed keeps you updated with the happenings around this monitor',
    withFiltersText: 'No activity found',
    withFiltersSubText: 'Adjust your filters and try again.' };


  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_job_information_EmptyState__WEBPACK_IMPORTED_MODULE_1__["default"], {
      body: props.isFilterApplied ? EMPTY_STATE.withFiltersSubText : EMPTY_STATE.subText,
      title: props.isFilterApplied ? EMPTY_STATE.withFiltersText : EMPTY_STATE.text,
      imageClass: 'activity',
      className:
      props.isFilterApplied ? 'activity-logs-empty-state-no-logs-with-filters' : 'activity-logs-empty-state-no-logs' }));



}

/***/ }),

/***/ 14967:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityFilter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3043);
/* harmony import */ var _js_components_base_Inputs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(309);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3055);
/* harmony import */ var _js_components_base_FuzzySearchInput__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3092);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3037);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3016);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(102);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9__);
var _class;








let


MonitorActivityFilter = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class MonitorActivityFilter extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      isFilterApplied: props.isFilterApplied,
      searchQuery: '',
      searchResults: [] };


    this.handleFuzzySearch = this.handleFuzzySearch.bind(this);
    this.getClasses = this.getClasses.bind(this);
  }

  handleFuzzySearch(searchQuery, searchResults) {
    this.setState({ searchQuery, searchResults });
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_7___default()({
      'select-dropdown-container': true,
      'applied': this.state.isFilterApplied,
      'selected': !this.state.isFilterApplied && this.state.isSelected });

  }

  render() {
    const members = _.map(_.cloneDeep(this.props.workspaceUsers), value => {
      value.name = value.name || value.username;

      return value;
    });

    let users = this.state.searchQuery ? this.state.searchResults : members;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_7___default()(this.props.className) },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["Dropdown"], { className: this.getClasses() },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropdownButton"], { dropdownStyle: 'nocaret' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_8__["Button"], { type: 'tertiary', tooltip: this.props.tooltip, disabled: this.props.disabled },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9___default.a, { name: 'icon-action-filter-stroke' }))),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropdownMenu"], null,
            this.props.disabled ? '' :
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'select-dropdown' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_FuzzySearchInput__WEBPACK_IMPORTED_MODULE_5__["default"], {
                searchQuery: this.state.searchQuery,
                placeholder: 'Search for user',
                onChange: this.handleFuzzySearch,
                items: members,
                searchFields: ['name', 'email'] }),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_7___default()({ 'select-dropdown-list': true, 'isLoading': this.props.loadingUsers }) },

                this.props.loadingUsers ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_6__["default"], null) :
                users && users.length ?
                _.map(users, user => {
                  return (
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                        key: user.id,
                        className: 'select-dropdown-item',
                        onClick: this.props.handleSelect.bind(this, user.id, users, !this.props.filtersApplied.members[user.id]) },

                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_2__["default"], { size: 'small', userId: user.id, customPic: user.profilePicUrl, linkProfile: false }),
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'select-dropdown-item-text', title: user.name }, user.name),
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Inputs__WEBPACK_IMPORTED_MODULE_3__["Checkbox"], {
                        onChange: this.props.handleSelect.bind(this, user.id, users),
                        className: 'select-dropdown-item-checkbox',
                        checked: this.props.filtersApplied.members[user.id] })));



                }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-user-filter-dropdown-no-user' }, 'No users found')))))));








  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);