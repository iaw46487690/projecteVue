(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[17],{

/***/ 14950:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _components_common_MonitorList__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(14951);
/* harmony import */ var _components_sidebar_MonitorSidebarMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(14955);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1835);
/* harmony import */ var _components_common_MonitorListEmpty__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(14956);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6772);
/* harmony import */ var _appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5455);
/* harmony import */ var _components_sidebar_MonitorSidebarError__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(14957);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3011);
/* harmony import */ var _components_common_MonitorOfflineSidebar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(14958);
var _class;










let


MonitorSidebarContainer = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class MonitorSidebarContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    if (!window.analytics.sentFirstLoadMetrics) {
      window.analytics.sendAnalytics(
      'workspace_page',
      'workspace_page_sidebar_monitor_listing_initial_render_time');

    }
    super(props);
    this.store = props.controller.store;
    this.activeWorkspaceStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('ActiveWorkspaceStore');
    this.openCreateMonitorTab = this.openCreateMonitorTab.bind(this);
    this.openMonitorTab = this.openMonitorTab.bind(this);
    this.initiateMonitorDelete = this.initiateMonitorDelete.bind(this);
    this.handleMonitorDelete = this.handleMonitorDelete.bind(this);
    this.handleMonitorRename = this.handleMonitorRename.bind(this);
    this.handleMonitorFocus = this.handleMonitorFocus.bind(this);
    this.refreshMonitorList = this.refreshMonitorList.bind(this);
  }

  openMonitorTab(monitorId, monitorName) {
    this.props.controller.openMonitorTab(monitorId, monitorName);
  }

  /**
     * Helper function to display sign in modal when needed
     * @returns {*}
     */
  showSignInModal() {
    return pm.mediator.trigger('showSignInModal', {
      type: 'generic',
      subtitle: 'You need an account to continue exploring Postman.',
      origin: 'monitors_sidebar_create_button' });

  }

  openCreateMonitorTab() {
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('CurrentUserStore').isLoggedIn) {
      return this.showSignInModal();
    }

    this.props.controller.openCreateMonitorTab();
  }

  initiateMonitorDelete(monitorId, monitorName) {
    pm.mediator.trigger('showDeleteMonitorModal', {
      id: monitorId,
      name: monitorName },

    () => {this.handleMonitorDelete(monitorId);},
    { origin: 'sidebar' });
  }

  handleMonitorDelete(monitorId) {
    pm.toasts.success('Monitor deleted.');
  }

  handleMonitorRename({ monitorId, name }) {
    this.props.controller.updateMonitorName({ monitorId, name });
  }

  handleMonitorFocus(monitorId) {
    this.store.setActiveMonitorId(monitorId);
  }

  refreshMonitorList() {
    this.store.refreshSidebarList();
  }

  getRenderedComponent() {

    const canAddMonitor = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('CurrentUserStore').isLoggedIn || this.activeWorkspaceStore.isMember;

    let sidebarContent;

    // Show offline state if the user is offline when sidebar is loaded
    if (this.store.isOffline && !this.store.values.length && !this.store.loaded) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_MonitorOfflineSidebar__WEBPACK_IMPORTED_MODULE_10__["default"], null));

    }

    // Show an error msg if we encountered an error while loading the list
    if (this.store.error) {
      sidebarContent = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_sidebar_MonitorSidebarError__WEBPACK_IMPORTED_MODULE_8__["default"], { handleRefresh: this.refreshMonitorList });
    }

    // Show loader while the store is loading
    else if ((!this.store.loaded || this.store.loading) && !this.store.values.length) {
        sidebarContent = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_7__["default"], null);
      }

      // Show empty list if there are no monitors in this workspace
      else if (!this.store.values.length && !this.store.searchQuery) {
          sidebarContent =
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_MonitorListEmpty__WEBPACK_IMPORTED_MODULE_5__["default"], {
            canAddMonitor: canAddMonitor,
            createNewMonitor: this.openCreateMonitorTab,
            isOffline: this.store.isOffline });


        }

        // Show 'No Results Found' view if there are no monitors matching the filter query
        else if (!this.store.filteredItems.length) {
            sidebarContent =
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_6__["default"], { searchQuery: this.store.searchQuery, icon: 'icon-entity-monitor-stroke' });

          }

          // There are some items we can display
          else {
              sidebarContent =
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: 'monitor' },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_MonitorList__WEBPACK_IMPORTED_MODULE_2__["default"], {
                  items: this.store.filteredItems,
                  activeMonitorId: this.store.activeMonitorId,
                  canAddMonitor: canAddMonitor,
                  createNewMonitor: this.openCreateMonitorTab,
                  openMonitorTab: this.openMonitorTab,
                  initiateMonitorDelete: this.initiateMonitorDelete,
                  handleMonitorRename: this.handleMonitorRename,
                  handleMonitorFocus: this.handleMonitorFocus,
                  isOffline: this.store.isOffline }));



            }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_sidebar_MonitorSidebarMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
          canAddMonitor: canAddMonitor,
          createNewMonitor: this.openCreateMonitorTab,
          store: this.store,
          isOffline: this.store.isOffline }),

        sidebarContent));


  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-sidebar-tab-content' },
        this.getRenderedComponent()));


  }}) || _class;


/* harmony default export */ __webpack_exports__["default"] = (MonitorSidebarContainer);

/***/ }),

/***/ 14951:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _MonitorListItem__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(14952);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4701);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3011);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1835);
/* harmony import */ var _js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(310);
var _class;







const DEBOUNCE_WAIT = 300;let


MonitorList = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class MonitorList extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.monitorPermissionStore = new _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_3__["default"]();
    this.listItemRefs = {};

    // Shortcut handlers
    this.focusNext = this.focusNext.bind(this);
    this.focusPrev = this.focusPrev.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.renameItem = this.renameItem.bind(this);

    this.openMonitorTabDebounced = _.debounce(monitor =>
    {
      this.props.openMonitorTab(monitor.id, monitor.name);
    }, DEBOUNCE_WAIT);

  }

  render() {
    let items = this.props.items,
    list = items.map((item, index) => {
      const isSelected = item.id === this.props.activeMonitorId ? true : undefined,

      // intentional true for signed out users,
      // so that we can show a 'sign in' modal when they click on the element
      // instead of disabling them
      canRename = !this.monitorPermissionStore.isUserLoggedIn() || this.monitorPermissionStore.can('edit', item.id),
      canDelete = !this.monitorPermissionStore.isUserLoggedIn() || this.monitorPermissionStore.can('delete', item.id),
      canManageRoles = !this.monitorPermissionStore.isUserLoggedIn() || !!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').team;

      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__["default"], { identifier: item.id, key: index },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_MonitorListItem__WEBPACK_IMPORTED_MODULE_1__["default"], {
            ref: ref => {this.listItemRefs[item.id] = ref;},
            data: item, key: index,
            selected: isSelected,
            initiateMonitorDelete: this.props.initiateMonitorDelete,
            handleMonitorRename: this.props.handleMonitorRename,
            canRename: canRename,
            canDelete: canDelete,
            canManageRoles: canManageRoles,
            isOffline: this.props.isOffline })));



    });

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_6__["default"], {
          keyMap: pm.shortcuts.getShortcuts(),
          handlers: this.getKeyMapHandlers() },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-listing' },
          list)));



  }


  getKeyMapHandlers() {
    return {
      nextItem: pm.shortcuts.handle('nextItem', this.focusNext),
      prevItem: pm.shortcuts.handle('prevItem', this.focusPrev),
      delete: pm.shortcuts.handle('delete', this.deleteItem),
      rename: pm.shortcuts.handle('rename', this.renameItem) };

  }

  focusNext(e) {
    e && e.preventDefault();
    this.focusItem(1);
  }

  focusPrev(e) {
    e && e.preventDefault();
    this.focusItem(-1);
  }

  focusItem(indexPadding) {
    const { items, activeMonitorId } = this.props;

    if (!activeMonitorId) {
      return;
    }

    if (_.isEmpty(items)) {
      return;
    }

    const itemIndex = _.findIndex(items, item => item.id === activeMonitorId);
    const selectedItem = items[(itemIndex + indexPadding) % items.length];

    this.props.handleMonitorFocus(selectedItem.id);
    this.openMonitorTabDebounced(selectedItem);
  }

  deleteItem() {
    const activeMonitorId = this.props.activeMonitorId;
    const monitors = this.props.items;
    const monitorToDelete = _.find(monitors, monitor => monitor.id === activeMonitorId);
    this.props.initiateMonitorDelete(monitorToDelete.id, monitorToDelete.name);
  }

  renameItem() {
    const activeMonitorId = this.props.activeMonitorId;
    _.invoke(this.listItemRefs, [activeMonitorId, 'handleEditName']);
  }}) || _class;


/* harmony default export */ __webpack_exports__["default"] = (MonitorList);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14952:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6767);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3055);
/* harmony import */ var _MonitorStatusIndicator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(14953);
/* harmony import */ var _js_modules_services_ManageRolesModalService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2700);
/* harmony import */ var _DisabledTooltipWrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4698);
/* harmony import */ var _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4701);
/* harmony import */ var _common_MonitorMetaIcons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(14954);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1835);
/* harmony import */ var _services_UrlService__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4804);
/* harmony import */ var _js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3594);
var _class;












const MANAGE_ROLES_DISABLED_TEXT = 'You need to be signed-in to a team to perform this action';let


MonitorListItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class MonitorListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);
    this.handleEditName = this.handleEditName.bind(this);
    this.containerRef = this.containerRef.bind(this);
    this.handleRenameSubmit = this.handleRenameSubmit.bind(this);
    this.getRightMetaComponent = this.getRightMetaComponent.bind(this);
    this.getMonitorMetaIcons = this.getMonitorMetaIcons.bind(this);
    this.monitorPermissionStore = new _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_7__["default"]();
  }

  getClassNames() {
    let classes = {
      'monitor-listing-list-item': true,
      'monitor-listing-list-item-focussed': this.props.selected };


    return _.keys(_.pickBy(classes, _.identity)).join(' ');
  }

  containerRef(ele) {
    this.listItem = ele;
  }

  getMonitorRouteConfig() {
    return {
      routeIdentifier: 'build.monitor',
      routeParams: {
        monitorPath: _services_UrlService__WEBPACK_IMPORTED_MODULE_10__["default"].getMonitorPath({ monitorName: this.props.data.name, monitorId: this.props.data.id }) } };


  }

  /**
     * When the rename is confirmed by clicking outside or hitting enter
     * @param value
     */
  handleRenameSubmit(value) {
    if (_.get(this.props, 'data.name') === value) {
      return;
    }

    // send the rename request here
    this.props.handleMonitorRename({
      monitorId: this.props.data.id,
      name: value });

  }

  handleEditName() {
    _.invoke(this.listItem, 'editText');
  }

  /**
     * Helper function to display sign in modal when needed
     * @returns {*}
     */
  showSignInModal(action) {
    return pm.mediator.trigger('showSignInModal', {
      type: 'generic',
      subtitle: 'You need an account to continue exploring Postman.',
      origin: `monitors_sidebar_${action}_action` });

  }

  /**
     * Different dropdown actions should be listed here
     * @param action
     */
  handleDropdownActionSelect(action) {
    if (!this.monitorPermissionStore.isUserLoggedIn()) {
      return this.showSignInModal(action);
    }

    switch (action) {
      case 'delete':
        this.props.initiateMonitorDelete(this.props.data.id, this.props.data.name);
        return;
      case 'manage-roles':
        Object(_js_modules_services_ManageRolesModalService__WEBPACK_IMPORTED_MODULE_5__["manageRolesOnMonitor"])(this.props.data.id);
        return;
      case 'rename':
        this.handleEditName();
        return;}

  }

  /**
     * What we want to show as dropdown actions in this list item
     * @returns {({xpathLabel: string, isEnabled: *, label: string, type: string}|{xpathLabel: string, disabledMsg: string, isEnabled: *, label: string, type: string}|{xpathLabel: string, isEnabled: *, label: string, type: string})[]}
     */
  getActions() {
    return [
    {
      type: 'rename',
      label: 'Rename',
      shortcut: 'rename',
      isEnabled: this.props.canRename,
      xpathLabel: 'rename' },

    {
      type: 'manage-roles',
      label: 'Manage Roles',
      isEnabled: this.props.canManageRoles,
      disabledMsg: MANAGE_ROLES_DISABLED_TEXT,
      xpathLabel: 'manageRoles' },

    {
      type: 'delete',
      label: 'Delete',
      shortcut: 'delete',
      isEnabled: this.props.canDelete,
      xpathLabel: 'delete' }];


  }

  getMenuItems() {
    return _.chain(this.getActions()).
    map(action => {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_3__["MenuItem"], {
            key: action.type,
            refKey: action.type,
            disabled: !action.isEnabled || this.props.isOffline },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DisabledTooltipWrapper__WEBPACK_IMPORTED_MODULE_6__["default"], {
              showTooltip: !action.isEnabled || this.props.isOffline,
              tooltipText: action.disabledMsg,
              isOffline: this.props.isOffline,
              wrapperPrefix: 'monitor-sidebar' },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-action-item' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, action.label),

              action.shortcut &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-shortcut' }, Object(_js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_11__["getShortcutByName"])(action.shortcut))))));





    }).value();
  }

  dropdownOptions() {
    const menuItems = this.getMenuItems();
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_3__["DropdownMenu"], {
          className: 'monitors-dropdown-menu',
          'align-right': true },

        menuItems));


  }

  /**
     * Component shown between the name and the sidebar actions icon
     * @param isHovered
     * @param isDropdownOpen
     * @returns {JSX.Element}
     */
  getRightMetaComponent(isHovered, isDropdownOpen) {
    // apply the actions-hidden class when the 'dropdown actions icon' is hidden
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_MonitorStatusIndicator__WEBPACK_IMPORTED_MODULE_4__["default"], { isPaused: this.props.data.isPaused, isHealthy: this.props.data.isHealthy, className: !isHovered && !isDropdownOpen ? 'actions-hidden' : '' }));

  }

  getMonitorMetaIcons() {
    let activeWorkspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ActiveWorkspaceStore').visibilityStatus;
    let isPublic = activeWorkspace === 'public',
    canTeamView = activeWorkspace === 'team';
    let isEditable = !this.monitorPermissionStore.isUserLoggedIn() || this.monitorPermissionStore.can('edit', this.props.data.id);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_MonitorMetaIcons__WEBPACK_IMPORTED_MODULE_8__["default"], {
        isPublic: isPublic,
        canTeamView: canTeamView,
        isEditable: isEditable }));


  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_2__["default"], {
        text: _.get(this.props, 'data.name', ''),
        ref: this.containerRef,
        isSelected: this.props.selected,
        onRename: this.handleRenameSubmit,
        moreActions: this.dropdownOptions(),
        onActionsDropdownSelect: this.handleDropdownActionSelect,
        rightMetaComponent: this.getRightMetaComponent,
        statusIndicators: this.getMonitorMetaIcons,
        routeConfig: this.getMonitorRouteConfig() }));


  }}) || _class;

/* harmony default export */ __webpack_exports__["default"] = (MonitorListItem);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14954:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorMetaIcons; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);

let

MonitorMetaIcons = class MonitorMetaIcons extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,

        this.props.isPublic === true &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-state-published-stroke',
          color: 'content-color-tertiary',
          className: 'monitor-meta-icon',
          size: 'small',
          title: 'Shared in a public workspace' }),



        !this.props.isPublic && this.props.canTeamView === true &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-descriptive-team-stroke',
          color: 'content-color-tertiary',
          className: 'monitor-meta-icon',
          size: 'small',
          title: 'Shared with team' }),



        this.props.isEditable === false &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-state-locked-stroke',
          color: 'content-color-tertiary',
          className: 'monitor-meta-icon',
          size: 'small',
          title: 'Read only' })));




  }};

/***/ }),

/***/ 14955:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorSidebarMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6770);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1835);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(80);
/* harmony import */ var _utils_disabled_messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4681);
var _class;



let


MonitorSidebarMenu = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class MonitorSidebarMenu extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleSearch = this.handleSearch.bind(this);
  }

  getTooltipText(isDisabled, isOffline) {
    if (isDisabled) {
      if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore').isMember)
      return Object(_utils_disabled_messages__WEBPACK_IMPORTED_MODULE_4__["getMessagesList"])().JOIN_WORKSPACE_MSG;else

      return Object(_utils_disabled_messages__WEBPACK_IMPORTED_MODULE_4__["getMessagesList"])().DEFAULT_DISABLED_MSG;
    }

    if (!isOffline) {
      return 'Create new Monitor';
    } else {
      return 'You can take this action once you are back online';
    }

  }

  handleSearch(query) {
    this.props.store.setSearchQuery(query);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_1__["default"], {
          createNewConfig: {
            tooltip: this.getTooltipText(!this.props.canAddMonitor, this.props.isOffline),
            disabled: !this.props.canAddMonitor || this.props.isOffline,
            onCreate: this.props.createNewMonitor,
            xPathIdentifier: 'addMonitor' },

          onSearch: this.handleSearch,
          searchQuery: this.props.store.searchQuery })));



  }}) || _class;

/***/ }),

/***/ 14956:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorListEmpty; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6773);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1835);
/* harmony import */ var _utils_disabled_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4681);
/* harmony import */ var _utils_offlineText__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4700);




let

MonitorListEmpty = class MonitorListEmpty extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  getTooltipText(isDisabled) {
    if (this.props.isOffline) {
      return _utils_offlineText__WEBPACK_IMPORTED_MODULE_4__["default"].isOffline;
    } else if (isDisabled) {
      if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore').isMember)
      return Object(_utils_disabled_messages__WEBPACK_IMPORTED_MODULE_3__["getMessagesList"])().JOIN_WORKSPACE_MSG;else

      return Object(_utils_disabled_messages__WEBPACK_IMPORTED_MODULE_3__["getMessagesList"])().DEFAULT_DISABLED_MSG;
    }
  }

  render() {
    const canAddMonitor = this.props.canAddMonitor;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_1__["default"], {
        icon: 'icon-entity-monitor-stroke',
        title: 'You have no monitors set up',
        message: 'A monitor lets you run a collection periodically to check for its performance and response.',
        action: {
          label: 'Create a monitor',
          handler: this.props.createNewMonitor,
          tooltip: this.getTooltipText(!canAddMonitor) },

        hasPermissions: !this.props.isOffline && canAddMonitor }));


  }};

/***/ }),

/***/ 14957:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3016);

let

MonitorSidebarError = class MonitorSidebarError extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-monitor-listing__error__wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-monitor-listing__error' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('h5', { className: 'sidebar-monitor-listing__error__header' }, 'Unable to load monitors'),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-monitor-listing__error__sub-header' }, 'Please try again.'),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__["Button"], {
              className: 'btn-small sidebar-monitor-listing__error__retry-button',
              type: 'secondary',
              onClick: this.props.handleRefresh }, 'Try Again'))));






  }};


/* harmony default export */ __webpack_exports__["default"] = (MonitorSidebarError);

/***/ }),

/***/ 14958:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorOfflineSidebar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6773);

let

MonitorOfflineSidebar = class MonitorOfflineSidebar extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-sidebar-offline-state' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_1__["default"], {
          icon: 'icon-state-offline-stroke',
          title: 'Unable to load data as you\u2019re offline.',
          message: 'Your data may not be up to date until you\u2019re back online.' })));



  }};

/***/ })

}]);