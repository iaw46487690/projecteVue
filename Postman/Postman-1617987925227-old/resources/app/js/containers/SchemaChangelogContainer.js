(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[60],{

/***/ 15700:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SchemaChangelogContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(10);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1835);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(74);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3037);
/* harmony import */ var _services_ChangelogService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15701);
/* harmony import */ var _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5323);
/* harmony import */ var _components_SchemaChangelog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(15702);
/* harmony import */ var _api_dev_services_APIPermissionService__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(4093);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(1834);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3016);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3505);
var _class;















let


SchemaChangelogContainer = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class SchemaChangelogContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      revisions: [],
      loading: true,
      error: false,
      isRestoring: false,
      restoreEntity: null,
      changelogsExhausted: false,
      fetchingMoreLogs: false,
      noSchema: false,
      isOffline: false };


    this.handleRestoreSchema = this.handleRestoreSchema.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
    this.handleRetry = this.handleRetry.bind(this);
    this.handleFetchChangelog = this.handleFetchChangelog.bind(this);
    this.handleFetchMoreChangelog = this.handleFetchMoreChangelog.bind(this);
    this.getRefreshIconText = this.getRefreshIconText.bind(this);
  }

  componentDidMount() {
    this.disposeChangelogReaction = Object(mobx__WEBPACK_IMPORTED_MODULE_4__["reaction"])(
    () => lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.id') &&
    lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.versionId') &&
    !lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.schemaLoading') &&
    lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.schemaId') &&
    Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected,
    fetch => {
      if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected && (this.state.loading || this.state.error)) {
        this.setState({ loading: false, isOffline: true });

        return;
      }

      if (!lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.schemaId')) {
        this.setState({ loading: false, noSchema: true, isOffline: false });
      }

      if (fetch) {
        this.setState({
          loading: true,
          noSchema: false,
          error: false,
          isOffline: false });


        this.handleFetchChangelog();

        _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_12__["default"].addEventV2({
          category: 'schema',
          action: 'view_changelog',
          entityId: lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.id') });

      }
    },
    { fireImmediately: true });

  }

  componentWillUnmount() {
    lodash__WEBPACK_IMPORTED_MODULE_2___default.a.isFunction(this.disposeChangelogReaction) && this.disposeChangelogReaction();
    lodash__WEBPACK_IMPORTED_MODULE_2___default.a.isFunction(this.disposeSocketReaction) && this.disposeSocketReaction();
  }

  handleFetchChangelog() {
    const contextData = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData', {}),
    apiId = contextData.id,
    versionId = contextData.versionId,
    schemaId = contextData.schemaId;

    if (!schemaId) {
      pm.toasts.error(_constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["ERROR_FETCHING_CHANGELOG"]);

      return;
    }

    Object(_services_ChangelogService__WEBPACK_IMPORTED_MODULE_8__["fetchChangelog"])(apiId, versionId, schemaId).
    then(body => {
      this.setState({
        revisions: body.revisions,
        loading: false,
        changelogsExhausted: !body.nextMaxId });

    }).
    catch(err => {
      this.setState({
        loading: false,
        error: true });


      pm.toasts.error(lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(err, 'message') || _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["ERROR_FETCHING_CHANGELOG"]);
    });
  }

  handleFetchMoreChangelog() {
    const maxId = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.state.revisions[this.state.revisions.length - 1], 'id'),
    contextData = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData', {}),
    apiId = contextData.id,
    versionId = contextData.versionId,
    schemaId = contextData.schemaId;

    this.setState({ fetchingMoreLogs: true }, () => {
      lodash__WEBPACK_IMPORTED_MODULE_2___default.a.invoke(this.refs, 'changelog.scrollMoreLoaderIntoView');
    });

    Object(_services_ChangelogService__WEBPACK_IMPORTED_MODULE_8__["fetchChangelog"])(apiId, versionId, schemaId, maxId).
    then(body => {
      this.setState({
        revisions: [...this.state.revisions, ...body.revisions],
        fetchingMoreLogs: false,
        changelogsExhausted: !body.nextMaxId });

    }).
    catch(err => {
      this.setState({
        fetchingMoreLogs: false,
        error: true });


      pm.toasts.error(lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(err, 'message') || _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["ERROR_FETCHING_CHANGELOG"]);
    });
  }

  handleRefresh() {
    this.setState({ loading: true, error: false });

    this.handleFetchChangelog();
  }

  handleRestoreSchema(revisionId = null) {
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore').isLoggedIn) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'schema',
        subtitle: _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["NOT_SIGNED_IN_ERROR"],
        origin: 'schema_changelog_restore' });

    }

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').isMember) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    }

    _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_12__["default"].addEventV2({
      category: 'schema',
      action: 'initiate_restore',
      entityId: lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.id') });


    this.setState({ isRestoring: true, restoreEntity: revisionId });

    if (revisionId) {
      const contextData = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData', {}),
      apiId = contextData.id,
      versionId = contextData.versionId,
      schemaId = contextData.schemaId;

      Object(_services_ChangelogService__WEBPACK_IMPORTED_MODULE_8__["restoreSchema"])(apiId, versionId, schemaId, revisionId).
      then(() => {
        _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_12__["default"].addEventV2({
          category: 'schema',
          action: 'successful_restore',
          entityId: apiId });


        this.setState({ isRestoring: false }, () => {
          this.handleRefresh();
        });

        pm.toasts.success('You restored the schema successfully.');
      }).
      catch(err => {
        this.setState({
          isRestoring: false,
          restoreEntity: null });


        _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_12__["default"].addEventV2({
          category: 'schema',
          action: 'fail_restore',
          entityId: apiId });


        pm.toasts.error(lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(err, 'message') || _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["SCHEMA_RESTORE_FAILED"]);
      });
    }
  }

  handleExpand(diff, apiName) {
    const data = {
      diff: diff,
      apiName: apiName };


    _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_12__["default"].addEventV2({
      category: 'schema',
      action: 'expand_changelog',
      entityId: lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.id') });


    pm.mediator.trigger('showSchemaChangelogModal', data);
  }

  handleRetry() {
    this.setState({ loading: true, error: false });

    this.handleFetchChangelog();
  }

  getRefreshTextClass() {
    return classnames__WEBPACK_IMPORTED_MODULE_5___default()({
      'schema-changelog-container__refresh-button': true,
      'schema-changelog-container__refresh-button__loading': this.state.loading });

  }

  getRefreshIconText() {
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected) {
      return _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["OFFLINE_ERROR"];
    }

    if (this.state.loading || this.state.fetchingMoreLogs || lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.schemaLoading')) {
      return 'Please wait...';
    }

    if (this.state.noSchema) {
      return 'No schema found';
    }

    return 'Fetch newer changelogs';
  }

  render() {
    const apiId = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.id'),
    permissions = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.permissions'),
    updateSchemaPermission = _api_dev_services_APIPermissionService__WEBPACK_IMPORTED_MODULE_11__["default"].hasPermission(permissions, 'updateSchema', 'api', apiId);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_14__["ContextBarViewHeader"], {
            title: this.props.title,
            onClose: this.props.onClose },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_13__["Button"], {
              className: this.getRefreshTextClass(),
              tooltip: this.getRefreshIconText(),
              tooltipImmediate: true,
              type: 'tertiary',
              onClick: this.handleRefresh,
              disabled: this.state.loading ||
              this.state.fetchingMoreLogs ||
              this.state.noSchema ||
              !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected },


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_6__["Icon"], {
              name: 'icon-action-refresh-stroke' }))),



        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'schema-changelog-container__content' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_SchemaChangelog__WEBPACK_IMPORTED_MODULE_10__["default"], {
            ref: 'changelog',
            apiName: lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.name'),
            changelogsExhausted: this.state.changelogsExhausted,
            error: this.state.error,
            fetchingMoreLogs: this.state.fetchingMoreLogs,
            isRestoring: this.state.isRestoring,
            isOffline: this.state.isOffline,
            loading: this.state.loading,
            noSchema: this.state.noSchema,
            restoreEntity: this.state.restoreEntity,
            revisions: this.state.revisions,
            schemaLoading: lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(this.props, 'contextData.schemaLoading'),
            updateSchemaPermission: updateSchemaPermission,
            onExpand: this.handleExpand,
            onRetry: this.handleRetry,
            onRestoreSchema: this.handleRestoreSchema,
            onFetchMoreChangelog: this.handleFetchMoreChangelog }))));




  }}) || _class;

/***/ }),

/***/ 15701:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchChangelog", function() { return fetchChangelog; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "restoreSchema", function() { return restoreSchema; });
/* harmony import */ var _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(303);


/**
                                                                                            * Get changelog for a schema
                                                                                            *
                                                                                            * @param {string} apiId
                                                                                            * @param {string} apiVersionId
                                                                                            * @param {string} schemaId
                                                                                            * @param {number} maxId
                                                                                            */
function fetchChangelog(apiId, apiVersionId, schemaId, maxId = null) {
  if (!apiId) {
    return Promise.reject(new Error('ChangelogService~fetchChangelog: apiId is a mandatory parameter'));
  }

  if (!apiVersionId) {
    return Promise.reject(new Error('ChangelogService~fetchChangelog: apiVersionId is a mandatory parameter'));
  }

  if (!schemaId) {
    return Promise.reject(new Error('ChangelogService~fetchChangelog: schemaId is a mandatory parameter'));
  }

  let url = `/apis/${apiId}/versions/${apiVersionId}/schemas/${schemaId}/revisions`;

  if (maxId) {
    url = url + `?maxId=${maxId}`;
  }

  return _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_0__["default"].request(url, {
    method: 'get' }).

  then(({ body }) => {
    return body;
  });
}

/**
   * Restore the schema to a particular changelog
   *
   * @param {string} apiId
   * @param {string} apiVersionId
   * @param {string} schemaId
   * @param {number} revisionId
   */
function restoreSchema(apiId, apiVersionId, schemaId, revisionId = null) {
  if (!apiId) {
    return Promise.reject(new Error('ChangelogService~restoreSchema: apiId is a mandatory parameter'));
  }

  if (!apiVersionId) {
    return Promise.reject(new Error('ChangelogService~restoreSchema: apiVersionId is a mandatory parameter'));
  }

  if (!schemaId) {
    return Promise.reject(new Error('ChangelogService~restoreSchema: schemaId is a mandatory parameter'));
  }

  let url = `/apis/${apiId}/versions/${apiVersionId}/schemas/${schemaId}/restore`;

  if (revisionId) {
    url = url + `?revisionId=${revisionId}`;
  }

  return _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_0__["default"].request(url, {
    method: 'put' }).

  then(({ body }) => {
    return body;
  });
}

/***/ }),

/***/ 15702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SchemaChangelog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(296);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_date_helper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3043);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1835);
/* harmony import */ var _OfflineChangelog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15703);
/* harmony import */ var _ErrorChangelog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15704);
/* harmony import */ var _EmptyChangelog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15705);
/* harmony import */ var _NoSchemaFound__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15706);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3037);
/* harmony import */ var _js_components_activity_feed_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(6775);
/* harmony import */ var _DiffStrap__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(15707);
/* harmony import */ var _DiffText__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(15709);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3016);
/* harmony import */ var _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(5323);
/* harmony import */ var _js_components_base_Icons_IntegrationIcon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(15710);
var _class;















let


SchemaChangelog = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class SchemaChangelog extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleScroll = this.handleScroll.bind(this);
    this.handleScrollDebounced = _.debounce(this.handleScroll, 100);
    this.scrollMoreLoaderIntoView = this.scrollMoreLoaderIntoView.bind(this);
    this.getRestoreTooltip = this.getRestoreTooltip.bind(this);
  }

  getDiffs(data) {
    let splitResponse = data['diff']['line'].split('\n'),
    diffs = [],
    diffChunkIdentifierIndexes = [];

    // Removes No newline at end of file text from the diff
    splitResponse.forEach((line, index) => {
      if (line.includes('No newline at end of file')) {
        splitResponse.splice(index, 1);
      }
    });

    // Find the indexes of chunk identifiers
    splitResponse.forEach((line, index) => {
      if (line.charAt(0) === '@') {
        diffChunkIdentifierIndexes.push(index);
      }
    });

    diffChunkIdentifierIndexes.forEach((chunkIndex, index) => {
      if (index === diffChunkIdentifierIndexes.length) {
        diffs.push(splitResponse.slice(chunkIndex, splitResponse.length));
      } else {
        diffs.push(splitResponse.slice(chunkIndex, diffChunkIdentifierIndexes[index + 1]));
      }
    });

    return diffs;
  }

  getActorName(revision) {
    if (_.get(revision, 'actor.type') === 'integration') {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog__header__action-description__actor-name' }, 'Integration'));



    } else
    if (_.get(revision, 'actor.isAccessible')) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_activity_feed_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_10__["User"], {
          id: _.get(revision, 'actor.id'),
          name: _.get(revision, 'actor.name') || _.get(revision, 'actor.username') }));


    }

    // Private/Anonymous User
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog__header__action-description__actor-name' },
        _.get(revision, 'actor.name')));


  }

  getActorIcon(revision) {
    if (_.get(revision, 'actor.type') === 'integration') {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog__actor-icon' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Icons_IntegrationIcon__WEBPACK_IMPORTED_MODULE_15__["default"], { className: 'changelog__actor-icon__content' })));


    } else
    if (_.get(revision, 'actor.isAccessible')) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_activity_feed_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_10__["ProfilePic"], { id: _.get(revision, 'actor.id') });
    }

    // Private/Anonymous User
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog__actor-icon' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_3__["default"], {
          size: 'medium',
          userId: _.get(revision, 'actor.id'),
          customPic: _.get(revision, 'actor.profilePicUrl') })));



  }

  getActionText(action) {
    if (action === 'create') {
      return ' created a new schema.';
    } else
    if (action === 'update') {
      return ' made the following changes:';
    } else
    if (action === 'restore') {
      return ' restored an older revision of the schema.';
    }
  }

  // If the user hits the bottom of the scrollbar then the call is made to fetch more changelogs
  handleScroll() {
    const node = this.refs.contentRef;

    if (!this.props.changelogsExhausted &&
    !this.props.fetchingMoreLogs &&
    Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('SyncStatusStore').isSocketConnected &&
    node && node.scrollHeight - (node.scrollTop + node.offsetHeight) <= 5)
    {
      this.props.onFetchMoreChangelog();
    }
  }

  scrollMoreLoaderIntoView() {
    const node = this.refs.moreLoader;

    node && node.scrollIntoView();
  }

  getRestoreTooltip() {
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('SyncStatusStore').isSocketConnected) {
      return _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_14__["OFFLINE_ERROR"];
    } else
    if (!this.props.updateSchemaPermission) {
      return _constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_14__["PERMISSION_ERROR"];
    } else
    {
      return '';
    }
  }

  render() {
    const isSyncEnabled = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('GateKeeperStore').isSyncEnabled,
    error = !isSyncEnabled || this.props.error;

    if (this.props.loading || this.props.schemaLoading) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog_loader' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_9__["default"], null)));


    }

    if (this.props.isOffline) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_OfflineChangelog__WEBPACK_IMPORTED_MODULE_5__["default"], null));

    }

    if (error) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ErrorChangelog__WEBPACK_IMPORTED_MODULE_6__["default"], {
          onRetry: this.props.onRetry }));


    }

    if (this.props.noSchema) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_NoSchemaFound__WEBPACK_IMPORTED_MODULE_8__["default"], null));

    }

    if (_.get(this.props, 'revisions.length', 0) === 0 && this.props.changelogsExhausted) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EmptyChangelog__WEBPACK_IMPORTED_MODULE_7__["default"], null));

    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: 'changelog__wrapper',
          ref: 'contentRef',
          onScroll: this.handleScrollDebounced },


        (this.props.revisions || []).map((revision, index) => {
          let diffs = this.getDiffs(revision),
          date = _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default.a.getFormattedDateAndTime(revision.createdAt);

          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { key: index, className: 'changelog' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_activity_feed_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_10__["Header"], null,

                this.getActorIcon(revision),

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog__header' },
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog__header__date' }, date),
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog__header__action-description' },

                    this.getActorName(revision),

                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, this.getActionText(revision.action))))),



              _.map(diffs, (diff, newIndex) => {
                let tempDiff;

                if (diff.length > 11) {
                  // take the index till 11 because the 0th index is the chunk identifier
                  tempDiff = diff.slice(0, 11);
                } else
                {
                  tempDiff = diff;
                }

                return (
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-content-container', key: newIndex },
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-content-container__content', onClick: () => {this.props.onExpand(diff, this.props.apiName);} },
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DiffStrap__WEBPACK_IMPORTED_MODULE_11__["default"], { diff: tempDiff }),
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DiffText__WEBPACK_IMPORTED_MODULE_12__["default"], { diff: tempDiff }),
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-content-container__content__shadow' }))));



              }),
              _.has(revision, 'allowedActions.restore') && revision.allowedActions.restore && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-restore' },
                this.props.isRestoring && this.props.restoreEntity === revision.id ?
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-restore__loading' }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_9__["default"], null)) :
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_13__["Button"], {
                    className: 'changelog-restore__button',
                    type: 'text',
                    onClick: () => {this.props.onRestoreSchema(revision.id);},
                    disabled: !this.props.updateSchemaPermission || !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('SyncStatusStore').isSocketConnected,
                    tooltip: this.getRestoreTooltip(),
                    tooltipImmediate: true }, 'Restore'))));







        }),


        !this.props.changelogsExhausted && this.props.fetchingMoreLogs &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            ref: 'moreLoader',
            className: 'changelog__more-loader' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_9__["default"], null))));




  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15703:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return OfflineChangelog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);

let

OfflineChangelog = class OfflineChangelog extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-offline__wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-offline' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-offline__illustration' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
              className: 'changelog-offline__illustration__icon',
              name: 'icon-state-offline-stroke',
              color: 'content-color-tertiary' })),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-offline__content' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-offline__content__header' }, 'Unable to load data as you\'re offline'),


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-offline__content__sub-header' }, 'Your data may not be up to date until you\u2019re back online.')))));






  }};

/***/ }),

/***/ 15704:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ErrorChangelog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3016);

let

ErrorChangelog = class ErrorChangelog extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty changelog_error' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__illustration-wrapper' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'offline__illustration' })),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__title' }, 'There was a problem loading the changelog'),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__subtitle' }, 'There was an unexpected error. Please try again.'),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__action' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__["Button"], {
                type: 'primary',
                onClick: this.props.onRetry }, 'Try Again')))));







  }};

/***/ }),

/***/ 15705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EmptyChangelog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(318);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3016);
/* harmony import */ var _js_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2690);




let

EmptyChangelog = class EmptyChangelog extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.openLink = this.openLink.bind(this);
  }

  openLink(link) {
    link && Object(_js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_1__["openExternalLink"])(link);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty changelog_empty' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__illustration-wrapper entity-empty__illustration-wrapper--changelog' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'changelog-empty__illustration' })),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__title' }, 'No changes yet'),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__subtitle' }, 'Any changes made to your schema show up in the changelog. Edit your schema by going to the Define tab of your API.'),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__["Button"], {
              type: 'text',
              onClick: this.openLink.bind(this, _js_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_3__["SCHEMA_CHANGELOG"]),
              className: 'learn-more-link create-new-monitor__body__input__helptext__link' }, 'Learn more about schema changelog'))));




  }};

/***/ }),

/***/ 15706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NoSchemaFound; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(318);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3016);
/* harmony import */ var _js_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2690);




let

NoSchemaFound = class NoSchemaFound extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.openLink = this.openLink.bind(this);
  }

  openLink(link) {
    link && Object(_js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_1__["openExternalLink"])(link);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty no_schema_found' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__title' }, 'No schema found'),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'entity-empty__subtitle' }, 'Add API schema by going to the Define tab of your API. You can create a schema from scratch, import one locally or from code repository.'),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__["Button"], {
              type: 'text',
              onClick: this.openLink.bind(this, _js_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_3__["DEFINE_SCHEMA"]),
              className: 'learn-more-link' }, 'Learn more about defining APIs'))));






  }};

/***/ }),

/***/ 15710:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return IntegrationIcon; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Icon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(316);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};


const icon =
react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('svg', { width: '14', height: '16', viewBox: '0 0 14 16', fill: 'none', xmlns: 'http://www.w3.org/2000/svg' },
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('defs', null,
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('path', { id: 'integration-icon', d: 'M1 1.5C1 0.671631 1.67163 0 2.5 0H5.58582C5.98352 0 6.36511 0.158081 6.64636 0.439331L7.20703 1H11.5C12.3284 1 13 1.67163 13 2.5V9.5C13 10.3284 12.3284 11 11.5 11H7.5V13.0853C7.92615 13.236 8.26404 13.5739 8.41467 14H14V15H8.41467C8.20874 15.5825 7.65308 16 7 16C6.34692 16 5.79126 15.5825 5.58533 15H0V14H5.58533C5.73596 13.5739 6.07385 13.236 6.5 13.0853V11H2.5C1.67163 11 1 10.3284 1 9.5V1.5ZM2.5 1C2.22388 1 2 1.22388 2 1.5V4H12V2.5C12 2.22388 11.7761 2 11.5 2H7C6.91736 2 6.83679 1.97949 6.76514 1.94141C6.7218 1.91846 6.68176 1.88892 6.64636 1.85352L5.93933 1.14648C5.84558 1.05273 5.71838 1 5.58582 1H2.5ZM2 9.5V5H12V9.5C12 9.77612 11.7761 10 11.5 10H2.5C2.22388 10 2 9.77612 2 9.5ZM7 15C7.27612 15 7.5 14.7761 7.5 14.5C7.5 14.2239 7.27612 14 7 14C6.72388 14 6.5 14.2239 6.5 14.5C6.5 14.7761 6.72388 15 7 15Z' })),

  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('use', { fill: '#7E7E7E', fillRule: 'evenodd', xlinkHref: '#integration-icon' }));



function IntegrationIcon(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Icon__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({},
    props, {
      icon: icon })));


}

/***/ })

}]);