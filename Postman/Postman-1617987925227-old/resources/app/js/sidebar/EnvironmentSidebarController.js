(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[30],{

/***/ 15011:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentSidebarController; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _js_modules_services_SubscribeToRealtimeEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(14998);
/* harmony import */ var _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(303);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1835);
/* harmony import */ var _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2717);
/* harmony import */ var _EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15010);
/* harmony import */ var _js_modules_sync_helpers_SocketEventsService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2232);
/* harmony import */ var _js_modules_controllers_PermissionController__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2619);
/* harmony import */ var _js_modules_model_event__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(305);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1856);











/**
                                                                  * Populates the Stores with Environment attributes
                                                                  *
                                                                  * @param {Array} environments List of environments in the current workspace
                                                                  */
function _populateStoresWithAttributes(environments, model) {
  if (_.isEmpty(environments)) {
    return;
  }

  const publicEnvironments = [],
  sharedEnvironments = [];

  environments.forEach(environment => {
    const isPublic = _.get(environment, 'attributes.permissions.anybodyCanView'),
    isShared = _.get(environment, 'attributes.permissions.teamCanView');

    isPublic && publicEnvironments.push({ id: environment.id });
    isShared && sharedEnvironments.push(environment.id);
  });

  !_.isEmpty(publicEnvironments) && Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('PublicEntityStore').add(publicEnvironments);
  !_.isEmpty(sharedEnvironments) && model.setSharedEnvironments(sharedEnvironments);
}let

EnvironmentSidebarController = class EnvironmentSidebarController {
  constructor() {
    this.model = null;
    this._realtime_subscription = null;
    this._connection_subscription = null;
  }

  async didCreate() {
    this.model = new _EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__["default"](this);

    // If this is Native App, we use the sync API and cache stores for realtime
    // updates
    if (window.SDK_PLATFORM !== 'browser') {
      // Additional step for integrating listing API for online app
      if (!pm.isScratchpad) {
        const environmentListResponse = await this.fetchEnvironments(),
        environments = _.get(environmentListResponse, 'body.data', []);

        _populateStoresWithAttributes(environments, this.model);
      }

      this._hydration_reaction = Object(mobx__WEBPACK_IMPORTED_MODULE_0__["reaction"])(() => Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').environments, environments => {
        const environmentStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('EnvironmentStore');

        this.model.hydrateWithCacheStores(
        environments.map(({ id }) => environmentStore.find(id)));

      }, { fireImmediately: true });

      // Subscribe to model-events for real-time updates in `delete` flow
      //
      // Note:
      // This is a hack because we haven't used the Adapters we had created for ModelEvents / Listing
      // in Environment Sidebar and are currently just relying on a `reaction` on `ActiveWorkspaceStore.dependencies`
      this.unsubscribeModelEvents = pm.eventBus.channel('model-events').subscribe(event => {
        // We only need Environment delete events
        if (!(Object(_js_modules_model_event__WEBPACK_IMPORTED_MODULE_8__["getEventNamespace"])(event) === 'environment' && Object(_js_modules_model_event__WEBPACK_IMPORTED_MODULE_8__["getEventName"])(event) === 'delete')) {
          return;
        }

        const lowLevelEvents = Object(_js_modules_model_event__WEBPACK_IMPORTED_MODULE_8__["getLowLevelEvents"])(event),
        environmentDeleteEvent = _.find(lowLevelEvents, event => event.name === 'deleted' &&
        event.namespace === 'environment'),
        environmentId = _.get(environmentDeleteEvent, 'data.id'),
        owner = _.get(environmentDeleteEvent, 'data.owner'),
        environmentUid = Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_9__["composeUID"])(environmentId, owner);

        this.model.remove(environmentUid);
      });

      return;
    }

    // Sync and re-subscribe to environment listing timeline when sync connects
    this._connection_subscription = Object(_js_modules_sync_helpers_SocketEventsService__WEBPACK_IMPORTED_MODULE_6__["getSocketConnectionObservable"])().
    subscribe(async event => {
      // If the event is not of connect then bailout
      if (event !== 'connect') {
        return;
      }

      // Fetch the environments from remote. Pass the subscribe option as true so that we get
      // realtime events for any subsequent updates that happen
      const environmentListResponse = await this.fetchEnvironments(),

      // Get the subscription ID from the environments list. This is to listen to any updates
      // that happen so that we can refresh the environments sidebar.
      realtimeSubscriptionId = _.get(environmentListResponse, 'body.subscription.id');

      // Subscribe to realtime events. As soon as we get an update, we refetch the environments
      // list
      realtimeSubscriptionId && this.subscribeToChangeEvents(realtimeSubscriptionId);
      if (!window.analytics.sentFirstLoadMetrics) {
        window.analytics.sentFirstLoadMetrics = true;
        window.analytics.sendAnalytics(
        'workspace_page',
        'workspace_page_sidebar_environment_listing_load_time');

        window.analytics.workspaces.sidebar = true;
        if (window.analytics.workspaces.overview && window.analytics.workspaces.activities &&
        window.analytics.workspaces.sidebar) {
          window.analytics.sendAnalytics(
          'workspace_page',
          'workspace_page_load_time');

          window.analytics.metricsCaptureComplete = true;
        }
      }
    });
  }

  subscribeToChangeEvents(realtimeSubscriptionId) {
    // If there was a pre-existing subscription then clear it off
    if (this._realtime_subscription) {
      this._realtime_subscription.unsubscribe();
    }

    this._realtime_subscription = Object(_js_modules_services_SubscribeToRealtimeEvents__WEBPACK_IMPORTED_MODULE_1__["realtimeEventsForSubscription"])(realtimeSubscriptionId).
    subscribe(event => {
      // We have all the models that got created/updated/deleted
      const models = _.get(event, 'data.data', []),

      // Filter out all the models that are either create or update
      createOrUpdateIds = models.
      filter(({ action }) => ['create', 'update'].includes(action)).
      map(({ id }) => id);

      Object(mobx__WEBPACK_IMPORTED_MODULE_0__["transaction"])(() => {
        // Delete the models which are not required
        models.
        filter(({ action }) => action === 'delete').
        forEach(({ id }) => this.model.remove(id));
      });

      if (_.isEmpty(createOrUpdateIds)) {
        return;
      }

      this.createOrUpdateEnvironment(createOrUpdateIds);
    });
  }

  async fetchEnvironments() {
    // Wait for sync to connect
    await Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').onSyncAvailable();

    const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id,

    // When sidebar loads then make a call the sync to get the list of environments
    environmentList = await _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_2__["default"].request(
    _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_4__["ENVIRONMENT_LIST_AND_SUBSCRIBE"]`${workspace}`,
    { method: 'POST' }),


    /**
                            * Update the environment userCanUpdate attribute to manage the glitch on fixing
                            * the rendering for lock icon appearing and disappearing.
                            *
                            * TODO: Fix this appropriately
                            */

    // Also checks whether the permission exists for environments in `PermissionStore`
    environments = _.forEach(
    _.get(environmentList, 'body.data', []), environment => {
      const criteria = {
        model: 'environment',
        modelId: environment.id,
        action: 'UPDATE_ENVIRONMENT' },

      compositeKey = _js_modules_controllers_PermissionController__WEBPACK_IMPORTED_MODULE_7__["default"].getCompositeKey(criteria),
      permissionPresent = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('PermissionStore').members.has(compositeKey);

      !permissionPresent && _.set(environment, 'attributes.permissions.userCanUpdate', true);
    });


    this.model.hydrate(environments);

    return environmentList;
  }

  async createOrUpdateEnvironment(ids = []) {
    // Wait for sync to connect
    await Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').onSyncAvailable();

    // Get the current workspace
    const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id,

    // Make the listing call with a single id
    environmentList = await _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_2__["default"].request(_constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_4__["ENVIRONMENT_LIST"]`${workspace}`, {
      method: 'POST',
      data: { ids } }),


    // Extract the environment
    environments = _.get(environmentList, 'body.data', []);

    // If environment not found then nothing can be done
    if (_.isEmpty(environments)) {
      return;
    }

    Object(mobx__WEBPACK_IMPORTED_MODULE_0__["transaction"])(() => {
      // Add the environment to the model. This will replace the older model
      environments.forEach(environment => {
        this.model.add(environment.id, environment);
      });
    });
  }

  beforeDestroy() {
    this.model.dispose();
    this.model = null;

    this._connection_subscription && this._connection_subscription.unsubscribe();
    this._realtime_subscription && this._realtime_subscription.unsubscribe();
    this._hydration_reaction && this._hydration_reaction();
    this.unsubscribeModelEvents && this.unsubscribeModelEvents();
  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);