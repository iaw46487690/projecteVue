(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ 14979:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(14980);
/* harmony import */ var _js_components_archivedResource_ArchivedResource__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(14982);
/* harmony import */ var _appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5455);
/* harmony import */ var _CollectionSidebarListContainer_CollectionSidebarListContainer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(14984);
/* harmony import */ var _CollectionSidebarMenu_CollectionSidebarMenu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(14994);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3011);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1835);
/* harmony import */ var _js_models_services_DashboardService__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2689);
/* harmony import */ var _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(4832);
/* harmony import */ var _base_SidebarErrorState_SidebarErrorState__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(14995);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3119);
var _class;
















let


CollectionSidebarView = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class CollectionSidebarView extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.focus = this.focus.bind(this);
    this.focusCollection = this.focusCollection.bind(this);
    this.focusNext = this.focusNext.bind(this);

    this.listRef = null;
  }

  getView() {
    const { model, status } = this.props.controller || {},

    // The API isConsistentlyOffline is only supposed to be used to show the offline state view to the user
    // when he has been consistently offline.
    // For disabling the actions on lack of connectivity, please rely on the socket connected state itself for now.
    // Otherwise, there is a chance of data loss if we let the user perform actions
    // when we are attempting a connection.
    { isConsistentlyOffline } = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('SyncStatusStore');

    if (!pm.isScratchpad && isConsistentlyOffline && status === _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_11__["LOADING"]) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_13__["SidebarOfflineState"], null);
    }

    if (status === _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_11__["ERROR"]) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_SidebarErrorState_SidebarErrorState__WEBPACK_IMPORTED_MODULE_12__["default"], {
          title: 'Couldn\u2019t load collections',
          description: 'Try refreshing this page or check back in some time' }));


    }

    if (status === _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_11__["READY"]) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_8__["default"], { identifier: 'collection' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListContainer_CollectionSidebarListContainer__WEBPACK_IMPORTED_MODULE_6__["default"], {
            ref: ref => {this.listRef = ref;},
            model: model })));



    }

    // Keeping sidebar in loading state if the `status` does not meet any of the above condition
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_5__["default"], { hasIcon: true });
  }

  focus() {
    _.invoke(this, 'listRef.__wrappedInstance.focus');
  }

  focusCollection(collectionId, CollectionSidebarContainer) {
    _.invoke(this, 'listRef.__wrappedInstance.focusCollection', collectionId, CollectionSidebarContainer);
  }

  focusNext() {
    const collection = _.head(_.get(this.props.controller, 'model.collections'));

    _.invoke(this, 'listRef.handleSelect', collection.id);
  }

  render() {
    const { model } = this.props.controller || {};

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-wrapper' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarMenu_CollectionSidebarMenu__WEBPACK_IMPORTED_MODULE_7__["default"], {
            model: model }),

          this.getView()),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_archivedResource_ArchivedResource__WEBPACK_IMPORTED_MODULE_4__["default"], {
          archivedResources: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ActiveWorkspaceStore'),
          label: 'COLLECTIONS',
          showWarning: true,
          onClick: _js_models_services_DashboardService__WEBPACK_IMPORTED_MODULE_10__["openArchivedCollections"] })));



  }}) || _class;


CollectionSidebarView.propTypes = {
  controller: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape({
    model: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_3__["default"]),
    status: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOf([_constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_11__["ERROR"], _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_11__["READY"], _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_11__["LOADING"]]) }).
  isRequired };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14984:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarListContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var react_window__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3299);
/* harmony import */ var react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4886);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(74);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3018);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_postman_react_click_outside__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _CollectionSidebarListEmpty_CollectionSidebarListEmpty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(14985);
/* harmony import */ var _js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(310);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3011);
/* harmony import */ var _CollectionSidebarListItemFolder_CollectionSidebarListItemFolder__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(14986);
/* harmony import */ var _CollectionSidebarListItemRequest_CollectionSidebarListItemRequest__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(14988);
/* harmony import */ var _CollectionSidebarListItemExample_CollectionSidebarListItemExample__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(14989);
/* harmony import */ var _CollectionSidebarListItemHead_CollectionSidebarListItemHead__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(14991);
/* harmony import */ var _CollectionSidebarListItemEmpty_CollectionSidebarListItemEmpty__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(14992);
/* harmony import */ var _helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(2675);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(14980);
/* harmony import */ var _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(2706);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(2705);
/* harmony import */ var _CollectionSidebarListItemLoading_CollectionSidebarListItemLoading__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(14993);
var _class;
























const DEFAULT_HEIGHT = 28,
EMPTY_ITEM_HEIGHT = 52,
OVERSCAN_COUNT = 5,
EMPTY = 'empty',
ZERO_INDEX = 0,
LOADING = 'loading',
DELAY_FOR_PREVIEW_TAB_DEBOUNCE = 200;let




CollectionSidebarListContainer = _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_6___default()(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class CollectionSidebarListContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    if (!window.analytics.sentFirstLoadMetrics) {
      window.analytics.sendAnalytics(
      'workspace_page',
      'workspace_page_sidebar_collection_listing_initial_render_time');

    }

    super(props);

    this.handleDragDrop = this.handleDragDrop.bind(this);
    this.handleCreateRequest = this.handleCreateRequest.bind(this);
    this.getListItem = this.getListItem.bind(this);
    this.getItemSize = this.getItemSize.bind(this);
    this.observeSizeChange = this.observeSizeChange.bind(this);
    this.unobserveSizeChange = this.unobserveSizeChange.bind(this);
    this.scrollToItemById = this.scrollToItemById.bind(this);
    this.debouncedOpenActiveItemInTab = _.debounce(this.openActiveItemInTab.bind(this), DELAY_FOR_PREVIEW_TAB_DEBOUNCE);

    // Edit cache
    // this.setEditCache = this.setEditCache.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);

    // Height set for empty list items, as their height may change due to sidebar resizing
    this.heightSetForEmptyItems = {};

    // To reset cached height when there are changes in list items, for example
    // if there is a drag/drop.
    this.heightReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_5__["reaction"])(() => _.get(this.props.model, 'items'), () => {
      this.listRef && this.listRef.resetAfterIndex(ZERO_INDEX);
    });

    // To move to the specified list item on focus shift
    this.focusReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_5__["reaction"])(() => _.get(this.props.model, 'activeItem'), activeItem => {
      if (!activeItem) {
        return;
      }

      // `requestIdleCallback` is used because mounting and rendering of new List items is of
      // higher priority and must be done before scrolling.
      // Also, this helps in waiting for the initial loading and mounting when Collection Sidebar is mounted,
      // before the initial scroll.
      requestIdleCallback(this.scrollToItemById.bind(this, activeItem.id));
    });

    // Resize observer for empty list items
    this.resizeObserver = new ResizeObserver(entries => {
      // eslint-disable-next-line no-unused-vars
      for (const entry of entries) {
        if (!(entry && entry.target && entry.target.dataset)) {
          return;
        }

        const { index } = entry.target.dataset;

        this.heightSetForEmptyItems[index] = entry.target.offsetHeight;
      }

      this.listRef.resetAfterIndex(ZERO_INDEX);
    });

    /**
         * Cache for item currently in edit mode
         *
         * @type {Object}
         * @property {String} id - id of the list item in edit mode
         * @property {String} type - type of the list item (collection / folder / request)
         * @property {Object} listItem - cache of EnvironmentPreviewListItem state
         * @property {Object} inlineInput - cache of inlineInput state
         */
    this.editCache = null;
  }

  componentWillUnmount() {
    this.heightReactionDisposer && this.heightReactionDisposer();
    this.focusReactionDisposer && this.focusReactionDisposer();

    // If any item is in edit mode and not in view,
    // this will update the value if the component unmounts
    this.props.model.activeItem && this.props.model.activeItem.commitCachedRename();

    clearTimeout(this.clearCollectionLoadingTimer);
  }

  getListItem({ index, style }) {
    const { model } = this.props,

    item = _.get(model, ['items', index]),
    prevItem = index && _.get(model, ['items', index - 1]), // If index is not zero
    { activeItem } = model;

    // If there is no item in this index then return null
    // Note: This should not happen
    if (!item) {
      pm.logger.warn('CollectionSideBarListContainer~getListItem', 'Could not find any item at index', index);

      return null;
    }

    const isFocused = model.isFocused(item.id),
    activeIndent = {};

    // Required to show active indentation line
    item.type === EMPTY && (item.parent = prevItem);

    if (activeItem) {
      activeIndent.show = this.shouldShowIndent(item, activeItem);
      activeIndent.depth = activeItem.depth - 1; // Subtract 1 to make the depth zero indexed
    }

    switch (item.type) {
      case LOADING:{
          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: 'collection-virtualized-list-item__loading',
                style: style },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListItemLoading_CollectionSidebarListItemLoading__WEBPACK_IMPORTED_MODULE_19__["default"], {
                depth: item.depth,
                activeIndent: activeIndent })));



        }

      case EMPTY:{
          const collection = prevItem.type === _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["COLLECTION"] ?
          prevItem : model.getParentCollection(prevItem);

          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: 'collection-virtualized-list-item__empty',
                style: style },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListItemEmpty_CollectionSidebarListItemEmpty__WEBPACK_IMPORTED_MODULE_14__["default"], {
                parent: prevItem,
                canAddRequest: collection.userCanUpdate,
                depth: item.depth,
                index: index,
                observeSizeChange: this.observeSizeChange,
                unobserveSizeChange: this.unobserveSizeChange,
                onAddRequest: this.handleCreateRequest,
                activeIndent: activeIndent })));



        }

      case _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["COLLECTION"]:{
          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-virtualized-list-item__head', style: style },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: item.xpath, key: item.xpath },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListItemHead_CollectionSidebarListItemHead__WEBPACK_IMPORTED_MODULE_13__["default"], {
                  key: item.id,

                  item: item,
                  model: model,
                  persistentState: item._persistentState

                  // Reordering
                  , onDragDrop: this.handleDragDrop }))));




        }

      case _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["FOLDER"]:{
          const collection = item.$collection;

          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: 'collection-virtualized-list-item__folder',
                style: style },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: item.xpath, key: item.xpath },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListItemFolder_CollectionSidebarListItemFolder__WEBPACK_IMPORTED_MODULE_10__["default"], {
                  key: item.id,

                  model: model,
                  item: item,
                  persistentState: item._persistentState,
                  collection: collection

                  // Flags
                  , isFocused: isFocused

                  // Reordering
                  , onDragDrop: this.handleDragDrop

                  // Indent marker config
                  , activeIndent: activeIndent }))));




        }

      case _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["REQUEST"]:{
          const collection = item.$collection;

          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: 'collection-virtualized-list-item__request',
                style: style },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: item.xpath, key: item.xpath },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListItemRequest_CollectionSidebarListItemRequest__WEBPACK_IMPORTED_MODULE_11__["default"], {
                  key: item.id,

                  model: model,
                  item: item,
                  persistentState: item._persistentState,
                  collection: collection

                  // Flags
                  , isFocused: isFocused,
                  isFirstChild: prevItem && prevItem.depth < item.depth

                  // Reordering
                  , onDragDrop: this.handleDragDrop

                  // Indent marker
                  , activeIndent: activeIndent }))));




        }

      case _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["RESPONSE"]:{
          const collection = item.$collection;

          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: 'collection-virtualized-list-item__example',
                style: style },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: item.xpath, key: item.xpath },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListItemExample_CollectionSidebarListItemExample__WEBPACK_IMPORTED_MODULE_12__["default"], {
                  key: item.id,

                  model: model,
                  item: item,
                  persistentState: item._persistentState,
                  collection: collection

                  // Flags
                  , isFocused: isFocused,
                  isFirstChild: prevItem && prevItem.depth < item.depth

                  // Reordering
                  , onDragDrop: this.handleDragDrop

                  // Indent marker
                  , activeIndent: activeIndent }))));




        }

      default:
        return null;}

  }

  getItemSize(index) {
    const currentItem = _.get(this.props.model, ['items', index]);

    if (!currentItem) {
      // eslint-disable-next-line no-magic-numbers
      return 0;
    }

    if (currentItem.type === EMPTY) {
      return this.heightSetForEmptyItems[index] || EMPTY_ITEM_HEIGHT;
    }

    return DEFAULT_HEIGHT;
  }

  getKeyMapHandlers() {
    return {
      nextItem: pm.shortcuts.handle('nextItem', this.focusNextShortcut.bind(this)),
      prevItem: pm.shortcuts.handle('prevItem', this.focusPrevShortcut.bind(this)),
      expandItem: pm.shortcuts.handle('expandItem', this.expandItemShortcut.bind(this)),
      collapseItem: pm.shortcuts.handle('collapseItem', this.collapseItemShortcut.bind(this)),
      select: pm.shortcuts.handle('select', this.selectItemShortcut.bind(this)),
      cut: pm.shortcuts.handle('cut', this.cutItemShortcut.bind(this)),
      copy: pm.shortcuts.handle('copy', this.copyItemShortcut.bind(this)),
      paste: pm.shortcuts.handle('paste', this.pasteItemShortcut.bind(this)),
      duplicate: pm.shortcuts.handle('duplicate', this.duplicateItemShortcut.bind(this)),
      delete: pm.shortcuts.handle('delete', this.deleteItemShortcut.bind(this)),
      rename: pm.shortcuts.handle('rename', this.handleRenameShortcut.bind(this)),
      search: pm.shortcuts.handle('search'),
      quit: pm.shortcuts.handle('quit', this.handleQuitShortcut.bind(this)) // Discard changes
    };
  }

  observeSizeChange(node) {
    this.resizeObserver && this.resizeObserver.observe(node);
  }

  unobserveSizeChange(node, index) {
    this.resizeObserver && this.resizeObserver.unobserve(node);
    delete this.heightSetForEmptyItems[index];
  }

  handleQuitShortcut() {
    this.props.model.activeItem && this.props.model.activeItem.cancelRename();
  }

  handleClickOutside(e) {
    const targetClassName = e && e.target && e.target.className,
    isFromInlineInput = targetClassName === 'input input-box inline-input',
    { activeItem } = this.props.model;


    if (
    // Click is coming from the inline input
    isFromInlineInput ||

    // Or there is no active item
    !activeItem ||

    // Or the active item is not being edited
    !activeItem._persistentState.isEditing)
    {return;} // Bail out

    this.props.model.activeItem && this.props.model.activeItem.commitCachedRename();

    // Stop the propagation to children elements, as this click is supposed to save the item
    // currently in edit mode. Also `commitCachedRename` will trigger a re-render.
    e && e.stopPropagation();
  }

  handleRenameShortcut() {
    const { activeItem } = this.props.model;

    if (!activeItem) {
      return;
    }

    // Enter into the renaming mode or exit out of it
    activeItem.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_18__["ACTION_TYPE_RENAME_TOGGLE"]);
  }

  shouldShowIndent(currentItem, activeItem) {
    if (!(activeItem && currentItem)) {
      return false;
    }

    const activeItemParent = this.props.model.getParent(activeItem);

    if (!activeItemParent) {
      return false;
    }

    if (activeItemParent.id === currentItem.id) {
      return true;
    }

    const currentItemParent = currentItem.type === EMPTY || currentItem.type === LOADING ?
    currentItem.parent :
    this.props.model.getParent(currentItem);

    if (!currentItemParent) {
      return false;
    }

    return this.shouldShowIndent(currentItemParent, activeItem);
  }

  scrollToItemById(itemId) {
    if (!itemId) {
      return;
    }

    const foundIndex = _.findIndex(_.get(this.props.model, 'items'), visibleItem => visibleItem.id === itemId);

    // Item not found
    if (foundIndex < ZERO_INDEX) {
      return;
    }

    this.listRef && this.listRef.scrollToItem(foundIndex);
  }

  focus() {
    if (!this.sidebarListRef) {
      return;
    }

    this.sidebarListRef.focus();
  }

  // Its a wrapper function which will be used in the debounce function. Its function is for opening preview of
  // respective items scrolled in the Sidebar.
  openActiveItemInTab() {
    const { model } = this.props;

    model.activeItem && model.activeItem.openInTab();
  }

  focusNextShortcut(e) {
    e && e.preventDefault();

    const { model } = this.props;

    model.focusNextItem();
    this.debouncedOpenActiveItemInTab();
  }

  focusPrevShortcut(e) {
    e && e.preventDefault();

    const { model } = this.props;

    model.focusPreviousItem();
    this.debouncedOpenActiveItemInTab();
  }

  expandItemShortcut(e) {
    e && e.preventDefault();

    const { model } = this.props || {},
    { activeItem } = model || {};

    if (!activeItem) {
      return;
    }

    model.expandItem(activeItem);
  }

  collapseItemShortcut(e) {
    e && e.preventDefault();

    const { model } = this.props || {},
    { activeItem } = model || {};

    if (!activeItem) {
      return;
    }

    // If item is already open, close the item
    if (model.isOpen(activeItem.id)) {
      return model.collapseItem(activeItem);
    }

    // If item is already closed, check the parent
    const parentItem = model.getParent(activeItem);

    // If not parent element found then bail
    if (!parentItem) {
      return;
    }

    // If parent item is open the collapse it or else focus on it
    if (model.isOpen(parentItem.id)) {
      model.collapseItem(parentItem);
    } else {
      model.focusItem(parentItem);
    }
  }

  selectItemShortcut(e) {
    const { activeItem } = this.props.model;

    if (!activeItem) {
      return;
    }

    activeItem.commitCachedRename();

    e && e.preventDefault();

    activeItem.type === _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["REQUEST"] && activeItem.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_18__["ACTION_TYPE_SELECT"], { returnKey: true });
  }

  cutItemShortcut() {
    const { model } = this.props || {},
    { activeItem } = model || {};

    if (!(model && activeItem)) {
      return;
    }

    switch (activeItem.type) {
      case _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["REQUEST"]:
        if (!activeItem.$collection.userCanUpdate) {
          return pm.toasts.error('You do not have permissions required to perform the action.');
        }

        break;

      case _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_17__["FOLDER"]:
        if (!activeItem.$collection.userCanUpdate) {
          return pm.toasts.error('You do not have permissions required to perform the action.');
        }

        break;

      default: // Collection cut is not supported
        return;}


    model.cutItem();
  }

  copyItemShortcut() {
    const { model } = this.props || {};

    model.copyItem();
  }

  pasteItemShortcut() {
    const { model } = this.props || {};

    if (!(model && model.clipboard)) {
      return false;
    }

    if (!Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_15__["isWorkspaceMember"])()) {
      if (Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_15__["canJoinWorkspace"])()) {
        return pm.mediator.trigger('openUnjoinedWorkspaceModal');
      }

      if (!Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_15__["isLoggedIn"])()) {
        return pm.mediator.trigger('showSignInModal', {
          type: 'collection_sidebar',
          origin: 'paste_item' });

      }

      return pm.toasts.error('You do not have permissions required to perform the action.');
    }

    model.pasteItem();
  }

  duplicateItemShortcut() {
    const { activeItem } = this.props.model || {};

    if (!activeItem) {
      return;
    }

    activeItem.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_18__["ACTION_TYPE_DUPLICATE"]);
  }

  deleteItemShortcut(e) {
    const { activeItem } = this.props.model || {};

    if (!activeItem || activeItem._persistentState.isEditing) {
      return;
    }

    e && e.preventDefault();

    activeItem.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_18__["ACTION_TYPE_DELETE"]);
  }

  handleDragDrop(source, destination, position) {
    // Bail if destination is runner
    if (destination.type === 'tab' && destination.id === 'runner') {
      return;
    }

    // If the sourceId is same a destination id then bail.
    // There is nothing to do
    if (source.id === destination.id) {
      return;
    }

    if (destination.type === 'request' && source.type === 'request') {
      // Only two possible positions before and after - In case of on make it before
      position === 'on' && (position = 'before');
    }

    this.props.model.reorder(source, destination, position);
  }

  handleCreateRequest(target) {
    return target.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_18__["ACTION_TYPE_ADD_REQUEST"]);
  }

  render() {
    const items = _.get(this.props.model, 'items');

    if (_.isEmpty(items)) {
      const query = _.get(this.props.model, 'searchQuery');

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CollectionSidebarListEmpty_CollectionSidebarListEmpty__WEBPACK_IMPORTED_MODULE_7__["default"], { query: query });
    }

    const lister = data =>
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_2__["Observer"], null,
      this.getListItem.bind(this, data)),


    sizer = ({ height, width }) =>
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_2__["Observer"], null,
      () =>
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_window__WEBPACK_IMPORTED_MODULE_3__["VariableSizeList"], {
          height: height,
          itemCount: items.length,
          itemSize: this.getItemSize,
          width: width,
          ref: ref => {this.listRef = ref;},
          overscanCount: OVERSCAN_COUNT,
          className: 'collection-sidebar-list__list' },

        lister));





    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_8__["default"], { handlers: this.getKeyMapHandlers() },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: 'collection-sidebar-list',
            ref: ref => {this.sidebarListRef = ref;}

            // Using `onClickCapture` instead of `onClick` to prevent propagation in cases of some item
            // already being in edit mode, and using the click to just save the item
            , onClickCapture: this.handleClickOutside },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_4__["default"], null, sizer))));



  }}) || _class) || _class;


CollectionSidebarListContainer.propTypes = {
  model: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_16__["default"]).isRequired };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14985:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarListEmpty; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1835);
/* harmony import */ var _api_CollectionInterface__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2692);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6773);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6772);
var _class;





let


CollectionSidebarListEmpty = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class CollectionSidebarListEmpty extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleCreateCollection = this.handleCreateCollection.bind(this);
  }

  getTooltipText(canAddCollection, isLoggedIn) {
    if (!isLoggedIn && window.SDK_PLATFORM === 'browser') {
      return 'Sign in to create a Collection';
    }

    if (!canAddCollection) {
      return 'You do not have permissions to perform this action.';
    }
  }

  handleCreateCollection() {
    this.props.onClose && this.props.onClose();

    Object(_api_CollectionInterface__WEBPACK_IMPORTED_MODULE_4__["createCollection"])();
  }

  isCollectionAddEnabled(canAddCollection, isLoggedIn) {
    if (window.SDK_PLATFORM === 'browser') {
      return isLoggedIn && canAddCollection;
    }

    return canAddCollection;
  }

  render() {
    const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('PermissionStore'),
    workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id,
    canAddCollection = permissionStore.can('addCollection', 'workspace', workspaceId),
    { isLoggedIn } = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore');

    if (this.props.query) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_6__["default"], { searchQuery: this.props.query, icon: 'icon-entity-collection-stroke' }));

    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_5__["default"], {
        icon: 'icon-entity-collection-stroke',
        title: 'You don\u2019t have any collections',
        message: 'Collections let you group related requests, making them easier to access and run.',
        action: {
          label: 'Create a new Collection',
          handler: this.handleCreateCollection,
          tooltip: this.getTooltipText(canAddCollection, isLoggedIn) },

        hasPermissions: this.isCollectionAddEnabled(canAddCollection, isLoggedIn) }));


  }}) || _class;


CollectionSidebarListEmpty.defaultProps = {
  query: null,
  onClose: null };


CollectionSidebarListEmpty.propTypes = {
  query: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func };

/***/ }),

/***/ 14986:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarListItemFolder; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3312);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_dnd__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(80);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3591);
/* harmony import */ var _js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3097);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(1835);
/* harmony import */ var _helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2675);
/* harmony import */ var _folders_view_FolderActionsDropdown__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(4820);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2705);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3011);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3016);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(14980);
/* harmony import */ var _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(14987);
var _dec, _dec2, _dec3, _class;




















const FOLDER_ITEM_HEIGHT = 30,

folderSource = {
  canDrag(props) {
    if (props.item._persistentState.isEditing) {
      return false;
    }

    return Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_10__["isWorkspaceMember"])() && props.collection.userCanUpdate;
  },

  beginDrag(props) {
    return {
      id: props.item.id,
      type: 'folder',
      collection: props.item.collection };

  },

  endDrag(props, monitor) {
    const dragItem = monitor.getItem(),
    { dropItem, position } = monitor.getDropResult() || {};

    if (_.isUndefined(dropItem)) {
      return;
    }

    props.onDragDrop(dragItem, dropItem, position);
  } },


folderTarget = {
  hover(props, monitor, component) {
    const { persistentState } = props,
    dragId = monitor.getItem().id,
    dropId = props.item.id,
    dragItem = monitor.getItem();

    let isEditable = false;

    if (dragItem.type === 'request' || dragItem.type === 'folder') {
      isEditable = props.collection.userCanUpdate;
    }

    if (dragId === dropId || !isEditable) {
      return {};
    }

    const sourceOffset = monitor.getClientOffset(),
    targetOffset = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(component).getBoundingClientRect(),
    threshold = FOLDER_ITEM_HEIGHT * 0.25,
    bottomThreshold = targetOffset.top + FOLDER_ITEM_HEIGHT - threshold,
    topThreshold = targetOffset.top + threshold;

    if (monitor.getItem().type !== 'request' && sourceOffset.y > bottomThreshold) {
      persistentState.scheduleHoverUpdate('bottom');
    } else if (monitor.getItem().type !== 'request' && sourceOffset.y < topThreshold) {
      persistentState.scheduleHoverUpdate('top');
    } else if (sourceOffset.y < bottomThreshold && sourceOffset.y > topThreshold) {
      persistentState.scheduleHoverUpdate('on');
    }
  },

  drop(props, monitor, component) {
    const dragId = monitor.getItem().id,
    dropId = props.item.id,
    dragItem = monitor.getItem();

    let isEditable = false;

    if (dragItem.type === 'request' || dragItem.type === 'folder') {
      isEditable = props.collection.userCanUpdate;
    }

    if (dragId === dropId || !isEditable) {
      return {};
    }

    const sourceOffset = monitor.getClientOffset(),
    targetOffset = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(component).getBoundingClientRect(),

    threshold = targetOffset.height * 0.25,

    bottomThreshold = targetOffset.bottom - threshold,
    topThreshold = targetOffset.top + threshold;

    let position;

    if (sourceOffset.y > bottomThreshold) {
      position = 'after';
    } else if (sourceOffset.y < topThreshold) {
      position = 'before';
    } else {
      position = 'on';
    }

    return {
      dropItem: {
        id: props.item.id,
        type: 'folder',
        collection: props.item.collection },

      position };

  } };let






































































CollectionSidebarListItemFolder = (_dec = Object(_js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_7__["default"])([{ label: 'Add Request', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_ADD_REQUEST"]);} }, { label: 'Add Folder', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_ADD_FOLDER"]);} }, { label: 'Rename', shortcut: 'rename', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_RENAME_TOGGLE"]);} }, { label: 'Duplicate', shortcut: 'duplicate', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DUPLICATE"]);} }, { label: 'Delete', shortcut: 'delete', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DELETE"]);} }], monitor => ({ isContextMenuOpen: monitor.isOpen })), _dec2 = Object(react_dnd__WEBPACK_IMPORTED_MODULE_4__["DropTarget"])(['collection-sidebar-request-item', 'collection-sidebar-folder-item'], folderTarget, (connect, monitor) => ({ connectDropTarget: connect.dropTarget(), isDragOver: monitor.isOver({ shallow: true }) })), _dec3 = Object(react_dnd__WEBPACK_IMPORTED_MODULE_4__["DragSource"])('collection-sidebar-folder-item', folderSource, (connect, monitor) => ({ connectDragSource: connect.dragSource(), connectDragPreview: connect.dragPreview(), isDragging: monitor.isDragging() })), _dec(_class = _dec2(_class = _dec3(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_5__["observer"])(_class = class CollectionSidebarListItemFolder extends _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_16__["default"] {
  constructor(props) {
    super(props);

    this.onItemClick = this.onItemClick.bind(this);
    this.handleItemToggle = this.handleItemToggle.bind(this);
    this.setInlineInputRef = this.setInlineInputRef.bind(this);
    this.onInlineInputSubmit = this.onInlineInputSubmit.bind(this);
    this.onInlineInputToggle = this.onInlineInputToggle.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleActionSelect = this.handleActionSelect.bind(this);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'collection-sidebar-list-item__entity': true,
      'collection-sidebar-list-item__folder': true,
      'is-drop-hovered-top': this.props.persistentState.dropHoverTop && this.props.isDragOver,
      'is-drop-hovered-bottom': this.props.persistentState.dropHoverBottom && this.props.isDragOver });

  }

  getHeadClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'collection-sidebar-list-item__folder__head': true,
      'is-open': this.isOpen(),
      'is-hovered': this.props.persistentState.dropdownOpen,
      'is-focused': this.props.isFocused,
      'is-dragged': this.props.isDragging,
      'is-right-above': this.props.persistentState.dropHover && this.props.isDragOver });

  }

  getFolderIconClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'pm-icon': true,
      'pm-icon-normal': true,
      'collection-sidebar-list-item__folder__head__icon': true,
      icon_folder: true });

  }

  isOpen() {
    return this.props.model.isOpen(this.props.item.id);
  }

  onItemClick() {
    if (this.props.isContextMenuOpen()) {
      return;
    }

    super.onItemClick();
  }

  render() {
    const
    {
      connectDragSource,
      connectDropTarget,
      connectDragPreview,
      item,
      model,
      persistentState } =
    this.props,
    itemDepth = item.depth,
    iconDirection = model.isOpen(item.id) ? 'down' : 'right';

    return super.render(
    this.getClasses(),
    connectDropTarget(connectDragPreview(connectDragSource(
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        title: item.name,
        className: this.getHeadClasses(),
        onClick: this.onItemClick },


      _.times(itemDepth, index =>
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        key: `indent-${index}`,
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()('collection-sidebar__indent',
        {
          'active-indent': this.showActiveIndent(index) }) })),




      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_14__["Button"], {
          type: 'icon',
          onClick: this.handleItemToggle,
          className: 'collection-sidebar-list-item__toggle-btn' },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_6__["Icon"], {
          name: `icon-direction-${iconDirection}`,
          className: 'pm-icon pm-icon-normal' })),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__folder__icon-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_6__["Icon"], { name: 'icon-entity-folder-stroke', className: this.getFolderIconClasses() })),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_13__["default"], { identifier: 'head' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__folder__head__meta' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_8__["default"], {
            className: 'collection-sidebar-list-item__entity__name collection-sidebar-list-item__folder__head__name',
            placeholder: 'Folder Name',
            ref: this.setInlineInputRef,
            value: item.name,
            onSubmit: this.onInlineInputSubmit,
            onToggleEdit: this.onInlineInputToggle }))),



      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__folder__head__actions' },

        (persistentState.isHovered || persistentState.dropdownOpen) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_13__["default"], { identifier: 'folderOption' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-folder-dropdown-actions-wrapper' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_folders_view_FolderActionsDropdown__WEBPACK_IMPORTED_MODULE_11__["default"], {
              className: 'collection-sidebar-folder-actions-dropdown',
              onSelect: this.handleActionSelect,
              onToggle: this.handleDropdownToggle,
              actions: item.actions })))))))));









  }}) || _class) || _class) || _class) || _class);



CollectionSidebarListItemFolder.defaultProps = {
  isFocused: false,
  isDragOver: false,
  isDragging: false,
  isContextMenuOpen: _.noop,
  connectDragSource: _.noop,
  connectDropTarget: _.noop,
  connectDragPreview: _.noop };


CollectionSidebarListItemFolder.propTypes = {
  collection: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,
  item: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,
  model: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_15__["default"]).isRequired,

  isFocused: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,

  isDragOver: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  isDragging: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,

  isContextMenuOpen: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,

  connectDragSource: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  connectDropTarget: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  connectDragPreview: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14987:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ICollectionSidebarListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(14980);




const MOUSE_HOVER_DEBOUNCE = 300;

/**
                                   * Collection Sidebar List Item Interface
                                   *
                                   * This class implements the common code that is required by all the sidebar list items
                                   * Collection/Folder/Request/Responses
                                   */let
ICollectionSidebarListItem = class ICollectionSidebarListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    // A reference to the inline input defined in the component
    this.inlineInputRef = null;
    this.handleMouseHover = _.debounce(this._handleMouseHover, MOUSE_HOVER_DEBOUNCE, {
      leading: false,
      trailing: true });

  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillUpdate(nextProps) {
    if (!this.props.isFocused && nextProps.isFocused) {
      if (!this.$node) {
        return;
      }

      if (this.$node.scrollIntoViewIfNeeded) {
        this.$node.scrollIntoViewIfNeeded();
      } else {
        this.$node.scrollIntoView && this.$node.scrollIntoView();
      }
    }
  }

  componentWillUnmount() {
    this.props.persistentState.cancelHoverUpdate();
    this.handleMouseHover.cancel();

    this.is_editing_reaction_dispose && this.is_editing_reaction_dispose();

    this.props.persistentState.clearInputRef();
  }

  onInlineInputToggle(active) {
    const { persistentState } = this.props;

    // If the toggle input has become inactive then clear the cache
    if (!active) {
      persistentState.clearEditStateCache();
    } else {
      // If inline input because active then focus on it and select all values
      persistentState.focusOnInput();
    }
  }

  onInlineInputSubmit(name) {
    const { item } = this.props;

    item.commitRename(name);
  }

  onItemClick() {
    // Get the item and the model from the props
    const { item, model } = this.props;

    // Set the current item to be the active item
    model.focusItem(item);

    // If the item is not already expanded then expand it as well
    !model.isOpen(item.id) && model.expandItem(item);

    // Finally open the item in a tab
    item.openInTab();
  }

  setInlineInputRef(ref) {
    this.props.persistentState.setInputRef(ref);
  }

  _handleMouseHover(hover) {
    this.props.persistentState.setHovered(hover);
  }

  handleActionSelect(action) {
    const { item } = this.props;

    item.handleAction(action);
  }

  handleDropdownToggle(value) {
    this.props.persistentState.setDropdownOpen(value);
  }

  handleItemToggle(e) {
    const { model, item } = this.props;

    // Stop propagation of `click` event from the icon button
    // so that list item's `onClick` handler doesn't get triggered
    // and it does not open the list item in the workbench.
    e && e.stopPropagation();
    model.toggleItem(item);
  }

  showActiveIndent(index) {
    const { show, depth } = this.props.activeIndent;

    // If show
    return show && index === Number(depth);
  }

  // The class by itself renders nothing at all
  render(className, dom) {
    const onMouseEnter = this.handleMouseHover.bind(this, true),
    onMouseLeave = this.handleMouseHover.bind(this, false);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          ref: node => {this.$node = node;},
          className: className,
          onMouseEnter: onMouseEnter,
          onMouseLeave: onMouseLeave },

        dom));


  }};


ICollectionSidebarListItem.defaultProps = {
  isFocused: false,
  activeIndent: {
    show: false,
    depth: 0 } };




ICollectionSidebarListItem.propTypes = {
  model: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_2__["default"]).isRequired,
  item: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([
  prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_2__["CollectionModel"]),
  prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_2__["FolderModel"]),
  prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_2__["RequestModel"]),
  prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_2__["ResponseModel"])]).
  isRequired,
  persistentState: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_2__["PersistentState"]).isRequired,
  activeIndent: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  isFocused: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14988:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarListItemRequest; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3312);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_dnd__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3591);
/* harmony import */ var _js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3097);
/* harmony import */ var _js_components_request_RequestIcon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4312);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1835);
/* harmony import */ var _requests_view_RequestActionsDropdown__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3513);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2705);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3011);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3016);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(14980);
/* harmony import */ var _helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(2675);
/* harmony import */ var _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(14987);
var _dec, _dec2, _dec3, _class;




















const getMiddleY = component => {
  const elementRect = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(component).getBoundingClientRect();

  return elementRect.top + elementRect.height / 2;
},

itemSource = {
  canDrag(props) {
    if (props.item._persistentState.isEditing) {
      return false;
    }

    return Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_16__["isWorkspaceMember"])() && props.collection.userCanUpdate;
  },

  beginDrag(props) {
    return {
      id: props.item.id,
      type: 'request',
      folder: props.item.folder,
      collection: props.item.collection };

  },

  endDrag(props, monitor) {
    const dragItem = monitor.getItem(),
    { dropItem, position } = monitor.getDropResult() || {};

    if (_.isUndefined(dropItem)) {
      return;
    }

    props.onDragDrop(dragItem, dropItem, position);
  } },


itemTarget = {
  hover(props, monitor, component) {
    const { persistentState } = props,
    dragId = monitor.getItem().id,
    dropId = props.item.id,
    dragItem = monitor.getItem();

    let isEditable = false;

    if (dragItem.type === 'request') {
      isEditable = props.collection.userCanUpdate;
    }

    if (dragId === dropId || !isEditable) {
      return {};
    }

    if (!props.isFirstChild) {
      persistentState.scheduleHoverUpdate('bottom');

      return;
    }

    // Will only be used for first request in its parent
    if (monitor.getClientOffset().y > getMiddleY(component)) {
      persistentState.scheduleHoverUpdate('bottom');
    } else {
      persistentState.scheduleHoverUpdate('top');
    }
  },

  drop(props, monitor, component) {
    const dragId = monitor.getItem().id,
    dropId = props.item.id,
    dragItem = monitor.getItem();

    let isEditable = false;

    if (dragItem.type === 'request') {
      isEditable = props.collection.userCanUpdate;
    }

    if (dragId === dropId || !isEditable) {
      return {};
    }

    let position = 'after';

    if (props.isFirstChild) {
      position = monitor.getClientOffset().y > getMiddleY(component) ? 'after' : 'before';
    }

    return {
      dropItem: {
        id: props.item.id,
        type: 'request',
        collection: props.item.collection },

      position };

  } };let





















































CollectionSidebarListItemRequest = (_dec = Object(_js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_7__["default"])([{ label: 'Open in Tab', onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_OPEN"]);} }, { label: 'Rename', shortcut: 'rename', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_10__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_RENAME_TOGGLE"]);} }, { label: 'Duplicate', shortcut: 'duplicate', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_10__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DUPLICATE"]);} }, { label: 'Delete', shortcut: 'delete', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_10__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DELETE"]);} }], monitor => ({ isContextMenuOpen: monitor.isOpen })), _dec2 = Object(react_dnd__WEBPACK_IMPORTED_MODULE_4__["DropTarget"])('collection-sidebar-request-item', itemTarget, (connect, monitor) => ({ connectDropTarget: connect.dropTarget(), isDragOver: monitor.isOver({ shallow: true }) })), _dec3 = Object(react_dnd__WEBPACK_IMPORTED_MODULE_4__["DragSource"])('collection-sidebar-request-item', itemSource, (connect, monitor) => ({ connectDragSource: connect.dragSource(), connectDragPreview: connect.dragPreview(), isDragging: monitor.isDragging() })), _dec(_class = _dec2(_class = _dec3(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_6__["observer"])(_class = class CollectionSidebarListItemRequest extends _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_17__["default"] {
  constructor(props) {
    super(props);

    this.onItemClick = this.onItemClick.bind(this);
    this.handleItemToggle = this.handleItemToggle.bind(this);
    this.setInlineInputRef = this.setInlineInputRef.bind(this);
    this.onInlineInputSubmit = this.onInlineInputSubmit.bind(this);
    this.onInlineInputToggle = this.onInlineInputToggle.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleActionSelect = this.handleActionSelect.bind(this);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'collection-sidebar-list-item__entity': true,
      'collection-sidebar-list-item__request': true,
      'is-active': this.props.selected });

  }

  getHeadClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'collection-sidebar-list-item__request__head': true,
      'is-open': this.isOpen(),
      'is-hovered': this.props.persistentState.dropdownOpen,
      'is-selected': this.props.selected,
      'is-focused': this.props.isFocused,
      'is-dragged': this.props.isDragging,
      'is-drop-hovered-top': this.props.persistentState.dropHoverTop && this.props.isDragOver,
      'is-drop-hovered-bottom': (this.props.persistentState.dropHover || this.props.persistentState.dropHoverBottom) &&
      this.props.isDragOver });

  }

  isOpen() {
    return this.props.model.isOpen(this.props.item.id);
  }

  onItemClick() {
    if (this.props.isContextMenuOpen()) {
      return;
    }

    super.onItemClick();
  }

  render() {
    const {
      connectDragSource,
      connectDropTarget,
      connectDragPreview,
      model,
      item,
      persistentState } =
    this.props,
    itemDepth = item.depth,
    hasExamples = Boolean(item.responses.size),
    iconDirection = model.isOpen(item.id) ? 'down' : 'right';


    return super.render(
    this.getClasses(),
    connectDropTarget(connectDragPreview(connectDragSource(
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        title: item.name,
        className: this.getHeadClasses(),
        onClick: this.onItemClick },


      _.times(itemDepth, index =>
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        key: `indent-${index}`,
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()('collection-sidebar__indent',
        {
          'active-indent': this.showActiveIndent(index) }) })),





      hasExamples ?

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_14__["Button"], {
          type: 'icon',
          onClick: this.handleItemToggle,
          className: 'collection-sidebar-list-item__toggle-btn' },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
          name: `icon-direction-${iconDirection}`,
          className: 'pm-icon pm-icon-normal' })) :



      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list__request-spacing' }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_request_RequestIcon__WEBPACK_IMPORTED_MODULE_9__["default"], { method: item.method || 'GET' }),
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_13__["default"], { identifier: 'head' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__request__meta' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_8__["default"], {
            className: 'collection-sidebar-list-item__entity__name collection-sidebar-list-item__request__name',
            placeholder: 'Request Name',
            ref: this.setInlineInputRef,
            value: item.name,
            onSubmit: this.onInlineInputSubmit,
            onToggleEdit: this.onInlineInputToggle }))),



      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__request__actions' },

        (persistentState.isHovered || persistentState.dropdownOpen) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_13__["default"], { identifier: 'requestOption' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-request-dropdown-actions-wrapper' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_requests_view_RequestActionsDropdown__WEBPACK_IMPORTED_MODULE_11__["default"], {
              className: 'collection-sidebar-request-actions-dropdown',
              actions: item.actions,
              onSelect: this.handleActionSelect,
              onToggle: this.handleDropdownToggle })))))))));









  }}) || _class) || _class) || _class) || _class);



CollectionSidebarListItemRequest.defaultProps = {
  isFocused: false,
  isDragOver: false,
  isDragging: false,
  selected: false,
  isContextMenuOpen: _.noop,
  connectDragSource: _.noop,
  connectDropTarget: _.noop,
  connectDragPreview: _.noop };


CollectionSidebarListItemRequest.propTypes = {
  collection: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,
  item: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,
  model: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_15__["default"]).isRequired,

  activeIndent: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,

  isFocused: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,

  isDragOver: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  isDragging: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  selected: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,

  isContextMenuOpen: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,

  connectDragSource: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  connectDropTarget: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  connectDragPreview: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14989:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarListItemExample; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(80);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1835);
/* harmony import */ var _js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3591);
/* harmony import */ var _js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3097);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2705);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3011);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(14980);
/* harmony import */ var _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(14987);
/* harmony import */ var _examples_view_ExampleActionsDropdown__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(14990);
var _dec, _class;













let






































CollectionSidebarListItemExample = (_dec = Object(_js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_6__["default"])([{ label: 'Rename', shortcut: 'rename', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_8__["ACTION_TYPE_RENAME_TOGGLE"]);} }, { label: 'Duplicate', shortcut: 'duplicate', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_8__["ACTION_TYPE_DUPLICATE"]);} }, { label: 'Delete', shortcut: 'delete', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('OnlineStatusStore').userCanSave && props.collection.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_8__["ACTION_TYPE_DELETE"]);} }], monitor => ({ isContextMenuOpen: monitor.isOpen })), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_4__["observer"])(_class = class CollectionSidebarListItemExample extends _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_11__["default"] {
  constructor(props) {
    super(props);

    this.onItemClick = this.onItemClick.bind(this);
    this.handleItemToggle = this.handleItemToggle.bind(this);
    this.setInlineInputRef = this.setInlineInputRef.bind(this);
    this.onInlineInputSubmit = this.onInlineInputSubmit.bind(this);
    this.onInlineInputToggle = this.onInlineInputToggle.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleActionSelect = this.handleActionSelect.bind(this);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'collection-sidebar-list-item__entity': true,
      'collection-sidebar-list-item__request': true,
      'is-drop-hovered-top': this.props.persistentState.dropHoverTop && this.props.isDragOver,
      'is-drop-hovered-bottom': this.props.persistentState.dropHoverBottom && this.props.isDragOver,
      'is-active': this.props.selected });

  }

  getHeadClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'collection-sidebar-list-item__request__head': true,
      'is-hovered': this.props.persistentState.dropdownOpen,
      'is-focused': this.props.isFocused,
      'is-dragged': this.props.isDragging });

  }

  onItemClick() {
    if (this.props.isContextMenuOpen()) {
      return;
    }

    super.onItemClick();
  }

  render() {
    const { item, persistentState } = this.props,
    itemDepth = item.depth;

    return super.render(
    this.getClasses(),
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        className: this.getHeadClasses(),
        title: item.name,
        onClick: this.onItemClick },


      _.times(itemDepth, index =>
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        key: `indent-${index}`,
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()('collection-sidebar__indent',
        {
          'active-indent': this.showActiveIndent(index) }) })),




      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__example-icon__wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_3__["Icon"], {
          name: 'icon-entity-example-stroke',
          color: 'content-color-secondary',
          className: 'collection-sidebar-list-item__example-icon' })),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: 'head' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__request__meta' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
            className: 'collection-sidebar-list-item__entity__name collection-sidebar-list-item__request__name',
            placeholder: 'Example Name',
            ref: this.setInlineInputRef,
            value: item.name,
            onSubmit: this.onInlineInputSubmit,
            onToggleEdit: this.onInlineInputToggle }))),



      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__request__actions' },

        (persistentState.isHovered || persistentState.dropdownOpen) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: 'responseOption' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-request-dropdown-actions-wrapper' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_examples_view_ExampleActionsDropdown__WEBPACK_IMPORTED_MODULE_12__["default"], {
              className: 'collection-sidebar-request-actions-dropdown',
              actions: item.actions,
              onSelect: this.handleActionSelect,
              onToggle: this.handleDropdownToggle }))))));








  }}) || _class) || _class);


CollectionSidebarListItemExample.defaultProps = {
  isFocused: false,
  isDragOver: false,
  isDragging: false,
  isContextMenuOpen: _.noop };


CollectionSidebarListItemExample.propTypes = {
  collection: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired,
  item: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired,
  model: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_10__["default"]).isRequired,

  activeIndent: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired,

  isFocused: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,

  isContextMenuOpen: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14990:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ExampleActionsDropdown; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _base_compound__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3514);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1835);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2705);
/* harmony import */ var _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3491);
var _class;







let


ExampleActionsDropdown = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class ExampleActionsDropdown extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);

    this.getDisabledText = this.getDisabledText.bind(this);
  }

  getDisabledText(isDisabled) {
    if (!isDisabled) {
      return;
    }

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('OnlineStatusStore').userCanSave) {
      return _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_6__["DISABLED_TOOLTIP_IS_OFFLINE"];
    }

    return _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_6__["DISABLED_TOOLTIP_NO_PERMISSION"];
  }

  getKeymapHandlers() {
    return {
      rename: pm.shortcuts.handle('rename', this.handleShortcutSelect.bind(this, _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_RENAME_TOGGLE"])),
      duplicate: pm.shortcuts.handle('duplicate', this.handleShortcutSelect.bind(this, _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_DUPLICATE"])),
      delete: pm.shortcuts.handle('delete', this.handleShortcutSelect.bind(this, _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_DELETE"])) };

  }


  handleShortcutSelect(action) {
    this.props.onSelect && this.props.onSelect(action);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_compound__WEBPACK_IMPORTED_MODULE_3__["EntityActionsDropdown"], {
        dropdownClassName: this.props.className,
        actions: this.props.actions,
        keymapHandlers: this.getKeymapHandlers(),
        onSelect: this.props.onSelect,
        onToggle: this.props.onToggle,
        getDisabledText: this.getDisabledText }));


  }}) || _class;


ExampleActionsDropdown.defaultProps = {
  onToggle: _.noop,
  className: null };


ExampleActionsDropdown.propTypes = {
  actions: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array.isRequired,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  onToggle: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14991:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarListItemHead; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3312);
/* harmony import */ var react_dnd__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dnd__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(80);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3016);
/* harmony import */ var _js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3097);
/* harmony import */ var _js_components_collections_CollectionMetaIcons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4654);
/* harmony import */ var _view_CollectionActionsDropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4839);
/* harmony import */ var _helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2675);
/* harmony import */ var _js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3591);
/* harmony import */ var _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3491);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(2705);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(1835);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3011);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(14980);
/* harmony import */ var _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(14987);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(1856);
var _dec, _dec2, _dec3, _class;



















let





























































































































































































































CollectionSidebarListItemHead = (_dec = Object(_js_components_base_ContextMenu__WEBPACK_IMPORTED_MODULE_11__["default"])([{ label: 'Add Request', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSave && props.item.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_ADD_REQUEST"]);} }, { label: 'Add Folder', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSave && props.item.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_ADD_FOLDER"]);} }, { label: 'Monitor Collection', isVisible(props) {const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('PermissionStore'),workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('ActiveWorkspaceStore').id;return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSaveAndSync && props.item.userCanCreateMonitor && permissionStore.can('addMonitor', 'workspace', workspaceId);}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_ADD_MONITOR"]);} }, { label: 'Mock Collection', isVisible(props) {const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('PermissionStore'),workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('ActiveWorkspaceStore').id;return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSaveAndSync && props.item.userCanCreateMock && permissionStore.can('addMock', 'workspace', workspaceId);}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_ADD_MOCK"]);} }, { label: 'Create a fork', isVisible() {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSaveAndSync;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_FORK"]);} }, { label: 'Merge changes', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSaveAndSync && props.item.isForked;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_MERGE"]);} }, { label: 'Create Pull Request', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSaveAndSync && props.item.isForked;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_PULL_REQUEST"]);} }, { label: 'Rename', shortcut: 'rename', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSave && props.item.userCanUpdate;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_RENAME_TOGGLE"]);} }, { label: 'Duplicate', shortcut: 'duplicate', isVisible() {const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('PermissionStore'),workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('ActiveWorkspaceStore').id;return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSave && permissionStore.can('addCollection', 'workspace', workspaceId);}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_DUPLICATE"]);} }, { label: 'Export', isVisible(props) {const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('PermissionStore'),collectionModelId = Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_18__["decomposeUID"])(props.item.id).modelId,{ userCanSave } = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore');return userCanSave && permissionStore.can('export', 'collection', collectionModelId);}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_DOWNLOAD"]);} }, { label: 'Manage Roles', isVisible() {const currentUserStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('CurrentUserStore');return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSave && Boolean(currentUserStore.team);}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_MANAGE_ROLES"]);} }, { label: 'Remove from workspace', isVisible() {const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('PermissionStore'),workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('ActiveWorkspaceStore').id,isOnline = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('SyncStatusStore').isSocketConnected;return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSaveAndSync && permissionStore.can('removeCollection', 'workspace', workspaceId) && isOnline;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_REMOVE_FROM_WORKSPACE"]);} }, { label: 'Delete', shortcut: 'delete', isVisible(props) {return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSave && props.item.userCanDelete;}, onClick(props) {props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_DELETE"]);} }], monitor => ({ isContextMenuOpen: monitor.isOpen })), _dec2 = Object(react_dnd__WEBPACK_IMPORTED_MODULE_1__["DropTarget"])(['collection-sidebar-request-item', 'collection-sidebar-folder-item'], { canDrop(props) {return props.item.userCanUpdate;}, drop(props, monitor) {const dragId = monitor.getItem().id,dropId = props.item.id,dragItem = monitor.getItem();let isEditable = false;switch (dragItem.type) {case 'folder':case 'request':if (props.item.userCanUpdate) {isEditable = true;}default:}if (dragId === dropId || !isEditable) {return {};}return { dropItem: { id: dropId, type: 'collection' }, position: 'on' };} }, (connect, monitor) => ({ connectDropTarget: connect.dropTarget(), isDragOver: monitor.isOver({ shallow: true }), canDrop: monitor.canDrop() })), _dec3 = Object(react_dnd__WEBPACK_IMPORTED_MODULE_1__["DragSource"])('collection-sidebar-collection-item', { canDrag(props) {if (props.item._persistentState.isEditing) {return false;}return Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_10__["isWorkspaceMember"])();}, beginDrag(props) {return { id: props.item.id, type: 'collection' };} }, (connect, monitor) => ({ connectDragSource: connect.dragSource(), connectDragPreview: connect.dragPreview(), isDragging: monitor.isDragging() })), _dec(_class = _dec2(_class = _dec3(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class CollectionSidebarListItemHead extends _ICollectionSidebarListItem__WEBPACK_IMPORTED_MODULE_17__["default"] {
  constructor(props) {
    super(props);

    this.onItemClick = this.onItemClick.bind(this);
    this.handleItemToggle = this.handleItemToggle.bind(this);
    this.setInlineInputRef = this.setInlineInputRef.bind(this);
    this.onInlineInputSubmit = this.onInlineInputSubmit.bind(this);
    this.onInlineInputToggle = this.onInlineInputToggle.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleActionSelect = this.handleActionSelect.bind(this);
    this.handleToggleFavorite = this.handleToggleFavorite.bind(this);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_4___default()({
      'collection-sidebar-list-item__head': true });

  }

  getHeadClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_4___default()({
      'collection-sidebar-list-item__head__head': true,
      'is-hovered': this.props.persistentState.dropdownOpen,
      'is-focused': this.isFocused(),
      'is-drop-hovered': this.props.item.userCanUpdate && this.props.isDragOver,
      'is-favorited': this.isFavorite(),
      'is-open': this.isOpen() });

  }

  getActionsClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_4___default()({
      'collection-sidebar-list-item__head__actions': true,
      'is-selected': this.isExpanded() });

  }

  isExpanded() {
    return this.props.model.expandedItem === this.props.item.id;
  }

  isFavorite() {
    return this.props.item.isFavorite;
  }

  isOpen() {
    return this.props.model.isOpen(this.props.item.id);
  }

  isFocused() {
    return this.props.item.id === _.get(this.props.model, 'activeItem.id');
  }

  onItemClick() {
    if (this.props.isContextMenuOpen()) {
      return;
    }

    super.onItemClick();
  }


  handleToggleFavorite(e) {
    e && e.stopPropagation();

    this.props.item.handleAction(_js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_FAVORITE"]);
  }

  render() {
    const {
      connectDragSource,
      connectDropTarget,
      connectDragPreview,

      item,
      persistentState } =
    this.props,

    isFavoriteDisabled = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_14__["getStore"])('OnlineStatusStore').userCanSave,
    iconDirection = this.isOpen() ? 'down' : 'right';

    return super.render(
    this.getClasses(),
    connectDropTarget(connectDragPreview(connectDragSource(
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        title: item.name,
        className: this.getHeadClasses(),
        onClick: this.onItemClick },

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_6__["Button"], {
          type: 'icon',
          onClick: this.handleItemToggle,
          className: 'collection-sidebar-list-item__toggle-btn' },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
          name: `icon-direction-${iconDirection}`,
          className: 'pm-icon pm-icon-normal' })),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_15__["default"], { identifier: 'head' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__head__meta' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
              className: 'collection-sidebar-list-item__head__name__wrapper',
              title: item.name },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__head__name-icon__wrapper' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_InlineInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
                className: 'collection-sidebar-list-item__head__name',
                placeholder: 'Collection Name',
                ref: this.setInlineInputRef,
                value: item.name,
                onSubmit: this.onInlineInputSubmit,
                onToggleEdit: this.onInlineInputToggle }),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_collections_CollectionMetaIcons__WEBPACK_IMPORTED_MODULE_8__["default"], {
                collection: item,
                userCanUpdate: item.userCanUpdate,
                showForkLabel: true })),


            (persistentState.isHovered || this.isFavorite()) &&
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_6__["Button"], {
                disabled: isFavoriteDisabled,
                tooltip: isFavoriteDisabled && _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_12__["DISABLED_TOOLTIP_IS_OFFLINE"],
                active: this.isFavorite(),
                className: 'collection-sidebar-list-item__head__favorite-button',
                onClick: this.handleToggleFavorite },


              this.isFavorite() ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                name: 'icon-action-favorite-fill',
                className: 'collection-sidebar-list-item__head__favorite-button-icon pm-icon pm-icon-secondary' }) :


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                name: 'icon-action-favorite-stroke',
                className: 'pm-icon pm-icon-normal collection-sidebar-list-item__head__favorite-button-icon' }))))),








      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getActionsClasses() },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__head__action' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_15__["default"], { identifier: 'options' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-dropdown-actions-wrapper' },

              (persistentState.isHovered || persistentState.dropdownOpen) &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_view_CollectionActionsDropdown__WEBPACK_IMPORTED_MODULE_9__["default"], {
                className: 'collection-sidebar-actions-dropdown',
                onSelect: this.handleActionSelect,
                onToggle: this.handleDropdownToggle,
                actions: item.actions }))))))))));










  }}) || _class) || _class) || _class) || _class);



CollectionSidebarListItemHead.defaultProps = {
  isEditing: false,
  isDragOver: false,
  setEditCache: _.noop,
  isContextMenuOpen: _.noop,
  editCache: null,
  connectDragSource: _.noop,
  connectDropTarget: _.noop,
  connectDragPreview: _.noop,
  canDrop: false };


CollectionSidebarListItemHead.propTypes = {
  item: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired,
  model: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_16__["default"]).isRequired,
  editCache: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object,
  isEditing: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  isDragOver: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  setEditCache: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  isContextMenuOpen: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,

  connectDragSource: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  connectDropTarget: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  connectDragPreview: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  canDrop: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14992:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarListItemEmpty; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _base_atom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3065);
/* harmony import */ var _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2706);





let

CollectionSidebarListItemEmpty = class CollectionSidebarListItemEmpty extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  componentDidMount() {
    this.selfNode = Object(react_dom__WEBPACK_IMPORTED_MODULE_2__["findDOMNode"])(this);

    // Starts observing element
    this.props.observeSizeChange && this.props.observeSizeChange(this.selfNode);
  }

  componentWillUnmount() {
    this.props.unobserveSizeChange && this.props.unobserveSizeChange(this.selfNode, this.props.index);
  }

  render() {
    const { parent } = this.props,
    isCollection = parent.type === _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_5__["COLLECTION"];

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: 'collection-sidebar-list-item__body--empty',
          'data-index': this.props.index },


        _.times(this.props.depth, index =>
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          key: `indent-${index}`,
          className: classnames__WEBPACK_IMPORTED_MODULE_3___default()('collection-sidebar__indent',
          {
            'active-indent': index === Number(this.props.activeIndent.depth) && this.props.activeIndent.show }) })),




        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_3___default()('collection-sidebar-list__empty-item__content',
            {
              'collection-sidebar-list__empty-item__content__for-collection': isCollection }) },


          `This ${parent.type} is empty`,

          this.props.canAddRequest &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_atom__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                type: 'text',
                className: 'learn-more-link',
                onClick: () => this.props.onAddRequest(this.props.parent) }, 'Add a request'), 'to start working.'))));










  }};


CollectionSidebarListItemEmpty.defaultProps = {
  observeSizeChange: null,
  unobserveSizeChange: null,
  onAddRequest: _.noop,
  canAddRequest: false };


CollectionSidebarListItemEmpty.propTypes = {
  parent: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  activeIndent: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  index: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired,
  depth: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired,
  observeSizeChange: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  unobserveSizeChange: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onAddRequest: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  canAddRequest: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14993:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);




/**
                                      *
                                      * @param {Object} props
                                      */
function CollectionSidebarListItemLoading(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-list-item__loading' },

      _.times(props.depth, index =>
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        key: `indent-${index}`,
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()('collection-sidebar__indent',
        {
          'active-indent': index === Number(props.activeIndent.depth) && props.activeIndent.show }) })),




      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-loading-state--item' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_2___default()('has', 'sidebar-loading-state__icon') }),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-loading-state__info' }))));



}

CollectionSidebarListItemLoading.propTypes = {
  activeIndent: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired,
  depth: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired };


/* harmony default export */ __webpack_exports__["default"] = (CollectionSidebarListItemLoading);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14994:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3016);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1835);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(69);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6770);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3055);
/* harmony import */ var _helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2675);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(14980);
/* harmony import */ var _api_CollectionInterface__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2692);
/* harmony import */ var _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3491);
var _class;












const NEW_COLLECTION_CLICK_THROTTLE_TIMEOUT = 1000; // 1 sec
let

CollectionSidebarMenu = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class CollectionSidebarMenu extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = { isCollectionCreateEnabled: true };

    this.handleNewCollectionClick = this.handleNewCollectionClick.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  handleNewCollectionClick() {
    // Disable the create collection button for the defined timeout to prevent
    // a lot of collection creates in a short ammount of time
    this.setState({ isCollectionCreateEnabled: false });

    // Enable after the timeout has elapsed
    //
    // Cancel any pending timeout before setting a new timeout
    this.timeoutId && clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(
    () => {this.setState({ isCollectionCreateEnabled: true });},
    NEW_COLLECTION_CLICK_THROTTLE_TIMEOUT);


    Object(_api_CollectionInterface__WEBPACK_IMPORTED_MODULE_10__["createCollection"])();
  }

  handleTrashOpen() {
    if (!Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_8__["isWorkspaceMember"])()) {
      if (Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_8__["canJoinWorkspace"])()) {
        return pm.mediator.trigger('openUnjoinedWorkspaceModal');
      }

      if (!Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_8__["isLoggedIn"])()) {
        return pm.mediator.trigger('showSignInModal', {
          type: 'trash',
          origin: 'open_trash' });

      }
    }

    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_5__["default"].transitionTo('trash');
  }

  handleSearchChange(query) {
    this.props.model && this.props.model.setSearchQuery(query);
  }

  render() {
    const permissionStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('PermissionStore'),
    workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('ActiveWorkspaceStore').id,
    canAddCollection = permissionStore.can('addCollection', 'workspace', workspaceId),
    { userCanSave } = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('OnlineStatusStore'),
    isCollectionCreateEnabled = userCanSave && canAddCollection && this.state.isCollectionCreateEnabled,
    isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('SyncStatusStore').isSocketConnected,
    disableTrash = Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_8__["isLoggedIn"])() && !(Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_8__["isWorkspaceMember"])() || Object(_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_8__["canJoinWorkspace"])()) || isOffline;

    let tooltipMessage = disableTrash ? 'Only members of this workspace can recover deleted collections' : 'Recover your deleted collections';

    tooltipMessage = isOffline ? 'You can perform this action once you\'re back online' : tooltipMessage;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-sidebar-menu' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_6__["default"], {
          createNewConfig: {
            tooltip: userCanSave ? // eslint-disable-line no-nested-ternary
            canAddCollection ?
            'Create new Collection' :
            'You don\'t have permission to create a Collection in this workspace.' :

            _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_IS_OFFLINE"],
            disabled: !isCollectionCreateEnabled,
            onCreate: this.handleNewCollectionClick,
            xPathIdentifier: 'addCollection' },

          onSearch: this.handleSearchChange,
          searchQuery: _.get(this.props.model, 'searchQuery'),
          moreActions:
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropdownMenu"], null,
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], null,
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                  onClick: this.handleTrashOpen,
                  tooltip: pm.isScratchpad ? 'You need to be in a workspace to open trash' : tooltipMessage,
                  disabled: pm.isScratchpad || disableTrash,
                  type: 'text',
                  className: 'collection-sidebar-menu__actions-trash' }, 'Open Trash'))) })));









  }}) || _class;


CollectionSidebarMenu.propTypes = {
  model: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_9__["default"]).isRequired };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 14995:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SidebarErrorState; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3016);
var _class;


let


SidebarErrorState = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class SidebarErrorState extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.renderButton = this.renderButton.bind(this);
  }

  renderButton() {
    if (this.props.buttonText) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
            type: 'secondary',
            onClick: this.props.handler },

          this.props.buttonText));


    }
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-error-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-error-thumbnail' }),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-error-content-container' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-error-header' }, this.props.title),
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sidebar-error-content' },
            this.props.description),

          this.renderButton())));



  }}) || _class;


SidebarErrorState.defaultProps = {
  description: null,
  buttonText: null,
  handler: _.noop };


SidebarErrorState.propTypes = {
  title: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired,
  description: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  buttonText: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  handler: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);