(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ 6779:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var _checkImportEnvironmentFromUrl__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6780);
/* harmony import */ var _importCollectionAndEnvironment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6781);
/* harmony import */ var _convertData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5198);
/* harmony import */ var _importFileFromUrl__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6782);
/* harmony import */ var _importFiles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6783);
/* harmony import */ var _importFolder__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5390);
/* harmony import */ var _saveEntity__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6784);
/* harmony import */ var _importers_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2917);
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(306);
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(async__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _runtime_helpers_RunnerHelper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4132);











/**
                                                                                          * This is like a step callback that will be called after each collection import
                                                                                          * to handle the success or failure case
                                                                                         */
function handleCollectionImportStatus(error, collection) {
  if (!error) {
    pm.toasts.success('Collection ' + collection.name + ' imported');
  } else
  {
    error.isUserFriendly ?
    pm.toasts.error(error.message, { dedupeId: 'failed-collection-import' }) :
    pm.toasts.error('Failed to import collection ' + collection.name, { dedupeId: 'failed-collection-import' });
  }
}

/**
   * Imports a Collection, Environment, Request, Globals, etc. in any supported format.
   * (Collection v2, Swagger RAML, etc.)
   *
   * @param {string|object} data - A string containing JSON/YAML/XML/etc, or other type of data
   * @param {object} options - options, Currently used by analytics handler to determine which analytics event to send
   * @param {Function} done - calls after every entity is imported
   *
   *
   * @returns {object} convertedData - imported data
   * Resolves with the Postman objects that were imported, and the format that they were imported from.
   */
async function importData(data, options, done) {
  if (_.isEmpty(data)) return _.isFunction(done) && done();
  async__WEBPACK_IMPORTED_MODULE_8___default.a.series([
  next => {
    _importers_index__WEBPACK_IMPORTED_MODULE_7__["default"].importCollections(data.collections, options, handleCollectionImportStatus, result => {
      next(null, result);
    });
  },
  next => {
    _importers_index__WEBPACK_IMPORTED_MODULE_7__["default"].importGlobals(data.globals, _.pick(options, ['activeWorkspace', 'silentToasts']), result => {
      next(null, result);
    });
  },
  next => {
    _importers_index__WEBPACK_IMPORTED_MODULE_7__["default"].importHeaderPresets(data.headerPresets, _.pick(options, ['currentUserID', 'activeWorkspace', 'silentToasts']), result => {
      next(null, result);
    });
  },
  next => {
    _importers_index__WEBPACK_IMPORTED_MODULE_7__["default"].importEnvironments(data.environments, _.pick(options, ['currentUserID', 'silentToasts', 'activeWorkspace']), result => {
      next(null, result);
    });
  },
  next => {
    _importers_index__WEBPACK_IMPORTED_MODULE_7__["default"].importRequests(data.requests, result => {
      next(null, result);
    });
  },
  next => {
    _importers_index__WEBPACK_IMPORTED_MODULE_7__["default"].importCollectionRuns(data.runsInfo, data.events, options,
    _runtime_helpers_RunnerHelper__WEBPACK_IMPORTED_MODULE_9__["handleCollectionRunImportStatus"], result => {
      next(null, result);
    });
  }],
  (err, results) => {
    let importedEntitiesMap = {};

    // Create a map of all entity with data (total, imported and failed count)
    _.forEach(_.compact(results), result => {
      importedEntitiesMap[result.type] = _.pick(result, ['total', 'imported', 'failed']);
    });

    _.isFunction(done) && done(importedEntitiesMap);
  });
}

/* harmony default export */ __webpack_exports__["default"] = ({
  checkImportEnvironmentFromUrl: _checkImportEnvironmentFromUrl__WEBPACK_IMPORTED_MODULE_0__["default"],
  importCollectionAndEnvironment: _importCollectionAndEnvironment__WEBPACK_IMPORTED_MODULE_1__["default"],
  importData,
  convertRawData: _convertData__WEBPACK_IMPORTED_MODULE_2__["default"],
  importDataAs: _convertData__WEBPACK_IMPORTED_MODULE_2__["importDataAs"],
  stringify: _convertData__WEBPACK_IMPORTED_MODULE_2__["stringify"],
  importFileFromUrl: _importFileFromUrl__WEBPACK_IMPORTED_MODULE_3__["default"],
  parseUrlData: _importFileFromUrl__WEBPACK_IMPORTED_MODULE_3__["parseUrlData"],
  importFiles: _importFiles__WEBPACK_IMPORTED_MODULE_4__["default"],
  importFolder: _importFolder__WEBPACK_IMPORTED_MODULE_5__["default"],
  saveEntity: _saveEntity__WEBPACK_IMPORTED_MODULE_6__["default"] });
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6780:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return checkImportEnvironmentFromUrl; });
/* harmony import */ var postman_collection_transformer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1910);
/* harmony import */ var postman_collection_transformer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(postman_collection_transformer__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_yaml__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5202);
/* harmony import */ var js_yaml__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_yaml__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _formats__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5199);
/* harmony import */ var _modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2187);
/* harmony import */ var _utils_util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1860);
/* harmony import */ var _modules_controllers_UserController__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2193);







/**
                                                                        * Checks a URL for presence of an environment, and imports the environment if it is present.
                                                                        * If a URL has been clicked on, it might include an environment
                                                                        * See https://postmanlabs.atlassian.net/wiki/display/PRODUCT/Run+in+Postman+-+Protocol+Proposal for more
                                                                        *
                                                                        * @param {URL} url - URL of the resource that is being fetched
                                                                        *
                                                                        * @fires Environments#importedEnvironmentFromURL
                                                                        */
function checkImportEnvironmentFromUrl(url) {
  url = decodeURIComponent(url);
  var urlVars = _utils_util__WEBPACK_IMPORTED_MODULE_4__["default"].getHashVars(url),
  i;
  _.each(urlVars, function (urlVar) {
    var key = decodeURIComponent(urlVar.key);
    if (key.indexOf('env[') == 0 && key[key.length - 1] == ']') {
      try {
        // if urlVar.value ends with ", trim it
        // for the extra trailing " in some cases
        var asciiVal = '';
        try {
          asciiVal = atob(urlVar.value);
        }
        catch (e) {
          if (_.endsWith(urlVar.value, '"')) {
            urlVar.value = urlVar.value.substring(0, urlVar.value.length - 1);
            asciiVal = atob(urlVar.value);
          } else
          {
            throw e;
          }
        }

        var name = key.substring(4, key.length - 1),
        envJson = JSON.parse(asciiVal),
        envArrayToUse = [];

        if (envJson instanceof Array) {
          var numVars = envJson.length;
          for (i = 0; i < numVars; i++) {
            envArrayToUse.push(_.assign({
              'type': 'text',
              'enabled': true },
            envJson[i]));
          }
        } else
        {
          // it's an object
          _.forOwn(envJson, function (value, key) {
            envArrayToUse.push({
              key: key,
              value: value,
              type: 'text',
              enabled: true });

          });
        }

        _modules_controllers_UserController__WEBPACK_IMPORTED_MODULE_5__["default"].
        get().
        then(user => {
          let createEnvironmentEvent = {
            name: 'create',
            namespace: 'environment',
            data: {
              id: _utils_util__WEBPACK_IMPORTED_MODULE_4__["default"].guid(),
              name: name,
              values: envArrayToUse,
              owner: user.id } };



          Object(_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_3__["default"])(createEnvironmentEvent);

          pm.toasts.success('Environment ' + name + ' imported');
        }).
        catch(e => {
          pm.logger.error('Error in creating evironment', e);
        });
      }
      catch (e) {
        console.log('Could not import environment');
      }
    }
  });
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6781:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return importCollectionAndEnvironment; });
/* harmony import */ var postman_collection_transformer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1910);
/* harmony import */ var postman_collection_transformer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(postman_collection_transformer__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_yaml__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5202);
/* harmony import */ var js_yaml__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_yaml__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _formats__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5199);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6779);
/* harmony import */ var _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1834);
/* harmony import */ var _utils_util__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1860);







/**
                                      * Handles Run in Postman imports.
                                      * Called when a getpostman.com/collection URL / postman:// protocol is clicked on. Can import collection + environment
                                      *
                                      * @param {URL} url - The URL that was used to import a collection / environment or both
                                      */
function importCollectionAndEnvironment(url) {
  _index__WEBPACK_IMPORTED_MODULE_3__["default"].importFileFromUrl(url, (err, data) => {
    if (err) {
      return pm.logger.error(err);
    }

    _index__WEBPACK_IMPORTED_MODULE_3__["default"].checkImportEnvironmentFromUrl(url);
    var urlVars = _utils_util__WEBPACK_IMPORTED_MODULE_5__["default"].getUrlVars(url),
    referrer = _.find(urlVars, function (v) {return v.key === 'referrer';}),
    referrerValue = referrer ? decodeURIComponent(referrer.value) : '';

    _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_4__["default"].addEvent('collection', 'create', 'run_button', null, {
      referrer: referrerValue,
      collection_id: data.id,
      collection_link_id: _utils_util__WEBPACK_IMPORTED_MODULE_5__["default"].getCollectionLinkId(url) });

  });
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6782:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return importFileFromUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseUrlData", function() { return parseUrlData; });
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(10);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _controllers_Importer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2915);
/* harmony import */ var _utils_util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1860);
/* harmony import */ var _utils_HttpService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2197);
/* harmony import */ var _modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(303);






/**
                                                                                         * Fetches resources at a given link and imports the resource by guessing the format.
                                                                                         * Called by the Import Modal (Import URL)
                                                                                         *
                                                                                         * @param {URL} url - URL of the resource
                                                                                         * @param {Function} callback - The function that will be called once the resource is imported
                                                                                         *
                                                                                         * @fires Mediator#failedCollectionImport
                                                                                         *
                                                                                         * @see Collections#importData
                                                                                         */
function importFileFromUrl(url, callback) {
  _utils_HttpService__WEBPACK_IMPORTED_MODULE_3__["default"].request(url).then(({ body }) => {
    try {
      _controllers_Importer__WEBPACK_IMPORTED_MODULE_1__["default"].importData(body, { origin: 'import/link', link: url }).
      then(() => {
        lodash__WEBPACK_IMPORTED_MODULE_0___default.a.isFunction(callback) && callback(null, body);
      });
    }
    catch (e) {
      if (e == 'Could not parse') {
        pm.mediator.trigger('failedCollectionImport', 'format not recognized');
        lodash__WEBPACK_IMPORTED_MODULE_0___default.a.isFunction(callback) && callback('format not recognized');
      } else
      {
        pm.mediator.trigger('failedCollectionImport', 'Could not import: ' + e);
        lodash__WEBPACK_IMPORTED_MODULE_0___default.a.isFunction(callback) && callback('Could not import: ' + e);
      }
    }
  }).catch(({ error }) => {
    console.log('Error response while importing: ', error);
    pm.mediator.trigger('failedCollectionImport', 'Error response received from URL. Check the console for more.');
    lodash__WEBPACK_IMPORTED_MODULE_0___default.a.isFunction(callback) && callback('Error response received from URL. Check the console for more.');
  });
}


/**
   * Fetches resource data at the given url
   * @param {String} url : url of the resource
   *
   * This function has been added to decouple fetching
   * the resource and importing data
   * In further refactoring, we can use this function
   * in the above importfileFromUrl function
   */
function parseUrlData(url) {
  if (window.SDK_PLATFORM === 'browser') {
    return _modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_4__["default"].request('/ws/proxy', {
      method: 'post',
      data: {
        path: '/api/v1/download',
        service: 'importer',
        method: 'post',
        body: { url } } }).


    then(({ body }) => {
      return body;
    }).
    catch(error => {
      pm.logger.warn('Error while parsing url data: ', error);
      pm.toasts.error('Error while fetching data from link.');
      return null;
    });
  }

  return _utils_HttpService__WEBPACK_IMPORTED_MODULE_3__["default"].request(url).then(({ body }) => {
    return body;
  }).catch(({ error }) => {
    pm.logger.warn('Error while parsing url data: ', error);
    pm.toasts.error('Error while fetching data from link.');
    return null;
  });
}

/***/ }),

/***/ 6783:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return importFiles; });
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(10);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _controllers_Importer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2915);
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(306);
/* harmony import */ var async__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(async__WEBPACK_IMPORTED_MODULE_2__);




/**
                            * Imports a list of files, guessing each file's format and importing as required.
                            * Called by the Import Modal (Import File)
                            *
                            * @param {File[]} files - The files to import
                            * @param {string} source - Source calling the function
                            * @param {Function} callback
                            *
                            * @fires Mediator#failedCollectionImport
                            *
                            * @see Collections#importData
                            *
                            * Since this function is being used at multiple places,
                            * added a parameter source to specify when called by
                            * the importByFileContainer
                            *
                            * Following are the components/sources calling the function -
                            * 1) ImportByFileContainer
                            * 2) ImportEnvironmentContainer
                            * 3) SettingsDataContainer
                            *
                            * The source value is right now only passed by the
                            * ImportByFileContainer component for its specific use case
                            * to return the callback
                            *
                            */
function importFiles(files, source, callback) {

  async__WEBPACK_IMPORTED_MODULE_2___default.a.eachSeries(files, (file, next) => {
    let reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = function (e) {
      var data = e.currentTarget.result;

      /**
                                          * Adding support logic for the case when one file is to be imported and
                                          * called by the importByFileContainer
                                          */
      if (source === 'importByFileContainer' && callback && files.length === 1) {
        return callback(data, { origin: 'import/file' });
      }
      _controllers_Importer__WEBPACK_IMPORTED_MODULE_1__["default"].importData(data, { origin: 'import/file' }, next);
    };

    // Read in the image file as a data URL.
    reader.readAsText(file);
  });
}

/***/ }),

/***/ 6784:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return saveEntity; });
/* harmony import */ var _models_services_filesystem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2398);
/* harmony import */ var _modules_export_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5493);
/* harmony import */ var _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1834);
/* harmony import */ var _export_all__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2394);





const SUPPORTED_FILE_TYPES = {
  json: 'application/json' },

TAB = '\t',
COLLECTION = 'collection',
ENVIRONMENT = 'environment',
GLOBALS = 'globals',
DEFAULT_FILE_NAME_EXT = '.json';

/**
                                  * Saves the exported entity to a file.
                                  * Will show a native prompt for the location to save the exported entity
                                  * @param {Object} criteria
                                  * @param {Object} options
                                  * @param {String} options.type
                                  * @param {String} options.inputVersion
                                  * @param {String} options.outputVersion
                                  */
function saveEntity(criteria, options = {}) {
  let model = options.type,
  exportVersion = _.get(options, 'outputVersion', '2.1.0'),
  exportedData,
  fileType,
  fileName;

  if (!model) {
    console.warn(`Unsupported type '${model}' for exporting`);
    return;
  }

  // This will fetch the entity from db, sanitize and transform it to the exportable format
  return _modules_export_index__WEBPACK_IMPORTED_MODULE_1__["default"].exportSingle(model, criteria, options)

  // check if the entity was exported correctly
  .then(data => {
    if (!(data && SUPPORTED_FILE_TYPES[data.type] && data[data.type])) {
      console.warn('Invalid data or unsupported file type', data);
      throw new Error('saveEntity: invalid data received');
    }

    return data;
  })

  // cache the data, file type and file name
  .then(data => {
    exportedData = data;
    fileType = SUPPORTED_FILE_TYPES[exportedData.type];
    fileName = _.get(exportedData, 'meta.fileName', model + DEFAULT_FILE_NAME_EXT);
  })

  // serialize the entity
  .then(() => {
    return JSON.stringify(exportedData.json, null, TAB);
  })

  // prompt the user to save the entity to the file system
  .then(serializedData => {
    return new Promise((resolve, reject) => {
      Object(_models_services_filesystem__WEBPACK_IMPORTED_MODULE_0__["saveAndOpenFile"])(fileName, serializedData, fileType, (err, state) => {
        if (err) {
          console.warn('Error while saving the exported entity', err);
          reject(err);
          return;
        }

        resolve(state);
      });
    });
  })

  // show the success notification and send analytics event
  .then(state => {
    if (state !== _export_all__WEBPACK_IMPORTED_MODULE_3__["EXPORT_STATE"].SUCCESS) {
      return;
    }
    let showToast = window.SDK_PLATFORM !== 'browser';

    switch (model) {
      case COLLECTION:
        _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_2__["default"].addEvent(COLLECTION, 'download', exportVersion, null, { collection_id: criteria.id });
        showToast && pm.toasts.success('Your collection was exported successfully.');
        break;

      case ENVIRONMENT:
        _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_2__["default"].addEvent(model, 'download');
        showToast && pm.toasts.success('Your environment was exported successfully.');
        break;

      case GLOBALS:
        _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_2__["default"].addEvent(model, 'download');
        showToast && pm.toasts.success('Your globals were exported successfully.');
        break;}

  }).

  catch(err => {
    pm.toasts.error('Unable to export this entity. Please check the DevTools.');
    console.warn('Error while exporting an entity', criteria, options, err);
  });
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);