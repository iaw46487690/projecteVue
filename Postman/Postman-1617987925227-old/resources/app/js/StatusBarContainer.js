(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[58],{

/***/ 15663:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return StatusBarContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _models_status_bar_StatusBar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(15664);
/* harmony import */ var _components_status_bar_plugins__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15666);
/* harmony import */ var _plugins_PluginInterface__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15665);
/* harmony import */ var _components_status_bar_base_Item__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15684);
/* harmony import */ var _components_status_bar_base_Text__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15685);
/* harmony import */ var _components_status_bar_base_Icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15686);
/* harmony import */ var _components_status_bar_base_Pane__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15687);
/* harmony import */ var _components_status_bar_base_Drawer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(15688);
/* harmony import */ var _constants_RequesterTabLayoutConstants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3009);
/* harmony import */ var _components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3011);
var _class;















let PluginEnvironment = {
  React: (react__WEBPACK_IMPORTED_MODULE_0___default()),
  PluginInterface: _plugins_PluginInterface__WEBPACK_IMPORTED_MODULE_4__["default"],
  StatusBarComponents: {
    Item: _components_status_bar_base_Item__WEBPACK_IMPORTED_MODULE_5__["default"],
    Text: _components_status_bar_base_Text__WEBPACK_IMPORTED_MODULE_6__["default"],
    Icon: _components_status_bar_base_Icon__WEBPACK_IMPORTED_MODULE_7__["default"],
    Pane: _components_status_bar_base_Pane__WEBPACK_IMPORTED_MODULE_8__["default"],
    Drawer: _components_status_bar_base_Drawer__WEBPACK_IMPORTED_MODULE_9__["default"] },

  constants: {
    layout: {
      REQUESTER_TAB_LAYOUT_1_COLUMN: _constants_RequesterTabLayoutConstants__WEBPACK_IMPORTED_MODULE_10__["REQUESTER_TAB_LAYOUT_1_COLUMN"],
      REQUESTER_TAB_LAYOUT_2_COLUMN: _constants_RequesterTabLayoutConstants__WEBPACK_IMPORTED_MODULE_10__["REQUESTER_TAB_LAYOUT_2_COLUMN"] } } };let





StatusBarContainer = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class StatusBarContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      activeItem: null };


    this.addItem = this.addItem.bind(this);
    this.addItems = this.addItems.bind(this);
    this.toggleActive = this.toggleActive.bind(this);
    this.items = null;
  }

  componentDidMount() {
    _models_status_bar_StatusBar__WEBPACK_IMPORTED_MODULE_2__["default"].initialize();
    _models_status_bar_StatusBar__WEBPACK_IMPORTED_MODULE_2__["default"].loadPlugins(_components_status_bar_plugins__WEBPACK_IMPORTED_MODULE_3__["default"]);
  }

  UNSAFE_componentWillMount() {
    _models_status_bar_StatusBar__WEBPACK_IMPORTED_MODULE_2__["default"].on('loadedPlugins', this.addItems);
    _models_status_bar_StatusBar__WEBPACK_IMPORTED_MODULE_2__["default"].on('add', this.addItem);
  }

  componentWillUnmount() {
    _models_status_bar_StatusBar__WEBPACK_IMPORTED_MODULE_2__["default"].off('loadedPlugins', this.addItems);
    _models_status_bar_StatusBar__WEBPACK_IMPORTED_MODULE_2__["default"].off('add', this.addItem);
    this.items = null;
  }

  addItem(item) {
    this.items.push({
      item: item,
      component: item.getComponent(PluginEnvironment) });

    let items = _.concat(this.state.items, item);
    this.setState({ items });
  }

  addItems(items) {
    this.items = _.map(_components_status_bar_plugins__WEBPACK_IMPORTED_MODULE_3__["default"], item => {
      return {
        item: item,
        component: item.getComponent(PluginEnvironment) };

    });
    this.setState({ items });
  }

  toggleActive(item) {
    this.setState({ activeItem: this.state.activeItem === item ? null : item });
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_11__["default"], { identifier: 'statusBar' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'status-bar-container status-bar' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'sb-section' },

            _.map(this.items, (item, index) => {
              return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(item.component, {
                key: index,
                isOpen: _.isEqual(this.state.activeItem, item.item.name),
                toggleActive: this.toggleActive.bind(this, item.item.name) });

            })))));





  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15664:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(374);
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _plugins_PluginInterface__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15665);

let

StatusBar = class StatusBar extends events__WEBPACK_IMPORTED_MODULE_0___default.a {
  constructor() {
    super();
  }

  initialize() {
    _plugins_PluginInterface__WEBPACK_IMPORTED_MODULE_1__["default"].initialize();
  }

  register(property, handler, context) {
    this.properties[property] &&
    this.properties[property].handler(context, handler);
  }

  loadPlugins(plugins) {
    this.emit('loadedPlugins', plugins);
  }

  addItem(sbItem) {
    this.emit('add', sbItem);
    sbItem.initialize && sbItem.initialize();
  }};


/* harmony default export */ __webpack_exports__["default"] = (new StatusBar());

/***/ }),

/***/ 15665:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2690);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1835);
/* harmony import */ var _services_EditorService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2647);
/* harmony import */ var _external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(318);
/* harmony import */ var _constants_RequesterBottomPaneConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2898);
/* harmony import */ var _services_NavigationService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(69);








let

PluginInterface = class PluginInterface {
  initialize() {
    this.properties = {
      layout: {
        value: Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ConfigurationStore').get('editor.requestEditorLayoutName'),
        registerer: function (context, cb) {
          Object(mobx__WEBPACK_IMPORTED_MODULE_0__["reaction"])(() => Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ConfigurationStore').get('editor.requestEditorLayoutName'), cb.bind(context));
        } },

      theme: {
        value: pm.settings.getSetting('postmanTheme') || 'light',
        registerer: function (context, cb) {
          pm.settings.on('setSetting:postmanTheme', cb, context);
        } },

      platform: {
        value: navigator.platform,
        registerer: _.noop },

      windows: {
        value: [],
        registerer: function (context, cb) {
          pm.appWindow.on('windowClosed', cb.bind(context, 'windowClosed'), context);
        } },

      modals: {
        value: null,
        registerer: function (context, cb) {
          pm.mediator.on('modalOpened', cb.bind(context, 'modalOpened'), context);
          pm.mediator.on('modalClosed', cb.bind(context, 'modalClosed'), context);
        } },

      xFlows: {
        value: null,
        registerer: function (context, cb) {
          pm.mediator.on('saveXFlowActivity', cb.bind(context), context);
        } } };


  }

  register(property, handler, context) {
    this.properties[property] &&
    this.properties[property].registerer(context, handler);
  }

  get(key) {
    return _.get(this, `properties[${key}].value`);
  }

  openWindow(windowType) {
    switch (windowType) {
      case 'requester':
        pm.mediator.trigger('newRequesterWindow');
        break;
      case 'console':
        pm.mediator.trigger('newConsoleWindow');
        break;
      default:
        break;}

  }

  toggleTwoPaneLayout() {
    pm.app.toggleLayout();
  }

  openURL(url) {
    Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_4__["openExternalLink"])(url);
  }

  openModal(modalName, options) {
    switch (modalName) {
      case 'settings':
        // open the settings modal via navigation
        pm.mediator.trigger('openSettingsModal', options.tab);
        break;
      case 'release-notes':
        _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].open('customview://releaseNotes');
        break;
      case 'x-flow-activity-feed':
        pm.mediator.trigger('openXFlowActivityFeed');
        break;
      default:
        break;}

  }

  toggleSidebar() {
    pm.mediator.trigger('toggleSidebar');
  }

  toggleFindReplace() {
    const store = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('RequesterBottomPaneUIStore');

    store && store.toggleTab(_constants_RequesterBottomPaneConstants__WEBPACK_IMPORTED_MODULE_5__["REQUESTER_BOTTOM_PAME_FIND_REPLACE"]);
  }

  toggleConsole() {
    const store = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('RequesterBottomPaneUIStore');

    store && store.toggleTab(_constants_RequesterBottomPaneConstants__WEBPACK_IMPORTED_MODULE_5__["REQUESTER_BOTTOM_PANE_CONSOLE"]);
  }};


/* harmony default export */ __webpack_exports__["default"] = (new PluginInterface());
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15666:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Help__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15667);
/* harmony import */ var _onboarding_src_features_Skills_Bootcamp_components_BootcampLauncher__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15668);
/* harmony import */ var _AgentSelection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(15669);
/* harmony import */ var _BrowserActiveTrackSelection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15671);
/* harmony import */ var _Trash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15682);
/* harmony import */ var _Runner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15689);
/* harmony import */ var _TwoPane__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15690);
/* harmony import */ var _Copyright__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15693);









let pluginsToAdd = [
_Help__WEBPACK_IMPORTED_MODULE_0__["default"],
_TwoPane__WEBPACK_IMPORTED_MODULE_6__["default"],
_Trash__WEBPACK_IMPORTED_MODULE_4__["default"],
_Runner__WEBPACK_IMPORTED_MODULE_5__["default"],
_AgentSelection__WEBPACK_IMPORTED_MODULE_2__["default"],
_onboarding_src_features_Skills_Bootcamp_components_BootcampLauncher__WEBPACK_IMPORTED_MODULE_1__["default"],
_Copyright__WEBPACK_IMPORTED_MODULE_7__["default"]];


if (window.SDK_PLATFORM === 'browser' && (window.RELEASE_CHANNEL === 'dev' || window.RELEASE_CHANNEL === 'beta')) {
  pluginsToAdd.push(_BrowserActiveTrackSelection__WEBPACK_IMPORTED_MODULE_3__["default"]);
}

/* harmony default export */ __webpack_exports__["default"] = (pluginsToAdd);

/***/ }),

/***/ 15667:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2690);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Help',
  position: 'right',

  getComponent: function ({
    React,
    PluginInterface,
    StatusBarComponents })
  {let
    Help = class Help extends React.Component {
      constructor(props) {
        super(props);
      }

      handleItemSelect(item) {
        switch (item) {
          case 'releases':
            PluginInterface.openModal('release-notes');
            break;
          case 'shortcuts':
            PluginInterface.openModal('settings', { tab: 'shortcuts' });
            break;
          case 'docs':
            PluginInterface.openURL(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_1__["DOCS_URL"]);
            break;
          case 'security':
            PluginInterface.openURL(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_1__["DOCS_SECURITY_URL"]);
            break;
          case 'support':
            PluginInterface.openURL(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_1__["SUPPORT_URL"]);
            break;
          case 'twitter':
            PluginInterface.openURL(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_1__["TWITTER_URL"]);
            break;
          case 'community':
            PluginInterface.openURL(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_1__["POSTMAN_COMMUNITY"]);
            break;
          default:
            break;}

      }

      getIcon() {
        return (
          React.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_0__["Icon"], { name: 'icon-state-help-stroke', size: 'small' }));

      }

      render() {
        let { Item, Icon, Drawer } = StatusBarComponents;

        return (
          React.createElement(Item, _extends({
              className: 'plugin__help',
              tooltip: 'Help & Feedback' },
            this.props),

            React.createElement(Drawer, {
              className: 'plugin__help__drawer',
              button: () => {
                return (
                  React.createElement(Icon, {
                    className: 'plugin__help__icon',
                    icon: this.getIcon() }));


              },
              onSelect: this.handleItemSelect,
              items: [
              ...(window.SDK_PLATFORM !== 'browser' ? [{
                key: 'releases',
                label: 'Release Notes' }] :
              []),
              {
                key: 'docs',
                label: 'Documentation' },

              {
                key: 'security',
                label: 'Security' },

              {
                key: 'support',
                label: 'Support' },

              {
                key: 'twitter',
                label: '@getpostman' },

              {
                key: 'community',
                label: 'Community' },

              {
                key: 'shortcuts',
                label: 'Keyboard Shortcuts' }] })));





      }};


    return Help;
  } });

/***/ }),

/***/ 15668:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _LessonConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3689);
/* harmony import */ var _common_apis_APIService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2665);
/* harmony import */ var _LessonController__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3653);
/* harmony import */ var _common_dependencies__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2646);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3011);
/* harmony import */ var _js_components_base_Icons_CloseIcon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3087);













const defaultState = {
  dismissable: false,
  currentState: null,
  label: 'Bootcamp',
  pausedLessonId: null },

HIGHLIGHT_STATE = 'highlight',
SUCCESS_STATE = 'success';

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'learningCenter',
  position: 'right',

  getComponent: function ({
    React,
    StatusBarComponents })
  {
    return class LearningCenter extends React.Component {
      constructor(props) {
        super(props);
        this.state = defaultState;

        this.handleReset = this.handleReset.bind(this);
        this.handleContinue = this.handleContinue.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleHighlightLessonButton = this.handleHighlightLessonButton.bind(this);
        _common_dependencies__WEBPACK_IMPORTED_MODULE_5__["UIEventService"].subscribe(_LessonConstants__WEBPACK_IMPORTED_MODULE_2__["LESSON_PAUSED"], this.handleContinue);
        _common_dependencies__WEBPACK_IMPORTED_MODULE_5__["UIEventService"].subscribe('highlightLessonButton', this.handleHighlightLessonButton);
        _common_dependencies__WEBPACK_IMPORTED_MODULE_5__["UIEventService"].subscribe(_common_dependencies__WEBPACK_IMPORTED_MODULE_5__["OPEN_LESSON_PLAN"], this.openBootcampTab);
      }

      openBootcampTab() {
        _common_dependencies__WEBPACK_IMPORTED_MODULE_5__["NavigationService"].transitionTo('bootcamp');
      }

      continuePausedLesson() {
        Object(_common_apis_APIService__WEBPACK_IMPORTED_MODULE_3__["fetchLesson"])(this.state.pausedLessonId, lesson => {
          if (_.isEmpty(lesson)) {
            return;
          }
          _LessonController__WEBPACK_IMPORTED_MODULE_4__["default"].continueLesson(lesson);
        });
        this.handleReset();
      }

      handleClick() {
        if (this.state.pausedLessonId) {
          this.continuePausedLesson();
          return;
        }
        let currentUser = Object(_common_dependencies__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore'),
        isLoggedIn = currentUser && currentUser.isLoggedIn;

        if (!isLoggedIn) {
          pm.mediator.trigger('showSignInModal', {
            title: 'Improve your skills in Postman',
            subtitle: 'Learn about different features inside Postman and improve your skills through an interactive tutorial. Please create an account to continue.',
            renderIcon: this.renderIcon,
            onAuthentication: this.openBootcampTab,
            origin: 'learning_center' });

          return;
        }

        this.openBootcampTab();
        this.handleReset();
      }

      renderIcon() {
        return React.createElement('div', { className: 'learning-center-empty-state-icon' });
      }

      handleReset(e) {
        e && e.stopPropagation();
        this.setState(defaultState);
      }

      handleContinue(pausedLessonId) {
        this.setState({
          dismissable: true,
          currentState: HIGHLIGHT_STATE,
          label: 'Continue learning',
          pausedLessonId });

      }

      handleHighlightLessonButton() {
        this.setState({
          currentState: HIGHLIGHT_STATE,
          label: 'Bootcamp' });

      }

      render() {

        if (pm.isScratchpad) {
          return null;
        }

        let { Item, Text } = StatusBarComponents;

        return (
          React.createElement(Item, {
              className: 'plugin__learningCenter',
              tooltip: 'Learning Center' },

            React.createElement(Text, {
              render: () => {
                return (
                  React.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_6__["default"], { identifier: 'learningCenter' },
                    React.createElement('div', {
                        className: classnames__WEBPACK_IMPORTED_MODULE_0___default()('learning-center-button', this.state.currentState),
                        onClick: this.handleClick },

                      React.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], { name: 'icon-descriptive-bootcamp-stroke', size: 'small' }),
                      React.createElement('span', { className:
                          classnames__WEBPACK_IMPORTED_MODULE_0___default()(
                          { 'dismissable': this.state.dismissable },
                          'learning-center-label') },


                        this.state.label),


                      this.state.dismissable &&
                      React.createElement(_js_components_base_Icons_CloseIcon__WEBPACK_IMPORTED_MODULE_7__["default"], { size: 'xs', onClick: this.handleReset }))));




              } })));



      }};

  } });
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15669:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_base_Icons_RunIcon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5495);
/* harmony import */ var _components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3011);
/* harmony import */ var _base_Tooltips__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3017);
/* harmony import */ var _runtime_components_agent_AgentSelectionContainer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15670);
/* harmony import */ var _runtime_constants_AgentConstants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3118);












/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'AgentSelection',
  position: 'right',

  getComponent: function ({
    React,
    PluginInterface,
    StatusBarComponents })
  {var _class;
    return Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class AgentSelection extends React.Component {
      constructor(props) {
        super(props);

        this.state = {
          isOpen: false };


        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleClick = this.handleClick.bind(this);
      }

      handleOpen() {
        this.setState({ isOpen: true });
      }

      handleClose() {
        this.setState({ isOpen: false });
      }

      handleClick() {
        this.setState(({ isOpen }) => ({ isOpen: !isOpen }));
      }

      UNSAFE_componentWillMount() {
        pm.mediator.on('showAgentSelectionPopover', this.handleOpen);
      }

      componentWillUnmount() {
        pm.mediator.off('showAgentSelectionPopover', this.handleOpen);
      }

      render() {
        if (!(window.SDK_PLATFORM === 'browser')) {
          return null;
        }

        let { Item, Text } = StatusBarComponents;
        const { type: agentType } = pm.runtime.agent.stat,
        agentTypeName = _runtime_constants_AgentConstants__WEBPACK_IMPORTED_MODULE_8__["FRIENDLY_TYPES"][agentType],
        hasError = !pm.runtime.agent.isReady,
        agentLabel = pm.runtime.agent.autoMode ?
        'Auto-select agent' :
        _runtime_constants_AgentConstants__WEBPACK_IMPORTED_MODULE_8__["SETTING_VALUES"][agentTypeName.toUpperCase()].label + ' Agent';

        return (
          React.createElement(_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_5__["default"], { identifier: 'runtime-agent' },
            React.createElement('div', { ref: ref => {this.containerRef = ref;} },
              React.createElement(Item, {
                  className: 'plugin__agent-selection-shortcut' },

                React.createElement(Text, {
                  render: () => {
                    return (
                      React.createElement('div', {
                          className: 'agent-selection-icon',
                          onClick: this.handleClick },

                        hasError ?
                        React.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_3__["Icon"], {
                          className: 'agent-selection-icon-error',
                          name: 'icon-state-error-stroke',
                          color: 'content-color-error',
                          size: 'small' }) :


                        React.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_3__["Icon"], {
                          className: 'agent-selection-icon-success',
                          name: 'icon-state-success-stroke',
                          color: 'content-color-success',
                          size: 'small' }),


                        React.createElement('span', { className: 'agent-selection-label' },
                          agentLabel)));



                  } })),


              React.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_6__["Tooltip"], {
                  className: `agent-selection-details__${pm.runtime.agent.autoMode ? _runtime_constants_AgentConstants__WEBPACK_IMPORTED_MODULE_8__["AUTOMATIC_SELECTION_SETTING_KEY"] : agentTypeName}`,
                  show: this.state.isOpen,
                  target: this.containerRef,
                  placement: 'top-right',
                  immediate: true },

                React.createElement(_runtime_components_agent_AgentSelectionContainer__WEBPACK_IMPORTED_MODULE_7__["default"], { onClose: this.handleClose })))));





      }}) || _class;

  } });

/***/ }),

/***/ 15670:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AgentSelectionContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(80);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3018);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _base_atom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3065);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3119);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1835);
/* harmony import */ var _js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(318);
/* harmony import */ var _constants_AgentConstants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3118);
/* harmony import */ var _js_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2690);
/* harmony import */ var _js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3594);
var _class;














let



AgentSelectionContainer = _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4___default()(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class AgentSelectionContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleAgentSelect = this.handleAgentSelect.bind(this);
    this.handleAgentOnboardingOpen = this.handleAgentOnboardingOpen.bind(this);
    this.handleLinkClick = this.handleLinkClick.bind(this);
    this.handleDesktopAgentLinkCLick = this.handleDesktopAgentLinkCLick.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.handleAutomaticSelectToggle = this.handleAutomaticSelectToggle.bind(this);
  }

  getDescriptionForDesktopAgent(isSafari, isCloudAgentEnabled) {
    if (isSafari) {
      return 'Safari doesn\'t support the desktop agent. Try switching browsers or using the browser agent instead.';
    }

    if (!isCloudAgentEnabled) {
      return 'The Desktop Agent allows you to send requests without any hiccups.';
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, 'Send requests via the locally running Postman',

        ' ',
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'details__link', onClick: this.handleDesktopAgentLinkCLick }, 'Desktop Agent'), '.'));



  }

  handleClickOutside(e) {
    e && e.stopPropagation();
    this.props.onClose();
  }

  handleLinkClick() {
    Object(_js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_9__["openExternalLink"])(_js_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_11__["POSTMAN_AGENTS"], '_blank');
  }

  handleDesktopAgentLinkCLick() {
    Object(_js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_9__["openExternalLink"])(_js_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_11__["POSTMAN_DESKTOP_AGENT_DOCS"], '_blank');
  }

  handleAgentOnboardingOpen() {
    this.props.onClose();
    _base_molecule__WEBPACK_IMPORTED_MODULE_7__["openAgentOnboardingModal"] && Object(_base_molecule__WEBPACK_IMPORTED_MODULE_7__["openAgentOnboardingModal"])(true);
  }

  handleAutomaticSelectToggle() {
    pm.settings.setSetting(_constants_AgentConstants__WEBPACK_IMPORTED_MODULE_10__["AUTOMATIC_SELECTION_SETTING_KEY"], !_.get(pm.runtime, 'agent.autoMode'));
  }

  handleAgentSelect(option) {
    if (_.get(pm.runtime, 'agent.autoMode')) {
      return;
    }

    pm.settings.setSetting(_constants_AgentConstants__WEBPACK_IMPORTED_MODULE_10__["SETTING_KEY"], option);
  }

  renderContent(selection, hasError) {
    const isCloudAgentEnabled = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('FeatureFlagsStore').isEnabled('runtime:isCloudAgentEnabled'),
    isSafari = Object(_js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_12__["getBrowser"])() === 'safari',
    agents = [
    ...(isCloudAgentEnabled ? [{
      label: 'Cloud Agent',
      description:
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, 'Send HTTP requests via Postman\u2019s secure cloud servers.'),



      key: 'cloud' }] :
    []),
    {
      label:
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, 'Desktop Agent',

        isSafari && ' (Unsupported)'),


      description:
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
        this.getDescriptionForDesktopAgent(isSafari, isCloudAgentEnabled),
        hasError &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_atom__WEBPACK_IMPORTED_MODULE_6__["Button"], {
            className: 'agent-selection-details__confirm',
            type: 'primary',
            onClick: this.handleAgentOnboardingOpen }, 'Download desktop agent')),






      key: 'desktop' },

    {
      label: 'Browser Agent',
      description:
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, 'Sending your requests through your browser comes with',

        ' ',
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'details__link', onClick: this.handleLinkClick }, 'limitations'), '.'),



      key: 'browser' }],


    AgentInfo = ({
      label, description, active, onClick, unsupported, disabled }) =>

    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
        onClick: onClick,
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()({
          'agent-selection-details__wrapper': true, active, unsupported, disabled }) },


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_2___default()({
          'agent-selection-item__radio': true, active, unsupported, disabled }) }),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'agent-selection-item__info' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_2___default()({ 'agent-selection-item__label': true, unsupported, disabled }) },
          label),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_2___default()({ 'agent-selection-item__description': true, unsupported, disabled }) },
          description)));





    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
        agents.map(agent =>
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AgentInfo, {
          key: agent.key,
          unsupported: isSafari && agent.key === 'desktop',
          active: selection === agent.key,
          label: agent.label,
          description: agent.description,
          addDividerOnTop: agent.addDividerOnTop,
          onClick: () => this.handleAgentSelect(agent.key),
          disabled: _.get(pm.runtime, 'agent.autoMode') }))));




  }


  render() {
    const agentTypeName = _.get(pm.runtime, 'agent.autoMode') ? _constants_AgentConstants__WEBPACK_IMPORTED_MODULE_10__["AUTOMATIC_SELECTION_SETTING_KEY"] : _constants_AgentConstants__WEBPACK_IMPORTED_MODULE_10__["FRIENDLY_TYPES"][_.get(pm.runtime.agent, 'stat.type')],
    hasError = !pm.runtime.agent.isReady;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: `agent-selection-details__${agentTypeName}` },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'details__header' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Text"], { type: 'label-primary-medium' }, 'Select Postman Agent')),



        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'agent-selection-details__wrapper auto-select' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'agent-selection-item__info' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'agent-selection-item__label' }, 'Auto-select'),


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'agent-selection-item__description' }, 'Postman will automatically select the best agent for your request.')),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_atom__WEBPACK_IMPORTED_MODULE_6__["ToggleSwitch"], {
            isActive: agentTypeName === _constants_AgentConstants__WEBPACK_IMPORTED_MODULE_10__["AUTOMATIC_SELECTION_SETTING_KEY"],
            onClick: this.handleAutomaticSelectToggle,
            activeLabel: '',
            inactiveLabel: '' })),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'agent-selection-details__divider' }),
        this.renderContent(agentTypeName, hasError)));


  }}) || _class) || _class;


AgentSelectionContainer.defaultProps = {
  onClose: () => {} };


AgentSelectionContainer.propTypes = {
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15671:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_BrowserActiveTrackService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15672);



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'BrowserActiveTrackSelection',
  position: 'right',

  getComponent: function ({
    React,
    PluginInterface,
    StatusBarComponents })
  {
    return class BrowserActiveTrackSelection extends React.Component {
      constructor(props) {
        super(props);

        this.state = {
          activeTrack: _services_BrowserActiveTrackService__WEBPACK_IMPORTED_MODULE_1__["default"].getCurrentTrack() };

      }

      handleClick() {
        PluginInterface.openModal('settings', { tab: 'devOptions' });
      }

      render() {
        let { Item, Text } = StatusBarComponents;

        return (
          React.createElement(Item, {
              className: 'plugin__activeTrack',
              tooltip: 'Switch Active Track' },

            React.createElement(Text, {
              render: () => {
                return (
                  React.createElement('div', {
                      className: 'active-track-button',
                      onClick: this.handleClick },

                    React.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_0__["Icon"], { name: 'icon-action-tag-stroke', size: 'small' }),
                    React.createElement('span', { className: 'active-track-label' },
                      this.state.activeTrack)));



              } })));



      }};

  } });

/***/ }),

/***/ 15682:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _version_control_trash_components_TrashLabel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15683);



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Trash',
  position: 'right',
  getComponent({
    React,
    StatusBarComponents })
  {
    return _version_control_trash_components_TrashLabel_js__WEBPACK_IMPORTED_MODULE_1__["default"];
  } });

/***/ }),

/***/ 15683:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TrashLabel; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(82);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3011);
/* harmony import */ var _appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3044);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1835);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4913);
/* harmony import */ var _js_components_status_bar_base_Item__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15684);
/* harmony import */ var _js_components_status_bar_base_Text__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(15685);
var _class;











const TrashWrapper = Object(styled_components__WEBPACK_IMPORTED_MODULE_2__["default"])(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_5__["default"])`
  cursor: ${props => props.disabled ? 'not-allowed' : 'pointer'};
  opacity: ${props => props.disabled && '0.5'};
  pointer-events: ${props => props.disabled && 'none'};
`;let


TrashLabel = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class TrashLabel extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    const isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('SyncStatusStore').isSocketConnected;

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').isLoggedIn) {
      return null;
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(TrashWrapper, {
          to: {
            routeIdentifier: 'trash' },

          disabled: isOffline },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__["default"], { identifier: 'trash' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_status_bar_base_Item__WEBPACK_IMPORTED_MODULE_8__["default"], { tooltip: 'Trash' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_status_bar_base_Text__WEBPACK_IMPORTED_MODULE_9__["default"], {
              render: () => {
                return (
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common__WEBPACK_IMPORTED_MODULE_7__["CustomTooltip"], {
                      align: 'top',
                      body: isOffline && 'You can perform this action once you\'re back online' },

                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                        className: 'trash-label-button' },

                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'trash-label-text' }, 'Trash'),


                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
                        name: 'icon-action-delete-stroke',
                        size: 'small' }))));




              } })))));





  }}) || _class;

/***/ }),

/***/ 15684:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Item; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Text__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(15685);
/* harmony import */ var _Icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15686);
/* harmony import */ var _Pane__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15687);
/* harmony import */ var _Drawer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15688);
/* harmony import */ var _base_Tooltips__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3017);







let

Item = class Item extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = { showTooltip: false };

    this.handleToggleTooltip = this.handleToggleTooltip.bind(this);
  }

  handleToggleTooltip(value = !this.state.showTooltip) {
    if (this.props.isOpen && value) {
      // Do not show the tooltip if the status bar / pane is open
      this.setState({ showTooltip: false });
      return;
    }

    this.setState({ showTooltip: value });
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'sb__item': true,
      'is-disabled': this.props.isDisabled,
      'is-active': this.props.isDisabled && this.props.isOpen },
    this.props.className);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          ref: 'item',
          className: this.getClasses() },


        react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.map(this.props.children, child => {
          if (child.type === _Icon__WEBPACK_IMPORTED_MODULE_3__["default"]) {
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.cloneElement(child, {
              onMouseEnter: this.handleToggleTooltip.bind(this, true),
              onMouseLeave: this.handleToggleTooltip.bind(this, false) });

          } else
          if (child.type === _Text__WEBPACK_IMPORTED_MODULE_2__["default"]) {
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.cloneElement(child);
          } else
          if (child.type === _Pane__WEBPACK_IMPORTED_MODULE_4__["default"]) {
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.cloneElement(child, {
              isOpen: this.props.isOpen,
              onClose: this.props.toggleActive });

          } else
          if (child.type === _Drawer__WEBPACK_IMPORTED_MODULE_5__["default"]) {
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.cloneElement(child, {
              isOpen: this.props.isOpen,
              onClose: this.props.toggleActive });

          } else
          {
            throw new Error('Invalid child type, must be Icon, Text, Drawer or Pane');
          }
        }),


        this.props.tooltip &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_6__["Tooltip"], {
            show: this.state.showTooltip,
            target: this.refs.item,
            placement: 'top' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_6__["TooltipBody"], null,
            this.props.tooltip))));





  }};

/***/ }),

/***/ 15685:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Text; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);

let

Text = class Text extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({ 'sb__item__text': true }, this.props.className);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getClasses() },
        this.props.render && this.props.render()));


  }};

/***/ }),

/***/ 15686:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Icon; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);

let

Icon = class Icon extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({ 'sb__item__icon': true }, this.props.className);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: this.getClasses(),
          onClick: this.props.onClick,
          onMouseEnter: this.props.onMouseEnter,
          onMouseLeave: this.props.onMouseLeave },

        this.props.icon));


  }};

/***/ }),

/***/ 15687:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Pane; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_react_draggable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3015);
/* harmony import */ var _postman_react_draggable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_react_draggable__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _base_Icons_CloseIcon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3087);



let


Pane = class Pane extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.state = { paneHeight: this.props.paneHeight || 200 };
    this.handleStart = this.handleStart.bind(this);
    this.handleDrag = this.handleDrag.bind(this);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen && nextProps.isOpen !== this.props.isOpen) {
      let documentHeight = _.get(document, 'body.offsetHeight', 800);
      if (documentHeight - this.state.paneHeight < 100) {
        this.setState({ paneHeight: documentHeight - 100 });
      }
    }
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'sb__item__pane': true,
      'is-hidden': !this.props.isOpen },
    this.props.className);
  }

  handleStart(event, data) {
    this.paneHeight = this.state.paneHeight;
    this.startClientY = data.y;
  }

  handleDrag(event, data) {
    let clientY = data.y,
    paneHeight = this.paneHeight + (this.startClientY - clientY),
    documentHeight = _.get(document, 'body.offsetHeight', 800);

    if (documentHeight - paneHeight < 100) {
      paneHeight = this.state.paneHeight;
    }

    if (paneHeight < 100) {
      paneHeight = 100;
    }

    this.setState({ paneHeight: paneHeight });
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_react_draggable__WEBPACK_IMPORTED_MODULE_2__["DraggableCore"], {
          axis: 'y',
          handle: '.plugin__pane-resize-wrapper',
          onStart: this.handleStart,
          onDrag: this.handleDrag },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: this.getClasses(),
            style: { 'height': this.state.paneHeight } },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'plugin__pane-resize-wrapper' }),
          this.props.children,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Icons_CloseIcon__WEBPACK_IMPORTED_MODULE_3__["default"], {
            className: 'plugin__pane-close',
            onClick: this.props.onClose }))));




  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Drawer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_Dropdowns__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3055);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};

let

Drawer = class Drawer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(item) {
    this.props.onSelect && this.props.onSelect(item);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({ 'sb__drawer': true }, this.props.className);
  }

  getItemProps(defaultArgs, props = {}) {
    return _extends({},
    defaultArgs,
    props, {
      className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(defaultArgs.className, props.className || '') });

  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getClasses() },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], { onSelect: this.handleSelect },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_1__["DropdownButton"], null,
            this.props.button && this.props.button()),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_1__["DropdownMenu"], { className: 'help-plugin-dropdown-menu', 'align-right': true },

            _.map(this.props.items, item => {
              return (
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_1__["MenuItem"], {
                    key: item.key,
                    refKey: item.key },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, item.label)));


            })))));





  }};


Drawer.defaultProps = {
  itemRenderer: (item, getItemProps) => {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', getItemProps(),
        item.label));


  } };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3011);
/* harmony import */ var _runtime_api_RunnerInterface__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2694);






/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Runner',
  position: 'right',
  getComponent({
    React,
    StatusBarComponents })
  {
    return class Runner extends React.Component {
      render() {
        const { Item, Text } = StatusBarComponents;

        return (
          React.createElement(_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__["default"], { identifier: 'runner' },
            React.createElement(Item, null,
              React.createElement(Text, {
                render: () => {
                  return (
                    React.createElement('div', {
                        className: 'runner-label-button',
                        onClick: () => Object(_runtime_api_RunnerInterface__WEBPACK_IMPORTED_MODULE_3__["openRunnerTab"])({}, { preview: false }) },

                      React.createElement('span', { className: 'runner-label-text' }, 'Runner'),


                      React.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
                        name: 'icon-action-run-stroke',
                        size: 'small' })));



                } }))));




      }};

  } });

/***/ }),

/***/ 15690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var _components_base_Icons_TwoPaneIcon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15691);
/* harmony import */ var _components_base_Icons_SinglePaneIcon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15692);



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'TwoPane',
  position: 'right',
  getComponent: function ({
    React,
    PluginInterface,
    StatusBarComponents,
    constants })
  {
    return class TwoPane extends React.Component {
      constructor(props) {
        super(props);
        this.state = { layout: PluginInterface.get('layout') };
      }

      UNSAFE_componentWillMount() {
        PluginInterface.register('layout', this.handleLayout, this);
      }

      handleLayout(payload) {
        this.setState({ layout: payload });
      }

      handleClick() {
        PluginInterface.toggleTwoPaneLayout();
      }

      getShortcut() {
        let platform = PluginInterface.get('platform');
        if (_.includes(platform, 'Mac')) {
          return '⌥⌘V';
        } else
        {
          return 'Ctrl + Alt + V';
        }
      }

      getTooltipContent(isTwoPane) {
        // when  Pane view switcher is relevant for the current Tab
        return `${isTwoPane ? 'Single pane view' : 'Two pane view'} (${this.getShortcut()})`;
      }

      getIcon() {
        let activeTheme = PluginInterface.get('theme'),
        layout = this.state.layout,
        { REQUESTER_TAB_LAYOUT_1_COLUMN, REQUESTER_TAB_LAYOUT_2_COLUMN } = constants.layout;

        if (_.isEqual(layout, REQUESTER_TAB_LAYOUT_2_COLUMN)) {
          return (
            React.createElement(_components_base_Icons_SinglePaneIcon__WEBPACK_IMPORTED_MODULE_1__["default"], { size: 'xs' }));

        } else
        {
          return (
            React.createElement(_components_base_Icons_TwoPaneIcon__WEBPACK_IMPORTED_MODULE_0__["default"], { size: 'xs' }));

        }
      }

      render() {
        const { Item, Icon } = StatusBarComponents,
        { REQUESTER_TAB_LAYOUT_2_COLUMN } = constants.layout,
        isTwoPane = this.state.layout === REQUESTER_TAB_LAYOUT_2_COLUMN,
        tooltipContent = this.getTooltipContent(isTwoPane);

        return (
          React.createElement(Item, {
              className: `plugin__layout ${isTwoPane ? 'single-pane' : 'two-pane'}`,
              tooltip: tooltipContent },

            React.createElement(Icon, {
              onClick: this.handleClick,
              icon: this.getIcon() })));



      }};

  } });
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TwoPaneIcon; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Icon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(316);
/* harmony import */ var _XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3011);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};



const icon =
react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('svg', { width: '16', height: '16', viewBox: '0 0 16 16' },
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('defs', null,
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('path', { id: 'two-pane', d: 'M7.5 1H1v14h6.521a.974.974 0 0 1-.021-.205V1zm1 0v13.795c0 .072-.007.14-.021.205H15V1H8.5zM15 0a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1a1 1 0 0 1 1-1h14zM2.25 10V6h4v4h-4zm9.5 0a2 2 0 1 1 0-4 2 2 0 0 1 0 4z' })),

  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('use', { fill: 'gray', fillRule: 'evenodd', xlinkHref: '#two-pane' }));



function TwoPaneIcon(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__["default"], { identifier: 'layout' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Icon__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({},
      props, {
        icon: icon }))));



}

/***/ }),

/***/ 15692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SinglePaneIcon; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Icon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(316);
/* harmony import */ var _XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3011);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};



const icon =
react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('svg', { width: '16', height: '16', viewBox: '0 0 16 16' },
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('defs', null,
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('path', { id: 'single-pane', d: 'M7.5 1H1v14h6.521a.974.974 0 0 1-.021-.205V1zm1 0v13.795c0 .072-.007.14-.021.205H15V1H8.5zM15 0a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1a1 1 0 0 1 1-1h14zM2.25 10V6h4v4h-4zm9.5 0a2 2 0 1 1 0-4 2 2 0 0 1 0 4z' })),

  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('use', { fill: 'gray', fillRule: 'evenodd', transform: 'rotate(90 8 8)', xlinkHref: '#single-pane' }));



function SinglePaneIcon(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__["default"], { identifier: 'layout' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Icon__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({},
      props, {
        icon: icon }))));



}

/***/ }),

/***/ 15693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3011);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1835);
/* harmony import */ var _constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2690);
/* harmony import */ var _constants_WorkspaceVisibilityConstants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2180);
/* harmony import */ var _external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(318);







const PRIVACY_POLICY = 'privacy-policy',
TERMS = 'terms';

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Copyright',
  position: 'right',
  getComponent({
    React,
    StatusBarComponents })
  {
    return class Copyright extends React.Component {
      constructor(props) {
        super(props);this.




        handleClick = name => {
          switch (name) {
            case PRIVACY_POLICY:
              return Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_5__["openExternalLink"])(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_3__["LICENSE_URL"]);
            case TERMS:
              return Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_5__["openExternalLink"])(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_3__["EULA_LINK"]);
            default:
              break;}

        };this.handleClick = this.handleClick.bind(this);}

      render() {
        const { Item, Text } = StatusBarComponents,
        isLoggedIn = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore').isLoggedIn,
        isPublicWorkspace = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore').visibilityStatus === _constants_WorkspaceVisibilityConstants__WEBPACK_IMPORTED_MODULE_4__["VISIBILITY"].public;

        return (
          React.createElement(_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_1__["default"], { identifier: 'copyright' },

            !isLoggedIn && isPublicWorkspace &&
            React.createElement(Item, null,
              React.createElement(Text, {
                className: 'copyright-container',
                render: () => {
                  return (
                    React.createElement('div', { className: 'copyright-text' },
                      React.createElement('span', null, '\xA9 ', new Date().getFullYear(), ' Postman, Inc.'), '\xA0-\xA0',

                      React.createElement('span', {
                          className: 'copyright-link',
                          onClick: () => this.handleClick(PRIVACY_POLICY) }, 'Privacy Policy'), '\xA0-\xA0',




                      React.createElement('span', {
                          className: 'copyright-link',
                          onClick: () => this.handleClick(TERMS) }, 'Terms')));





                } }))));






      }};

  } });

/***/ })

}]);