(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[26],{

/***/ 14999:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionInfoCBView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3119);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3505);
/* harmony import */ var _monitors_components_context_bar_MonitorContextBarList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15000);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1835);
/* harmony import */ var _mocks_components_InfoPaneMockListing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15001);
/* harmony import */ var _integrations_components_context_bar_IntegrationContextBarList__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(14971);











/**
                                                                                                                           * Component for showing info about the collection in the context bar
                                                                                                                           *
                                                                                                                           * @component
                                                                                                                           */
function CollectionInfoCBView(props) {
  const { id } = props.contextData,
  collection = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CollectionStore').find(id),

  // @todo[EntityInTabs]: Pipe data via the CollectionInfoCBController
  info = collection ? {
    id: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').isLoggedIn ? collection.uid : id,
    createdOn: collection.createdAt,
    owner: collection.owner } :
  {};

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-info-cb' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__["ContextBarViewHeader"], {
        title: 'Collection details',
        onClose: props.onClose }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'collection-info-cb__container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_2__["EntityMetaInfoView"], {
          userFriendlyEntityName: 'collection',
          info: info }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_mocks_components_InfoPaneMockListing__WEBPACK_IMPORTED_MODULE_6__["default"], {
          collectionId: id,
          collectionUid: collection.uid,
          entity: 'collection' }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_monitors_components_context_bar_MonitorContextBarList__WEBPACK_IMPORTED_MODULE_4__["MonitorContextBarList"], {
          id: collection.uid,
          entity: 'collection',
          showCreateMonitor: true }),

        _.includes(['dev', 'beta', 'stage', 'canary', 'prod'], window.RELEASE_CHANNEL) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_integrations_components_context_bar_IntegrationContextBarList__WEBPACK_IMPORTED_MODULE_7__["IntegrationContextBarList"], {
          entityId: collection.uid,
          entityType: 'collection' }))));





}

CollectionInfoCBView.propTypes = {
  contextData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired };


CollectionInfoCBView.defaultProps = {
  contextData: {} };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);