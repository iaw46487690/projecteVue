(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ 15015:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RequestInfoCBView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3119);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3505);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1835);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1856);









/**
                                                                  * Component for showing info about the example in the context bar
                                                                  *
                                                                  * @component
                                                                  */
function RequestInfoCBView(props) {
  const { id } = props.contextData,
  request = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('RequestStore').find(id),
  info = request ? {
    id: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('CurrentUserStore').isLoggedIn ? Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_5__["composeUID"])(id, request.owner) : id,
    createdOn: request.createdAt } :
  {};

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'request-info-cb' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__["ContextBarViewHeader"], {
        title: 'Request details',
        onClose: props.onClose }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'request-info-cb__container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_2__["EntityMetaInfoView"], {
          userFriendlyEntityName: 'request',
          info: info }))));




}

RequestInfoCBView.propTypes = {
  contextData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired };


RequestInfoCBView.defaultProps = {
  contextData: {} };

/***/ })

}]);