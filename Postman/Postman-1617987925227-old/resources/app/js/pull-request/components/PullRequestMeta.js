(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[37],{

/***/ 15033:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PullRequestMeta; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(10);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment_timezone__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2666);
/* harmony import */ var moment_timezone__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment_timezone__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3505);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3043);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4913);









const prStatusMap = {
  merged: 'Merged',
  declined: 'Declined' };let


PullRequestMeta = class PullRequestMeta extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_4__["ContextBarViewHeader"], {
          title: 'Pull request details',
          onClose: this.props.onClose }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pull-request-meta' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('section', { className: 'pull-request-meta__section' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'pull-request-meta__section-header' }, 'Created by'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pull-request-user__label' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_5__["default"], {
                size: 'medium',
                userId: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.createdBy.id'),
                customPic: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.createdBy.profilePicUrl') }),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common__WEBPACK_IMPORTED_MODULE_6__["Username"], {
                className: 'pull-request-user__name',
                currentUserId: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.createdBy.id'),
                id: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.createdBy.id'),
                user: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.createdBy') }))),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('section', { className: 'pull-request-meta__section' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'pull-request-meta__section-header' }, 'Created on'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', { className: 'push-half--top' }, moment_timezone__WEBPACK_IMPORTED_MODULE_3___default()(lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.createdAt')).format('DD MMM YYYY'))),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('section', { className: 'pull-request-meta__section' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'pull-request-meta__section-header' },
              lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.status') === 'open' || lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.status') === 'approved' ?
              'Last Updated' :
              `${prStatusMap[lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.status')]} by`),



            prStatusMap[lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.status')] ?
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'push-half--top' },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pull-request-user__label' },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_5__["default"], {
                  size: 'medium',
                  userId: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.updatedBy.id'),
                  customPic: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.updatedBy.profilePicUrl') }),

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common__WEBPACK_IMPORTED_MODULE_6__["Username"], {
                  className: 'pull-request-user__name',
                  currentUserId: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.updatedBy.id'),
                  id: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.updatedBy.id'),
                  user: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.updatedBy') }))) :




            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', { className: 'push-half--top' }, moment_timezone__WEBPACK_IMPORTED_MODULE_3___default()(lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.updatedAt')).fromNow())),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('section', { className: 'pull-request-meta__section' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'pull-request-meta__section-header' }, 'Reviewers'),
            (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.reviewers') || []).length ?
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('ul', { className: 'pull-request-reviewers-list' },
              (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.props.contextData, 'pullRequest.reviewers') || []).map(reviewer => {

                return (
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('li', {
                      className: 'pull-request-reviewer',
                      key: reviewer.id },

                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                          className: reviewer.status &&
                          reviewer.status === 'approved' ?
                          'pull-request-reviewer-label-inline' : 'pull-request-reviewer-label' },

                        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pull-request-user__label' },
                          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_5__["default"], {
                            size: 'medium',
                            userId: reviewer.id,
                            customPic: reviewer.profilePicUrl }),

                          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common__WEBPACK_IMPORTED_MODULE_6__["Username"], {
                            className: 'pull-request-user__name',
                            currentUserId: reviewer.id,
                            id: reviewer.id,
                            user: reviewer }))),



                      reviewer.status && reviewer.status === 'approved' &&
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pull-request-reviewer-approved' },
                        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common__WEBPACK_IMPORTED_MODULE_6__["CustomTooltip"], {
                            align: 'bottom',
                            body: 'Has approved PR' },

                          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
                            name: 'icon-state-success-stroke',
                            size: 'large',
                            color: 'base-color-success' }))))));







              })) :

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', { className: 'pull-request-reviewer-empty' }, 'No reviewers')))));





  }};

/***/ })

}]);