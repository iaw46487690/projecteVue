(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[35],{

/***/ 15016:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistorySidebarContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3018);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_react_click_outside__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1835);
/* harmony import */ var _modules_model_event__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(305);
/* harmony import */ var _modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2187);
/* harmony import */ var _components_history_HistorySidebar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15017);
/* harmony import */ var _modules_services_HistoryService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2659);
/* harmony import */ var _new_button_CreateNewXService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15030);
/* harmony import */ var _runtime_api_RunnerInterface__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2694);
/* harmony import */ var _runtime_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2680);
/* harmony import */ var _runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2675);
/* harmony import */ var _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(15020);
/* harmony import */ var _runtime_components_base_molecule__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3119);
var _class;















let



HistorySidebarContainer = _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_1___default()(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class HistorySidebarContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    if (!window.analytics.sentFirstLoadMetrics) {
      window.analytics.sendAnalytics(
      'workspace_page',
      'workspace_page_sidebar_history_listing_initial_render_time');

    }
    super(props);

    this.store = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('HistorySidebarStore');

    this.focus = this.focus.bind(this);
    this.handleLoadRequestDebounced = _.debounce(this.handleLoadRequest, 100, { leading: true }).bind(this);
    this.handleLoadCollectionRunDebounced = _.debounce(this.handleLoadCollectionRun, 100, { leading: true }).bind(this);
    this.handleActiveRequestChange = this.handleActiveRequestChange.bind(this);
    this.handleAddToCollection = this.handleAddToCollection.bind(this);
    this.handleDeleteMultiple = this.handleDeleteMultiple.bind(this);
    this.handleItemSelect = this.handleItemSelect.bind(this);
    this.handleSelectAction = this.handleSelectAction.bind(this);
    this.handleSelectHistoryAction = this.handleSelectHistoryAction.bind(this);
    this.handleSelectCollectionRunAction = this.handleSelectCollectionRunAction.bind(this);
  }

  componentWillUnmount() {
    pm.mediator.off('activeRequestChanged', this.handleActiveRequestChange);
  }

  UNSAFE_componentWillMount() {
    pm.mediator.on('activeRequestChanged', this.handleActiveRequestChange);
  }

  focus() {
    _.invoke(this, 'refs.history.focus');
  }

  selectNext() {
    _.invoke(this, 'refs.history.selectNext');
  }

  handleActiveRequestChange(requestId) {
    if (!this.store.itemExists(requestId)) {
      this.store.resetSelection();
      return;
    }

    this.store.resetSelection([requestId]);
  }

  handleClickOutside(e) {
    if (!_.isEmpty(this.store.getSelectedItems())) {
      let modalElm = document.getElementsByClassName('ReactModal__Content')[0];
      let visibleDropdownElm = document.querySelector('.history-sidebar-multiple-request-selected-dropdown');

      // Exclude both delete modal & more actions dropdown from the reset selection condition.
      if (modalElm && modalElm.contains(e.target) || visibleDropdownElm && visibleDropdownElm.contains(e.target)) {
        return;
      }
      this.store.resetSelection();
    }
  }

  handleLoadRequest(id, newTab = false) {
    if (!id) {
      return;
    }

    _modules_services_HistoryService__WEBPACK_IMPORTED_MODULE_7__["default"].openHistoryInTab(id, {
      newTab: newTab,
      preview: true });

  }

  handleLoadCollectionRun(id, newTab = false) {
    if (!id) {
      return;
    }

    const uid = (Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('RunnerRunStore').find(id) || {}).uid;

    Object(_runtime_api_RunnerInterface__WEBPACK_IMPORTED_MODULE_9__["openRunInTab"])(uid, { newTab, preview: !newTab });
  }

  handleItemSelect({ id, type }, modifiers = {}) {
    let isOsxPlatform = _.includes(navigator.platform, 'Mac');

    if (modifiers.shiftKey && (isOsxPlatform ? modifiers.metaKey : modifiers.ctrlKey)) {
      type === 'collectionrun' ?
      this.handleLoadCollectionRunDebounced(id, true) :
      this.handleLoadRequestDebounced(id, true); // open request in a new tab

      pm.mediator.trigger('focusSidebar');
    } else
    if (modifiers.ctrlKey || modifiers.metaKey) {// select multiple requests
      if (this.store.isItemSelected(id)) {
        this.store.unselectItem(id);
      } else
      {
        this.store.selectItem(id);
      }
    } else
    {
      type === 'collectionrun' ?
      this.handleLoadCollectionRunDebounced(id, false) :
      this.handleLoadRequestDebounced(id, false); // open request in current tab

      pm.mediator.trigger('focusSidebar');

      this.store.resetSelection([id]);
    }
  }

  handleAddToCollection(options = {}) {
    const { requests } = options;
    let selectedItems = _.isEmpty(requests) ? this.store.getSelectedItems() : requests;
    if (_.isEmpty(selectedItems)) {
      return;
    }

    _modules_services_HistoryService__WEBPACK_IMPORTED_MODULE_7__["default"].
    convertHistoryToRequests(selectedItems).
    then(requests => {
      if (_.size(requests) === 1) {
        requests[0] && pm.mediator.trigger('showAddToCollectionModal', requests[0], { from: options.from });
      } else
      {
        pm.mediator.trigger('addRequestsToCollection', requests, options);
      }
    });
  }

  handleToggleHistoryResponseSaving() {
    if (!Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["isWorkspaceMember"])()) {
      if (Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["canJoinWorkspace"])()) {
        return pm.mediator.trigger('openUnjoinedWorkspaceModal');
      }

      if (!Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["isLoggedIn"])()) {
        return pm.mediator.trigger('showSignInModal', {
          type: 'history',
          origin: 'history_save_response' });

      }

      return pm.toasts.error('You do not have permissions required to perform the action.');
    }

    let workspaceSettings = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').settings,
    enableHistoryResponseSaving = workspaceSettings && workspaceSettings.enableHistoryResponseSaving,

    // compute the new value after toggle
    newSettings = { enableHistoryResponseSaving: !enableHistoryResponseSaving };

    return Object(_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["createEvent"])('updateSettings', 'workspace', { id: Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id, settings: newSettings })).
    catch(e => {
      let errorMessage = e.isFriendly ? e.message : 'Could not update workspace settings';

      pm.toasts.error(errorMessage, { dedupeId: 'ws-settings-error' });
    });
  }

  resetHistorySelection(nextSelection) {
    if (nextSelection && nextSelection.id) {
      this.store.resetSelection([nextSelection.id]);
    } else {
      this.store.resetSelection();
    }
  }

  handleDeleteMultiple(ref, nextSelection) {
    const permissionStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('PermissionStore'),
    runStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('RunnerRunStore'),
    workspaceId = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id;

    if (!Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["isWorkspaceMember"])()) {
      if (Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["canJoinWorkspace"])()) {
        return pm.mediator.trigger('openUnjoinedWorkspaceModal');
      }

      if (!Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["isLoggedIn"])()) {
        return pm.mediator.trigger('showSignInModal', {
          type: 'history',
          origin: 'history_delete_multiple' });

      }

      return pm.toasts.error('You do not have permissions required to perform the action.');
    }

    if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('OnlineStatusStore').userCanSave) {
      return Object(_runtime_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_10__["showOfflineActionDisabledToast"])();
    }

    return Promise.resolve().
    then(() => {

      if (ref === 'all') {
        Object(_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["createEvent"])('deleteAllInWorkspace', 'history', Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id));
        runStore.deleteRun(_.map(runStore.values, run => run.id));

        return;
      } else
      if (ref === 'selected') {
        if (permissionStore.can('delete', 'history')) {
          Object(_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["createEvent"])('deleteMultiple', 'history', { ids: this.store.getSelectedItems() }, null, null));
        } else
        {
          pm.toasts.error('You do not have permissions to delete history.');
        }

        if (permissionStore.can('delete', 'collectionrun')) {
          Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('RunnerRunStore').deleteRun(this.store.getSelectedItems());
        } else
        {
          pm.toasts.error('You do not have permissions to delete collection runs.');
        }
      }
    }).

    then(() => {
      this.resetHistorySelection(nextSelection);
    }).

    catch(() => {
      this.resetHistorySelection(nextSelection);
    });

  }

  handleAddRequestToService(options, event) {
    const {
      requests,
      defaultCollectionName = '',
      from = '' } =
    options;

    let selectedItems = _.isEmpty(requests) ? this.store.getSelectedItems() : requests;
    if (_.isEmpty(selectedItems)) {
      return;
    }

    _modules_services_HistoryService__WEBPACK_IMPORTED_MODULE_7__["default"].
    convertHistoryToRequests(selectedItems).
    then(requests => {

      if (!_.isEmpty(requests)) {
        let opts = {
          name: defaultCollectionName,
          from };


        _new_button_CreateNewXService__WEBPACK_IMPORTED_MODULE_8__["default"].create(event, { requests, opts }, null, { from: 'history_sidebar' });
      }
    });
  }

  handleSelectHistoryAction(action, options) {
    switch (action) {
      case _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_SAVE"]:
        if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('OnlineStatusStore').userCanSave) {
          return Object(_runtime_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_10__["showOfflineActionDisabledToast"])();
        }

        this.handleAddToCollection(options);
        break;

      case _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_MONITOR"]:
        this.handleAddRequestToService(options, 'openCreateNewMonitorModal');
        break;

      case _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DOCUMENT"]:
        this.handleAddRequestToService(options, 'openCreateNewDocumentationModal');
        break;

      case _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_MOCK"]:
        this.handleAddRequestToService(options, 'openCreateNewMockModal');
        break;

      case _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DELETE"]:
        if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('OnlineStatusStore').userCanSave) {
          return Object(_runtime_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_10__["showOfflineActionDisabledToast"])();
        }

        Object(_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["createEvent"])('deleteMultiple', 'history', { ids: options.requests }, null, null));
        break;

      default:return;}

  }

  handleSelectCollectionRunAction(action, options) {
    switch (action) {
      case _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DELETE"]:
        if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('OnlineStatusStore').userCanSave) {
          return Object(_runtime_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_10__["showOfflineActionDisabledToast"])();
        }

        Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('RunnerRunStore').deleteRun(options.runs);
        break;

      default:return;}

  }

  handleSelectAction(entityType, action, options) {
    if (!Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["isWorkspaceMember"])()) {
      if (Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["canJoinWorkspace"])()) {
        return pm.mediator.trigger('openUnjoinedWorkspaceModal');
      }

      if (!Object(_runtime_helpers_WorkspaceHelpers__WEBPACK_IMPORTED_MODULE_11__["isLoggedIn"])()) {
        return pm.mediator.trigger('showSignInModal', {
          type: 'history',
          origin: `history_${action}` });

      }

      return pm.toasts.error('You do not have permissions required to perform the action.');
    }

    if (entityType === 'collectionrun') {
      return options.runs && this.handleSelectCollectionRunAction(action, options);
    }

    if (entityType === 'request') {
      return this.handleSelectHistoryAction(action, options);
    }

    if (entityType === 'all') {
      options.runs && this.handleSelectCollectionRunAction(action, options);
      options.requests && this.handleSelectHistoryAction(action, options);
      return;
    }
  }

  render() {

    // The API isConsistentlyOffline is only supposed to be used to show the offline state view to the user when he has been consistently offline.
    // For disabling the actions on lack of connectivity, please rely on the socket connected state itself for now.
    // Otherwise, there is a chance of data loss if we let the user perform actions when we are attempting a connection.
    const isConsistentlyOffline = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isConsistentlyOffline;

    if (!pm.isScratchpad && isConsistentlyOffline && this.store.loading) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_runtime_components_base_molecule__WEBPACK_IMPORTED_MODULE_13__["SidebarOfflineState"], null);
    }

    let workspaceSettings = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').settings,
    syncStatusStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore'),
    currentUserStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore'),
    historyStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('HistoryStore'),
    workspaceStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore'),
    enableHistoryResponseSaving = workspaceSettings && workspaceSettings.enableHistoryResponseSaving;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_history_HistorySidebar__WEBPACK_IMPORTED_MODULE_6__["default"], {
        store: this.store,
        enableHistoryResponseSaving: enableHistoryResponseSaving,
        initializing: currentUserStore.isHydrating || historyStore.isHydrating || !workspaceStore.id,
        isLoggedIn: currentUserStore.isLoggedIn,
        isSocketConnected: syncStatusStore.isSocketConnected,
        workspace: workspaceStore.id,
        onSelectAction: this.handleSelectAction,
        onDeleteMultiple: this.handleDeleteMultiple,
        onSelect: this.handleItemSelect,
        onSaveCurrentRequest: this.handleAddToCollection,
        onToggleHistoryResponseSaving: this.handleToggleHistoryResponseSaving,
        ref: 'history' }));


  }}) || _class) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15017:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistorySidebar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var pure_render_decorator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3056);
/* harmony import */ var pure_render_decorator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pure_render_decorator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _HistorySidebarMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15018);
/* harmony import */ var _HistorySidebarList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15022);
/* harmony import */ var _archivedResource_ArchivedResource__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(14982);
/* harmony import */ var _base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(310);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1835);
/* harmony import */ var _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15020);
/* harmony import */ var _base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3011);
var _class;








let


HistorySidebar = pure_render_decorator__WEBPACK_IMPORTED_MODULE_2___default()(_class = class HistorySidebar extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.store = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('HistorySidebarStore');
    this.historyStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('HistoryStore');

    this.focus = this.focus.bind(this);
    this.groupItems = this.groupItems.bind(this);
    this.selectNext = this.selectNext.bind(this);
    this.selectPrev = this.selectPrev.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.multiselectNextItem = this.multiselectNextItem.bind(this);
    this.multiselectPrevItem = this.multiselectPrevItem.bind(this);
  }

  getKeyMapHandlers() {
    return {
      groupItems: pm.shortcuts.handle('groupItems', this.groupItems),
      nextItem: pm.shortcuts.handle('nextItem', this.selectNext),
      prevItem: pm.shortcuts.handle('prevItem', this.selectPrev),
      delete: pm.shortcuts.handle('delete', this.deleteItem),
      multiselectNextItem: pm.shortcuts.handle('multiselectNextItem', this.multiselectNextItem),
      multiselectPrevItem: pm.shortcuts.handle('multiselectPrevtItem', this.multiselectPrevItem),
      select: pm.shortcuts.handle('select', this.selectItem),
      search: pm.shortcuts.handle('search'),
      saveCurrentRequest: e => {
        e && e.stopPropagation();
        this.props.onSaveCurrentRequest();
      } };

  }

  focus() {
    let $history = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(this.refs.history);
    $history && $history.focus();
  }

  selectNext(e) {
    e && e.preventDefault();
    let item = this.store.getNextItem();
    item && _.invoke(this, 'props.onSelect', item);
  }

  selectPrev(e) {
    e && e.preventDefault();
    let item = this.store.getPrevItem();
    item && _.invoke(this, 'props.onSelect', item);
  }

  deleteItem(e) {
    if (document.activeElement.classList.contains('history-sidebar__filter')) {
      return;
    }

    e && e.preventDefault();
    let item = this.store.getPrevItem();
    console.warn('prevItemId fix');
    _.invoke(this, 'props.onDeleteMultiple', 'selected', item);
  }

  selectItem(e) {
    e && e.preventDefault();
    _.invoke(this, 'refs.list.selectItem');
  }

  multiselectNextItem(e) {
    e && e.preventDefault();
    let item = this.store.getNextItem();
    item && _.invoke(this, 'props.onSelect', item, { metaKey: true });
  }

  multiselectPrevItem(e) {
    e && e.preventDefault();
    let item = this.store.getPrevItem();
    item && _.invoke(this, 'props.onSelect', item, { metaKey: true });
  }

  groupItems() {
    this.props.onSelectAction && this.props.onSelectAction('all', _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_8__["ACTION_TYPE_SAVE"], {
      requests: this.store.getSelectedItems(),
      from: 'history_multiple' });

  }

  render() {
    let ActiveWorkspaceStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('ActiveWorkspaceStore');

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_6__["default"], {
          handlers: this.getKeyMapHandlers(),
          keyMap: pm.shortcuts.getShortcuts() },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: 'history-sidebar',
            ref: 'history' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-wrapper' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistorySidebarMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
              historySidebar: this.store,
              historyStore: this.historyStore,
              enableHistoryResponseSaving: this.props.enableHistoryResponseSaving,
              onDeleteMultiple: this.props.onDeleteMultiple,
              onSelectAction: this.props.onSelectAction.bind(this, 'all'),
              onToggleHistoryResponseSaving: this.props.onToggleHistoryResponseSaving }),

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_9__["default"], { identifier: 'history' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistorySidebarList__WEBPACK_IMPORTED_MODULE_4__["default"], {
                ref: 'list',
                initializing: this.props.initializing,
                isSocketConnected: this.props.isSocketConnected,
                isLoggedIn: this.props.isLoggedIn,
                workspace: this.props.workspace,
                onDeleteMultiple: this.props.onDeleteMultiple,
                onLoadMore: this.props.onLoadMore,
                onSelect: this.props.onSelect,
                onSelectAction: this.props.onSelectAction }))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_archivedResource_ArchivedResource__WEBPACK_IMPORTED_MODULE_5__["default"], { archivedResources: ActiveWorkspaceStore, label: 'HISTORIES' }))));



  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15018:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistorySidebarMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3016);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(80);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _base_ToggleSwitch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3113);
/* harmony import */ var _base_Dropdowns__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3055);
/* harmony import */ var _base_Tooltips__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3017);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(1835);
/* harmony import */ var _HistoryItemActionsDropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(15019);
/* harmony import */ var _runtime_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2680);
/* harmony import */ var _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(15020);
/* harmony import */ var _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(15021);
/* harmony import */ var _containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(6770);
/* harmony import */ var _runtime_helpers_RunnerHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(4132);
var _class;














let


HistorySidebarMenu = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class HistorySidebarMenu extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      showResponseToggleTooltip: false };


    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);
    this.handleAddToCollectionClick = this.handleAddToCollectionClick.bind(this);
    this.handleDeleteSelected = this.handleDeleteSelected.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleActionsDropdownSelect = this.handleActionsDropdownSelect.bind(this);
    this.handleSaveResponseToggle = this.handleSaveResponseToggle.bind(this);
    this.showResponseToggleTooltip = this.showResponseToggleTooltip.bind(this);
    this.hideResponseToggleTooltip = this.hideResponseToggleTooltip.bind(this);
    this.disableRequestItemOptions = this.disableRequestItemOptions.bind(this);
  }

  handleAddToCollectionClick(e) {
    e.stopPropagation();
    this.props.onSelectAction(_constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_11__["ACTION_TYPE_SAVE"], {
      // @note `getSelectedItems` returns the ids of requests and runs combined.
      // the handler of action should be able to handle the case when request
      // with given id does not exist
      requests: this.props.historySidebar.getSelectedItems(),
      from: 'history_multiple' });

  }

  handleDeleteClick() {
    pm.mediator.trigger('showDeleteHistoryModal', async () => {
      try {this.props.onDeleteMultiple && (await this.props.onDeleteMultiple('all'));}
      catch (e) {pm.logger.error('Could not delete history', e);pm.toasts.error('Could not delete history');}
    }, {
      identifier: 'all' });

  }

  handleDropdownToggle(value) {
    if (value === this.state.dropdownOpen) {
      return;
    }

    this.setState({ dropdownOpen: value });
  }

  handleDropdownActionSelect(action) {
    if (action === _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_11__["ACTION_TYPE_DELETE"]) {
      if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('OnlineStatusStore').userCanSave) {
        return Object(_runtime_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_10__["showOfflineActionDisabledToast"])();
      }

      pm.mediator.trigger('showDeleteHistoryModal', () => {
        this.props.onDeleteMultiple('selected');
      }, {
        identifier: 'selected' });

      return;
    }

    if (action === _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_11__["ACTION_TYPE_DOWNLOAD"]) {
      return Object(_runtime_helpers_RunnerHelper__WEBPACK_IMPORTED_MODULE_14__["exportRuns"])(this.props.historySidebar.getSelectedItems());
    }

    this.props.onSelectAction(action, {
      // @note `getSelectedItems` returns the ids of requests and runs combined.
      // the handler of action should be able to handle the case when request
      // with given id does not exist
      requests: this.props.historySidebar.getSelectedItems(),
      from: 'history_multiple' });

  }

  handleActionsDropdownSelect(value) {
    switch (value) {
      case 'history-sidebar--clear-all':
        this.handleDeleteClick();
        break;

      default:return;}

  }

  handleDeleteSelected() {
    this.handleDropdownActionSelect(_constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_11__["ACTION_TYPE_DELETE"]);
  }

  disableRequestItemOptions() {
    let requestItemFocusedInHistory = _.filter(this.props.historySidebar.historyItems, item => {
      return this.props.historySidebar.selectedItems.has(item.id) && item.type === 'request';
    });
    return _.isEmpty(requestItemFocusedInHistory);
  }

  showResponseToggleTooltip() {
    this.setState({ showResponseToggleTooltip: true });
  }

  hideResponseToggleTooltip() {
    this.setState({ showResponseToggleTooltip: false });
  }

  handleSaveResponseToggle(e) {
    e && e.stopPropagation(); // prevent dropdown from closing
    this.props.onToggleHistoryResponseSaving && this.props.onToggleHistoryResponseSaving();
  }

  handleSearchChange(query) {
    this.props.historySidebar.setSearchQuery(query);
  }

  getToggleSaveResponseClasses(isDisabled) {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'dropdown-menu-item history-sidebar-menu__actions-save-response-toggle': true,
      'is-disabled': isDisabled });

  }

  render() {
    let selectionSize = this.props.historySidebar.selectedItems.size;

    const isLoggedIn = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('CurrentUserStore').isLoggedIn,
    { isOffline, userCanSave } = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('OnlineStatusStore'),
    disableClearAll = isOffline && isLoggedIn,
    disableSaveResponses = !Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('PermissionStore').can('editMeta', 'workspace', Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('ActiveWorkspaceStore').id);

    if (selectionSize > 1) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-menu multiple-request-selected' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-menu__left' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_4__["Text"], { type: 'body-small', color: 'content-color-primary' },
              selectionSize, ' items selected')),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-menu__right' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-menu__actions-delete-history-wrapper' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_2__["Button"], {
                  disabled: !userCanSave,
                  className: 'history-sidebar-list-item__button__delete',
                  type: 'icon',
                  onClick: this.handleDeleteSelected,
                  tooltip: _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_12__["ACTION_TYPE_DELETE_REQUESTS_TOOLTIP"] },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_4__["Icon"], { name: 'icon-action-delete-stroke', size: 'large', className: 'history-sidebar-list-item__actions__delete pm-icon pm-icon-normal' })),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistoryItemActionsDropdown__WEBPACK_IMPORTED_MODULE_9__["default"], {
                count: selectionSize,
                onSelect: this.handleDropdownActionSelect,
                onToggle: this.handleDropdownToggle,
                disableRequestSpecificActions: this.disableRequestItemOptions() })))));





    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-menu' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_13__["default"], {
          className: 'history-sidebar__filter',
          onSearch: this.handleSearchChange,
          onActionsDropdownSelect: this.handleActionsDropdownSelect,
          searchQuery: this.props.historySidebar.searchQuery,
          moreActions:
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_6__["DropdownMenu"], { className: 'history-sidebar__header-actions-dropdown--menu' },
            !_.isEmpty(this.props.historySidebar.historyItems) &&
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_6__["MenuItem"], {
                className: 'history-sidebar__header-actions-dropdown--menu-item history-sidebar-menu__actions-delete-history-wrapper',
                key: 'history-sidebar--clear-all',
                refKey: 'history-sidebar--clear-all',
                disabled: disableClearAll,
                disabledText: 'You need to be online to clear history.' },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, 'Clear all')),


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: this.getToggleSaveResponseClasses(disableSaveResponses),
                onClick: disableSaveResponses ? undefined : this.handleSaveResponseToggle,
                ref: r => {this.saveResponseToggleContainerRef = r;},
                onMouseEnter: this.showResponseToggleTooltip,
                onMouseLeave: this.hideResponseToggleTooltip }, 'Save Responses',


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_ToggleSwitch__WEBPACK_IMPORTED_MODULE_5__["default"], {
                isActive: this.props.enableHistoryResponseSaving,
                activeLabel: '',
                inactiveLabel: '' }),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_7__["Tooltip"], {
                  className: 'history-sidebar-menu--save-response__tooltip',
                  show: this.state.showResponseToggleTooltip,
                  target: this.saveResponseToggleContainerRef },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_7__["TooltipBody"], null,

                  disableSaveResponses ? 'You don’t have permission to take this action.' :
                  'When turned off, the response headers and body won\'t be saved, though the the response code, time, size, and language will be saved for reporting purposes.')))) })));









  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15019:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistoryItemActionsDropdown; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3055);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3016);
/* harmony import */ var _utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2683);
/* harmony import */ var _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15020);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1835);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(80);
/* harmony import */ var _constants_MoreButtonTooltipConstant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3115);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4841);
/* harmony import */ var _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3491);
/* harmony import */ var _controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3594);
var _class;














const DISABLED_REQUEST_OPTIONS_TOOLTIP = 'Not available to collection run items',
COLLECTION_RUN_EXPORT_DISABLED_TOOLTIP = 'You can only export collection runs';let


HistoryItemActionsDropdown = Object(mobx_react__WEBPACK_IMPORTED_MODULE_7__["observer"])(_class = class HistoryItemActionsDropdown extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);
    this.handleShortcutSelect = this.handleShortcutSelect.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }

  getKeymapHandlers() {
    return { delete: pm.shortcuts.handle('delete', this.handleShortcutSelect.bind(this, _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_DELETE"])) };
  }

  handleShortcutSelect(action) {
    pm.mediator.trigger('focusSidebar');
    this.handleSelect(action);
  }

  handleSelect(action) {
    this.props.onSelect && this.props.onSelect(action);
    this.handleToggle(false);
  }

  handleToggle(isOpen) {
    this.props.onToggle && this.props.onToggle(isOpen);
  }

  getDropDownActionWrapperClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({ 'collection-sidebar-request-dropdown-actions-wrapper': true });
  }

  getDisabledText(isDisabled) {
    if (!isDisabled) {
      return;
    }

    if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('OnlineStatusStore').userCanSave) {
      return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_IS_OFFLINE"];
    }

    return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_NO_PERMISSION"];
  }


  render() {
    let pluralizeRequest = _utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_4__["default"].pluralize({
      count: this.props.count,
      singular: 'Request',
      plural: 'Requests' });


    const permissionStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('PermissionStore'),
    { userCanSave } = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('OnlineStatusStore'),
    canDeleteHistory = userCanSave && permissionStore.can('delete', 'history');

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getDropDownActionWrapperClasses() },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["Dropdown"], {
            keyMapHandlers: this.getKeymapHandlers(),
            ref: 'menu',
            onSelect: this.handleSelect,
            onToggle: this.handleToggle,
            className: 'collection-sidebar-request-actions-dropdown history-sidebar-multiple-request-selected-dropdown' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["DropdownButton"], {
              dropdownStyle: 'nocaret',
              type: 'custom' },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                className: 'history-sidebar-list-item__button__options',
                tooltip: _constants_MoreButtonTooltipConstant__WEBPACK_IMPORTED_MODULE_8__["ACTION_TYPE_VIEW_MORE_ACTIONS_TOOLTIP"] }, ' ',

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
                name: 'icon-action-options-stroke',
                className: 'history-sidebar-list-item__actions__options pm-icon pm-icon-normal' }))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["DropdownMenu"], {
              'align-right': true },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["MenuItem"], {
                refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_SAVE"],
                disabled: !userCanSave || this.props.disableRequestSpecificActions,
                disabledText: userCanSave ? DISABLED_REQUEST_OPTIONS_TOOLTIP : _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_IS_OFFLINE"] },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, `Save ${pluralizeRequest}`)),

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["MenuItem"], {
                refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_MONITOR"],
                disabled: !userCanSave || this.props.disableRequestSpecificActions,
                disabledText:
                userCanSave ?
                this.props.disableRequestSpecificActions ?
                DISABLED_REQUEST_OPTIONS_TOOLTIP :
                _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_10__["DISABLED_IN_SCRATCHPAD_TOOLTIP"] :
                _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_IS_OFFLINE"] },


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, `Monitor ${pluralizeRequest}`)),

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["MenuItem"], {
                refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_DOCUMENT"],
                disabled: !userCanSave || this.props.disableRequestSpecificActions,
                disabledText:
                userCanSave ?
                this.props.disableRequestSpecificActions ?
                DISABLED_REQUEST_OPTIONS_TOOLTIP :
                _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_10__["DISABLED_IN_SCRATCHPAD_TOOLTIP"] :
                _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_IS_OFFLINE"] },


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, `Document ${pluralizeRequest}`)),

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["MenuItem"], {
                refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_MOCK"],
                disabled: !userCanSave || this.props.disableRequestSpecificActions,
                disabledText:
                userCanSave ?
                this.props.disableRequestSpecificActions ?
                DISABLED_REQUEST_OPTIONS_TOOLTIP :
                _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_10__["DISABLED_IN_SCRATCHPAD_TOOLTIP"] :
                _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_IS_OFFLINE"] },


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, `Mock ${pluralizeRequest}`)),


            window.SDK_PLATFORM !== 'browser' &&
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["MenuItem"], {
                refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_DOWNLOAD"],
                disabled: !userCanSave || !this.props.disableRequestSpecificActions,
                disabledText: userCanSave ? COLLECTION_RUN_EXPORT_DISABLED_TOOLTIP : _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_11__["DISABLED_TOOLTIP_IS_OFFLINE"] },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, 'Export')),


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_2__["MenuItem"], {
                refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_TYPE_DELETE"],
                disabled: !canDeleteHistory,
                disabledText: this.getDisabledText(!canDeleteHistory) },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, 'Delete'),
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-shortcut' }, Object(_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_12__["getShortcutByName"])('delete')))))));





  }}) || _class;

/***/ }),

/***/ 15020:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_SAVE", function() { return ACTION_TYPE_SAVE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_MONITOR", function() { return ACTION_TYPE_MONITOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_DOCUMENT", function() { return ACTION_TYPE_DOCUMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_MOCK", function() { return ACTION_TYPE_MOCK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_DELETE", function() { return ACTION_TYPE_DELETE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_DOWNLOAD", function() { return ACTION_TYPE_DOWNLOAD; });
const ACTION_TYPE_SAVE = 'save',
ACTION_TYPE_MONITOR = 'monitor',
ACTION_TYPE_DOCUMENT = 'document',
ACTION_TYPE_MOCK = 'mock',
ACTION_TYPE_DELETE = 'delete',
ACTION_TYPE_DOWNLOAD = 'download';

/***/ }),

/***/ 15021:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_DELETE_REQUEST_TOOLTIP", function() { return ACTION_TYPE_DELETE_REQUEST_TOOLTIP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_DELETE_REQUESTS_TOOLTIP", function() { return ACTION_TYPE_DELETE_REQUESTS_TOOLTIP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_SAVE_REQUEST_TOOLTIP", function() { return ACTION_TYPE_SAVE_REQUEST_TOOLTIP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTION_TYPE_SAVE_REQUESTS_TOOLTIP", function() { return ACTION_TYPE_SAVE_REQUESTS_TOOLTIP; });
const ACTION_TYPE_DELETE_REQUEST_TOOLTIP = 'Delete request',
ACTION_TYPE_DELETE_REQUESTS_TOOLTIP = 'Delete items',
ACTION_TYPE_SAVE_REQUEST_TOOLTIP = 'Add request to Collection',
ACTION_TYPE_SAVE_REQUESTS_TOOLTIP = 'Add requests to Collection';

/***/ }),

/***/ 15022:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistorySidebarList; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _HistorySidebarListEmptyItem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15023);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1835);
/* harmony import */ var _empty_states_HistorySidebarEmptyShell__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15024);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6773);
/* harmony import */ var _HistoryListItem__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15025);
/* harmony import */ var _HistoryListMetaItem__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15026);
/* harmony import */ var _HistoryListErrorItem__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(15027);
/* harmony import */ var _HistoryListCollectionRunItem__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(15028);
/* harmony import */ var react_window__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3299);
/* harmony import */ var react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(4886);
/* harmony import */ var _HistorySidebarErrorState__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(15029);
/* harmony import */ var _utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(2683);
/* harmony import */ var _base_Icons_RefreshIcon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3047);
/* harmony import */ var _appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(5455);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(3016);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_18__);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;



















const MIN_ROW_HEIGHT = 28,
OVERSCAN_COUNT = 10,
VIEW_HISTORY_PERMISSION_ERROR = 'You don\'t have permission to view history in this workspace';let


HistorySidebarList = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class HistorySidebarList extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.store = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('HistorySidebarStore');
    this.requesterSidebarStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('RequesterSidebarStore');

    this.handleToggleGroup = this.handleToggleGroup.bind(this);
    this.getListItem = this.getListItem.bind(this);
    this.getItemSize = this.getItemSize.bind(this);
    this.handleClearCachedHeight = this.handleClearCachedHeight.bind(this);
    this.observeSizeChange = this.observeSizeChange.bind(this);
    this.unobserveSizeChange = this.unobserveSizeChange.bind(this);
    this.handleItemsRender = this.handleItemsRender.bind(this);
    this.handleItemsRenderDebounced = _.debounce(this.handleItemsRender, 1000);
    this.handleListScroll = this.handleListScroll.bind(this);
    this.handleListScrollDebounced = _.debounce(this.handleListScroll, 300);
    this.handleJumpToTop = this.handleJumpToTop.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);

    this.heightSet = {};

    this.resizeObserver = new ResizeObserver(entries => {
      for (let entry of entries) {
        if (!(entry && entry.target && entry.target.dataset)) {
          return;
        }

        let index = entry.target.dataset.index;

        this.heightSet[index] = entry.target.offsetHeight;
      }

      this.handleClearCachedHeight(0);
    });
  }

  componentDidMount() {
    if (!this.props.initializing) {
      this.store.load();
    }
  }

  componentDidUpdate(prevProps) {
    // are stores still initializing?
    if (this.props.initializing) {
      return;
    }

    // everthing is initialized, load data
    if (prevProps.initializing) {
      this.store.load();

      return;
    }

    // workspace has switched or user logged in, load data again and scroll to top
    if (this.props.workspace !== prevProps.workspace || this.props.isLoggedIn && !prevProps.isLoggedIn) {
      this.store.load();
      this.listRef && this.listRef.scrollTo(0);
      this.store.setScrollOffset(0);

      return;
    }
  }

  observeSizeChange(node) {
    this.resizeObserver && this.resizeObserver.observe(node);
  }

  unobserveSizeChange(node, index) {
    this.resizeObserver && this.resizeObserver.unobserve(node);
    delete this.heightSet[index];
  }

  handleClearCachedHeight(index) {
    this.listRef.resetAfterIndex(index);
  }

  handleToggleGroup(name) {
    if (this.store.isItemCollapsed(name)) {
      return this.store.expandItem(name);
    }

    this.store.collapseItem(name);
  }

  getItemSize(index) {
    let currentItem = _.get(this.store, ['historyItems', index]);

    if (currentItem.type === 'meta' || currentItem.type === 'error') {
      return MIN_ROW_HEIGHT;
    }

    return this.heightSet[index] || MIN_ROW_HEIGHT;
  }

  getListItem(data) {
    let historyItem = _.get(this.store, ['historyItems', data.index]);

    if (!historyItem) {
      return null;
    }

    if (historyItem.type === 'error') {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-list__error', style: data.style },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistoryListErrorItem__WEBPACK_IMPORTED_MODULE_9__["default"], {
            key: historyItem.name,
            onRetry: () => {
              if (historyItem.key === 'load-error') {
                return this.store.load(true);
              }

              if (historyItem.key === 'load-more-error') {
                return this.store.loadMore();
              }
            } })));



    }

    if (historyItem.type === 'meta') {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-list__meta', style: data.style },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistoryListMetaItem__WEBPACK_IMPORTED_MODULE_8__["default"], {
            name: historyItem.name,
            key: historyItem.name,
            onSelect: this.props.onSelect,
            onSelectAction: this.props.onSelectAction.bind(this, 'all'),
            items: historyItem.items,
            disableRequestSpecificActions: historyItem.disableRequestSpecificActions })));



    }

    if (historyItem.type === 'collectionrun') {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-list__collection-run', style: data.style },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistoryListCollectionRunItem__WEBPACK_IMPORTED_MODULE_10__["default"], _extends({},
          historyItem, {
            name: historyItem.name,
            hideActions: _.size(this.store.getSelectedItems()) > 1,
            key: historyItem.id,
            selected: this.store.isItemSelected(historyItem.id),
            onSelect: this.props.onSelect,
            onSelectAction: this.props.onSelectAction.bind(this, 'collectionrun'),
            observeSizeChange: this.observeSizeChange,
            unobserveSizeChange: this.unobserveSizeChange,
            index: data.index }))));



    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-list__item', style: data.style },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistoryListItem__WEBPACK_IMPORTED_MODULE_7__["default"], _extends({},
        historyItem, {
          hideActions: _.size(this.store.getSelectedItems()) > 1,
          key: historyItem.id,
          selected: this.store.isItemSelected(historyItem.id),
          onSelect: this.props.onSelect,
          onSelectAction: this.props.onSelectAction.bind(this, 'request'),
          observeSizeChange: this.observeSizeChange,
          unobserveSizeChange: this.unobserveSizeChange,
          viewLargeBody: this.props.viewLargeBody,
          index: data.index }))));



  }

  getNotification() {
    if (this.store.showReloadNotification) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list__notification' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_17__["Button"], {
              type: 'primary',
              size: 'small',
              className: 'history-sidebar-list__refresh',
              onClick: this.handleRefresh },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_18__["Icon"], {
              name: 'icon-action-refresh-stroke',
              color: 'content-color-primary',
              size: 'small',
              className: 'pm-icon' }), 'Refresh')));





    }

    if (this.store.newHistoryIds.size) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list__notification' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_17__["Button"], {
              type: 'primary',
              size: 'small',
              className: 'history-sidebar-list__more-on-top',
              onClick: this.handleJumpToTop },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_18__["Icon"], {
              name: 'icon-direction-up',
              color: 'content-color-primary',
              size: 'small',
              className: 'pm-icon' }),

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, this.store.newHistoryIds.size, ' new ', _utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_14__["default"].pluralize({
                count: this.store.newHistoryIds.size,
                singular: 'item',
                plural: 'items' })))));




    }

    return null;
  }

  handleListScroll({ scrollOffset }) {
    if (scrollOffset === 0 && this.store.newHistoryIds.size) {
      this.store.showNewHistory();
    }

    this.store.setScrollOffset(scrollOffset);
  }

  handleItemsRender(data) {
    // no need to load more data
    if (this.store.scrolledToEnd || this.store.loadingMore || this.requesterSidebarStore.query) {
      return;
    }

    // scrolled to the end or last history request is visible
    if (data.overscanStopIndex === data.visibleStopIndex) {
      this.store.loadMore();
    }
  }

  handleJumpToTop() {
    this.listRef && this.listRef.scrollTo(0);
  }

  handleRefresh() {
    this.listRef && this.listRef.scrollTo(0);
    this.store.load(true);
  }

  render() {
    const
    lister = data => {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_2__["Observer"], null,
          this.getListItem.bind(this, data)));


    },
    sizer = ({ height, width }) => {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_2__["Observer"], null,
          () =>
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_window__WEBPACK_IMPORTED_MODULE_11__["VariableSizeList"], {
              height: height,
              itemCount: this.store.historyItems.length,
              itemSize: this.getItemSize,
              width: width,
              ref: ref => this.listRef = ref,
              overscanCount: OVERSCAN_COUNT,
              onItemsRendered: this.handleItemsRenderDebounced,
              onScroll: this.handleListScrollDebounced },

            lister)));




    };

    if (_.isEmpty(this.store.historyItems)) {
      if (this.props.initializing || this.store.loading) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list' }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_16__["default"], { hasHierarchy: true }));
      }

      if (this.store.loadError) {
        if (this.store.loadError.message === VIEW_HISTORY_PERMISSION_ERROR) {
          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list-permission-error' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_6__["default"], {
                icon: 'icon-descriptive-history-stroke',
                title: 'You don\'t have permission to view history',
                message: 'Only team members can view history of this workspace' })));



        }

        return (
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistorySidebarErrorState__WEBPACK_IMPORTED_MODULE_13__["default"], { onRetry: () => this.store.load(true) })));


      }

      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistorySidebarListEmptyItem__WEBPACK_IMPORTED_MODULE_3__["default"], { query: this.store.searchQuery })));


    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: 'history-sidebar-list',
          ref: 'history' },

        this.getNotification(),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_12__["default"], null, sizer)));


  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15023:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistorySidebarListEmptyItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _onboarding_public_Skills__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3651);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6773);
/* harmony import */ var _containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6772);
/* harmony import */ var _external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(318);
/* harmony import */ var _constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2690);
var _class;






let


HistorySidebarListEmptyItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class HistorySidebarListEmptyItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleShowMeHow = this.handleShowMeHow.bind(this);
  }

  handleShowMeHow() {
    // runTaggedLesson('debugging', {
    //   signInModalOptions: {
    //     type: 'history',
    //     origin: 'history_sidebar_show_me_how'
    //   }
    // });

    Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_5__["openExternalLink"])(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_6__["SEND_FIRST_REQUEST_DOCS"], '_blank');
  }

  render() {
    if (this.props.query) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_apps_requester_sidebar_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_4__["default"], { searchQuery: this.props.query, icon: 'icon-descriptive-history-stroke' }));

    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_3__["default"], {
        icon: 'icon-descriptive-history-stroke',
        title: 'You haven\'t sent any requests.',
        message: 'Any request you send in this workspace will appear here.',
        action: {
          label: 'Show me how',
          handler: this.handleShowMeHow } }));



  }}) || _class;

/***/ }),

/***/ 15024:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const NUMBER_OF_HISTORY_ITEMS_PER_SECTION = 5,
NUMBER_OF_SECTIONS = 2;

const HistorySidebarEmptyShell = () => {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-empty-shell' },

      _.times(NUMBER_OF_SECTIONS, sectionIndex => {
        return (
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { key: sectionIndex, className: 'history-sidebar-empty-shell--section' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-empty-shell--section--title' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-empty-shell--section--title__dropdown' }),
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-empty-shell--section--title__name' })),


            _.times(NUMBER_OF_HISTORY_ITEMS_PER_SECTION, index => {
              return (
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                    key: index,
                    className: 'history-sidebar-empty-shell--section--item' },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-empty-shell--section--item__icon' }),
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                    className: 'history-sidebar-empty-shell--section--item__url',
                    style: { 'width': `${Math.floor(Math.random() * 74) + 50}px` } })));



            })));



      })));



};

/* harmony default export */ __webpack_exports__["default"] = (HistorySidebarEmptyShell);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15025:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistoryListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(297);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(80);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1835);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3016);
/* harmony import */ var _request_RequestIcon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4312);
/* harmony import */ var _base_Dropdowns__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3055);
/* harmony import */ var _base_Avatar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3043);
/* harmony import */ var _base_Tooltips__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3017);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(6767);
/* harmony import */ var _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(15020);
/* harmony import */ var _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(15021);
/* harmony import */ var _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3491);
/* harmony import */ var _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(4841);
/* harmony import */ var _controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(3594);
var _class;

















let


HistoryListItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_4__["observer"])(_class = class HistoryListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      isTooltipVisible: false,
      isHovered: false };


    this.userAvatar = react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef();
    this.handleSelect = this.handleSelect.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);
    this.handleAddRequestToCollectionClick = this.handleAddRequestToCollectionClick.bind(this);
    this.handleRemoveRequest = this.handleRemoveRequest.bind(this);
    this.showTooltip = this.showTooltip.bind(this);
    this.hideTooltip = this.hideTooltip.bind(this);
    this.handleMouseOver = this.handleMouseOver.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.getLeftAlignedComponents = this.getLeftAlignedComponents.bind(this);
    this.getRightAlignedComponents = this.getRightAlignedComponents.bind(this);
  }


  componentDidMount() {
    this.selfNode = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(this);

    // Starts observing element
    this.props.observeSizeChange && this.props.observeSizeChange(this.selfNode);
  }

  componentWillUnmount() {
    this.props.unobserveSizeChange && this.props.unobserveSizeChange(this.selfNode, this.props.index);
  }

  UNSAFE_componentWillUpdate(nextProps) {
    if (!this.props.selected && nextProps.selected) {
      let $node = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(this);
      if (!$node) {
        return;
      }

      if ($node.scrollIntoViewIfNeeded) {
        $node.scrollIntoViewIfNeeded();
      } else
      {
        $node.scrollIntoView();
      }
    }
  }

  handleMouseOver() {
    this.setState({ isHovered: true });
  }

  handleMouseOut() {
    !this.state.isTooltipVisible && !this.state.dropdownOpen && this.setState({ isHovered: false });
  }

  showTooltip() {
    this.setState({ isTooltipVisible: true });
  }

  hideTooltip() {
    this.setState({ isTooltipVisible: false });
  }

  handleSelect(e) {
    e.stopPropagation();
    let modifiers = {
      ctrlKey: e.ctrlKey,
      metaKey: e.metaKey,
      shiftKey: e.shiftKey };

    this.props.onSelect && this.props.onSelect({ id: this.props.id, type: 'request' }, modifiers);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'history-sidebar-list-item': true,
      'history-sidebar-list-item__request': true,
      'is-hovered': this.state.dropdownOpen,
      'is-selected': this.props.selected });

  }

  handleDropdownToggle(value) {
    if (value === this.state.dropdownOpen) {
      return;
    }

    this.setState({ dropdownOpen: value });
  }

  handleDropdownActionSelect(action) {
    this.props.onSelectAction(action, {
      requests: [this.props.id],
      from: 'history_single' });

  }

  handleAddRequestToCollectionClick(e) {
    e.stopPropagation();
    this.handleDropdownActionSelect(_constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_SAVE"], { from: 'history_single' });
  }

  handleRemoveRequest(e) {
    e.stopPropagation();
    this.handleDropdownActionSelect(_constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_DELETE"]);
  }

  getTooltipText(isDisabled, isOffline) {
    if (!isDisabled) {
      return _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_14__["ACTION_TYPE_DELETE_REQUEST_TOOLTIP"];
    }

    if (isOffline) {
      return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_IS_OFFLINE"];
    }

    return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_NO_PERMISSION"];
  }

  getDisabledText(isDisabled, isOffline) {
    if (!isDisabled) {
      return;
    }

    if (isOffline) {
      return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_IS_OFFLINE"];
    }

    return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_NO_PERMISSION"];
  }

  getLeftAlignedComponents(isHovered, isMoreActionsDropdownOpen) {
    const classNames = classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'history-sidebar-list-item-updatedBy': true,
      hovered: isHovered });


    if (!isHovered && !isMoreActionsDropdownOpen) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar__indent' }));

    }

    let updatedByMember = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').teamMembers.get(this.props.lastUpdatedBy),
    updatedByMemberName = updatedByMember && (updatedByMember.name || updatedByMember.username);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: classNames,
          onMouseEnter: this.showTooltip,
          onMouseLeave: this.hideTooltip },


        !this.props.lastUpdatedBy || this.props.lastUpdatedBy === '0' ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], { name: 'icon-descriptive-user-stroke', className: 'pm-icon pm-icon-normal' }) :
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Avatar__WEBPACK_IMPORTED_MODULE_10__["default"], {
          ref: this.userAvatar,
          size: 'small',
          userId: this.props.lastUpdatedBy }),



        this.userAvatar.current &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_11__["Tooltip"], {
            immediate: true,
            show: this.state.isTooltipVisible,
            target: this.userAvatar.current,
            placement: 'right' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_11__["TooltipBody"], null,
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
                updatedByMemberName),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
                moment__WEBPACK_IMPORTED_MODULE_3___default()(this.props.createdAt).format('DD MMM YYYY, h:mm A')))))));







  }

  getRightAlignedComponents(isHovered, isMoreActionsDropdownOpen) {
    const classNames = classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'sidebar-list-item__right-section-actions': true,
      hovered: isHovered });


    if (!isHovered && !isMoreActionsDropdownOpen) return null;

    const permissionStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('PermissionStore'),
    { userCanSave } = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('OnlineStatusStore'),
    canDeleteHistory = userCanSave && permissionStore.can('delete', 'history', this.props.id);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classNames },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_7__["Button"], {
            className: 'history-sidebar-list-item__button__add',
            type: 'icon',
            onClick: this.handleAddRequestToCollectionClick,
            disabled: !userCanSave,
            tooltip: userCanSave ? _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_14__["ACTION_TYPE_SAVE_REQUEST_TOOLTIP"] : _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_IS_OFFLINE"] },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], { size: 'large', name: 'icon-action-add-stroke', className: 'history-sidebar-list-item__actions__add pm-icon pm-icon-normal' })),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_7__["Button"], {
            className: 'history-sidebar-list-item__button__delete',
            type: 'icon',
            onClick: this.handleRemoveRequest,
            disabled: !canDeleteHistory,
            tooltip: this.getTooltipText(!canDeleteHistory, !userCanSave) },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
            name: 'icon-action-delete-stroke',
            size: 'large',
            className: 'history-sidebar-list-item__actions__delete pm-icon pm-icon-normal' }))));




  }

  getActionsMenuItems(canDeleteHistory) {
    const { userCanSave, userCanSaveAndSync } = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('OnlineStatusStore');

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_9__["DropdownMenu"], {
          'align-right': true },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_9__["MenuItem"], {
            refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_SAVE"],
            disabled: !userCanSave,
            disabledText: _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_IS_OFFLINE"] },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, 'Save Request')),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_9__["MenuItem"], {
            refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_MONITOR"],
            disabled: !userCanSaveAndSync,
            disabledText: userCanSave ? _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_16__["DISABLED_IN_SCRATCHPAD_TOOLTIP"] : _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_IS_OFFLINE"] },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, 'Monitor Request')),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_9__["MenuItem"], {
            refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_DOCUMENT"],
            disabled: !userCanSaveAndSync,
            disabledText: userCanSave ? _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_16__["DISABLED_IN_SCRATCHPAD_TOOLTIP"] : _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_IS_OFFLINE"] },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, 'Document Request')),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_9__["MenuItem"], {
            refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_MOCK"],
            disabled: !userCanSaveAndSync,
            disabledText: userCanSave ? _runtime_constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_16__["DISABLED_IN_SCRATCHPAD_TOOLTIP"] : _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_15__["DISABLED_TOOLTIP_IS_OFFLINE"] },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, 'Mock Request')),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_9__["MenuItem"], {
            refKey: _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_13__["ACTION_TYPE_DELETE"],
            disabled: !userCanSave || !canDeleteHistory,
            disabledText: this.getDisabledText(!canDeleteHistory, !userCanSave) },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, 'Delete'),
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-shortcut' }, Object(_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_17__["getShortcutByName"])('delete')))));



  }

  render() {
    const permissionStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('PermissionStore'),
    canDeleteHistory = permissionStore.can('delete', 'history', this.props.id);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          onMouseOver: this.handleMouseOver,
          onMouseOut: this.handleMouseOut,
          'data-index': this.props.index },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_12__["default"], {
          text: this.props.url,
          onClick: this.handleSelect,
          isSelected: this.props.selected,
          icon: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_request_RequestIcon__WEBPACK_IMPORTED_MODULE_8__["default"], { method: this.props.method || 'GET' }),
          className: this.getClasses(),
          moreActions: this.getActionsMenuItems(canDeleteHistory),
          rightMetaComponent: this.getRightAlignedComponents,
          leftMetaComponent: this.getLeftAlignedComponents,
          onActionsDropdownSelect: this.handleDropdownActionSelect })));



  }}) || _class;

/***/ }),

/***/ 15026:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistoryListMetaItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _HistoryItemActionsDropdown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15019);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3016);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1835);
/* harmony import */ var _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15020);
/* harmony import */ var _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15021);
/* harmony import */ var _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3491);












const ADD_REQUEST_TO_COLLECTION_DISABLED_TOOLTIP = 'Not available to collection run items';let

HistoryListMetaItem = class HistoryListMetaItem extends react__WEBPACK_IMPORTED_MODULE_0__["PureComponent"] {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      isHovered: false };


    this.store = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('HistorySidebarStore');

    this.handleToggleGroup = this.handleToggleGroup.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);
    this.handleAddRequestToCollectionClick = this.handleAddRequestToCollectionClick.bind(this);
    this.handleRemoveRequest = this.handleRemoveRequest.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  handleMouseEnter() {
    this.setState({ isHovered: true });
  }

  handleMouseLeave() {
    !this.state.dropdownOpen && this.setState({ isHovered: false });
  }

  getCollapseButtonClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'caret': true,
      'pm-icon': true,
      'pm-icon-normal': true,
      'is-closed': this.store.isItemCollapsed(this.props.name) });

  }

  getMetaClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'history-sidebar-list-item-group__meta': true,
      'is-hovered': this.state.dropdownOpen });

  }

  handleToggleGroup() {
    let name = this.props.name;

    if (this.store.isItemCollapsed(name)) {
      return this.store.expandItem(name);
    }

    this.store.collapseItem(name);
  }

  handleDropdownToggle(value) {
    if (value === this.state.dropdownOpen) {
      return;
    }

    this.setState({ dropdownOpen: value });
  }

  handleDropdownActionSelect(action) {
    let modalOptions = {
      requests: _.reduce(this.props.items, (requests, item) => {
        item.type === 'request' && requests.push(item.id);

        return requests;
      }, []),
      runs: _.reduce(this.props.items, (runs, item) => {
        item.type === 'collectionrun' && runs.push(item.id);

        return runs;
      }, []),
      defaultCollectionName: this.props.name,
      from: 'history_date_group' };

    if (action === _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_DELETE"]) {
      pm.mediator.trigger('showDeleteHistoryModal', () => {
        this.props.onSelectAction(action, modalOptions);
      }, {
        identifier: 'selected' });

      return;
    }
    this.props.onSelectAction(action, modalOptions);
  }

  handleAddRequestToCollectionClick(e) {
    e.stopPropagation();
    this.handleDropdownActionSelect(_constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_SAVE"]);
  }

  handleRemoveRequest(e) {
    e.stopPropagation();
    this.handleDropdownActionSelect(_constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_DELETE"]);
  }

  render() {
    const hideActions = _.size(this.store.getSelectedItems()) > 1,
    { userCanSave } = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('OnlineStatusStore');

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: 'history-sidebar-list-item-group',
          onMouseEnter: this.handleMouseEnter,
          onMouseLeave: this.handleMouseLeave },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: this.getMetaClasses(),
            onClick: this.handleToggleGroup },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list-item-group__name' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Icon"], { name: 'icon-direction-down', className: this.getCollapseButtonClasses() }),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, this.props.name)),


          !hideActions && this.state.isHovered &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list-item-group__actions' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                className: 'history-sidebar-list-item__button__add',
                type: 'icon',
                disabled: !userCanSave || this.props.disableRequestSpecificActions,
                onClick: this.handleAddRequestToCollectionClick,
                tooltip:
                userCanSave ?
                this.props.disableRequestSpecificActions ?
                ADD_REQUEST_TO_COLLECTION_DISABLED_TOOLTIP :
                _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_7__["ACTION_TYPE_SAVE_REQUESTS_TOOLTIP"] :
                _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_8__["DISABLED_TOOLTIP_IS_OFFLINE"] },


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
                name: 'icon-action-add-stroke',
                className: 'history-sidebar-list-item__actions__add pm-icon pm-icon-normal' })),


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                className: 'history-sidebar-list-item__button__delete',
                type: 'icon',
                onClick: this.handleRemoveRequest,
                disabled: !userCanSave,
                tooltip: userCanSave ? _constants_HistoryActionsTooltipConstants__WEBPACK_IMPORTED_MODULE_7__["ACTION_TYPE_DELETE_REQUESTS_TOOLTIP"] : _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_8__["DISABLED_TOOLTIP_IS_OFFLINE"] },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Icon"], { name: 'icon-action-delete-stroke', className: 'history-sidebar-list-item__actions__delete pm-icon pm-icon-normal' })),

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HistoryItemActionsDropdown__WEBPACK_IMPORTED_MODULE_3__["default"], {
              count: _.size(this.props.items),
              onSelect: this.handleDropdownActionSelect,
              onToggle: this.handleDropdownToggle,
              disableRequestSpecificActions: this.props.disableRequestSpecificActions })))));






  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15027:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistoryListErrorItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


/**
                            * Error item for history list
                            *
                            * @param {Object} props - React props
                            */
function HistoryListErrorItem(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list-item-group' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list-item-group__error' }, 'Unable to load items. ',
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { onClick: props.onRetry }, 'Retry'))));



}

/***/ }),

/***/ 15028:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistoryListCollectionRunItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(308);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(297);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(80);
/* harmony import */ var _renderer_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6767);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3016);
/* harmony import */ var _base_Avatar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3043);
/* harmony import */ var _base_Tooltips__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3017);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(1835);
/* harmony import */ var _constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(15020);
/* harmony import */ var _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3491);
var _class;














const ACTION_TYPE_DELETE_RUN_TOOLTIP = 'Delete collection run';let


HistoryListCollectionRunItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_5__["observer"])(_class = class HistoryListCollectionRunItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
      isTooltipVisible: false,
      isHovered: false };


    this.userAvatar = react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef();
    this.handleSelect = this.handleSelect.bind(this);
    this.handleDropdownToggle = this.handleDropdownToggle.bind(this);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);
    this.handleRemoveRun = this.handleRemoveRun.bind(this);
    this.showTooltip = this.showTooltip.bind(this);
    this.hideTooltip = this.hideTooltip.bind(this);
    this.handleMouseOver = this.handleMouseOver.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.getRightAlignedComponents = this.getRightAlignedComponents.bind(this);
    this.getLeftAlignedComponents = this.getLeftAlignedComponents.bind(this);
  }


  componentDidMount() {
    this.selfNode = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(this);

    // Starts observing element
    this.props.observeSizeChange && this.props.observeSizeChange(this.selfNode);
  }

  componentWillUnmount() {
    this.props.unobserveSizeChange && this.props.unobserveSizeChange(this.selfNode, this.props.index);
  }

  componentWillUpdate(nextProps) {
    if (!this.props.selected && nextProps.selected) {
      let $node = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(this);
      $node && $node.scrollIntoViewIfNeeded && $node.scrollIntoViewIfNeeded();
    }
  }

  handleMouseOver() {
    this.setState({ isHovered: true });
  }

  handleMouseOut() {
    !this.state.isTooltipVisible && !this.state.dropdownOpen && this.setState({ isHovered: false });
  }

  showTooltip() {
    this.setState({ isTooltipVisible: true });
  }

  hideTooltip() {
    this.setState({ isTooltipVisible: false });
  }

  handleSelect(e) {
    e.stopPropagation();
    let modifiers = {
      ctrlKey: e.ctrlKey,
      metaKey: e.metaKey,
      shiftKey: e.shiftKey };

    this.props.onSelect && this.props.onSelect({ id: this.props.id, type: 'collectionrun' }, modifiers);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'history-sidebar-list-item': true,
      'history-sidebar-list-item__collection': true,
      'is-hovered': this.state.dropdownOpen,
      'is-selected': this.props.selected });

  }

  handleDropdownToggle(value) {
    if (value === this.state.dropdownOpen) {
      return;
    }

    this.setState({ dropdownOpen: value });
  }

  handleDropdownActionSelect(action) {
    this.props.onSelectAction(action, {
      runs: [this.props.id] });

  }

  handleRemoveRun(e) {
    e.stopPropagation();
    this.handleDropdownActionSelect(_constants_HistoryActionsConstants__WEBPACK_IMPORTED_MODULE_11__["ACTION_TYPE_DELETE"]);
  }

  getTooltipText(isDisabled) {
    if (!isDisabled) {
      return ACTION_TYPE_DELETE_RUN_TOOLTIP;
    }

    if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_10__["getStore"])('OnlineStatusStore').userCanSave) {
      return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_12__["DISABLED_TOOLTIP_IS_OFFLINE"];
    }

    return _runtime_constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_12__["DISABLED_TOOLTIP_NO_PERMISSION"];
  }

  getLeftAlignedComponents(isHovered, isMoreActionsDropdownOpen) {
    const classNames = classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'history-sidebar-list-item-updatedBy': true,
      hovered: isHovered });


    if (!isHovered && !isMoreActionsDropdownOpen) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar__indent' }));

    }

    let owner = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_10__["getStore"])('CurrentUserStore').teamMembers.get(this.props.owner),
    ownerName = owner && (owner.name || owner.username);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: classNames,
          onMouseEnter: this.showTooltip,
          onMouseLeave: this.hideTooltip },


        !this.props.owner || this.props.owner === '0' ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_3__["Icon"], { name: 'icon-descriptive-user-stroke', className: 'pm-icon pm-icon-normal' }) :
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Avatar__WEBPACK_IMPORTED_MODULE_8__["default"], {
          ref: this.userAvatar,
          size: 'small',
          userId: this.props.owner }),



        this.userAvatar.current &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
            immediate: true,
            show: this.state.isTooltipVisible,
            target: this.userAvatar.current,
            placement: 'right' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Tooltips__WEBPACK_IMPORTED_MODULE_9__["TooltipBody"], null,
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
                ownerName),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
                moment__WEBPACK_IMPORTED_MODULE_4___default()(this.props.createdAt).format('DD MMM YYYY, h:mm A')))))));







  }

  getRightAlignedComponents(isHovered, isMoreActionsDropdownOpen) {
    const classNames = classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'sidebar-list-item__right-section-actions': true,
      hovered: isHovered });


    if (!isHovered && !isMoreActionsDropdownOpen) return null;

    const permissionStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_10__["getStore"])('PermissionStore'),
    { userCanSave } = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_10__["getStore"])('OnlineStatusStore'),
    canDeleteCollectionRun = userCanSave && permissionStore.can('delete', 'collectionrun', this.props.id);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classNames },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_7__["Button"], {
            className: 'history-sidebar-list-item__button__delete',
            type: 'icon',
            onClick: this.handleRemoveRun,
            disabled: !canDeleteCollectionRun,
            tooltip: this.getTooltipText(!canDeleteCollectionRun) },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_3__["Icon"], {
            name: 'icon-action-delete-stroke',
            className: 'history-sidebar-list-item__actions__delete pm-icon pm-icon-normal' }))));




  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          onMouseOver: this.handleMouseOver,
          onMouseOut: this.handleMouseOut,
          'data-index': this.props.index },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_renderer_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_6__["default"], {
          text: this.props.name,
          icon: 'icon-action-run-stroke',
          isSelected: this.props.selected,
          onClick: this.handleSelect,
          className: this.getClasses(),
          rightMetaComponent: this.getRightAlignedComponents,
          leftMetaComponent: this.getLeftAlignedComponents })));



  }}) || _class;

/***/ }),

/***/ 15029:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HistorySidebarErrorState; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3016);



/**
                                           * Error state when loading history from server fails
                                           *
                                           * @param {Object} props - React props
                                           */
function HistorySidebarErrorState(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'history-sidebar-list-error' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('h4', null, 'Couldn\'t load history'),
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', null, 'Something went wrong while trying to load history items. Check your internet and try again.'),



      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_1__["Button"], {
          type: 'secondary',
          onClick: props.onRetry }, 'Retry')));





}

/***/ })

}]);