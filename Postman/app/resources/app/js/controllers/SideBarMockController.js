(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ 7118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SideBarMockController; });
/* harmony import */ var _stores_MockListStore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7119);
let

SideBarMockController = class SideBarMockController {
  didCreate() {
    this.mockListStore = new _stores_MockListStore__WEBPACK_IMPORTED_MODULE_0__["default"]();
  }

  beforeDestroy() {
    this.mockListStore && this.mockListStore.cleanUp();
    this.mockListStore = null;
  }};

/***/ }),

/***/ 7119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MockListStore; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _js_stores_BaseListStore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(423);
/* harmony import */ var _js_stores_BaseStore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3040);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(420);
/* harmony import */ var _services_MockService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4830);
/* harmony import */ var _services_FetchMockService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4855);
/* harmony import */ var _services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4648);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(69);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2304);
var _desc, _value, _class, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _desc2, _value2, _class3, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _descriptor13, _descriptor14;function _initDefineProp(target, property, descriptor, context) {if (!descriptor) return;Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 });}function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}function _initializerWarningHelper(descriptor, context) {throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');}








let

MockMetaItemStore = (_class = class MockMetaItemStore extends _js_stores_BaseStore__WEBPACK_IMPORTED_MODULE_2__["default"] {











  constructor(definition) {
    super();this.id = '';this.url = '';_initDefineProp(this, 'name', _descriptor, this);_initDefineProp(this, 'private', _descriptor2, this);_initDefineProp(this, 'collection', _descriptor3, this);_initDefineProp(this, 'environment', _descriptor4, this);_initDefineProp(this, 'createdAt', _descriptor5, this);_initDefineProp(this, 'config', _descriptor6, this);_initDefineProp(this, 'createdBy', _descriptor7, this);_initDefineProp(this, 'active', _descriptor8, this);

    this.id = definition.id;
    this.url = definition.url;

    this.update(definition);
  }


  update(definition) {
    _.has(definition, 'name') && (this.name = definition.name);
    _.has(definition, 'published') && (this.private = !definition.published);
    _.has(definition, 'collection') && (this.collection = definition.collection);
    _.has(definition, 'environment') && (this.environment = definition.environment);
    _.has(definition, 'createdAt') && (this.createdAt = definition.createdAt);
    _.has(definition, 'config') && (this.config = definition.config);
    _.has(definition, 'createdBy') && (this.createdBy = definition.createdBy);
    _.has(definition, 'active') && (this.active = definition.active);
  }}, (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'name', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return '';} }), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'private', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return '';} }), _descriptor3 = _applyDecoratedDescriptor(_class.prototype, 'collection', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return {};} }), _descriptor4 = _applyDecoratedDescriptor(_class.prototype, 'environment', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return {};} }), _descriptor5 = _applyDecoratedDescriptor(_class.prototype, 'createdAt', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return '';} }), _descriptor6 = _applyDecoratedDescriptor(_class.prototype, 'config', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return {};} }), _descriptor7 = _applyDecoratedDescriptor(_class.prototype, 'createdBy', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return {};} }), _descriptor8 = _applyDecoratedDescriptor(_class.prototype, 'active', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return true;} }), _applyDecoratedDescriptor(_class.prototype, 'update', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'update'), _class.prototype)), _class);let


MockListStore = (_class3 = class MockListStore extends _js_stores_BaseListStore__WEBPACK_IMPORTED_MODULE_1__["default"] {









  constructor() {
    super(MockMetaItemStore, { ignoreHydration: true });_initDefineProp(this, 'errorFetchingMocks', _descriptor9, this);_initDefineProp(this, 'searchQuery', _descriptor10, this);_initDefineProp(this, 'permissions', _descriptor11, this);_initDefineProp(this, 'isOffline', _descriptor12, this);_initDefineProp(this, 'activeItem', _descriptor13, this);_initDefineProp(this, 'focusedItem', _descriptor14, this);this.mockSubscriptionMap = new Map();this.syncStatusStoreReactionDisposer = null;

    this.setIsHydrating(true);

    const syncStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore');

    this.syncStatusStoreOfflineReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_0__["reaction"])(() => syncStore.isConsistentlyOffline, isConsistentlyOffline => {
      isConsistentlyOffline && (this.isHydrating || this.errorFetchingMocks) && this.setIsOffline(true);

      if (!isConsistentlyOffline) {
        this.setIsOffline(false);
      }
    }, {
      fireImmediately: true });


    this.syncStatusStoreSocketReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_0__["reaction"])(() => syncStore.isSocketConnected, isSocketConnected => {
      if (isSocketConnected) {
        if (this.errorFetchingMocks) {
          this.reload();
        } else
        {
          this.hydrate();
        }
      }

      /**
          * workaround for now
          * to resubscribe when the socket is reconnected
          *
          * Also, used fireImmediately to subscribe when the app loads
          * and socket is already connected
          */
      isSocketConnected && this.subscribeToWorkspaceRoom();
    },
    {
      fireImmediately: true });


    Object(mobx__WEBPACK_IMPORTED_MODULE_0__["reaction"])(() => Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore').isLoggedIn, isLoggedIn => {
      isLoggedIn && this.reload();
    });

    this.openMockDebounced = _.debounce(this.openMock, 300);
  }

  // The API isOffline is only supposed to be used to show the offline state view to the user when he has been consistently offline.
  // For disabling the actions on lack of connectivity, please rely on the socket connected state itself for now.
  // Otherwise, there is a chance of data loss if we let the user perform actions when we are attempting a connection.

  setIsOffline(isOffline) {
    this.isOffline = isOffline;
  }


  cleanUp() {
    // Dispose reaction
    this.syncStatusStoreSocketReactionDisposer && this.syncStatusStoreSocketReactionDisposer();
    this.syncStatusStoreOfflineReactionDisposer && this.syncStatusStoreOfflineReactionDisposer();

    // Clear and unsubscribe mock rooms
    this.clear();
  }


  setActiveItem(id) {
    id && (this.activeItem = id);
  }


  resetActiveItem() {
    this.activeItem = null;
  }


  setFocusedItem(id) {
    id && (this.focusedItem = id);
  }


  resetFocusedItem() {
    this.focusedItem = null;
  }


  get activeItemIndex() {
    return _.findIndex(this.filteredItems, mock => mock.id === this.activeItem);
  }


  get focusedItemIndex() {
    return _.findIndex(this.filteredItems, mock => mock.id === this.focusedItem);
  }

  openMock(mockId) {
    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_7__["default"].transitionTo(
    'build.mock',
    { mockId: mockId },
    {},
    { additionalContext: { origin: 'shortcut' } });


    // Reset focused item before active item is set
    this.resetFocusedItem();
  }

  focusNext() {
    if (!this.activeItem) {
      return;
    }

    if (_.isEmpty(this.filteredItems)) {
      this.resetActiveItem();

      this.resetFocusedItem();

      return;
    }

    if (!this.focusedItem) {
      this.activeItem && this.setFocusedItem(this.activeItem);
    }

    const nextItem = this.filteredItems[(this.focusedItemIndex + 1) % this.filteredItems.length].id;

    this.setFocusedItem(nextItem);

    this.openMockDebounced(nextItem);
  }

  focusPrev() {
    if (!this.activeItem) {
      return;
    }

    if (_.isEmpty(this.filteredItems)) {
      this.resetActiveItem();

      this.resetFocusedItem();

      return;
    }

    if (!this.focusedItem) {
      this.activeItem && this.setFocusedItem(this.activeItem);
    }

    const filteredItemsLength = this.filteredItems.length,
    prevItem = this.filteredItems[((this.focusedItemIndex - 1) % filteredItemsLength + filteredItemsLength) % filteredItemsLength].id;

    this.setFocusedItem(prevItem);

    this.openMockDebounced(prevItem);
  }


  setSearchQuery(query) {
    this.searchQuery = query;
  }


  updatePermissions(permissions) {
    this.permissions = permissions;
  }

  reload() {
    this.setIsHydrating(true);
    this.setErrorFetchingMocks(false);
    this.clear();
    this.hydrate();
  }

  hydrate() {
    return Object(_services_MockService__WEBPACK_IMPORTED_MODULE_4__["getMocks"])({ workspace: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id, inactive: true }).
    then(mocks => {
      this.clear();

      if (_.isEmpty(mocks)) {
        return;
      }

      this.add(mocks);
      this.setPermissions(mocks);
    }).
    then(() => {
      this.setErrorFetchingMocks(false);
      this.setIsHydrating(false);
    }).
    catch(err => {
      this.setIsHydrating(false);
      this.setErrorFetchingMocks(true);

      pm.logger.warn('Failed to fetch Mocks List', err);
    });
  }

  setPermissions(mocks) {
    const currentUser = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore'),
    userId = currentUser.id;

    Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_6__["default"])(mocks, userId).
    then(permissions => {
      this.updatePermissions(permissions);
    }).
    catch(err => {
      pm.logger.warn('Failed to get permissions', err);
    });
  }


  setErrorFetchingMocks(value) {
    !_.isNil(value) && (this.errorFetchingMocks = value);
  }

  handleError(error, errMsg) {
    let toastMessage = _.get(error, 'error.details.message') || _.get(error, 'error.message') || errMsg,
    toastTitle = _.get(error, 'error.title'),
    errorType = _.get(error, 'error.type');

    if (errorType === 'Socket Disconnected' || errorType === 'Request Timeout') {
      toastTitle = 'Something went wrong';
      toastMessage = 'Unable to complete this action. Please try again.';
    }

    pm.toasts.error(toastMessage, {
      noIcon: true,
      title: toastTitle });

  }


  updateName(oldMock, value) {
    oldMock.name = value;
  }

  rename(data, options = {}) {
    return Object(_services_MockService__WEBPACK_IMPORTED_MODULE_4__["updateMock"])(data).
    then(() => {
      const mock = this.find(data.id);

      this.updateName(mock, data.name);

      _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_8__["default"].addEventV2({
        category: 'mock',
        action: 'confirm_rename',
        label: options.origin,
        entityId: data.id });

    }).
    catch(err => {
      this.handleError(err, 'There was an unexpected error updating this Mock. Please try again.');

      _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_8__["default"].addEventV2({
        category: 'mock',
        action: 'failure_rename',
        label: options.origin,
        entityId: data.id });


      return Promise.reject();
    });
  }


  get filteredItems() {
    // get the filter term input in the sidebar
    const query = this.searchQuery.trim();

    // no or empty filter should return all values
    if (!query)
    return this.values;

    let lowerCasedFilterQuery = query.toLowerCase();

    // otherwise return only the mocks whose names have the search term in them.
    return this.values.filter(mock => {
      return _.includes(mock.name && _.toLower(mock.name), lowerCasedFilterQuery);
    });
  }

  /**
     * Override the BaseListStore add function
     * to subscribe the Mock item added in the list
     *
     * @param {Array|Object} mocks
     */

  add(mocks) {
    let mocksToAdd = _.isArray(mocks) ? mocks : [mocks];

    super.add(mocksToAdd);

    _.forEach(mocksToAdd, mock => {
      if (this.mockSubscriptionMap.has(mock.id)) {
        return;
      }

      this.mockSubscriptionMap.set(mock.id, this.subscribeToMockRoom(mock.id));
    });
  }

  /**
     * Override the BaseListStore remove function
     * to clear the subscription when the Mock item is
     * removed from the members
     *
     * @param {string} mockId
     */

  remove(mockId) {
    if (this.mockSubscriptionMap.has(mockId)) {
      this.mockSubscriptionMap.get(mockId).unsubscribe();
      this.mockSubscriptionMap.delete(mockId);
    }

    super.remove(mockId);
  }

  /**
     * Override the BaseListStore clear function
     * to clear all subscriptions when the members are cleared
     */

  clear() {
    for (let key of this.mockSubscriptionMap.keys()) {
      this.mockSubscriptionMap.get(key).unsubscribe();
    }

    this.mockSubscriptionMap.clear();
    super.clear();
  }

  /**
     * Subscribe to the workspace room
     */
  subscribeToWorkspaceRoom() {
    let workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id;

    return Object(_services_MockService__WEBPACK_IMPORTED_MODULE_4__["getAllRealtimeEvents"])(workspaceId).
    subscribe(event => {
      const eventModel = _.get(event, 'meta.model'),
      eventAction = _.get(event, 'meta.action'),
      eventData = _.get(event, 'data');

      if (_.isEmpty(eventModel) || eventModel !== 'mock') {
        return;
      }

      switch (eventAction) {
        case 'create':
          Object(_services_FetchMockService__WEBPACK_IMPORTED_MODULE_5__["fetchMock"])(eventData.mock, { populate: true }).
          then(mock => {
            this.add(mock);
          }).
          catch(err => {
            pm.logger.warn('Failed to fetch Mock', err);
          });

          break;
        case 'addedToWorkspace':
          eventData.mock && Object(_services_FetchMockService__WEBPACK_IMPORTED_MODULE_5__["fetchMock"])(eventData.mock, { populate: true }).
          then(mock => {
            this.add(mock);
          }).
          catch(err => {
            pm.logger.warn('Failed to fetch Mock', err);
          });

          break;}

    },
    error => pm.logger.warn('Failed to subscribe to workspace for mock events', error));
  }

  /**
     * Subscribe to Mock room
     *
     * @param {string} mockId
     */
  subscribeToMockRoom(mockId) {
    if (!mockId) return;

    return Object(_services_MockService__WEBPACK_IMPORTED_MODULE_4__["getAllRealtimeEvents"])(null, mockId).subscribe(event => {
      const eventModel = _.get(event, 'meta.model'),
      eventAction = _.get(event, 'meta.action'),
      eventData = _.get(event, 'data');

      if (_.isEmpty(eventModel) || eventModel !== 'mock') {
        return;
      }

      switch (eventAction) {
        case 'update':
          Object(_services_FetchMockService__WEBPACK_IMPORTED_MODULE_5__["fetchMock"])(eventData.mock, { populate: true }).
          then(mock => {
            if (_.isEmpty(mock)) {
              return;
            }

            if (this.find(mock.id)) {
              this.update(mock);
            }
          }).
          catch(err => {
            pm.logger.warn('Failed to fetch Mock', err);
          });

          break;
        case 'destroy':
          this.remove(eventData.mock);

          break;
        case 'updateRoles':
          const currentUser = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore'),
          userId = currentUser.id;

          Object(_services_FetchPermissionService__WEBPACK_IMPORTED_MODULE_6__["default"])([{ id: eventData.mock }], userId).
          then(permission => {
            this.updatePermissions(_.assign({}, this.permissions, permission));
          }).
          catch(err => {
            pm.logger.warn('Failed to get permission', err);
          });

          break;
        case 'removedFromWorkspace':
          this.remove(eventData.mock);

          break;}

    },
    error => pm.logger.warn('Failed to subscribe to mock room for events', error));
  }}, (_descriptor9 = _applyDecoratedDescriptor(_class3.prototype, 'errorFetchingMocks', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor10 = _applyDecoratedDescriptor(_class3.prototype, 'searchQuery', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return '';} }), _descriptor11 = _applyDecoratedDescriptor(_class3.prototype, 'permissions', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return {};} }), _descriptor12 = _applyDecoratedDescriptor(_class3.prototype, 'isOffline', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor13 = _applyDecoratedDescriptor(_class3.prototype, 'activeItem', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return null;} }), _descriptor14 = _applyDecoratedDescriptor(_class3.prototype, 'focusedItem', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return null;} }), _applyDecoratedDescriptor(_class3.prototype, 'setIsOffline', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'setIsOffline'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'cleanUp', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'cleanUp'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'setActiveItem', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'setActiveItem'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'resetActiveItem', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'resetActiveItem'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'setFocusedItem', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'setFocusedItem'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'resetFocusedItem', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'resetFocusedItem'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'activeItemIndex', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'activeItemIndex'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'focusedItemIndex', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'focusedItemIndex'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'setSearchQuery', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'setSearchQuery'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'updatePermissions', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'updatePermissions'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'setErrorFetchingMocks', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'setErrorFetchingMocks'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'updateName', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'updateName'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'filteredItems', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'filteredItems'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'add', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'add'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'remove', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'remove'), _class3.prototype), _applyDecoratedDescriptor(_class3.prototype, 'clear', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class3.prototype, 'clear'), _class3.prototype)), _class3);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);