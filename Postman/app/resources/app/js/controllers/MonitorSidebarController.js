(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[16],{

/***/ 15306:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorSidebarController; });
/* harmony import */ var _stores_domain_MonitorSidebarStore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15307);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(420);
/* harmony import */ var _services_UrlService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4985);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(74);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(444);




let

MonitorSidebarController = class MonitorSidebarController {

  didCreate() {
    this.store = new _stores_domain_MonitorSidebarStore__WEBPACK_IMPORTED_MODULE_0__["default"]();
    this.currentWorkspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceStore').id;

    // start the request to load data for sidebar
    this.store.refreshSidebarList(false);


    // @todo: When CFDTN-94 is resolved use that instead of a reaction.
    // whenever the focussed tab changes, we need to update our activeMonitorId
    this.activeTabDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_3__["reaction"])(() => {
      const editor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('EditorStore').find(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceSessionStore').activeEditor),
      monitorParams = Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_4__["decomposeUID"])(_.last(_.split(_.get(editor, 'resource', undefined), '/'))).modelId.split('~');

      return monitorParams.length >= 1 ? monitorParams.pop() : undefined;
    }, monitorId => {
      this.store.setActiveMonitorId(monitorId);
    }, {
      fireImmediately: true });

  }

  /**
     * Func opens a monitor tab based on monitor ID
     * Previously this accepted a monitor object, but we may not have the whole object often.
     * @param monitorId
     * @param monitorName
     */
  openMonitorTab(monitorId, monitorName) {
    _services_UrlService__WEBPACK_IMPORTED_MODULE_2__["default"].gotoMonitorUrl({ monitorId, monitorName });
  }

  openCreateMonitorTab() {
    _services_UrlService__WEBPACK_IMPORTED_MODULE_2__["default"].openCreateMonitorUrl();
  }

  /**
     * Func that handles the rename triggered from the sidebar actions
     * @param monitorId
     * @param name
     * @returns {Promise<void>}
     */
  async updateMonitorName({ monitorId, name }) {
    const body = {
      name: name };


    await this.store.masterMonitorStore.updateMonitorConfiguration({ monitorId, body });
  }

  beforeDestroy() {
    // we dispose off our reaction when we close the tab, as otherwise it will stay in the memory.
    this.activeTabDisposer();

    // before we switch workspace, let's unsubscribe to the realtime events for this one
    this.store.masterMonitorStore.realtimeManager.unsubscribe({ model: 'workspace', modelId: this.currentWorkspaceId });

    this.store = null;
  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(420);
/* harmony import */ var _MasterMonitorStore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4866);
var _desc, _value, _class, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5;function _initDefineProp(target, property, descriptor, context) {if (!descriptor) return;Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 });}function _initializerWarningHelper(descriptor, context) {throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');}function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;} /* eslint-disable lodash/prefer-lodash-chain */


let

MonitorSidebarStore = (_class = class MonitorSidebarStore {
  constructor() {_initDefineProp(this, 'loading', _descriptor, this);_initDefineProp(this, 'loaded', _descriptor2, this);_initDefineProp(this, 'error', _descriptor3, this);_initDefineProp(this, 'activeMonitorId', _descriptor4, this);_initDefineProp(this, 'searchQuery', _descriptor5, this);
    this.masterMonitorStore = new _MasterMonitorStore__WEBPACK_IMPORTED_MODULE_2__["default"]();
  }

  // Get offline status from master monitor store
  // @computed to ensure isOffline will never have a stale value

  get isOffline() {
    return this.masterMonitorStore.isOffline;
  }


  get values() {
    return this.masterMonitorStore.activeWorkspaceMonitorsMetadata.map(metadata => {
      return {
        isHealthy: metadata._healthy,
        isPaused: !metadata.active,
        id: metadata.id,
        name: metadata.name };

    });
  }

  /**
     * Func refreshes the data in the sidebar
     * Note that displayLoader can only control the display of loader after the first refresh,
     * the loader will always show up on the first call, because the this.loaded property is also false before the first load
     * @param displayLoader: controls whether to show a loading state or not(should be false for silent updates)
     * @returns {Promise<void>}
     */
  async refreshSidebarList(displayLoader = true) {
    // initially the workspaceId is empty
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceStore').id) {
      return;
    }

    // control if want to show the loading state or not
    if (displayLoader)
    this.setLoading(true);

    this.setError(false);

    try {
      await this.masterMonitorStore.loadActiveWorkspaceJobTemplates();
      this.masterMonitorStore.setSidebarOpenState(true);

      this.setLoading(false);
      this.setLoaded(true);
    }
    catch (e) {
      this.setError(true);
      this.setLoading(false);
    }
  }








  setSearchQuery(query) {
    this.searchQuery = query;
  }


  setError(status) {
    this.error = status;
  }


  setLoading(status) {
    this.loading = status;
  }


  setLoaded(status) {
    this.loaded = status;
  }


  setActiveMonitorId(monitorId) {
    this.activeMonitorId = monitorId;
  }


  get filteredItems() {
    // get the filter term input in the sidebar
    const query = this.searchQuery.trim();

    // no or empty filter should return all values
    if (!query)
    return this.values;

    let lowerCasedFilterQuery = query.toLowerCase();

    // otherwise return only the monitors whose names have the search term in them.
    return _.filter(this.values, monitor => {
      return _.includes(monitor.name && _.toLower(monitor.name), lowerCasedFilterQuery);
    });
  }}, (_applyDecoratedDescriptor(_class.prototype, 'isOffline', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'isOffline'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'values', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'values'), _class.prototype), _descriptor = _applyDecoratedDescriptor(_class.prototype, 'loading', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'loaded', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor3 = _applyDecoratedDescriptor(_class.prototype, 'error', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return false;} }), _descriptor4 = _applyDecoratedDescriptor(_class.prototype, 'activeMonitorId', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return undefined;} }), _descriptor5 = _applyDecoratedDescriptor(_class.prototype, 'searchQuery', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return '';} }), _applyDecoratedDescriptor(_class.prototype, 'setSearchQuery', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setSearchQuery'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setError', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setError'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setLoading', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setLoading'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setLoaded', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setLoaded'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setActiveMonitorId', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setActiveMonitorId'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'filteredItems', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'filteredItems'), _class.prototype)), _class);

/* harmony default export */ __webpack_exports__["default"] = (MonitorSidebarStore);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);