(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[43],{

/***/ 15418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return APISidebarController; });
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(420);
let

APISidebarController = class APISidebarController {
  didCreate() {
    // search query is reset when the controller is created.
    // this ensures that the filter on the data is cleared every time either when the workspace is switched
    // or when the sidebar component is mounted.
    // e.g, when the page loads or when the user switches back from the home page.
    this.store = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_0__["getStore"])('APISidebarStore');
    this.store.setSearchQuery('');
  }

  beforeDestroy() {
    this.store = null;
  }};

/***/ })

}]);