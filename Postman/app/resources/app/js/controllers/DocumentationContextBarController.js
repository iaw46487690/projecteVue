(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[41],{

/***/ 15411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DocumentationContextBarController; });
/* harmony import */ var _stores_DocumentationContextBarStore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15412);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(74);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(420);
var _desc, _value, _class;function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}

let

DocumentationContextBarController = (_class = class DocumentationContextBarController {constructor() {this.
    entityId = undefined;this.
    entityUid = undefined;this.
    entityType = undefined;this.
    owner = undefined;this.

    unsavedDescription = null;this.




























    updateUnsavedDescription = updatedDescription => {
      this.unsavedDescription = updatedDescription;
    };}get store() {const activeEditor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceSessionStore').activeEditor;const editor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('EditorStore').find(activeEditor);if (editor) {const resource = editor.resource;const v2ResourceBasePath = 'v2implview://stubView/';if (resource.startsWith(v2ResourceBasePath)) {const [entityType, entityUid] = resource.replace(v2ResourceBasePath, '').split('/');return new _stores_DocumentationContextBarStore__WEBPACK_IMPORTED_MODULE_0__["default"](entityType, entityUid);}}return null;}willDestroy() {this.codegen = undefined;} /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * Stores the unsavedDescription for the context bar
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     * {String} @param description - unsaved Description from the editor
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     */get contextBarUnsavedDescription() {return this.unsavedDescription;}}, (_applyDecoratedDescriptor(_class.prototype, 'store', [mobx__WEBPACK_IMPORTED_MODULE_1__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'store'), _class.prototype)), _class);

/***/ }),

/***/ 15412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DocumentationContextBarStore; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4152);
/* harmony import */ var _DocumentationMasterStore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5291);
var _desc, _value, _class, _class2, _temp;function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}


let

DocumentationContextBarStore = (_class = (_temp = _class2 = class DocumentationContextBarStore {






  /**
                                                                                                 * @param {String} entityType - The type of the entity
                                                                                                 * @param {String} entityUid - The uid of the entity in form of (ownerId-entityId)
                                                                                                 */
  constructor(entityType, entityUid) {
    this.entityType = entityType;
    this.entityUid = entityUid;

    this.masterStore = _DocumentationMasterStore__WEBPACK_IMPORTED_MODULE_2__["default"].getInstance();
  }

  get isOffline() {
    return this.masterStore.isOffline;
  }

  get isEditable() {
    return this.masterStore.isEditable;
  }

  get activeEnvironmentVariables() {
    return this.masterStore.activeEnvironmentVariables;
  }

  get isUserMemberOfActiveWorkspace() {
    return this.masterStore.isUserMemberOfActiveWorkspace;
  }


  get entityData() {
    let { data, status } = this.masterStore.getData({
      uid: this.entityUid,
      type: this.entityType,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_LATEST_VERSION_STRING"] },
    {
      withPublishedData: true });

    return {
      isLoading: status === _constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_STATUS"].LOADING,
      error: status === _constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_STATUS"].ERROR,
      data };

  }


  get parentCollectionData() {
    const { data } = this.entityData;

    if (this.entityType === _constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_ENTITY"].COLLECTION) {
      return data;
    }

    if (!data) {
      return undefined;
    }

    const collectionUid = `${data.owner}-${data.collection}`;

    return this.masterStore.getData({
      uid: collectionUid,
      type: _constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_ENTITY"].COLLECTION,
      versionTag: _constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_LATEST_VERSION_STRING"] }).
    data;
  }

  updateDescription(description) {
    this.masterStore.updateDescription(
    { type: this.entityType, uid: this.entityUid },
    description);

  }}, _class2.masterStoreGetMethod = { [_constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_ENTITY"].COLLECTION]: 'getCollection', [_constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_ENTITY"].FOLDER]: 'getFolder', [_constants__WEBPACK_IMPORTED_MODULE_1__["DOCUMENTATION_ENTITY"].REQUEST]: 'getRequest' }, _temp), (_applyDecoratedDescriptor(_class.prototype, 'entityData', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'entityData'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'parentCollectionData', [mobx__WEBPACK_IMPORTED_MODULE_0__["computed"]], Object.getOwnPropertyDescriptor(_class.prototype, 'parentCollectionData'), _class.prototype)), _class);

/***/ })

}]);