(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[59],{

/***/ 16060:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ArtemisBlanketState; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(420);
/* harmony import */ var _constants_ArtemisBlanketStateConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(16061);


let

ArtemisBlanketState = class ArtemisBlanketState extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super();
  }

  render() {
    const artemisBlanketStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ArtemisBlanketStore'),
    blanketIdentifier = artemisBlanketStore.identifier,
    BlanketComponent = _constants_ArtemisBlanketStateConstants__WEBPACK_IMPORTED_MODULE_2__["default"][blanketIdentifier];

    if (BlanketComponent) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(BlanketComponent,
        artemisBlanketStore.state));


    }
  }};

/***/ }),

/***/ 16061:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _collaboration_components_workspace_artemis_WorkspaceJoin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(16062);
/* harmony import */ var _collaboration_components_workspace_artemis_WorkspaceNotFound__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(16063);
/* harmony import */ var _collaboration_components_workspace_artemis_ShareEntityWorkspace__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(16064);
/* harmony import */ var _collaboration_components_workspace_artemis_ErrorState__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(16065);





const ArtemisBlanketStateConstants = {
  'unjoinedWorkspace': _collaboration_components_workspace_artemis_WorkspaceJoin__WEBPACK_IMPORTED_MODULE_0__["default"],
  'workspaceNotFound': _collaboration_components_workspace_artemis_WorkspaceNotFound__WEBPACK_IMPORTED_MODULE_1__["default"],
  'entityShare': _collaboration_components_workspace_artemis_ShareEntityWorkspace__WEBPACK_IMPORTED_MODULE_2__["default"],
  'error': _collaboration_components_workspace_artemis_ErrorState__WEBPACK_IMPORTED_MODULE_3__["default"] };


/* harmony default export */ __webpack_exports__["default"] = (ArtemisBlanketStateConstants);

/***/ }),

/***/ 16062:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return WorkspaceJoin; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(74);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3170);
/* harmony import */ var _services_DashboardService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7015);
/* harmony import */ var _js_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2290);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(420);
/* harmony import */ var _js_modules_model_event__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(429);
/* harmony import */ var _js_services_WorkspaceSwitchService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2288);







let

WorkspaceJoin = class WorkspaceJoin extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      isJoining: false };


    this.handleBrowseWorkspace = this.handleBrowseWorkspace.bind(this);
    this.handleWorkspaceJoin = this.handleWorkspaceJoin.bind(this);
  }

  handleBrowseWorkspace() {
    Object(_services_DashboardService__WEBPACK_IMPORTED_MODULE_3__["openWorkspace"])(this.props.workspaceId);
  }

  handleWorkspaceJoin() {
    if (!this.props.workspaceId) {
      return;
    }

    let joinWorkspaceEvent = Object(_js_modules_model_event__WEBPACK_IMPORTED_MODULE_6__["createEvent"])('join', 'workspace', { model: 'workspace', workspace: { id: this.props.workspaceId } });

    this.setState({
      isJoining: true });


    Object(_js_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_4__["default"])(joinWorkspaceEvent).
    then(() => {
      // This is done because after join it takes time for workspace to be hydrated in local DB
      this.workspaceStoreReaction = Object(mobx__WEBPACK_IMPORTED_MODULE_1__["reaction"])(() => {
        return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('WorkspaceStore').find(this.props.workspaceId);
      }, workspace => {
        if (_.isEmpty(workspace)) {
          this.setState({ isJoining: false });
          return this.workspaceStoreReaction && this.workspaceStoreReaction();
        }

        // Close the tab and set joining as false once workspace is present in DB
        this.setState({
          isJoining: false },
        () => {
          _js_services_WorkspaceSwitchService__WEBPACK_IMPORTED_MODULE_7__["default"].switchWorkspace(workspace.id).then(() => {
            Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ArtemisBlanketStore').clearState();
            return this.workspaceStoreReaction && this.workspaceStoreReaction();
          });
        });
      }, {
        fireImmediately: true });

    }).
    catch(e => {
      this.setState({
        isJoining: false });

    });
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-join' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-join__background' }),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-join__title' }, 'Join this workspace'),



        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-join__description' }, 'To work with any of the items in this workspace, first join the workspace.'),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: 'artemis-workspace-join__join-btn',
            type: 'primary',
            onClick: this.handleWorkspaceJoin,
            disabled: this.state.isJoining }, 'Join Workspace'),



        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            className: 'artemis-workspace-join__browse-btn',
            type: 'text',
            onClick: this.handleBrowseWorkspace }, 'Browse Workspace')));





  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 16063:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return WorkspaceNotFound; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3170);
/* harmony import */ var _services_DashboardService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7015);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(420);



let

WorkspaceNotFound = class WorkspaceNotFound extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleViewWorkspaces = this.handleViewWorkspaces.bind(this);
  }

  handleViewWorkspaces() {
    let type = !_.get(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore'), 'organizations.[0]') ? 'personal' : 'team';

    Object(_services_DashboardService__WEBPACK_IMPORTED_MODULE_2__["openWorkspaces"])(type);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-not-found' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-not-found__background' }),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-not-found__title' }, 'Workspace not found'),



        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-not-found__description' }, 'We can\'t find this workspace, make sure this isn\'t a private/personal workspace or it hasn\'t been deleted.'),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__["Button"], {
            className: 'artemis-workspace-not-found__view-workspaces-btn',
            type: 'primary',
            onClick: this.handleViewWorkspaces }, 'View All Workspaces')));





  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 16064:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ShareEntityWorkspace; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(74);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3170);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3211);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(420);
/* harmony import */ var _js_components_base_Icons_designSystemIcons_Icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3217);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3192);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _js_modules_model_event__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(429);
/* harmony import */ var _js_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2290);
/* harmony import */ var _constants_workspace__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3009);
/* harmony import */ var _js_services_WorkspaceDependencyService__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2281);
var _class, _desc, _value, _class2;function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}











let


ShareEntityWorkspace = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = (_class2 = class ShareEntityWorkspace extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      selectedWorkspace: null,
      isJoining: false,
      isSharing: false,
      joiningWorkspaceId: '' };


    this.workspaceStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('WorkspaceStore');

    this.handleWorkspaceJoin = this.handleWorkspaceJoin.bind(this);
    this.handleWorkspaceSelect = this.handleWorkspaceSelect.bind(this);
    this.handleShareEntity = this.handleShareEntity.bind(this);
    this.handleViewEntity = this.handleViewEntity.bind(this);
    this.postJoinWorkspace = this.postJoinWorkspace.bind(this);
  }


  postJoinWorkspace(workspaceId) {
    let workspace = _.find(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ArtemisBlanketStore').state.workspaces, ['id', workspaceId]);

    // set membership true as it has become true now
    _.set(workspace, 'state.isMember', true);
  }

  handleWorkspaceJoin(workspaceId) {
    if (!workspaceId) {
      return;
    }

    let joinWorkspaceEvent = Object(_js_modules_model_event__WEBPACK_IMPORTED_MODULE_9__["createEvent"])('join', 'workspace', { model: 'workspace', workspace: { id: workspaceId } });

    this.setState({
      isJoining: true,
      joiningWorkspaceId: workspaceId });


    Object(_js_modules_pipelines_user_action__WEBPACK_IMPORTED_MODULE_10__["default"])(joinWorkspaceEvent).
    then(() => {
      this.setState({
        isJoining: false,
        joiningWorkspaceId: '' },
      () => {
        return this.postJoinWorkspace(workspaceId);
      });
    }).
    catch(e => {
      this.setState({
        isJoining: false,
        joiningWorkspaceId: '' });

    });
  }

  handleWorkspaceSelect(workspace) {
    this.setState({ selectedWorkspace: _.find(this.props.workspaces, { 'id': workspace }) });
  }

  handleShareEntity() {
    this.setState({ isSharing: true });

    let workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ActiveWorkspaceStore').id,
    addEntity = [{
      entityType: this.props.entityType,
      entityId: this.props.entityId,
      workspaceId: workspaceId }];


    return _js_services_WorkspaceDependencyService__WEBPACK_IMPORTED_MODULE_12__["default"].modifyDependencies(addEntity).
    then(() => {
      return Promise.resolve().
      then(() => {
        return _.isFunction(this.props.postShareHandler) ? this.props.postShareHandler(this.props.entityId, workspaceId) :
        this.props.viewEntityHandler(this.props.entityId, workspaceId);
      }).
      then(() => {
        return this.setState({ isSharing: false });
      }).
      catch(e => {
        return this.setState({ isSharing: false });
      });
    }).
    catch(e => {
      return this.setState({ isSharing: false });
    });
  }

  handleViewEntity() {
    return _.isFunction(this.props.viewEntityHandler) && this.props.viewEntityHandler(this.props.entityId, _.get(this.state.selectedWorkspace, 'id'));
  }

  render() {
    const entityType = _constants_workspace__WEBPACK_IMPORTED_MODULE_11__["ENTITY_MAP"][this.props.entityType],
    entityName = this.props.entityName,
    workspaceName = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('ActiveWorkspaceStore').name,

    // This will be false right now. Once client foundation enable this we will have correct logic in place
    isCanonicalURL = this.props.isCanonicalURL;

    if (this.props.workspaces && isCanonicalURL && this.props.workspaces.length === 1) {
      _.isFunction(this.props.viewEntityHandler) && this.props.viewEntityHandler(this.props.entityId, _.get(this.props.workspaces, '[0].id'));
      return null;
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity__background' }),

        !this.props.isCanonicalURL &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity__title' },
            entityType, ' not in this workspace'),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity__description' }, 'The ',
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'artemis-workspace-share-entity__description-entity-name' }, entityName, ' '),
            entityType, ' is not present in the',
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'artemis-workspace-share-entity__description-workspace-name' }, ' ', workspaceName), ' workspace.'),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
              className: 'artemis-workspace-share-entity__share-btn',
              type: 'primary',
              onClick: this.handleShareEntity },


            this.state.isSharing ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__["default"], null) : `Share ${entityType} to this workspace`)),






        !_.isEmpty(this.props.workspaces) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity__workspace-action' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity__workspace-action-text' }, 'or ',
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('br', null), 'View ',
            entityType, ' in a ', isCanonicalURL ? '' : 'different ', 'workspace'),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity__workspace-action-actions' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["Dropdown"], {
                className: 'artemis-workspace-share-entity__dropdown',
                onSelect: this.handleWorkspaceSelect },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropdownButton"], {
                  className: 'artemis-workspace-share-entity__dropdown-btn',
                  type: 'secondary',
                  size: 'small' },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], null, _.get(this.state.selectedWorkspace, 'name', 'Select workspace'))),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropdownMenu"], {
                  fluid: true,
                  className: 'artemis-workspace-share-entity__dropdown-menu' },


                _.map(this.props.workspaces, workspace => {
                  let isWorkspaceUnjoined = !_.get(workspace, 'state.isMember', false);

                  return (
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["MenuItem"], {
                        className: 'artemis-workspace-share-entity__workspace-item',
                        key: workspace.id,
                        refKey: workspace.id,
                        disabled: isWorkspaceUnjoined },

                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                          className: classnames__WEBPACK_IMPORTED_MODULE_8___default()('artemis-workspace-share-entity__workspace-list', { 'unjoined': isWorkspaceUnjoined }) },

                        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_8___default()('artemis-workspace-share-entity__workspace', { 'unjoined': isWorkspaceUnjoined }) },
                          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Icons_designSystemIcons_Icon__WEBPACK_IMPORTED_MODULE_6__["default"], {
                            icon: 'icon-entity-workspaces-stroke' }),

                          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-share-entity__workspace-name' },
                            workspace.name)),



                        isWorkspaceUnjoined && (
                        this.state.isJoining && workspace.id === this.state.joiningWorkspaceId ?
                        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__["default"], null) :
                        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                            className: 'artemis-workspace-share-entity__join-btn',
                            type: 'text',
                            onClick: () => {this.handleWorkspaceJoin(workspace.id);} }, 'Join')))));








                }))),



            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                className: 'artemis-workspace-share-entity__view-btn',
                type: 'secondary',
                onClick: this.handleViewEntity }, 'View ',

              entityType)))));






  }}, (_applyDecoratedDescriptor(_class2.prototype, 'postJoinWorkspace', [mobx__WEBPACK_IMPORTED_MODULE_1__["action"]], Object.getOwnPropertyDescriptor(_class2.prototype, 'postJoinWorkspace'), _class2.prototype)), _class2)) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 16065:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ErrorState; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3170);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(69);
/* harmony import */ var _navigation_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2285);





/**
                                                                            * Handles switching of workspace to default workspace
                                                                            */
function handleWorkspaces() {
  return _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_2__["default"].transitionTo(_navigation_constants__WEBPACK_IMPORTED_MODULE_3__["ALL_WORKSPACES_IDENTIFIER"]);
}

/**
   * Generic Error state presentational component
   */
function ErrorState() {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-error-state' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-error-state__background' }),
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-error-state__title' }, 'Something Went Wrong'),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'artemis-workspace-error-state__description' }, 'Postman has encountered an error. If this problem persists, contact us at help@postman.com'),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_1__["Button"], {
          className: 'artemis-workspace-error-state__switch-btn',
          type: 'primary',
          onClick: handleWorkspaces }, 'Go to Workspaces')));





}

/***/ })

}]);