(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[29],{

/***/ 15364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentSidebarView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _EnvironmentSidebarContainer_EnvironmentSidebarContainer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15365);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3470);
/* harmony import */ var _EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15370);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(420);
var _class;







let


EnvironmentSidebarView = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class EnvironmentSidebarView extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    const { controller } = this.props,

    // The API isConsistentlyOffline is only supposed to be used to show the offline state view to the user
    // when he has been consistently offline.
    // For disabling the actions on lack of connectivity, please rely on the socket connected state itself for now.
    // Otherwise, there is a chance of data loss if we let the user perform actions
    // when we are attempting a connection.
    { isConsistentlyOffline } = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('SyncStatusStore');

    if (!pm.isScratchpad && isConsistentlyOffline && _.get(controller, 'model.isLoading')) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_4__["SidebarOfflineState"], null);
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EnvironmentSidebarContainer_EnvironmentSidebarContainer__WEBPACK_IMPORTED_MODULE_3__["default"], { model: controller.model });
  }}) || _class;


EnvironmentSidebarView.propTypes = {
  controller: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape({
    model: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__["default"]) }).
  isRequired };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentSidebarContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var _EnvironmentSidebarMenu_EnvironmentSidebarMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15366);
/* harmony import */ var _EnvironmentSidebarListContainer_EnvironmentSidebarListContainer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15367);
/* harmony import */ var _EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15370);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3159);
var _class;






let


EnvironmentSidebarContainer = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class EnvironmentSidebarContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'env-sidebar-container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EnvironmentSidebarMenu_EnvironmentSidebarMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
          model: this.props.model }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_6__["default"], { identifier: 'environment' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EnvironmentSidebarListContainer_EnvironmentSidebarListContainer__WEBPACK_IMPORTED_MODULE_4__["default"], {
            model: this.props.model }))));




  }}) || _class;


EnvironmentSidebarContainer.propTypes = {
  model: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__["default"]).isRequired };

/***/ }),

/***/ 15366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentSidebarMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(420);
/* harmony import */ var _api_EnvironmentInterface__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3002);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7125);
/* harmony import */ var _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3651);
var _class;







const NEW_ENV_CLICK_THROTTLE_TIMEOUT = 1000; // 1 sec
let

EnvironmentSidebarMenu = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class EnvironmentSidebarMenu extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = { isEnvCreateEnabled: true };

    this.handleCreate = this.handleCreate.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  handleCreate() {
    // Disable the create environment button for the defined timeout to prevent
    // a lot of environment creates in a short amount of time
    this.setState({ isEnvCreateEnabled: false });

    // Enable after the timeout has elapsed
    //
    // Cancel any pending timeout before setting a new timeout
    this.timeoutId && clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(
    () => {this.setState({ isEnvCreateEnabled: true });},
    NEW_ENV_CLICK_THROTTLE_TIMEOUT);


    Object(_api_EnvironmentInterface__WEBPACK_IMPORTED_MODULE_3__["createEnvironment"])({ openInTab: true });
  }


  handleSearchChange(query) {
    this.props.model && this.props.model.setSearchQuery(query);
  }

  render() {
    const { userCanSave } = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('OnlineStatusStore'),
    canAddEnvironment = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('PermissionStore').can('addEnvironment', 'workspace', Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore').id),
    isEnvCreateEnabled = userCanSave && canAddEnvironment && this.state.isEnvCreateEnabled;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_4__["default"], {
        createNewConfig: {
          tooltip: userCanSave ? // eslint-disable-line no-nested-ternary
          canAddEnvironment ?
          'Create new Environment' :
          _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_5__["DISABLED_TOOLTIP_NO_PERMISSION"] :

          _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_5__["DISABLED_TOOLTIP_IS_OFFLINE"],
          disabled: !isEnvCreateEnabled,
          onCreate: this.handleCreate,
          xPathIdentifier: 'addEnvironment' },

        onSearch: this.handleSearchChange,
        searchQuery: this.props.model.searchQuery }));


  }}) || _class;

/***/ }),

/***/ 15367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentSidebarListContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(74);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(80);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3172);
/* harmony import */ var _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5074);
/* harmony import */ var react_window__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3443);
/* harmony import */ var _EnvironmentSidebarListItem_EnvironmentSidebarListItem__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15368);
/* harmony import */ var _globals_sidebar_GlobalsSidebarListItem_GlobalsSidebarListItem__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15369);
/* harmony import */ var _js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3165);
/* harmony import */ var _EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(15370);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(420);
/* harmony import */ var _api_EnvironmentInterface__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3002);
/* harmony import */ var _helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(2790);
/* harmony import */ var _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3003);
/* harmony import */ var _constants_ModelConstant__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(2817);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(7128);
/* harmony import */ var _appsdk_sidebar_SidebarNoResultsFound_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(7127);
/* harmony import */ var _appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(5667);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(3159);
var _class;
























const OVERSCAN_COUNT = 10,
DEFAULT_ITEM_HEIGHT = 28;

/**
                           * Get input value from the inlineInput
                           *
                           * @param {Object} editStateFromInput - state of InlineInput
                           * @returns {String|undefined}
                           */
function getInputValueFromInputEditState(editStateFromInput) {
  return _.get(editStateFromInput, 'inlineInput.value');
}let



EnvironmentSidebarListContainer = _postman_react_click_outside__WEBPACK_IMPORTED_MODULE_4___default()(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class EnvironmentSidebarListContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    /**
                   * Cache for list item currently in edit (renaming) mode
                   *
                   * @type {Object}
                   * @property {Number} index - index of the list item in edit mode
                   * @property {Object} inlineInput - cache of inlineInput's state
                   */
    this.editCache = null;

    this.listItemRefs = {};

    this.getItemSize = this.getItemSize.bind(this);
    this.scrollToItem = this.scrollToItem.bind(this);
    this.setEditCache = this.setEditCache.bind(this);
    this.saveCachedValue = this.saveCachedValue.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);

    // Shortcut handlers
    this.focusNext = this.focusNext.bind(this);
    this.focusPrev = this.focusPrev.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.renameItem = this.renameItem.bind(this);
    this.duplicateItem = this.duplicateItem.bind(this);
  }

  componentDidMount() {
    this.scrollToViewReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_2__["reaction"])(() => _.get(this.props, 'model.activeItemIndex'), activeItemIndex => this.scrollToItem(activeItemIndex));

    // This is required to clear the height cache of items present after the first element
    // when the items shown are filtered and does not have `Globals` item in the list.
    this.resetHeightReactionDisposer = Object(mobx__WEBPACK_IMPORTED_MODULE_2__["reaction"])(() => _.get(this.props, 'model.items'), () => {this.listRef && this.listRef.resetAfterIndex(0);});
  }

  componentWillUnmount() {
    this.scrollToViewReactionDisposer && this.scrollToViewReactionDisposer();
    this.resetHeightReactionDisposer && this.resetHeightReactionDisposer();

    clearTimeout(this.clearCollectionLoadingTimer);
  }

  getSidebarEmptyState() {
    const canAddEnvironment = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_11__["getStore"])('PermissionStore').can('addEnvironment', 'workspace', Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_11__["getStore"])('ActiveWorkspaceStore').id);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_16__["default"], {
        illustration: 'no-environment',
        title: 'You don\u2019t have any environments.',
        message: 'An environment is a set of variables that allows you to switch the context of your requests.',
        action: canAddEnvironment && {
          label: 'Create Environment',
          handler: _api_EnvironmentInterface__WEBPACK_IMPORTED_MODULE_12__["createEnvironment"] } }));



  }

  getGlobalsListItem() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_19__["default"], { identifier: 'globals' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_globals_sidebar_GlobalsSidebarListItem_GlobalsSidebarListItem__WEBPACK_IMPORTED_MODULE_8__["default"], {
          model: this.props.model.globals })));



  }

  getItemSize() {
    return DEFAULT_ITEM_HEIGHT;
  }

  getListItem({ style, index }) {
    const { model } = this.props,
    item = _.get(model, ['items', index]);

    if (!item) {
      return null;
    }

    const { id: itemId } = item;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_19__["default"], { identifier: itemId },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { style: style },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EnvironmentSidebarListItem_EnvironmentSidebarListItem__WEBPACK_IMPORTED_MODULE_7__["default"], {
            ref: ref => {this.listItemRefs[itemId] = ref;},
            key: itemId,
            index: index,
            item: item,
            model: model,
            setEditCache: this.setEditCache,
            editCache: _.get(this.editCache, 'index') === index ? this.editCache : null }))));




  }

  getKeyMapHandlers() {
    return {
      quit: pm.shortcuts.handle('quit', this.setEditCache.bind(this, null)),
      select: pm.shortcuts.handle('select', this.saveCachedValue),
      nextItem: pm.shortcuts.handle('nextItem', this.focusNext),
      prevItem: pm.shortcuts.handle('prevItem', this.focusPrev),

      // @todo[EntityInTabs]: Stop firing of these shortcuts for Globals list item
      delete: pm.shortcuts.handle('delete', this.deleteItem),
      rename: pm.shortcuts.handle('rename', this.renameItem),
      duplicate: pm.shortcuts.handle('duplicate', this.duplicateItem) };

  }

  setEditCache(value) {
    this.editCache = value;
  }

  saveCachedValue() {
    if (!this.editCache) {
      return;
    }

    const { index, inlineInput: inlineInputEditState } = this.editCache,
    item = _.get(this.props.model, ['items', index]),
    valueToUpdate = getInputValueFromInputEditState(inlineInputEditState);

    // `valueToUpdate` can be an empty string which is a falsy values
    // but a valid one while the item is in edit mode,
    // hence checking for `isNil` (null/undefined)
    if (!item || _.isNil(valueToUpdate)) {
      return;
    }

    this.props.model.handleAction(item.id, _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_14__["ACTION_TYPE_RENAME"], { name: valueToUpdate });
  }

  focusNext(e) {
    e && e.preventDefault();
    this.props.model.focusNext();
  }

  focusPrev(e) {
    e && e.preventDefault();
    this.props.model.focusPrev();
  }

  deleteItem() {
    this.props.model.handleAction(_.get(this.props.model, 'activeItem'), _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_14__["ACTION_TYPE_DELETE"]);
  }

  renameItem() {
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_11__["getStore"])('OnlineStatusStore').userCanSave) {
      return Object(_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_13__["showOfflineActionDisabledToast"])();
    }

    // FIXME: This should find the index of the activeItem and then trigger the
    _.invoke(this.listItemRefs, [_.get(this.props.model, 'activeItem'), 'handleEditName']);
  }

  duplicateItem() {
    this.props.model.handleAction(_.get(this.props.model, 'activeItem'), _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_14__["ACTION_TYPE_DUPLICATE"]);
  }

  handleClickOutside(e) {
    const targetClassName = e && e.target && e.target.className,
    isClickFromInsideInput = targetClassName === 'input input-box inline-input';

    // Bailout if the click is coming from inside the inline input or
    // there is no cache present for any list item
    if (!this.editCache || isClickFromInsideInput) {
      return;
    }

    this.saveCachedValue();
  }

  scrollToItem(index) {
    !_.isEmpty(this.listRef) && !_.isNil(index) && this.listRef.scrollToItem(index);
  }

  render() {
    const { model } = this.props,
    itemLength = _.size(model.items);


    // Show loading state
    if (model.isLoading) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'env-sidebar-list-container' },
          this.getGlobalsListItem(),
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_18__["default"], null)));


    }

    // Items is all empty then we need to show some empty states
    if (_.isEmpty(model.items)) {
      // If the searchQuery empty
      if (_.isEmpty(model.searchQuery)) {
        return (
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'env-sidebar-list-container' },
            this.getGlobalsListItem(),
            this.getSidebarEmptyState()));


      }

      // If the search term is not empty and does not contain the term global
      if (!_constants_ModelConstant__WEBPACK_IMPORTED_MODULE_15__["GLOBALS"].includes(model.searchQuery)) {
        return (
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarNoResultsFound_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_17__["default"], { searchQuery: model.searchQuery, illustration: 'no-environment' }));

      }

      // Note: If it contains the term global then we just let it fall through
      // to be shown as part of the listing
    }

    const lister = data =>
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_3__["Observer"], null,
      this.getListItem.bind(this, data));



    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_9__["default"], {
          keyMap: pm.shortcuts.getShortcuts(),
          handlers: this.getKeyMapHandlers() },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'env-sidebar-list-container', onClick: this.handleClickOutside },
          this.getGlobalsListItem(),
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'env-sidebar-list' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_virtualized_auto_sizer__WEBPACK_IMPORTED_MODULE_5__["default"], null,
              ({ height, width }) =>
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_window__WEBPACK_IMPORTED_MODULE_6__["VariableSizeList"], {
                  height: height,
                  width: width,
                  itemCount: itemLength,
                  itemSize: this.getItemSize,
                  overscanCount: OVERSCAN_COUNT,
                  ref: ref => {this.listRef = ref;} },

                lister))))));







  }}) || _class) || _class;


EnvironmentSidebarListContainer.propTypes = {
  model: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.instanceOf(_EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_10__["default"]).isRequired };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15368:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentSidebarListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _EnvironmentMetaIcons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5052);
/* harmony import */ var _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3003);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(7122);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3470);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(420);
/* harmony import */ var _js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(3756);
/* harmony import */ var _base_atom__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3223);
/* harmony import */ var _constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(5024);
/* harmony import */ var _helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(2790);
/* harmony import */ var _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(3651);
var _class;















let


EnvironmentSidebarListItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class EnvironmentSidebarListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleEditName = this.handleEditName.bind(this);
    this.handleItemSelect = this.handleItemSelect.bind(this);
    this.handleRenameSubmit = this.handleRenameSubmit.bind(this);
    this.handleSetActiveEnvironment = this.handleSetActiveEnvironment.bind(this);
    this.handleRemoveActiveEnvironment = this.handleRemoveActiveEnvironment.bind(this);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);

    this.containerRef = this.containerRef.bind(this);
    this.getStatusIndicators = this.getStatusIndicators.bind(this);
    this.getDisabledText = this.getDisabledText.bind(this);
  }

  // ToDo: Need to move edit cache capability to SidebarListItem and implement it for all sidebars
  componentDidMount() {
    // If the list item was in edit (renaming) state, restore its state
    if (this.props.editCache && this.listItemRef) {
      this.handleEditName();

      this.listItemRef.setEditState(this.props.editCache.inlineInput);
    }
  }

  // ToDo: Need to move edit cache capability to SidebarListItem and implement it for all sidebars
  componentWillUnmount() {
    const { listItemRef } = this;

    if (!listItemRef) {
      return;
    }

    if (!listItemRef.isEditing()) {
      return;
    }

    // If the list item is in its edit state, cache the internal state
    this.props.setEditCache({
      index: this.props.index,
      inlineInput: listItemRef.getEditState() });

  }

  getStatusIndicators() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_EnvironmentMetaIcons__WEBPACK_IMPORTED_MODULE_5__["default"], {
        environment: this.props.item,
        showForkLabel: true }));


  }

  getRightAlignedContainer(isHovered, isMoreActionsDropdownOpen) {
    const classNames = classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'env-actions-icon-container': true,
      isHovered: isHovered || isMoreActionsDropdownOpen,
      isActive: this.props.item.isActive });


    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classNames },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_atom__WEBPACK_IMPORTED_MODULE_11__["Button"], {
              type: 'icon',
              tooltip: 'Set inactive',
              onClick: this.handleRemoveActiveEnvironment,
              className: 'sidebar-action-btn active-env-icon' },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_4__["Icon"], { name: 'icon-state-success-fill-small', size: 'large', color: 'content-color-primary' })),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_atom__WEBPACK_IMPORTED_MODULE_11__["Button"], {
              type: 'icon',
              tooltip: 'Set active',
              onClick: this.handleSetActiveEnvironment,
              className: 'sidebar-action-btn inactive-env-icon' },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_4__["Icon"], { name: 'icon-state-success-stroke', size: 'large', color: 'content-color-primary' }))),


        !isHovered && !isMoreActionsDropdownOpen && this.props.item.isActive && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'env-actions__more-actions-placeholder' })));


  }

  getDisabledText(isDisabled, actionType) {
    if (!isDisabled) {
      return;
    }

    if (pm.isScratchpad) {
      return _constants_ScratchpadConstants__WEBPACK_IMPORTED_MODULE_12__["DISABLED_IN_SCRATCHPAD_TOOLTIP"];
    }

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('OnlineStatusStore').userCanSave) {
      return _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_14__["DISABLED_TOOLTIP_IS_OFFLINE"];
    }

    const defaultMessage = _constants_DisabledTooltipConstants__WEBPACK_IMPORTED_MODULE_14__["DISABLED_TOOLTIP_NO_PERMISSION"],
    manageRolesMessage = 'You need to be signed-in to a team to perform this action',
    workspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ActiveWorkspaceStore').id;

    switch (actionType) {
      case _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_MANAGE_ROLES"]:
        return manageRolesMessage;

      case _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_REMOVE_FROM_WORKSPACE"]:
        if (Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('PermissionStore').can('removeEnvironment', 'workspace', workspaceId)) {
          return 'Get online to perform this action';
        }

        return defaultMessage;

      case _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_PULL_REQUEST"]:
        return 'Coming soon in a future release!';

      case _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_MERGE"]:
        return 'Coming soon in a future release!';

      default:
        return defaultMessage;}

  }

  getMenuItemIconClasses(label) {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({ 'dropdown-menu-item-icon': true }, { 'pm-icon': true }, { 'pm-icon-normal': true }, `menu-icon--${label}`);
  }

  getMenuItems() {
    const { model, item } = this.props;

    return _.map(model.getActions(item.id), action => {
      const item =
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'env-action-item' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' }, action.label),

        action.shortcut &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-shortcut' }, Object(_js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_10__["getShortcutByName"])(action.shortcut)));




      return !action.hide &&
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
          key: action.type,
          refKey: action.type,
          disabled: !action.isEnabled,
          disabledText: this.getDisabledText(!action.isEnabled, action.type) },

        action.xpathLabel ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_atom__WEBPACK_IMPORTED_MODULE_11__["XPath"], { identifier: action.xpathLabel }, item) : item);


    });
  }

  getActionsMenuItems() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_8__["DropdownMenu"], {
          className: 'env-dropdown-menu',
          'align-right': true },

        this.getMenuItems()));


  }

  containerRef(ele) {
    this.listItemRef = ele;
  }

  handleSetActiveEnvironment() {
    this.props.model.handleAction(this.props.item.id, _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_SET_ACTIVE"]);
  }

  handleRemoveActiveEnvironment() {
    this.props.model.handleAction(this.props.item.id, _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_SET_ACTIVE"], { noEnvironment: true });
  }

  handleEditName() {
    _.invoke(this.listItemRef, 'editText');
  }

  async handleRenameSubmit(newName) {
    await this.props.model.handleAction(this.props.item.id, _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_RENAME"], { name: newName });
    this.props.setEditCache(null);
  }

  async handleDropdownActionSelect(action) {
    // If action is of rename toggle then just handle it here
    if (action === _constants_EnvironmentActionsConstant__WEBPACK_IMPORTED_MODULE_6__["ACTION_TYPE_RENAME_TOGGLE"]) {
      if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('OnlineStatusStore').userCanSave) {
        return Object(_helpers_ToastHelpers__WEBPACK_IMPORTED_MODULE_13__["showOfflineActionDisabledToast"])();
      }

      return this.handleEditName();
    }

    await this.props.model.handleAction(this.props.item.id, action);
  }

  handleItemSelect() {
    this.props.model.openInTab(this.props.item.id);
  }

  render() {
    const { item, model } = this.props,
    rightAlignedContainer = (...args) =>
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_1__["Observer"], null,
      this.getRightAlignedContainer.bind(this, ...args));



    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_7__["default"], {
        ref: this.containerRef,
        text: item.name,
        isForked: !!(_.get(item, 'attributes.fork.forkId') || _.get(item, 'attributes.fork.id')),
        isSelected: model.activeItem === item.id,
        onClick: this.handleItemSelect,
        onRename: this.handleRenameSubmit,
        statusIndicators: this.getStatusIndicators,
        rightMetaComponent: rightAlignedContainer,
        moreActions: this.getActionsMenuItems(),
        onActionsDropdownSelect: this.handleDropdownActionSelect }));


  }}) || _class;



EnvironmentSidebarListItem.propTypes = {
  index: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number.isRequired,
  item: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,
  model: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,
  editCache: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object.isRequired,
  setEditCache: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func.isRequired };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GlobalsSidebarListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7122);
/* harmony import */ var _environment_sidebar_EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15370);
var _class;




let


GlobalsSidebarListItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class GlobalsSidebarListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);
  }

  getItemClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'globals-sidebar-list-item': true,
      selected: this.props.model.isActive });

  }

  handleSelect() {
    this.props.model.openInTab();
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_4__["default"], {
          text: 'Globals',
          isSelected: this.props.model.isActive,
          onClick: this.handleSelect }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'globals-separator' })));


  }}) || _class;


GlobalsSidebarListItem.propTypes = {
  model: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.instanceOf(_environment_sidebar_EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_5__["GlobalsModel"]).isRequired };

/***/ })

}]);