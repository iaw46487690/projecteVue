(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[25],{

/***/ 15355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionSidebarController; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15339);
/* harmony import */ var _adapters_CollectionModelEventsAdapter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4289);
/* harmony import */ var _adapters_FolderModelEventsAdapter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4287);
/* harmony import */ var _adapters_RequestModelEventsAdapter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4292);
/* harmony import */ var _adapters_ResponseModelEventsAdapter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4295);
/* harmony import */ var _adapters_CollectionListingAPIAdapter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15356);
/* harmony import */ var _js_modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2077);
/* harmony import */ var _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5015);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(420);
/* harmony import */ var _onboarding_src_common_dependencies__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2756);
/* harmony import */ var _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2829);
/* harmony import */ var _js_modules_controllers_PermissionController__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2730);
var _desc, _value, _class, _descriptor;function _initDefineProp(target, property, descriptor, context) {if (!descriptor) return;Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 });}function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {var desc = {};Object['ke' + 'ys'](descriptor).forEach(function (key) {desc[key] = descriptor[key];});desc.enumerable = !!desc.enumerable;desc.configurable = !!desc.configurable;if ('value' in desc || desc.initializer) {desc.writable = true;}desc = decorators.slice().reverse().reduce(function (desc, decorator) {return decorator(target, property, desc) || desc;}, desc);if (context && desc.initializer !== void 0) {desc.value = desc.initializer ? desc.initializer.call(context) : void 0;desc.initializer = undefined;}if (desc.initializer === void 0) {Object['define' + 'Property'](target, property, desc);desc = null;}return desc;}function _initializerWarningHelper(descriptor, context) {throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');}













// eslint-disable-next-line no-magic-numbers
const REQUEST_TIMEOUT = 30 * 1000; // 30 seconds

/**
 * Populates the Stores with Collection attributes
 *
 * @param {Array} collectionList List of collections in the current workspace
 */
function _populateStoresWithAttributes(collectionList) {
  if (_.isEmpty(collectionList)) {
    return;
  }

  const favoritedCollections = [],
  sharedCollections = [],
  publicCollections = [],
  archivedCollections = [];

  collectionList.forEach(collection => {
    const isFavorite = _.get(collection, 'attributes.flags.isFavorite'),
    isShared = _.get(collection, 'attributes.permissions.teamCanView'),
    isPublic = _.get(collection, 'attributes.permissions.anybodyCanView'),
    isArchived = _.get(collection, 'attributes.flags.isArchived'),
    collectionModelId = Object(_onboarding_src_common_dependencies__WEBPACK_IMPORTED_MODULE_10__["decomposeUID"])(collection.id).modelId,
    itemToPopulate = { id: collectionModelId };

    isFavorite && favoritedCollections.push(itemToPopulate);
    isShared && sharedCollections.push(itemToPopulate);
    isPublic && publicCollections.push({ id: collection.id });
    isArchived && archivedCollections.push({
      model: 'collection',
      modelId: collectionModelId });

  });

  !_.isEmpty(favoritedCollections) && Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('FavoritedCollectionStore').add(favoritedCollections);
  !_.isEmpty(sharedCollections) && Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('SharedCollectionsStore').add(sharedCollections);
  !_.isEmpty(publicCollections) && Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('PublicEntityStore').add(publicCollections);
  !_.isEmpty(archivedCollections) && Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ArchivedResourceStore').add(archivedCollections);
}let

CollectionSidebarController = (_class = class CollectionSidebarController {constructor() {this.
    model = null;this.
    _adapters = [];_initDefineProp(this, 'status', _descriptor, this);}

  // Sidebar Status


  async didCreate() {
    this.model = new _CollectionSidebarModel__WEBPACK_IMPORTED_MODULE_1__["default"]();

    if (window.SDK_PLATFORM !== 'browser') {
      // Additional step for integrating listing API for online app
      if (!pm.isScratchpad) {
        const collectionList = await this._fetchCollections();

        // Set sidebar ready after loading the collection list
        this.setSidebarStatus(_constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_8__["READY"]);

        _populateStoresWithAttributes(collectionList);
      }

      const session = await _js_modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_7__["default"].getActiveSession(),
      workspace = session && session.workspace;

      if (!workspace) {
        this.setSidebarStatus(_constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_8__["ERROR"]);

        return Promise.reject(new Error('CollectionSidebarController~didCreate: Could not find the active workspace'));
      }

      const collectionAdapter = new _adapters_CollectionModelEventsAdapter__WEBPACK_IMPORTED_MODULE_2__["default"](
      this.model.getCollectionAdapterModel(),
      {
        activeWorkspace: workspace,
        useUID: true }),


      folderAdapter = new _adapters_FolderModelEventsAdapter__WEBPACK_IMPORTED_MODULE_3__["default"](
      this.model.getFolderAdapterModel()),

      requestAdapter = new _adapters_RequestModelEventsAdapter__WEBPACK_IMPORTED_MODULE_4__["default"](
      this.model.getRequestAdapterModel()),

      hydrateEntities = [
      collectionAdapter.hydrate(),
      folderAdapter.hydrate(),
      requestAdapter.hydrate()];


      this._adapters.push(collectionAdapter, folderAdapter, requestAdapter);

      // We're hydrating the responses after the promise is resolved because
      // response needs the request to be hydrated in the sidebar model to
      // add references (requestModel.responses) to the responses. If response
      // is hydrated before its parent request, it cannot be linked to the
      // parent request and does not show up in the sidebar.
      return Promise.all(hydrateEntities).
      then(() => {
        const responseAdapter = new _adapters_ResponseModelEventsAdapter__WEBPACK_IMPORTED_MODULE_5__["default"](
        this.model.getResponseAdapterModel());


        this._adapters.push(responseAdapter);
        this.setSidebarStatus(_constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_8__["READY"]);

        return responseAdapter.hydrate();
      });
    }

    const listingAPIAdapter = new _adapters_CollectionListingAPIAdapter__WEBPACK_IMPORTED_MODULE_6__["default"](
    this.model.getCollectionAdapterModel());


    this._adapters.push(listingAPIAdapter);

    listingAPIAdapter.hydrate().
    then(() => {
      this.setSidebarStatus(_constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_8__["READY"]);
    }).
    catch(err => {
      this.setSidebarStatus(_constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_8__["ERROR"]);
      pm.logger.error('CollectionSidebarController~fetchCollections: Error in loading collection list', err);
    });
  }

  beforeDestroy() {
    this._adapters && this._adapters.forEach(adapter => {
      _.isFunction(adapter.dispose) && adapter.dispose();
    });

    this.model = null;
    this._adapters = [];
  }

  /**
     * Update status of the sidebar
     *
     * @param {String} value
     */

  setSidebarStatus(value) {
    this.status = value;
  }

  /**
     * Fetches collections from the Listing endpoint and hydrates the model
     *
     * @private
     * @return {Promise<Array<Object>>} - A promise that resolves with an array
     *    of collections fetched from the API
     */
  _fetchCollections() {
    // Wait for sync to connect
    return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('SyncStatusStore').onSyncAvailable().
    then(() => {
      const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ActiveWorkspaceStore').id;

      return _onboarding_src_common_dependencies__WEBPACK_IMPORTED_MODULE_10__["RemoteSyncRequestService"].request(_constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_11__["COLLECTION_LIST_AND_SUBSCRIBE"]`${workspace}`, {
        method: 'POST',
        timeout: REQUEST_TIMEOUT });

    }).
    then(collectionList => {
      /**
                             * Update the collection userCanUpdate attribute to manage the glitch on fixing
                             * the rendering for lock icon appearing and disappearing.
                             *
                             * TODO: Fix this appropriately
                             */

      // Also checks whether the permission exists for collections in `PermissionStore`
      const data = _.forEach(
      _.get(collectionList, 'body.data', []), collection => {
        const criteria = {
          model: 'collection',
          modelId: collection.id,
          action: 'UPDATE_COLLECTION' },

        compositeKey = _js_modules_controllers_PermissionController__WEBPACK_IMPORTED_MODULE_12__["default"].getCompositeKey(criteria),
        permissionPresent = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('PermissionStore').members.has(compositeKey);

        !permissionPresent && _.set(collection, 'attributes.permissions.userCanUpdate', true);
      });


      this.model.hydrate && this.model.hydrate(data);

      return data;
    });
  }}, (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'status', [mobx__WEBPACK_IMPORTED_MODULE_0__["observable"]], { enumerable: true, initializer: function () {return _constants_WorkbenchStatusConstants__WEBPACK_IMPORTED_MODULE_8__["LOADING"];} }), _applyDecoratedDescriptor(_class.prototype, 'setSidebarStatus', [mobx__WEBPACK_IMPORTED_MODULE_0__["action"]], Object.getOwnPropertyDescriptor(_class.prototype, 'setSidebarStatus'), _class.prototype)), _class);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionListingAPIAdapter; });
/* harmony import */ var _ListingAPIAdapter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15357);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(420);
/* harmony import */ var _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2829);
/* harmony import */ var _js_common_model_event__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(439);
/* harmony import */ var _js_common_model_event__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_js_common_model_event__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(444);






/**
                                                         * Adapter for Collection Listing API
                                                         *
                                                         * The model which uses this adapter should have these methods:
                                                         *   - hydrate (entities)
                                                         *   - add (entities)
                                                         *   - remove (ids)
                                                         *   - updateFavorite (id)
                                                         */let
CollectionListingAPIAdapter = class CollectionListingAPIAdapter extends _ListingAPIAdapter__WEBPACK_IMPORTED_MODULE_0__["default"] {
  getListAndSubscribeAPIEndpoint() {
    const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceStore').id;

    return _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__["COLLECTION_LIST_AND_SUBSCRIBE"]`${workspace}`;
  }

  getListAPIEndpoint() {
    const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceStore').id;

    return _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__["COLLECTION_LIST"]`${workspace}`;
  }

  dispose() {
    super.dispose();

    this.unsubscribeModelEvents && this.unsubscribeModelEvents();
  }

  _subscribeToChangeEvents(realtimeSubscriptionId) {
    super._subscribeToChangeEvents(realtimeSubscriptionId);

    // Currently there are no real-time events for favorite/unfavorite in
    // Listing API, so we're using the model-events to create a bridge in the
    // Listing adapter.
    // See: RUNTIME-2780

    // If the subscription for favorite/unfavorite already exists, clear it
    this.unsubscribeModelEvents && this.unsubscribeModelEvents();
    this.unsubscribeModelEvents = null;

    this.unsubscribeModelEvents = pm.eventBus.channel('model-events').subscribe(event => {
      if (Object(_js_common_model_event__WEBPACK_IMPORTED_MODULE_3__["getEventNamespace"])(event) !== 'collection') {
        return;
      }

      const eventName = Object(_js_common_model_event__WEBPACK_IMPORTED_MODULE_3__["getEventName"])(event);

      if (!['favorite', 'unfavorite'].includes(eventName)) {
        return;
      }

      const eventData = Object(_js_common_model_event__WEBPACK_IMPORTED_MODULE_3__["getEventData"])(event),
      collectionId = _.get(eventData, 'collection.id'),
      owner = _.get(eventData, 'collection.owner'),
      collectionUid = Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_4__["composeUID"])(collectionId, owner),
      isFavorite = eventName === 'favorite';

      // To-do: remove this when RUNTIME-2780 completed
      this.model.updateFavorite && this.model.updateFavorite(collectionUid, isFavorite);
    });
  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);