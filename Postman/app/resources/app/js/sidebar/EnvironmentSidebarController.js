(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[30],{

/***/ 15371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentSidebarController; });
/* harmony import */ var _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2282);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(420);
/* harmony import */ var _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2829);
/* harmony import */ var _EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(15370);
/* harmony import */ var _js_modules_controllers_PermissionController__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2730);
/* harmony import */ var _adapters_EnvironmentListingAPIAdapter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15372);
/* harmony import */ var _adapters_EnvironmentModelEventsAdapter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4274);








/**
                                                                                                 * Populates the Stores with Environment attributes
                                                                                                 *
                                                                                                 * @param {Array} environments List of environments in the current workspace
                                                                                                 */
function _populateStoresWithAttributes(environments) {
  if (_.isEmpty(environments)) {
    return;
  }

  const publicEnvironments = [],
  sharedEnvironments = [];

  environments.forEach(environment => {
    const isPublic = _.get(environment, 'attributes.permissions.anybodyCanView'),
    isShared = _.get(environment, 'attributes.permissions.teamCanView');

    isPublic && publicEnvironments.push({ id: environment.id });
    isShared && sharedEnvironments.push({ id: environment.id });
  });

  !_.isEmpty(publicEnvironments) && Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('PublicEntityStore').add(publicEnvironments);
  !_.isEmpty(sharedEnvironments) && Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('SharedEnvironmentsStore').add(sharedEnvironments);
}let

EnvironmentSidebarController = class EnvironmentSidebarController {
  constructor() {
    this.model = null;
    this.adapter = null;
  }

  async didCreate() {
    this.model = new _EnvironmentSidebarModel__WEBPACK_IMPORTED_MODULE_3__["default"](this);

    this.model.setLoading && this.model.setLoading(true);

    // If this is Native App, we use the sync API and cache stores for realtime
    // updates
    if (window.SDK_PLATFORM !== 'browser') {
      // Additional step for integrating listing API for online app
      if (!pm.isScratchpad) {
        const environmentListResponse = await this.fetchEnvironments(),
        environments = _.get(environmentListResponse, 'body.data', []);

        _populateStoresWithAttributes(environments);
      }

      this.adapter = new _adapters_EnvironmentModelEventsAdapter__WEBPACK_IMPORTED_MODULE_6__["default"](this.model);
    } else {
      this.adapter = new _adapters_EnvironmentListingAPIAdapter__WEBPACK_IMPORTED_MODULE_5__["default"](this.model);
    }

    await this.adapter.hydrate();

    this.model.setLoading && this.model.setLoading(false);
  }

  async fetchEnvironments() {
    // Wait for sync to connect
    await Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('SyncStatusStore').onSyncAvailable();

    const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceStore').id,

    // When sidebar loads then make a call the sync to get the list of environments
    environmentList = await _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_0__["default"].request(
    _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__["ENVIRONMENT_LIST_AND_SUBSCRIBE"]`${workspace}`,
    { method: 'POST' }),


    /**
                            * Update the environment userCanUpdate attribute to manage the glitch on fixing
                            * the rendering for lock icon appearing and disappearing.
                            *
                            * TODO: Fix this appropriately
                            */

    // Also checks whether the permission exists for environments in `PermissionStore`
    environments = _.forEach(
    _.get(environmentList, 'body.data', []), environment => {
      const criteria = {
        model: 'environment',
        modelId: environment.id,
        action: 'UPDATE_ENVIRONMENT' },

      compositeKey = _js_modules_controllers_PermissionController__WEBPACK_IMPORTED_MODULE_4__["default"].getCompositeKey(criteria),
      permissionPresent = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('PermissionStore').members.has(compositeKey);

      !permissionPresent && _.set(environment, 'attributes.permissions.userCanUpdate', true);
    });


    this.model.hydrate && this.model.hydrate(environments);
    this.model.setLoading && this.model.setLoading(false);

    return environmentList;
  }

  beforeDestroy() {
    this.model.dispose();
    this.model = null;

    this.adapter && this.adapter.dispose();
    this.adapter = null;
  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentListingAPIAdapter; });
/* harmony import */ var _ListingAPIAdapter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15357);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(420);
/* harmony import */ var _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2829);




/**
                               * Adapter for Environment Listing API
                               *
                               * The model which uses this adapter should have these methods:
                               *   - hydrate (entities)
                               *   - add (entity)
                               *   - remove (ids)
                               */let
EnvironmentListingAPIAdapter = class EnvironmentListingAPIAdapter extends _ListingAPIAdapter__WEBPACK_IMPORTED_MODULE_0__["default"] {
  getListAndSubscribeAPIEndpoint() {
    const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceStore').id;

    return _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__["ENVIRONMENT_LIST_AND_SUBSCRIBE"]`${workspace}`;
  }

  getListAPIEndpoint() {
    const workspace = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_1__["getStore"])('ActiveWorkspaceStore').id;

    return _constants_SyncEndpoints__WEBPACK_IMPORTED_MODULE_2__["ENVIRONMENT_LIST"]`${workspace}`;
  }};

/***/ })

}]);