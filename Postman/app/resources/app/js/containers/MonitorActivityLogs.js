(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[19],{

/***/ 15319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityLogs; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3667);
/* harmony import */ var _components_activity_logs_MonitorActivityFeed__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15320);
/* harmony import */ var _components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15325);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(420);
/* harmony import */ var _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4865);
/* harmony import */ var _services_UrlService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4985);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3170);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(109);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _components_activity_logs_MonitorActivityFilter__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(15326);
/* harmony import */ var _utils_messages__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(4868);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;













const DEBOUNCE_TIMEOUT = 600;let


MonitorActivityLogs = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class MonitorActivityLogs extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);this.

































    handleSearch = _.debounce(filter => {
      const monitorId = _services_UrlService__WEBPACK_IMPORTED_MODULE_8__["default"].getMonitorIdFromActiveRoute();

      this.setState({ isFilterApplied: true, filter });
      this.props.controller.monitorActivityStore.getActivitiesByMonitor({
        monitorId,
        members: Object.keys(filter.members),
        page: 1 },
      true);
      this.props.controller.monitorActivityStore.setActiveMonitorId(monitorId);
    }, DEBOUNCE_TIMEOUT);this.state = { isFilterApplied: false, filter: { members: {} } };this.handleLoadMore = this.handleLoadMore.bind(this);this.canViewActivityLogs = this.canViewActivityLogs.bind(this);this.handleRefresh = this.handleRefresh.bind(this);this.handleSearch = this.handleSearch.bind(this);this.getRefreshTextClass = this.getRefreshTextClass.bind(this);this.getWorkspaceUsers = this.getWorkspaceUsers.bind(this);this.handleFilterSelect = this.handleFilterSelect.bind(this);this.monitorPermissionStore = new _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_7__["default"]();}getRefreshTextClass() {return classnames__WEBPACK_IMPORTED_MODULE_2___default()({ 'pm-activity-section__refresh__btn': true, 'is-loading': this.props.controller.monitorActivityStore.loading });}handleLoadMore() {this.props.controller.monitorActivityStore.getActivitiesByMonitor({ monitorId: this.props.controller.monitorActivityStore.activeMonitorId, members: Object.keys(this.state.filter.members), page: this.props.controller.monitorActivityStore.nextPage }, false);this.props.controller.monitorActivityStore.setMoreLoading(true);}

  canViewActivityLogs() {
    // contextData here refers to the MonitorConfigurationStore
    return this.monitorPermissionStore.can('viewActivityLogs', this.props.contextData.id);
  }

  handleRefresh() {
    const monitorId = _services_UrlService__WEBPACK_IMPORTED_MODULE_8__["default"].getMonitorIdFromActiveRoute();

    this.setState({
      isFilterApplied: false,
      filter: { members: {} } });


    this.props.controller.monitorActivityStore.getActivitiesByMonitor({ monitorId, page: 1 }, true);
    this.props.controller.monitorActivityStore.setActiveMonitorId(monitorId);
  }

  handleFilterSelect(id, users, isSelected) {
    let updatedSelectedMembers = {};

    if (isSelected) {
      updatedSelectedMembers = _extends({},
      this.state.filter.members, {
        [id]: _.find(users, { id }) });

    } else {
      updatedSelectedMembers = _.omitBy(this.state.filter.members, { id });
    }

    this.setState({
      isFilterApplied: true,
      filter: {
        members: updatedSelectedMembers } },

    () => {
      this.handleSearch(this.state.filter);
    });
  }

  getWorkspaceUsers() {
    const members = _.get(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('ActiveWorkspaceStore'), 'members');
    const users = {};

    _.forEach(members, member => {
      let memberDetails = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').teamMembers.get(member.id);

      if (!_.isEmpty(memberDetails)) {
        users[member.id] = _.omit(_extends({}, memberDetails), ['role']);
      }
    });

    return users;
  }

  render() {
    let workspaceUsers = this.getWorkspaceUsers();

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'contextCustomScroll' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__["ContextBarViewHeader"], {
            title: this.props.title,
            onClose: this.props.onClose },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_9__["Button"], {
              onClick: this.handleRefresh,
              className: this.getRefreshTextClass(),
              tooltip:
              this.props.controller.monitorActivityStore.isOffline ? _utils_messages__WEBPACK_IMPORTED_MODULE_12__["TOOLTIP_TEXT"].isOffline :
              _utils_messages__WEBPACK_IMPORTED_MODULE_12__["TOOLTIP_TEXT"].isOnline,

              disabled: this.props.controller.monitorActivityStore.isOffline ||
              this.props.controller.monitorActivityStore.loading },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_10___default.a, {
              className: 'pm-activity-section__refresh__btn__icon',
              name: 'icon-action-refresh-stroke',
              color: 'content-color-primary',
              size: 'large' })),


          Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').isLoggedIn && workspaceUsers &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_activity_logs_MonitorActivityFilter__WEBPACK_IMPORTED_MODULE_11__["default"], {
            filtersApplied: this.state.filter,
            loadingUsers: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').isHydrating || !_.size(workspaceUsers),
            handleRefresh: this.handleRefresh,
            handleSearch: this.handleSearch,
            isFilterApplied: this.state.isFilterApplied,
            workspaceUsers: workspaceUsers,
            handleSelect: this.handleFilterSelect,
            disabled: this.props.controller.monitorActivityStore.isOffline,
            tooltip: this.props.controller.monitorActivityStore.isOffline ?
            _utils_messages__WEBPACK_IMPORTED_MODULE_12__["TOOLTIP_TEXT"].isOffline : _utils_messages__WEBPACK_IMPORTED_MODULE_12__["ACTIVITY_LOGS_FILTER"] })),




        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-activity' },

          this.props.controller.monitorActivityStore.isOffline && !this.props.controller.monitorActivityStore.values.length ?
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_5__["default"], null) :

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_activity_logs_MonitorActivityFeed__WEBPACK_IMPORTED_MODULE_4__["default"], {
            isFilterApplied: this.state.isFilterApplied,
            activities: this.props.controller.monitorActivityStore.values,
            teamUsers: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('TeamStore').allTeamMembers,
            nextPage: this.props.controller.monitorActivityStore.nextPage,
            loading: this.props.controller.monitorActivityStore.loading,
            error: this.props.controller.monitorActivityStore.error,
            loaded: this.props.controller.monitorActivityStore.loaded,
            loadingMore: this.props.controller.monitorActivityStore.loadingMore,
            handleLoadMore: this.handleLoadMore,
            listLoading: this.props.controller.monitorActivityStore.listLoading,
            moreLoading: this.props.controller.monitorActivityStore.moreLoading,
            canViewActivityLogs: this.canViewActivityLogs(),
            isOffline: this.props.controller.monitorActivityStore.isOffline }))));





  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadMore", function() { return LoadMore; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchTeamUsers", function() { return fetchTeamUsers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityFeed; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(10);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3170);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3192);
/* harmony import */ var _MonitorActivityItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15321);
/* harmony import */ var _NoMonitorActivity__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15323);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(420);
/* harmony import */ var _js_components_empty_states_SignInModal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4891);
/* harmony import */ var _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3947);
/* harmony import */ var _common_MonitorContextBarError__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(15324);
/* harmony import */ var _utils_messages__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4868);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};











function formatDateAsMoment(datetime) {
  const time = new Date(datetime),
  monthName = _utils_messages__WEBPACK_IMPORTED_MODULE_10__["MONTH_NAMES"][time.getMonth()],
  date = `${time.getDate()},`,
  year = time.getFullYear(),
  formattedDate = [monthName, date, year].join(' '),
  relativeDays = getRelativeDays(datetime);

  if (relativeDays === 0) {
    return 'Today';
  } else
  if (relativeDays === -1) {
    return 'Yesterday';
  }

  return formattedDate;
}

function getRelativeDays(datetime) {
  const time = new Date(datetime),
  dateWithoutTime = new Date(time.toDateString()),
  currentTime = new Date(),
  currentDateWithoutTime = new Date(currentTime.toDateString()),
  diffTime = Math.abs(dateWithoutTime - currentDateWithoutTime),
  diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

  if (dateWithoutTime < currentDateWithoutTime) {
    return -diffDays;
  }

  return diffDays;
}

/**
   * getActivitySections
   * This function groups the activity feed based on the date the action was performed
   *
   * @param {Array} activityFeed  Array containing all the activity in descending order
   * @returns {Array} Array containing activity grouped based on the date it happened
   *
   */
function getActivitySections(activityFeed) {
  if (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activityFeed)) {
    return [];
  }

  let prevGroupDate = '',
  sections = [];

  activityFeed.forEach(feed => {
    let currentGroupDate = new Date(feed.timestamp).toDateString(),
    dateSection = formatDateAsMoment(feed.timestamp);

    if (currentGroupDate !== prevGroupDate) {
      prevGroupDate = currentGroupDate;
      sections.push({
        name: dateSection,
        items: [feed] });

    } else
    {
      sections[sections.length - 1].items.push(feed);
    }
  });

  return sections;
}

/**
   * Load more activity link for monitor activity
   *
   * @param {Object} props - react props
   * @param {Function} props.onClick - function to call on link click
   * @returns {JSX} div with load more link
   */
function LoadMore(props) {

  let content;

  if (!props.show) {
    return null;
  }

  if (props.loading) {
    content = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__["default"], { className: 'loader-activity-logs' });
  } else
  if (!props.nextPage) {
    content = _utils_messages__WEBPACK_IMPORTED_MODULE_10__["ACTIVITY_LOGS_LIST_END"];
  } else
  {
    content =
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        className: 'loader-link',
        onClick: props.onClick,
        disabled: props.isOffline,
        tooltip: props.isOffline && _utils_messages__WEBPACK_IMPORTED_MODULE_10__["TOOLTIP_TEXT"].isOffline },

      _utils_messages__WEBPACK_IMPORTED_MODULE_10__["ACTIVITY_LOGS_LOAD_MORE"]);


  }

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-activity__load-more' }, content);
}

function fetchTeamUsers(data) {
  let result = Object.assign({}, ...Object.entries(_extends({}, data)).map(([a, b]) => ({ [Number(b.id)]: b })));
  return result;
}

function MonitorActivityFeed(props) {
  const { activities, loading, nextPage, moreLoading, error } = props,
  groupedActivities = getActivitySections(activities);

  function handleSignIn() {
    _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_8__["default"].initiateLogin({ isSignup: false });
  }

  function handleSignUp() {
    _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_8__["default"].initiateLogin({ isSignup: true });
  }

  if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_6__["getStore"])('CurrentUserStore').isLoggedIn) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-logs-public-view' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_empty_states_SignInModal__WEBPACK_IMPORTED_MODULE_7__["default"], {
          title: _utils_messages__WEBPACK_IMPORTED_MODULE_10__["SIGN_IN_MESSAGE"].title,
          subtitle: _utils_messages__WEBPACK_IMPORTED_MODULE_10__["SIGN_IN_MESSAGE"].subTitle,
          onSignIn: handleSignIn,
          onCreateAccount: handleSignUp })));



  } else
  if (!props.canViewActivityLogs) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-logs-public-view' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'empty-state-activity-logs' }),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-logs-public-view__entity' },
          _utils_messages__WEBPACK_IMPORTED_MODULE_10__["PERMISSION_TEXT"].CONSOLE_DISABLED_MSG)));



  } else
  if (error) {
    // Show an error msg if we encountered an error while loading the logs
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_MonitorContextBarError__WEBPACK_IMPORTED_MODULE_9__["default"], null));

  } else
  if (loading && !moreLoading) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__["default"], { className: 'loader-activity-logs' }));

  } else
  if (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activities) && loading) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_3__["default"], { className: 'loader-activity-logs' }));

  } else
  if (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activities) && !loading) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_NoMonitorActivity__WEBPACK_IMPORTED_MODULE_5__["default"], { isFilterApplied: props.isFilterApplied }));

  }

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-activity-feed' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-activity-section-container' },

        lodash__WEBPACK_IMPORTED_MODULE_1___default.a.map(groupedActivities, subActivities => {
          let currentFeedUserId = '',
          showUserIcon = false;

          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                className: 'pm-activity-section',
                key: subActivities.name },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-monitor-activity-section__date' },
                subActivities.name),


              lodash__WEBPACK_IMPORTED_MODULE_1___default.a.map(subActivities.items, activity => {
                showUserIcon = currentFeedUserId !== activity.actor.id;
                currentFeedUserId = activity.actor.id;

                return (
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_MonitorActivityItem__WEBPACK_IMPORTED_MODULE_4__["default"], {
                    activity: activity,
                    key: activity.id,
                    showUserIcon: showUserIcon,
                    isOffline: props.isOffline }));


              })));



        })),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LoadMore, {
        loading: loading,
        nextPage: nextPage,
        show: !lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(activities),
        onClick: props.handleLoadMore,
        isOffline: props.isOffline })));



}

/***/ }),

/***/ 15321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3659);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_date_helper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _utils_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4868);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3199);
/* harmony import */ var _components_common_UserInfo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15322);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};







const SYSTEM_ACTOR_ID = 0;

function getTimeString(dateString) {
  let dateObj = new Date(dateString);

  return dateObj.toLocaleString('en-US', {
    hour: 'numeric',
    minute: 'numeric',
    hour12: true });

}

// eslint-disable-next-line require-jsdoc
function MonitorActivityItem(props) {
  const { activity, showUserIcon } = props,
  relativeDays = _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default.a.getFormattedDate(activity.timestamp);
  let formattedTime;

  // Get relative time if activity is made on the same day
  if (relativeDays === 0) {
    formattedTime = _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default.a.getFormattedTime(activity.timestamp);
  } else
  {
    formattedTime = getTimeString(activity.timestamp);
  }

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()({ 'pm-activity-list-item': true, 'show-user': showUserIcon, 'activity-logs-offline': props.isOffline }) },

      showUserIcon &&
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-monitor-activity-list-item__icon' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_4__["default"], { type: 'user', customPic: activity.actor.profilePicUrl, userId: activity.actor.id, size: 'large' })),


      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', { className: 'pm-activity-list-item__time' },
        formattedTime),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'pm-activity-list-item__text' },
        activity.actor.id !== SYSTEM_ACTOR_ID &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_UserInfo__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({},
        activity.actor, {
          showAvatar: false,
          allowAlias: true })),


        activity.actor.id !== SYSTEM_ACTOR_ID ? Object(_utils_messages__WEBPACK_IMPORTED_MODULE_3__["getUserVisibleAction"])(activity.action) : Object(_utils_messages__WEBPACK_IMPORTED_MODULE_3__["getUserVisibleSystemAction"])(activity.action, activity.context))));



}

/***/ }),

/***/ 15323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NoMonitorActivity; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _job_information_EmptyState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4860);
/* harmony import */ var _utils_messages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4868);




/**
                                                                   * Empty state for no activity in monitor
                                                                   */
function NoMonitorActivity(props) {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_job_information_EmptyState__WEBPACK_IMPORTED_MODULE_1__["default"], {
      body: props.isFilterApplied ? _utils_messages__WEBPACK_IMPORTED_MODULE_2__["ACTIVITY_LOGS_EMPTY_STATE"].withFiltersSubText : _utils_messages__WEBPACK_IMPORTED_MODULE_2__["ACTIVITY_LOGS_EMPTY_STATE"].subText,
      title: props.isFilterApplied ? _utils_messages__WEBPACK_IMPORTED_MODULE_2__["ACTIVITY_LOGS_EMPTY_STATE"].withFiltersText : _utils_messages__WEBPACK_IMPORTED_MODULE_2__["ACTIVITY_LOGS_EMPTY_STATE"].text,
      illustration: 'no-activity',
      className:
      props.isFilterApplied ? 'activity-logs-empty-state-no-logs-with-filters' : 'activity-logs-empty-state-no-logs' }));



}

/***/ }),

/***/ 15324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorContextBarError; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_2__);


let

MonitorContextBarError = class MonitorContextBarError extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-contextbar-error-view' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-contextbar-error-view__illustration' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Illustration"], {
            name: 'unable-to-load' })),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-contextbar-error-view__title' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Heading"], {
            type: 'h4',
            color: 'content-color-secondary',
            text: this.props.title })),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-contextbar-error-view__subtitle' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Text"], { type: 'body-medium', color: 'content-color-secondary' }, this.props.subtitle)))));




  }};


MonitorContextBarError.defaultProps = {
  title: 'Unable to load activity feed',
  subtitle: 'Please try again later' };


MonitorContextBarError.propTypes = {
  title: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  message: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string };

/***/ }),

/***/ 15326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityFilter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3199);
/* harmony import */ var _js_components_base_Inputs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3215);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3211);
/* harmony import */ var _js_components_base_FuzzySearchInput__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3251);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3192);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3170);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(109);
/* harmony import */ var _postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _utils_messages__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4868);
var _class;









let

MonitorActivityFilter = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class MonitorActivityFilter extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      isFilterApplied: props.isFilterApplied,
      searchQuery: '',
      searchResults: [] };


    this.handleFuzzySearch = this.handleFuzzySearch.bind(this);
    this.getClasses = this.getClasses.bind(this);
  }

  handleFuzzySearch(searchQuery, searchResults) {
    this.setState({ searchQuery, searchResults });
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_7___default()({
      'select-dropdown-container': true,
      'applied': this.state.isFilterApplied,
      'selected': !this.state.isFilterApplied && this.state.isSelected });

  }

  render() {
    const members = _.map(_.cloneDeep(this.props.workspaceUsers), value => {
      value.name = value.name || value.username;

      return value;
    });

    let users = this.state.searchQuery ? this.state.searchResults : members;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_7___default()(this.props.className) },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["Dropdown"], { className: this.getClasses() },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropdownButton"], { dropdownStyle: 'nocaret' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_8__["Button"], { type: 'tertiary', tooltip: this.props.tooltip, disabled: this.props.disabled },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether_lib_components_Icon_Icon__WEBPACK_IMPORTED_MODULE_9___default.a, { name: 'icon-action-filter-stroke' }))),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_4__["DropdownMenu"], null,
            this.props.disabled ? '' :
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'select-dropdown' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_FuzzySearchInput__WEBPACK_IMPORTED_MODULE_5__["default"], {
                searchQuery: this.state.searchQuery,
                placeholder: 'Search for user',
                onChange: this.handleFuzzySearch,
                items: members,
                searchFields: ['name', 'email'] }),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: classnames__WEBPACK_IMPORTED_MODULE_7___default()({ 'select-dropdown-list': true, 'isLoading': this.props.loadingUsers }) },

                this.props.loadingUsers ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_6__["default"], null) :
                users && users.length ?
                _.map(users, user => {
                  return (
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                        key: user.id,
                        className: 'select-dropdown-item',
                        onClick: this.props.handleSelect.bind(this, user.id, users, !this.props.filtersApplied.members[user.id]) },

                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_2__["default"], { size: 'small', userId: user.id, customPic: user.profilePicUrl, linkProfile: false }),
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'select-dropdown-item-text', title: user.name }, user.name),
                      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Inputs__WEBPACK_IMPORTED_MODULE_3__["Checkbox"], {
                        onChange: this.props.handleSelect.bind(this, user.id, users),
                        className: 'select-dropdown-item-checkbox',
                        checked: this.props.filtersApplied.members[user.id] })));



                }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-user-filter-dropdown-no-user' }, _utils_messages__WEBPACK_IMPORTED_MODULE_10__["ACTIVITY_LOGS_NO_USERS"])))))));








  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);