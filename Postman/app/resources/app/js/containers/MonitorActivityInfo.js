(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[21],{

/***/ 15329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MonitorActivityInfo; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(80);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(74);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2778);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3170);
/* harmony import */ var _components_common_UserInfo__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15322);
/* harmony import */ var _appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3200);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3667);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(420);
/* harmony import */ var _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4865);
/* harmony import */ var _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3130);
/* harmony import */ var _components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(15325);
/* harmony import */ var _integrations_components_context_bar_IntegrationContextBarList__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(15330);
/* harmony import */ var _version_control_common_ForkLabel__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(4997);
/* harmony import */ var _js_containers_new_button_services_VersionTagService__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(4829);
/* harmony import */ var _utils_messages__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(4868);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;

















// Duration for which we will show the success state after performing the Copy action
const SHOW_COPY_SUCCESS_DURATION = 3000;let


MonitorActivityInfo = Object(mobx_react__WEBPACK_IMPORTED_MODULE_2__["observer"])(_class = class MonitorActivityInfo extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.monitorPermissionStore = new _stores_domain_MonitorPermissionStore__WEBPACK_IMPORTED_MODULE_10__["default"]();
    this.showPrvtUserTooltip = this.showPrvtUserTooltip.bind(this);
    this.hidePrvtUserTooltip = this.hidePrvtUserTooltip.bind(this);
    this.state = {
      showPrvtUserTooltip: false,
      idCopySuccess: false,
      forkedFromCollectionId: null,
      forkName: null,
      versionTag: null,
      versionName: '' };

    this.workspaceStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ActiveWorkspaceStore');
    this.loadOwnVersions = this.loadOwnVersions.bind(this);
  }

  componentDidMount() {
    Object(mobx__WEBPACK_IMPORTED_MODULE_3__["when"])(
    () => this.props.controller.store.data.collection,
    () => this.loadMonitorInfo());

  }

  loadMonitorInfo() {
    Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('SyncStatusStore').onSyncAvailable().
    then(() => {

      const versionTag = this.props.controller.store.data.versionTag;

      this.loadOwnVersions({ collectionUid: this.props.controller.store.data.collection }).
      then(versions => {

        let item = _.find(versions, ['id', versionTag]);

        if (item) {
          this.setState(() => ({
            versionName: item.name }));

        } else {
          this.setState(() => ({
            versionName: 'CURRENT' }));

        }
      }).
      catch(err => {
        pm.logger.warn(_utils_messages__WEBPACK_IMPORTED_MODULE_16__["VERSION_TAGS_ERROR"].warn, err);
        pm.toasts.error(_.get(err, 'message') || _utils_messages__WEBPACK_IMPORTED_MODULE_16__["VERSION_TAGS_ERROR"].error);
      });
    });
  }

  loadOwnVersions(criteria) {
    return Object(_js_containers_new_button_services_VersionTagService__WEBPACK_IMPORTED_MODULE_15__["fetchAllTags"])(criteria).
    then(versions => {
      return versions;
    });
  }

  componentWillUnmount() {
    clearTimeout(this.clearIdCopySuccessTimeout);
  }

  showPrvtUserTooltip() {
    this.setState({ showPrvtUserTooltip: true });
  }

  hidePrvtUserTooltip() {
    this.setState({ showPrvtUserTooltip: false });
  }

  fetchEnvironmentName(id) {
    // Fetch the uid from the workspace store
    if (id) {
      const environments = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ActiveWorkspaceStore').environments.find(_ => id == _.uid);

      if (environments === undefined) {
        return null;
      } else {
        return environments && environments.name;
      }
    } else {
      return null;
    }
  }

  fetchUserName(userId, data) {
    let teams = data.reduce((obj, item) => {
      obj[item.id] = item;
      return obj;
    }, {});
    let userInfo = _.get(teams, userId, {});
    return userInfo.name;
  }

  canViewCreatorInfo() {
    // contextData here refers to the MonitorConfigurationStore
    return this.monitorPermissionStore.can('viewCreatorInfo', this.props.contextData.id);
  }

  copyMonitorId(id) {
    if (this.state.idCopySuccess) {
      return;
    }

    _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_11__["default"].copy(id);

    this.setState(
    { idCopySuccess: true },
    () => {
      this.clearIdCopySuccessTimeout = setTimeout(
      () => this.setState({ idCopySuccess: false }),
      SHOW_COPY_SUCCESS_DURATION);

    });

  }

  fetchCopyIcon(statusCheck) {
    if (statusCheck) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-state-success-stroke',
          className: 'monitor-info-context-view__entity__content__toggle__success' }));


    } else
    {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-action-copy-stroke',
          className: 'monitor-info-context-view__entity__content__toggle__copy' }));


    }
  }

  render() {
    const { isOffline } = this.props.controller.monitorActivityDetailsStore;

    this.monitorInfo = _.get(this.props, 'contextData', {});

    const createdAt = moment__WEBPACK_IMPORTED_MODULE_4___default()(this.monitorInfo.createdAt).format('DD MMM YYYY, h:mm A');

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_8__["ContextBarViewHeader"], {
          title: this.props.title,
          onClose: this.props.onClose }),


        isOffline && typeof this.props.contextData.createdBy === 'number' ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_MonitorOfflineContextBar__WEBPACK_IMPORTED_MODULE_12__["default"], null) :

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'ID'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'monitor-info-context-view__entity__content__id' },

                this.monitorInfo.id),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
                  className: 'monitor-info-context-view__entity__content__toggle',
                  tooltip: this.state.idCopySuccess ? 'Copied' : 'Copy Monitor ID',
                  type: 'icon',
                  onClick: () => this.copyMonitorId(this.monitorInfo.id) },

                this.fetchCopyIcon(this.state.idCopySuccess)))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'Created by'),
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  disabled: isOffline,
                  className: isOffline && 'monitor-info-userinfo-offline' },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_common_UserInfo__WEBPACK_IMPORTED_MODULE_6__["default"], _extends({},
                this.monitorInfo.creatorInfo, {
                  containerClass: 'monitor-info-context-view__entity__content' }))))),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'Created on'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },
              createdAt)),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'Collection'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },

              this.monitorInfo.collection ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_7__["default"], { to: isOffline ? '' : { routeIdentifier: 'build.collection',
                      routeParams: { cid: this.monitorInfo.collection } },
                    className: 'monitor-info-context-view__entity__content__collection' },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
                      type: 'tertiary',
                      className: 'monitor-info-btn',
                      disabled: isOffline,
                      tooltip: isOffline && 'You can take this action once you are back online' },

                    CollectionItem(this.monitorInfo.collection))),


                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                    className: 'monitor-info-collection__collection-version-tag' },

                  this.state.versionName)) :


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-info-context-view__entity__content__collection-empty' }, _utils_messages__WEBPACK_IMPORTED_MODULE_16__["MONITOR_DETAILS_NO_COLLECTION"]))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__label' }, 'Environment'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-context-view__entity__content' },

              this.fetchEnvironmentName(this.monitorInfo.environment) ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_7__["default"], { to: isOffline ? '' : {
                    routeIdentifier: 'build.environment', routeParams: { eid: this.monitorInfo.environment } },
                  className: 'monitor-info-context-view__entity__content__environment' },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
                    type: 'tertiary',
                    className: 'monitor-info-btn',
                    disabled: isOffline,
                    tooltip: isOffline && 'You can take this action once you are back online' },

                  this.fetchEnvironmentName(this.monitorInfo.environment))) :


              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-info-context-view__entity__content__environment-empty' }, 'No environment'))),



          _.includes(['dev', 'beta', 'stage', 'canary', 'prod'], window.RELEASE_CHANNEL) &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_integrations_components_context_bar_IntegrationContextBarList__WEBPACK_IMPORTED_MODULE_13__["IntegrationContextBarList"], {
            entityId: this.monitorInfo.id,
            entityType: 'monitor' }))));






  }}) || _class;


function CollectionItem(uid) {
  let collection = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('ActiveWorkspaceStore').collections.find(_ => uid == _.uid);

  if (uid) {
    if (collection === undefined) {
      return null;
    } else {

      let forkedFrom = _.get(collection, 'meta.forkedFrom');
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-collection' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'monitor-info-collection__collection-name' },
            collection.name),

          collection.isForked ?
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'monitor-info-collection__qualifiers' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_version_control_common_ForkLabel__WEBPACK_IMPORTED_MODULE_14__["default"], {
              className: 'monitor-info-collection__fork-label',
              destination: _.pick(forkedFrom, ['name', 'id']),
              label: _.get(collection, 'forkInfo.forkLabel') })) :


          null));



    }
  } else {
    return null;
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);