(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ 6800:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_, process) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Shortcuts; });
/* harmony import */ var _ShortcutsList__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3756);
/* harmony import */ var _constants_SettingsTypeConstants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3762);
/* harmony import */ var _stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(420);
/* harmony import */ var _services_EditorService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2757);
/* harmony import */ var _api_dev_services_SaveAsNewAPIService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2762);
/* harmony import */ var _appsdk_workbench_TabService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2830);
/* harmony import */ var _modules_view_manager_ActiveViewManager__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3167);
/* harmony import */ var _constants_RequesterBottomPaneConstants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3046);
/* harmony import */ var _services_NavigationService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(69);
/* harmony import */ var _schema_constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5526);
/* harmony import */ var _integrations_navigation_constants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4215);
/* harmony import */ var _appsdk_services_PageService__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(75);
/* harmony import */ var _appsdk_navigation_constants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2286);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};















const SHORTCUT_THROTTLE_INTERVAL = 500;

let throttledUndoClose = _.throttle(_services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].undoClose, SHORTCUT_THROTTLE_INTERVAL),
isBrowser = window.SDK_PLATFORM === 'browser';

/**
                                                * Save API Schema
                                                * @param {Object} editor - APIWorkbenchController
                                                */
function saveAPISchema(editor) {
  const canUpdateSchema = _.invoke(editor, 'model.apiPermissionStore.can', 'updateSchema');

  if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore').isLoggedIn) {
    return pm.mediator.trigger('showSignInModal', {
      type: 'schema',
      subtitle: _schema_constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["NOT_SIGNED_IN_ERROR"],
      origin: 'schema_save_shortcut' });

  }

  if (!canUpdateSchema) {
    return pm.toasts.error(_schema_constants_SchemaErrors__WEBPACK_IMPORTED_MODULE_9__["PERMISSION_ERROR"]);
  }

  if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore').isMember) {
    return pm.mediator.trigger('openUnjoinedWorkspaceModal');
  }

  let schemaStore = _.get(editor, 'model.activeVersion.schema'),
  { schemaId, schema, activeType, activeLanguage } = editor;

  return schemaStore && schemaStore.saveSchema(schema, activeType, activeLanguage).
  then(() => {
    // Update tracked state values
    editor.handleIncomingUpdates(schemaId, schema, activeType, activeLanguage);
  }).
  catch(() => {});
}

/**
   * @param {Object} editor - APIWorkbenchController
   * @param {String} from - conflicted state ['deleted', 'updated']
   */
function saveAsNewAPI(editor, from) {
  let type = editor.activeType,
  language = editor.activeLanguage,
  schema = editor.schema;

  return Object(_api_dev_services_SaveAsNewAPIService__WEBPACK_IMPORTED_MODULE_4__["default"])(type, language, schema).then(() => {
    // Update tracked state value with latest schema values
    if (from === 'updated') {
      editor.handleIncomingUpdates(
      editor.model.activeVersion.schema.schemaId,
      editor.model.activeVersion.schema.schema,
      editor.model.activeVersion.schema.type,
      editor.model.activeVersion.schema.language);

    }

    // Clear updates from tracked state as current changes are saved in new API
    if (from === 'deleted') {
      editor.clearTrackedUpdates();
    }
  });
}let
Shortcuts = class Shortcuts {

  constructor() {
    const isTestChannel = window.RELEASE_CHANNEL === 'test';

    switch (process.platform) {
      case 'darwin':
        this.defaultShortcuts = Object(_ShortcutsList__WEBPACK_IMPORTED_MODULE_0__["getDarwinShortcuts"])();
        break;
      case 'win32':
        this.defaultShortcuts = Object(_ShortcutsList__WEBPACK_IMPORTED_MODULE_0__["getWindowsShortcuts"])();
        break;
      case 'linux':
        this.defaultShortcuts = Object(_ShortcutsList__WEBPACK_IMPORTED_MODULE_0__["getLinuxShortcuts"])();
        break;
      default:
        this.defaultShortcuts = Object(_ShortcutsList__WEBPACK_IMPORTED_MODULE_0__["getDarwinShortcuts"])();
        break;}


    this.handlers = _.reduce(_.keys(this.defaultShortcuts), (acc, shortcut) => {
      let isMenuShortcut = this.defaultShortcuts[shortcut].menuShortcut;

      // We check the following for each shortcut to decide if we want to skip handling for it or not -
      // 1. If the handler is not a function, we skip the shortcut
      //
      // Now considering the handler for the shortcut is a function -
      // 1. If it is not a menu shortcut, we don't skip it
      // 2. If it is  a menu shortcut -
      //    a) If we are on the browser OR on the test channel - we don't skip it
      //    b) If we are on neither, we skip it

      // Case when we skip the handling
      if (!_.isFunction(this[shortcut]) || isMenuShortcut && !(isBrowser || isTestChannel)) {
        return acc;
      }

      // Case when we are NOT skipping the handling
      return _extends({},
      acc, {
        [shortcut]: this[shortcut] });

    }, {});

    this.reducedShortcuts = this._getReducedShortcuts();
    this.menuActionHandlers = {};
    this.overrideBrowserSet = isBrowser ? this.getBrowserOverrideSet() : new Set();
  }

  // In case of Artemis, get the set of shortcuts for which we need to override the
  // browser behaviour.
  getBrowserOverrideSet() {
    let shortcutsMap = this.defaultShortcuts || {},
    overrideSet = new Set();

    Object.keys(shortcutsMap).forEach(shortcut => {
      shortcutsMap[shortcut].overrideBrowser && overrideSet.add(shortcut);
    });

    return overrideSet;
  }

  shouldOverrideBrowser(shortcut) {
    if (!isBrowser || !shortcut) {
      return false;
    }

    return this.overrideBrowserSet.has(shortcut);
  }

  _getReducedShortcuts() {
    let reducedShortcuts = {};
    for (let [key, value] of Object.entries(this.defaultShortcuts)) {
      reducedShortcuts[value.name] = value.shortcut;
    }

    return reducedShortcuts;
  }

  getShortcuts() {
    return this.reducedShortcuts;
  }

  handle(eventName, handler) {
    if (handler) {
      return handler;
    }

    let defaultHandler = this.handlers[eventName];
    if (defaultHandler) {
      return defaultHandler;
    }
    return _.noop;
  }

  registerMenuAction(action, handler) {
    if (this.menuActionHandlers[action]) {
      pm.logger.warn('Shortcuts~registerMenuActions: Error while registering handler - A handler for the action already exists');
      return;
    }

    this.menuActionHandlers[action] = handler;
  }

  getMenuActionHandler(action) {
    return this.menuActionHandlers[action];
  }

  checkConstraints(meta) {
    if (!meta) {
      return false;
    }

    if (meta.isGlobal) {
      return true;
    }

    // @todo remove the dependency on activeView
    // currently the active view is being used to prevent the shortcuts
    // from working in case any modal is open or not.
    // If the dependency on activeView needs to be removed we need a way to ascertain
    // if any modal is open or not

    let isAllowedInThisView = true,
    isAllowedInThisPage = true,
    isBlockedInThisPage = false;

    if (meta.allowedViews) {
      let allowedViewsSet = new Set(meta.allowedViews),
      activeView = _modules_view_manager_ActiveViewManager__WEBPACK_IMPORTED_MODULE_6__["default"].activeView,
      isAllowedInThisView = allowedViewsSet.has(activeView);

      if (!isAllowedInThisView) {
        return false;
      }
    }

    // two new meta properties are added allowedPages and blockedPages
    // in the menu action registration, one can provide either of the two, but not both of them
    // if both of them are provided 'blockedPages' block will be executed and
    // 'allowedPages' block will be skipped
    // Reason: If someone provides both of these values by mistake, and it satisfies the
    // the conditions of blocked pages, we do not respond to that shortcut.
    // hence mitigating any adverse actions.
    if (meta.blockedPages) {
      let currentPage = _appsdk_services_PageService__WEBPACK_IMPORTED_MODULE_11__["PageService"].activePage;
      isBlockedInThisPage = _.includes(meta.blockedPages, currentPage);
      if (isBlockedInThisPage) {
        return false;
      }
    } else

    if (meta.allowedPages) {
      let currentPage = _appsdk_services_PageService__WEBPACK_IMPORTED_MODULE_11__["PageService"].activePage;
      isAllowedInThisPage = _.includes(meta.allowedPages, currentPage);
      if (!isAllowedInThisPage) {
        return false;
      }
    }

    return isAllowedInThisView && !isBlockedInThisPage && isAllowedInThisPage;
  }

  /** Custom handlers **/

  // Tabs
  // openNewTab, closeCurrentTab, forceCloseCurrentTab, switchToNextTab, switchToPreviousTab
  // these are handled in App.js

  switchToTab1() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(1);
  }

  switchToTab2() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(2);
  }

  switchToTab3() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(3);
  }

  switchToTab4() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(4);
  }

  switchToTab5() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(5);
  }

  switchToTab6() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(6);
  }

  switchToTab7() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(7);
  }

  switchToTab8() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusAtPosition(8);
  }

  switchToLastTab() {
    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].focusLast();
  }

  reopenLastClosedTab() {
    throttledUndoClose();
  }

  // Request
  requestUrl() {
    pm.mediator.trigger('focusRequestUrl');
  }

  saveCurrentRequest() {
    let currentEditorId = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceSessionStore').activeEditor;

    if (!currentEditorId) {
      return;
    }

    /**
       * Added extra handling for saving API schema with cmd+s in API tab,
       * since CustomViewsWriter does not have access to editor.
       * Editor is required to save the changes to the APISchemaStore.
       */
    let editor = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('EditorStore').find(currentEditorId);

    if (!editor) {
      return;
    }

    let editorResource = _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].isV1Compatible(editor) ? editor.model.resourceId : editor.resource,
    resourceURL = editorResource && new URL(editorResource),
    model = resourceURL && resourceURL.pathname;

    model && model.startsWith('//') && (model = model.slice(2));

    // In case if model is `stubview/api/some-id`, split the path and get the model
    if (model === 'api' || model.split('/')[1] === 'api') {

      if (editor.model.isConflicted) {
        let conflictedState = _.get(editor, 'model.tabController.conflictedState');

        if (conflictedState === 'deleted') {
          return pm.mediator.trigger('showSwitchDeletedVersionModal',

          // Create a new API with unsaved schema changes
          () => saveAsNewAPI(_.get(editor, 'model.tabController'), conflictedState),

          () => {
            // Clear tracked updates (unsaved changes) if any
            _.get(editor, 'model.tabController.clearTrackedUpdates') && editor.model.tabController.clearTrackedUpdates();

            // Close the tab
            _appsdk_workbench_TabService__WEBPACK_IMPORTED_MODULE_5__["default"].closeByRoute('api/:apiId', { apiId: _.get(editor, 'model.tabController.model.id') });
          });

        } else

        if (conflictedState === 'updated') {
          return pm.mediator.trigger('showSwitchConflictedVersionModal',

          // Overwrite and save changes to schema
          () => saveAPISchema(_.get(editor, 'model.tabController')),

          // Create a new API with current schema changes
          () => saveAsNewAPI(_.get(editor, 'model.tabController'), conflictedState));

        }
      } else
      {

        /**
         * added this check to prevent spamming.
         * not making save schema call when either the tab is not dirty or already a save schema call is in progress.
         */
        if (!editor.model.isDirty || _.get(editor, 'model.tabController.model.activeVersion.schema.isSavingSchema')) {
          return;
        }

        return saveAPISchema(_.get(editor, 'model.tabController'));
      }
    }

    // if editor state is dirty and the user cannot edit the request then show a modal which
    // provides option to save the request in a different collection or create a fork of the
    // collection and update the request in newly created fork
    if (model === 'request' && editor.model.isDirty && !editor.model.isEditable) {
      const requestId = editor.model.requestId,
      parentCollectionUid = _.get(Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CollectionStore').find(_.get(Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('RequestStore').find(requestId), 'collection')), 'uid');

      // Show showSignInModal if user is not signed in
      if (!Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore').isLoggedIn) {
        return pm.mediator.trigger('showSignInModal', {
          type: 'save',
          origin: 'save_shortcut' });

      }

      pm.mediator.trigger('showForkRecommendationModal', {
        editorId: currentEditorId,
        collectionId: parentCollectionUid });


      return;
    }

    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].requestSave({ id: currentEditorId });
  }

  saveCurrentRequestAs() {
    let currentEditorId = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceSessionStore').activeEditor,
    editor = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('EditorStore').find(currentEditorId);

    if (!editor) {
      return;
    }

    // Show showSignInModal if user is not workspace member and not signed in
    if (!(Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore').isMember || Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore').isLoggedIn)) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'saveAs',
        origin: 'save_as_shortcut' });

    }

    _services_EditorService__WEBPACK_IMPORTED_MODULE_3__["default"].saveAs({ id: currentEditorId });
  }

  sendCurrentRequest() {
    let currentEditorId = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceSessionStore').activeEditor,
    editor = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('EditorStore').find(currentEditorId);

    if (!editor || !editor.model) {
      return;
    }

    let activeWorkspaceStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore'),
    canSendRequests = activeWorkspaceStore.isMember && Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('PermissionStore').can('addHistory', 'workspace', activeWorkspaceStore.id);

    // if agent is not ready or don't have permission to send request, bail
    if (!pm.runtime.agent.isReady || !canSendRequests) {
      return;
    }

    // Do not send request if the editor is in Comment Mode
    if (editor.model.getAdditionalData('commentMode')) {
      return;
    }

    let sendIndividualRequest = _.get(editor.model, 'actions.sendIndividualRequest'),
    resourceType = _.get(editor.model, 'info.model');

    resourceType === 'request' && _.isFunction(sendIndividualRequest) && sendIndividualRequest(editor.model);
  }

  sendAndDownloadCurrentRequest() {
    let currentEditorId = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceSessionStore').activeEditor,
    editor = currentEditorId && Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('EditorStore').find(currentEditorId),
    editorModel = editor && editor.model;

    if (!editor || !editorModel) {
      return;
    }

    // @todo send and download is disabled until runtime agent is capable
    if (window.SDK_PLATFORM === 'browser') {
      return;
    }

    // if agent is not ready, bail
    if (!pm.runtime.agent.isReady) {
      return;
    }

    // Do not send request if the editor is in Comment Mode
    if (editor.model.getAdditionalData('commentMode')) {
      return;
    }

    let sendAndDownloadRequest = _.get(editorModel, 'actions.sendAndDownloadRequest'),
    resourceType = _.get(editorModel, 'info.model');

    resourceType === 'request' && _.isFunction(sendAndDownloadRequest) && sendAndDownloadRequest(editorModel);
  }


  scrollToResponse() {
    pm.mediator.trigger('handleScrollToBottomShortcut');
  }

  scrollToRequest() {
    pm.mediator.trigger('handleScrollToTopShortcut');
  }

  // Sidebar
  search() {
    pm.mediator.trigger('focusSearchBox');
  }

  toggleSidebar() {
    pm.mediator.trigger('toggleSidebar');
  }

  // Modals
  import() {
    let currentUserStore = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore');

    /**
                                                          * Need to check if the user is not signed in.
                                                          * Scenarios:
                                                          *  Signed out and viewing public workspace on the web - Show the signin modal to use the import.
                                                          *  Signed out and in the desktop app - scratchpad mode, should be able to use import.
                                                          */
    if (!currentUserStore.isLoggedIn && !pm.isScratchpad) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'generic',
        subtitle: 'To import, you need to have an account with Postman.',
        origin: 'shortcuts_import_action' });

    }

    _services_NavigationService__WEBPACK_IMPORTED_MODULE_8__["default"].transitionTo(_integrations_navigation_constants__WEBPACK_IMPORTED_MODULE_10__["OPEN_IMPORT_IDENTIFIER"]);
  }

  settings() {
    pm.mediator.trigger('openSettingsModal');
  }

  shortcutCheats() {
    pm.mediator.trigger('openSettingsModal', _constants_SettingsTypeConstants__WEBPACK_IMPORTED_MODULE_1__["SETTINGS_SHORTCUTS"]);
  }

  // Windows
  openCreateNewModal() {
    pm.mediator.trigger('openCreateNewXModal');
  }

  newRequesterWindow() {
    pm.mediator.trigger('newRequesterWindow');
  }

  newConsoleWindow() {
    pm.mediator.trigger('newConsoleWindow');
  }

  toggleFindReplace() {
    const store = Object(_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('RequesterBottomPaneUIStore');

    store && store.toggleTab(_constants_RequesterBottomPaneConstants__WEBPACK_IMPORTED_MODULE_7__["REQUESTER_BOTTOM_PAME_FIND_REPLACE"]);
  }

  closeCurrentWindow() {
    pm.mediator.trigger('closeRequesterWindow');
  }

  // UI
  increaseUIZoom() {
    pm.uiZoom.increase();
  }

  decreaseUIZoom() {
    pm.uiZoom.decrease();
  }

  resetUIZoom() {
    pm.uiZoom.reset();
  }

  toggleLayout() {
    pm.app.toggleLayout();
  }

  switchPane() {
    pm.mediator.trigger('switchPane');
  }

  universalSearchFocus() {
    pm.mediator.trigger('universalSearchFocus');
  }

  // Console
  clearConsole() {
    pm.mediator.trigger('clearConsolePane');
  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10), __webpack_require__(38)))

/***/ }),

/***/ 6801:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ToastManager; });
let ToastManager = class ToastManager {
  constructor() {
    this.notificationQueue = [];
    this.isPaused = false;
  }

  isVisible() {
    var notifs = !document.getElementsByClassName('notification'),
    tooltips = !document.getElementsByClassName('tooltip'),
    fullscreenModals = !document.getElementsByClassName('modal-fullscreen');

    return !(notifs && tooltips && fullscreenModals);
  }

  enqueue(callback, priority) {
    this.notificationQueue.push({
      callback: callback,
      priority: priority });


    this.process();
  }

  dequeue() {
    var minPriority = _.min(_.map(this.notificationQueue, 'priority'));
    var toRunIndex = _.findKey(this.notificationQueue, function (element) {
      return element.priority === minPriority;
    });
    return this.notificationQueue.splice(toRunIndex, 1)[0];
  }

  process() {
    if (this.isVisible() || this.isPaused) {
      setTimeout(() => {
        this.process();
      }, 1000); // Try again after 1 second
    } else
    {
      this.isPaused = true;
      this.dequeue().callback();

      setTimeout(() => {
        this.isPaused = false;
      }, 2000); // Separation between notifications
    }
  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6805:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEventBus", function() { return getEventBus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setNotificationComponent", function() { return setNotificationComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "attachLinkListeners", function() { return attachLinkListeners; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRef", function() { return getRef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_getBaseOptions", function() { return _getBaseOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_show", function() { return _show; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "error", function() { return error; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "info", function() { return info; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "success", function() { return success; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "warn", function() { return warn; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _postman_sanitise_user_content__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1972);
/* harmony import */ var _postman_sanitise_user_content__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_sanitise_user_content__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_DashboardService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2799);
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(13);
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(uuid_v4__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_messaging_Toast__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6806);
/* harmony import */ var _external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2301);








const getEventBus = function () {
  if (!this.eventBusChannel) this.eventBusChannel = pm.eventBus.channel('notifications');
  return this.eventBusChannel;
};

const setNotificationComponent = function (ref) {
  this._ref = ref;
  this.attachLinkListeners();

  // Listen for notifications from windows with no UI
  if (_.get(pm, 'windowConfig.ui')) {
    this.getEventBus().subscribe(options => {
      this._show(options);
    });
  }
};

const attachLinkListeners = function () {
  if (!this._ref) {
    return;
  }

  let notificationWrapper = Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(this._ref);
  if (!notificationWrapper) {
    return;
  }

  notificationWrapper.addEventListener('click', e => {
    let classList = e.target.classList,
    allowClicks = ['toast-dismiss', 'toast-primary-action', 'toast-secondary-action'];

    if (_.isEmpty(_.intersection(classList, allowClicks))) {
      // This check is there if the target is SVG (close icon)
      // and its parent is 'toast-dismiss'
      const maxParentNodeLookupCount = 4;
      let currentTarget = e.target,
      targetClass = currentTarget.className,
      notificationClass = 'notifications-wrapper',
      lookUpCount = 0;

      while (targetClass && targetClass !== notificationClass) {
        if (lookUpCount > maxParentNodeLookupCount) {
          break;
        }

        // Do not prevent if the icon is a child node of toast-dismiss
        if (targetClass === 'toast-dismiss') {
          return;
        }

        currentTarget = _.get(currentTarget, 'parentNode', null);
        targetClass = _.get(currentTarget, 'className', null);
        lookUpCount++;
      }

      e.preventDefault();
      e.stopPropagation();
    }

    if (e.target.tagName !== 'A') {
      return;
    }
    e.preventDefault();
    e.stopPropagation();
    let link = e.target.href;
    if (link) {
      Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_6__["openExternalLink"])(link);
    } else
    {
      try {
        let slug = e.target.dataset.slug;
        if (slug === 'invite_users') {
          Object(_services_DashboardService__WEBPACK_IMPORTED_MODULE_3__["openInviteUsers"])();
        }
      }
      catch (e) {
        pm.logger.error(e);
      }
    }
  });
};

const getRef = function () {
  return this._ref;
};

const _getBaseOptions = function (persist, timeout) {
  return {
    position: 'tr',
    dismissible: true,
    autoDismiss: persist ? 0 : timeout };

};

/**
    * Returns level for type for the react notification system
    *
    * @param {*} type
    */
function getLevelForType(type) {
  let map = {
    success: 'success',
    error: 'error',
    warn: 'warning',
    info: 'info' };

  return map[type];
}

const _show = function (options) {
  if (!this._ref) {
    _.get(pm, 'windowConfig.ui') ? pm.logger.error('Notification System not initialized') : this.getEventBus().publish(options);
    return;
  }

  let {
    type = 'info',
    message,
    dedupeId,
    primaryAction = null,
    secondaryAction = null,
    persist = false,
    timeout = 3000,
    title = null,
    primaryButtonLabel = '',
    secondaryButtonLabel = '',
    enableActions = null,
    noIcon = false,
    isDismissable = true,
    onDismiss = null,
    onView = _.noop,

    // HACK: quick fix for 100K. cleanup.
    skipSanitise = false } =
  options;

  // react notification system takes seconds timeout
  timeout /= 1000;
  let notificationId = uuid_v4__WEBPACK_IMPORTED_MODULE_4___default()(),
  notification = _.extend(
  this._getBaseOptions(persist, timeout), {
    uid: notificationId,
    level: getLevelForType(type),
    children:
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_messaging_Toast__WEBPACK_IMPORTED_MODULE_5__["default"], {
      isDismissable: isDismissable,
      noIcon: noIcon,
      uid: notificationId,
      dismiss: this._ref.removeNotification,
      type: type,
      title: title || null,
      message: message,
      primaryAction: primaryAction,
      secondaryAction: secondaryAction,
      onDismiss: onDismiss,
      onView: onView }) });





  if (dedupeId) {
    notification = _.extend(notification, { uid: `${getLevelForType(type)}-${dedupeId}` });
  }

  this._ref.addNotification(notification);
};

const error = function (message, options) {
  message || (message = 'Something went wrong. Please try again.');
  options || (options = {});

  this._show(
  _.extend(options, {
    type: 'error',
    message: message }));


};

const info = function (message, options) {
  if (!message) {
    return;
  }

  options || (options = {});

  this._show(
  _.extend(options, {
    type: 'info',
    message: message }));


};

const success = function (message, options) {
  if (!message) {
    return;
  }

  options || (options = {});

  this._show(
  _.extend(options, {
    type: 'success',
    message: message }));


};

const warn = function (message, options) {
  if (!message) {
    return;
  }

  options || (options = {});

  this._show(
  _.extend(options, {
    type: 'warn',
    message: message }));


};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6806:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Toast; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3170);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _base_Icons_InformationIcon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3191);
/* harmony import */ var _base_Icons_CloseIcon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3246);
/* harmony import */ var _base_Markdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3201);
/* harmony import */ var _services_UIEventService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2673);







let

Toast = class Toast extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor() {
    super();

    this.handlePrimaryAction = this.handlePrimaryAction.bind(this);
    this.handleSecondaryAction = this.handleSecondaryAction.bind(this);
    this.dismissToast = this.dismissToast.bind(this);
  }

  componentDidMount() {
    this.props.onView && this.props.onView();
    this.unsubscribeHandler = this.props.dismissEvent && _services_UIEventService__WEBPACK_IMPORTED_MODULE_7__["default"].subscribe(this.props.dismissEvent, this.dismissToast);
  }

  componentWillUnmount() {
    this.unsubscribeHandler && this.unsubscribeHandler();
  }

  handlePrimaryAction() {
    if (this.props.primaryAction && this.props.primaryAction.onClick) {
      this.props.primaryAction.onClick();
    }
    this.props.isDismissable && this.props.dismiss(this.props.uid);
  }

  handleSecondaryAction() {
    if (this.props.secondaryAction && this.props.secondaryAction.onClick) {
      this.props.secondaryAction.onClick();
    }
    this.props.isDismissable && this.props.dismiss(this.props.uid);
  }

  dismissToast() {
    // The notification system provides a handler
    // to remove the toast
    this.props.dismiss(this.props.uid);
    this.props.onDismiss && this.props.onDismiss(this.props.uid);
  }

  getTypeClass() {
    let typeClass = this.props.type ? `toast-${this.props.type}` : 'toast-info';
    return classnames__WEBPACK_IMPORTED_MODULE_3___default()({
      'toast': true,
      [typeClass]: true });

  }

  getActions() {
    const { primaryAction = null, secondaryAction = null } = this.props;

    if (primaryAction || secondaryAction) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'toast-actions' },

          primaryAction &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_1__["Button"], {
              className: 'toast-primary-action',
              type: primaryAction.type || 'primary',
              size: 'small',
              disabled: primaryAction.disabled,
              onClick: this.handlePrimaryAction },

            primaryAction.label),



          secondaryAction &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_1__["Button"], {
              className: classnames__WEBPACK_IMPORTED_MODULE_3___default()({ 'toast-secondary-action': true, 'toast-secondary-action-only': !primaryAction }),
              type: secondaryAction.type || 'text',
              disabled: secondaryAction.disabled,
              onClick: this.handleSecondaryAction },

            secondaryAction.label)));




    }
  }

  getContentClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_3___default()({
      'toast-content': true,
      'toast-content-shrink': this.props.isDismissable });

  }

  render() {
    const {
      title,
      message,
      onMessageLinkClick,
      disabled,
      isDismissable,
      noIcon } =
    this.props;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getTypeClass() },

        !noIcon &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Icons_InformationIcon__WEBPACK_IMPORTED_MODULE_4__["default"], { className: 'toast-icon' }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getContentClasses() },

          title &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'toast-title' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Markdown__WEBPACK_IMPORTED_MODULE_6__["default"], {
              disableGfm: true,
              source: title,
              onLinkClick: onMessageLinkClick,
              isToastMsg: true })),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'toast-body' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Markdown__WEBPACK_IMPORTED_MODULE_6__["default"], {
              disableGfm: true,
              source: message,
              onLinkClick: onMessageLinkClick,
              isToastMsg: true })),


          this.getActions()),


        isDismissable &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: 'toast-dismiss',
            onClick: this.dismissToast },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Icons_CloseIcon__WEBPACK_IMPORTED_MODULE_5__["default"], {
            className: 'toast-close',
            size: 'xs' }))));





  }};


Toast.defaultProps = {
  type: 'info',
  message: '',
  disabled: false,
  isDismissable: true,
  noIcon: false,
  onMessageLinkClick: null };


Toast.propTypes = {
  type: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.oneOf(['error', 'success', 'warn', 'info']),
  message: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  isDismissable: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  noIcon: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  dismiss: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func.isRequired,
  onMessageLinkClick: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func };

/***/ }),

/***/ 6807:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global, _) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findSession", function() { return findSession; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clearOrphanSessions", function() { return clearOrphanSessions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bootWindowConfig", function() { return bootWindowConfig; });
/* harmony import */ var _common_controllers_WindowController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(442);
/* harmony import */ var _common_controllers_WindowController__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_common_controllers_WindowController__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2077);
/* harmony import */ var _modules_controllers_WorkspaceController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(424);
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(13);
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(uuid_v4__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _modules_model_event__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(429);
/* harmony import */ var _utils_default_workspace__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2082);
/* harmony import */ var _collaboration_services_CollaborationNavigationService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2287);
/* harmony import */ var _constants_RequesterTabConstants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2081);
/* harmony import */ var _electron_ElectronService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(2302);
/* harmony import */ var _external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2301);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};










const isBrowser = window.SDK_PLATFORM === 'browser';

let getDefaultSession = window => {
  return Object(_utils_default_workspace__WEBPACK_IMPORTED_MODULE_5__["defaultUserWorkspaceId"])().
  then(defaultWorkspaceId => {
    if (!defaultWorkspaceId) {
      return Promise.reject(new Error('Could not find default workspace while booting session'));
    }

    console.log('Session~boot - get default session');

    // If the default workspace does not exist, we create a default workspace here
    // this is done on web after moving workpspace and session models into in-memory data store.
    // On hosted environments like beta, the app would authenticate the user and run a post login
    // migration flow where default workspace for signed in user id would be created
    // but on dev apps, the authentication flow does not work. So we explicitly handle the
    // default workspace creation flow here to unblock the app for dev env.
    return _modules_controllers_WorkspaceController__WEBPACK_IMPORTED_MODULE_2__["default"].get({ id: defaultWorkspaceId }).
    then(workspace => {
      console.log('Session~boot - get default workspace id', workspace);

      if (workspace) {
        console.log('Session~boot - default workspace is present');

        return workspace;
      }
      console.log('Session~boot - default workspace is not present - create one');

      return _modules_controllers_WorkspaceController__WEBPACK_IMPORTED_MODULE_2__["default"].create({ id: defaultWorkspaceId, name: 'My Workspace', type: 'personal', dependencies: {} }).
      then(() => {
        console.log('Session~boot - default workspace created');

        return _modules_controllers_WorkspaceController__WEBPACK_IMPORTED_MODULE_2__["default"].get({ id: defaultWorkspaceId });
      })

      // This catch for the workspace creation will only handle errors where the initial workspace get did not find a workspace,
      // but there was still a workspace found after the create workspace failed. This can only happen when there's a
      // parallel create running elsewhere which happens to create the workspace after the get but before the create.
      // Any other error is bubbled up again with a subsequent throw.
      .catch(err => {
        console.log('Session~boot - default workspace creation failed');
        return _modules_controllers_WorkspaceController__WEBPACK_IMPORTED_MODULE_2__["default"].get({ id: defaultWorkspaceId }).
        then(workspace => {
          if (workspace) {
            return workspace;
          } else {
            throw err;
          }
        })

        // We are throwing the error from the workspace create here, and not the error from the get failure
        // this is because we want the stack trace to reflect the workspace creation failure in every case other
        // than the race condition. The catch is supposed to handle only the race condition. Anything else should
        // preserve original stack.
        .catch(unusedErr => {
          throw err;
        });
      });
    });
  }).
  then(defaultWorkspace => {
    return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].
    create({
      id: uuid_v4__WEBPACK_IMPORTED_MODULE_3___default()(),
      window: window.id,
      workspace: defaultWorkspace.id,
      state: {} }).

    then(sessionCreatedEvent => {
      return Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["getEventData"])(sessionCreatedEvent);
    });
  });
};

let findSession = (window, options) => {
  return Promise.resolve()

  // First, check if this window is supposed to be opened with a workspace (New window with workspace pre-selected)
  .then(() => {
    // If this window is not being opened with a pre-selected workspace, move on
    if (!options.session || !options.session.workspace) {
      return;
    }

    // If this window is supposed to be opened with a workspace, just create the session
    // with that workspace and move on
    return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].
    getSessionFor(window.id, options.session.workspace).
    then(session => {
      // If the window is not supposed to have pre-selected state, move on
      if (!options.session.state) {
        return session;
      }

      // Otherwise, update the session's state with whatever is supplied
      return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].
      update({
        id: session.id,
        state: _extends({},
        session.state,
        options.session.state) }).


      then(sessionUpdatedEvent => {
        return Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["getEventData"])(sessionUpdatedEvent);
      });
    });
  })

  // First, check if this window is supposed to be opened with a defined session (restore flow)
  .then(session => {
    // If we've already found a session, move on
    if (session) {
      return session;
    }

    // if the view is Scratch Pad, get the session for current window and default offline
    // workspace and return that. If such combination is not found, create a new session with
    // the same combination and return that.
    // See https://postmanlabs.atlassian.net/browse/QUAL-1363
    if (global.SDK_PLATFORM === 'desktop' && pm.isScratchpad) {
      return Object(_utils_default_workspace__WEBPACK_IMPORTED_MODULE_5__["defaultOfflineWorkspaceId"])().then(workspaceId => {
        return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].
        getAll({ window: window.id, workspace: workspaceId }).
        then(sessions => {
          let session = _.first(sessions);
          if (!session || session.window !== window.id) {
            return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].create({
              id: uuid_v4__WEBPACK_IMPORTED_MODULE_3___default()(),
              window: window.id,
              workspace: workspaceId,
              state: {},
              viewMode: _constants_RequesterTabConstants__WEBPACK_IMPORTED_MODULE_7__["WORKSPACE_BUILDER_VIEW"] }).

            then(sessionCreateEvent => {
              return Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["getEventData"])(sessionCreateEvent);
            });
          }
          return session;
        });
      });
    }

    // If this window is not being restored, move on
    if (!options.session || !window.activeSession) {
      return;
    }

    return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].
    get({ id: window.activeSession }).
    then(session => {
      if (!session || session.window !== window.id) {
        return;
      }

      return session;
    });
  })

  // If the window's activeSession exists, continue, or else try finding another suitable session
  .then(session => {
    if (session) {
      return session;
    }

    // Active session on this window does not exist, trying to find some other session on this window
    return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].
    getAll({ window: pm.window.id }).
    then(allSessions => {

      if (allSessions && allSessions.length > 0 && allSessions[0]) {
        return allSessions[0];
      }

      return _collaboration_services_CollaborationNavigationService__WEBPACK_IMPORTED_MODULE_6__["default"].getInitialRouteParams();
    }).
    then(workspaceContext => {
      if (!workspaceContext || !workspaceContext.wid) {
        return getDefaultSession(window);
      }

      return _collaboration_services_CollaborationNavigationService__WEBPACK_IMPORTED_MODULE_6__["default"].computeWorkspaceIdFromParam(workspaceContext).
      catch(() => {
        // Passing the workspace id as empty as if there is error in
        // computing workspace id we should not fail the bootSession
        return '';
      }).
      then(workspaceId => {
        if (_.isEmpty(workspaceId)) {
          return getDefaultSession(window);
        }

        // cannot block this, create the workspace session and then
        // fire and forget the nav to take up opening of workspace.
        return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].create({
          id: uuid_v4__WEBPACK_IMPORTED_MODULE_3___default()(),
          window: pm.window.id,
          workspace: workspaceId,
          state: {},
          viewMode: _constants_RequesterTabConstants__WEBPACK_IMPORTED_MODULE_7__["WORKSPACE_BUILDER_VIEW"] }).

        then(sessionCreateEvent => {
          // Earlier line written below was not commented, but now is. Read the reason below.

          // Do not trigger this from here. Make the session and continue.
          // Let navigation call this and will take care of it.
          // what is happening is when getting called twice, second time the workspace id
          // does not get resolved correctly, because the route params change as the first
          // function convert it to an id. And just id on explore will not work.
          // also this should not be called twice.

          // isBrowser && CollaborationNavigation.handleOpenWorkspace({ wid: workspaceId });

          return Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_4__["getEventData"])(sessionCreateEvent);
        });
      });
    });
  })

  // Verify if the session being restored points to a valid workspace.
  .then(session => {
    // Redirect to explore if no session
    if (!session) {
      Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_9__["openExternalLink"])(global.window.postman_explore_redirect_url);
    }

    return session;
  });
};

let clearOrphanSessions = (activeWindowId, { activeSessionId }) => {
  // Find all sessions, if for a session, the window / workspace does not exist, delete the session
  return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].
  getAll().
  then(allSessions => {
    // First, take the currently active session out of the picture.
    // Don't want to ever delete that
    _.remove(allSessions, session => session.id === activeSessionId);
    return allSessions;
  }).
  then(allSessions => {
    // Remove all sessions with an invalid window ID
    return Promise.all(
    _.map(allSessions, session => {
      return _common_controllers_WindowController__WEBPACK_IMPORTED_MODULE_0___default.a.get({ id: session.window }).
      then(window => {
        if (window) {
          return session;
        }

        return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].delete({ id: session.id }).
        then(sessionDeletedEvent => {
          return;
        }).
        catch(e => {
          pm.logger.error(`Failed to clear orphan sessions on window ${session.window}`, e);
          return session;
        });
      });
    }));

  }).
  then(sessions => {
    // Clean the result, remove all undefined values from the previous loop
    return _.compact(sessions);
  }).
  then(allSessions => {
    // Now, remove all sessions on the currentWindow, with an invalid workspace
    return Promise.all(
    _.map(allSessions, session => {
      return _modules_controllers_WorkspaceController__WEBPACK_IMPORTED_MODULE_2__["default"].get({ id: session.workspace }).
      then(workspace => {
        if (session.window === activeWindowId && !workspace) {
          return _modules_controllers_WorkspaceSessionController__WEBPACK_IMPORTED_MODULE_1__["default"].delete({ id: session.id }).
          catch(e => {
            pm.logger.error(`Failed to clear orphan sessions with workspace ${session.workspace}`, e);
          });
        }
      });
    }));

  });
};

// IMPORTANT: DO NOT REORDER PROMISES HERE
// 1. Getting launch params from window
// 2. Get / create a window
// 3. Find a suitable session (find / create). Session create should always be after window create, otherwise this might be pruned
// 4. Update the new window with the new session's ID
// 5. Complete the boot process
// 6. Start pruning orphan sessions
let bootWindowConfig = cb => {

  return Promise.resolve().
  then(() => {
    if (window.SDK_PLATFORM === 'browser') {
      return _common_controllers_WindowController__WEBPACK_IMPORTED_MODULE_0___default.a.create({ type: 'requester', id: 'w1' }).
      catch(() => {}).
      then(() => {
        return [{ type: 'requester', id: 'w1' }, {}];
      });
    }

    return Object(_electron_ElectronService__WEBPACK_IMPORTED_MODULE_8__["getLaunchParams"])();
  })

  // First, initialize the WindowController
  .then(windowParams => {
    return _common_controllers_WindowController__WEBPACK_IMPORTED_MODULE_0___default.a.bootstrap(...windowParams);
  })

  // Find the window being restored / opened
  .then(() => {
    return _common_controllers_WindowController__WEBPACK_IMPORTED_MODULE_0___default.a.
    getCurrentWindow().
    then(currentWindow => {
      if (currentWindow) {
        return currentWindow;
      }

      // Worst case scenario
      return Promise.reject(new Error('Window to open does not exist ' + pm.window.id));
    })

    // Complete the boot process
    .then(() => {
      pm.logger.info('Session~boot - Success');
      cb && cb(null);
    }).

    catch(e => {
      pm.logger.error('Session~boot - Failed', e);
      cb && cb(e);
    }).

    catch(() => {});
  });
};


/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(11), __webpack_require__(10)))

/***/ }),

/***/ 6809:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _stores_store_handler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6810);
/* harmony import */ var _modules_model_event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(429);



const STORE_HANDLER_TIMEOUT = 15 * 1000; // 15 seconds

/**
 *
 */
function bootStore(cb) {

  Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["subscribeToQueue"])(_stores_store_handler__WEBPACK_IMPORTED_MODULE_0__["handleModelEventOnStore"], STORE_HANDLER_TIMEOUT);
  pm.logger.info('Store~boot - Success');
  cb && cb(null);
}

/* harmony default export */ __webpack_exports__["default"] = (bootStore);

/***/ }),

/***/ 6810:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleModelEventOnStore", function() { return handleModelEventOnStore; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(74);
/* harmony import */ var _modules_model_event__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(429);



/**
                                                                                                                         *
                                                                                                                         * @param {Object} modelEvent
                                                                                                                         */
function handleModelEventOnStore(modelEvent, callback) {
  let eventNamespace = Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["getEventNamespace"])(modelEvent),
  storeListeners = _.get(pm, ['stores', '_listeners']),
  activeListenersFor = new Set(),
  matchedListeners = [];

  // @todo: this may have issues, when a top level event does not have a store initialized
  // but low level events have stores initialized
  if (!storeListeners) {
    return callback();
  }

  storeListeners.forEach(storeListenerForNamespace => {
    _.forEach(_.keys(storeListenerForNamespace), listenerName => {
      activeListenersFor.add(listenerName);
    });
  });

  let start = performance.now();

  performance.mark(`${modelEvent.name}:${modelEvent.namespace}:${start}:start`);

  Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["processEventSync"])(modelEvent, Array.from(activeListenersFor), function processEventOnStore(event) {
    if (!storeListeners.has(Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["getEventNamespace"])(event))) {
      return;
    }
    let storeListenersForNamespace = storeListeners.get(Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["getEventNamespace"])(event)),
    action = Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["getEventName"])(event),
    listeners = storeListenersForNamespace[action];

    _.forEach(listeners, function (listener) {
      // we create new functions instead of binding
      // listener is already bound with the `store`, we can't find it, or change it
      listener && matchedListeners.push(function () {
        listener(Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["getEventData"])(event), Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_1__["getEventMeta"])(event), modelEvent);
      });
    });
  });

  // wrapping all the individual listeners for an event in a transactions
  Object(mobx__WEBPACK_IMPORTED_MODULE_0__["transaction"])(function () {
    // listeners are synchronous anyway
    _.forEach(matchedListeners, function invokeStoreListener(listener) {
      listener && listener();
    });
  });

  performance.mark(`${modelEvent.name}:${modelEvent.namespace}:${start}:end`);

  performance.measure(`${modelEvent.name}:${modelEvent.namespace}:measure`, `${modelEvent.name}:${modelEvent.namespace}:${start}:start`, `${modelEvent.name}:${modelEvent.namespace}:${start}:end`);

  callback();
}


/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6811:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var _controllers_Shortcuts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6800);


/**
                                                      *
                                                      * @param {*} cb
                                                      */
function bootShortcuts(cb) {
  _.assign(window.pm, { shortcuts: new _controllers_Shortcuts__WEBPACK_IMPORTED_MODULE_0__["default"]() });

  // Registering menu action handlers
  pm.app.registerMenuActions();

  pm.logger.info('Shortcuts~boot - Success');
  return cb && cb(null);
}

/* harmony default export */ __webpack_exports__["default"] = (bootShortcuts);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6814:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var electron__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2303);
/* harmony import */ var electron__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(electron__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2835);
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_1__);


const _ = __webpack_require__(10);

const DEFAULT_ZOOM_LEVEL = 0;

const ZOOM_SCALE_FACTORS = [0.25, 0.33, 0.5, 0.67, 0.75, 0.8, 0.9, 1.0, 1.1, 1.25, 1.5, 1.75, 2.0, 2.5, 3.0, 4.0, 5.0];

const MIN_ZOOM_FACTOR = _.first(ZOOM_SCALE_FACTORS),
MAX_ZOOM_FACTOR = _.last(ZOOM_SCALE_FACTORS),
DEFAULT_ZOOM_FACTOR = 1.0;

var UIZoom = backbone__WEBPACK_IMPORTED_MODULE_1___default.a.Model.extend({
  initialize: function () {
    this._loadZoomFactor();
    this.applyCurrentZoom();
  },

  increase: function () {
    let nextZoomFactor = this.getNextZoomFactor(this.currentZoomFactor);

    this._validateZoomFactor(nextZoomFactor) && this._setZoomFactor(nextZoomFactor);
    this.trigger('change');
  },

  decrease: function () {
    let previousZoomFactor = this.getPreviousZoomFactor(this.currentZoomFactor);

    this._validateZoomFactor(previousZoomFactor) && this._setZoomFactor(previousZoomFactor);
    this.trigger('change');
  },

  reset: function () {
    this._setZoomFactor(DEFAULT_ZOOM_FACTOR);
    this.trigger('change');
  },

  getNextZoomFactor: function (zoomFactor) {
    return _.find(ZOOM_SCALE_FACTORS, nextZoom => zoomFactor < nextZoom);
  },

  getPreviousZoomFactor: function (zoomFactor) {
    return _.findLast(ZOOM_SCALE_FACTORS, prevZoom => zoomFactor > prevZoom);
  },

  getMaxZoomFactor: function () {
    return MAX_ZOOM_FACTOR;
  },

  getMinZoomFactor: function () {
    return MIN_ZOOM_FACTOR;
  },

  applyCurrentZoom: function () {
    electron__WEBPACK_IMPORTED_MODULE_0__["webFrame"].setZoomFactor(this.currentZoomFactor);
  },

  _loadZoomFactor: function () {
    let initialZoomFactor = pm.settings.getSetting('zoomFactor'),
    zoomFactor = initialZoomFactor;

    if (!zoomFactor) {
      zoomFactor = this._loadZoomFactorFromLevel();
    }

    zoomFactor = this._normalizeZoomFactor(zoomFactor);
    initialZoomFactor !== zoomFactor && this._saveZoomFactor(zoomFactor);
    this.currentZoomFactor = zoomFactor;
  },

  _setZoomFactor: function (zoomFactor) {
    this.currentZoomFactor = zoomFactor;
    this.applyCurrentZoom();
    this._saveZoomFactor(this.currentZoomFactor);
  },

  _saveZoomFactor: function (zoomFactor) {
    pm.settings.setSetting('zoomFactor', zoomFactor);
  },

  _validateZoomFactor: function (zoomFactor) {
    return Number.isFinite(zoomFactor) &&
    zoomFactor <= MAX_ZOOM_FACTOR && zoomFactor >= MIN_ZOOM_FACTOR;
  },

  /**
        * This function normalizes the invalid zoomFactor to the nearest supported zoomFactor
        * @param {Number} zoomfactor
        */
  _normalizeZoomFactor: function (zoomFactor) {
    // For invalid zoom factors set to default
    if (!Number.isFinite(zoomFactor)) {
      return DEFAULT_ZOOM_FACTOR;
    }

    // Otherwise normalize to nearest supported zoom factor
    if (zoomFactor > MAX_ZOOM_FACTOR) {
      return MAX_ZOOM_FACTOR;
    } else
    if (zoomFactor < MIN_ZOOM_FACTOR) {
      return MIN_ZOOM_FACTOR;
    } else {
      return zoomFactor;
    }
  },

  getCurrentScaleFactor: function () {
    return 1 + this.currentZoomLevel * 0.05;
  },

  /**
      * Valid zoom levels in apps before v7.22.0 were in range [-10, 10]
      * The new zoom levels introduced in are in range [0, 16]
      * This function returns the zoom scale factor associated with the older zoom levels
      */
  _loadZoomFactorFromLevel: function () {
    this.currentZoomLevel = pm.settings.getSetting('uiZoom') || DEFAULT_ZOOM_LEVEL;

    return this.getCurrentScaleFactor();
  } });


/* harmony default export */ __webpack_exports__["default"] = (UIZoom);

/***/ }),

/***/ 6857:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return bootThemeManager; });
/* harmony import */ var _controllers_theme_ThemeManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2669);
/* harmony import */ var _controllers_theme_ThemeDomDelegator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6858);
/* harmony import */ var _utils_ScratchpadUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(435);




/**
                                                                     * Initialize Theme Dom delegator with current theme and eventBus
                                                                     *
                                                                     * @param {*} cb
                                                                     */
function bootThemeManager(cb) {
  let currentTheme = _controllers_theme_ThemeManager__WEBPACK_IMPORTED_MODULE_0__["default"].getCurrentTheme();

  if (Object(_utils_ScratchpadUtils__WEBPACK_IMPORTED_MODULE_2__["isEmbeddedScratchpad"])()) {
    let currentURL = new URL(location.href),
    outerViewTheme = currentURL.searchParams.get('currentTheme');
    if (outerViewTheme && outerViewTheme !== currentTheme) {
      _controllers_theme_ThemeManager__WEBPACK_IMPORTED_MODULE_0__["default"].changeTheme(outerViewTheme);
      currentTheme = outerViewTheme;
    }
  }

  _controllers_theme_ThemeDomDelegator__WEBPACK_IMPORTED_MODULE_1__["default"].init(currentTheme);

  pm.logger.info('ThemeManager~boot - Success');
  cb && cb(null);
}

/***/ }),

/***/ 6858:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ThemeHelpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6859);
/* harmony import */ var _themes_design_system_light_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6860);
/* harmony import */ var _themes_design_system_dark_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6862);
/* harmony import */ var _themes_light_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6863);
var _themes_light_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(6863, 1);
/* harmony import */ var _themes_dark_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6864);
var _themes_dark_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(6864, 1);






const THEME_LIGHT = 'light',
THEME_DARK = 'dark';

const THEME_COLORS_MAP = {
  light: _themes_light_json__WEBPACK_IMPORTED_MODULE_3__,
  dark: _themes_dark_json__WEBPACK_IMPORTED_MODULE_4__ };


const DESIGN_SYSTEM_THEME_COLORS_MAP = {
  light: _themes_design_system_light_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  dark: _themes_design_system_dark_js__WEBPACK_IMPORTED_MODULE_2__["default"] };


/**
                                        * Theme manager delegator to manipulate DOM
                                        */
/* harmony default export */ __webpack_exports__["default"] = ({

  /**
                 * Initialize the ThemeDomDeligator
                 * Removes the old theme and updates new theme to the dom
                 * Attaches handler to event themeChanged
                 * @param {string} theme - The name of the theme
                 * @param {Object} eventBus
                 * @public
                 */
  init(theme) {
    this._applyTheme(theme);
    this._subscribeThemeChange();
  },

  /**
     * Removes the old theme and initiate the theme updation process
     * @param {String} theme- The name of theme to be applied
     * @private
     */
  _applyTheme(theme) {
    let currentTheme = theme;
    currentTheme = currentTheme === THEME_LIGHT || currentTheme === THEME_DARK ? currentTheme : THEME_LIGHT;
    this._useTheme(currentTheme);
    this._triggerMediator(currentTheme);
    this._addThemeClass(currentTheme);
  },

  /**
     * Attaches a handler to themeChanged event
     * @param {Object} eventBus
     * @private
     */
  _subscribeThemeChange() {
    let appEventsChannel = pm.eventBus.channel('theme-events');
    appEventsChannel.subscribe(event => {
      if (event.name === 'themeChanged') {
        this._applyTheme(event.data.apptheme.currentTheme);
        pm.settings._updateCache('postmanTheme', event.data.apptheme.currentTheme);
      }
    });
  },

  /**
     * Adds the current theme name to app root level class
     * @param {String} theme- The name of theme to be added
     * @private
     */
  _addThemeClass(theme) {
    let rootEl = document.getElementsByClassName('app-root')[0];
    rootEl.dataset.theme = theme;
  },

  /**
     * Uses the given theme
     * @param {string} theme- The name of theme
     * @private
     */
  _useTheme(theme) {
    let themeColors = THEME_COLORS_MAP[theme];
    let designSystemThemeColors = DESIGN_SYSTEM_THEME_COLORS_MAP[theme];
    let style = document.getElementById('theme');
    style.innerHTML = `
      :root {
        ${Object(_ThemeHelpers__WEBPACK_IMPORTED_MODULE_0__["generateCSSVariables"])(themeColors)}
        ${Object(_ThemeHelpers__WEBPACK_IMPORTED_MODULE_0__["populateThemedDesignTokens"])(designSystemThemeColors)}
      }
    `;
  },

  /* TODO:  Need to move this to store and observe on reactions */
  _triggerMediator(theme) {
    if (pm.mediator) {
      pm.mediator.trigger('themeChange', theme);
    }
  } });

/***/ }),

/***/ 6859:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lighten", function() { return lighten; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "darken", function() { return darken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLightColor", function() { return isLightColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hexToRGB", function() { return hexToRGB; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "generateCSSVariables", function() { return generateCSSVariables; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "populateThemedDesignTokens", function() { return populateThemedDesignTokens; });
/**
 * To lighten or darken a color
 *
 * HEX --> RGB --> HSL -->   Change L value   --> RGB --> HEX
 *                          by given shade %
 *
 *
 */

const shades = {
  one: 0.04,
  two: 0.08,
  three: 0.15,
  four: 0.25 },

highlightOpacity = 0.2;


/**
                         *
                         * @param {*} hex
                         */
function hexToHSL(hex) {
  let { r, g, b } = hexToRGB(hex),
  max = 0,
  min = 0;

  r /= 255, g /= 255, b /= 255,
  max = Math.max(r, g, b),
  min = Math.min(r, g, b);

  var h,
  s,
  l = (max + min) / 2;
  if (max == min) {
    h = s = 0; // achromatic
  } else
  {
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch (max) {
      case r:
        h = (g - b) / d + (g < b ? 6 : 0);
        break;
      case g:
        h = (b - r) / d + 2;
        break;
      case b:
        h = (r - g) / d + 4;
        break;}

    h /= 6;
  }

  return { h, s, l };
}

/**
   * hue to rgb
   */
function _hue2rgb(p, q, t) {
  if (t < 0) t += 1;
  if (t > 1) t -= 1;
  if (t < 1 / 6) return p + (q - p) * 6 * t;
  if (t < 1 / 2) return q;
  if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;

  return p;
}

/**
   * RGB to HEX
   */
function _rgbToHex(rgb) {
  var hex = Number(rgb).toString(16);
  if (hex.length < 2) {
    hex = '0' + hex;
  }
  return hex;
}

/**
   * HSL to HEX
   */
function hslToHEX(h, s, l) {
  var r, g, b;
  if (l < 0) {
    l = 0;
  }
  if (l > 1) {
    l = 1;
  }
  if (s == 0) {
    r = g = b = l; // achromatic
  } else {
    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = _hue2rgb(p, q, h + 1 / 3);
    g = _hue2rgb(p, q, h);
    b = _hue2rgb(p, q, h - 1 / 3);
  }

  r = Math.round(r * 255);
  g = Math.round(g * 255);
  b = Math.round(b * 255);

  return `#${_rgbToHex(r)}${_rgbToHex(g)}${_rgbToHex(b)}`;
}

/**
   * Lighten the color by given percentage
   */
function lighten(color, percentage) {
  let hsl = hexToHSL(color);
  return hslToHEX(hsl.h, hsl.s, hsl.l + percentage);
}

/**
   * Darken the color by given percentage
   */
function darken(color, percentage) {
  let hsl = hexToHSL(color);
  return hslToHEX(hsl.h, hsl.s, hsl.l - percentage);
}

/**
   * Check if color is perceived as light color
   *  https://en.wikipedia.org/wiki/Luma_(video)
   */
function isLightColor({ r, g, b }) {
  0.2126 * r + 0.7152 * g + 0.0722 * b > 180;
}

/**
   * Convert hex to hexToRGB
   */
function hexToRGB(hex) {
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16) };

}

/**
   *
   * @param {*} theme - Object of theme colors
   */
function generateCSSVariables(theme) {
  let cssColorVariables = [];
  for (let key in theme) {
    cssColorVariables.push(`${key}: ${theme[key]};`);
    let { l } = hexToHSL(theme[key]);
    if (_.includes(key, 'accent') || _.includes(key, 'brand')) {
      let { r, g, b } = hexToRGB(theme[key]);
      cssColorVariables.push(`${key}--highlight: rgba(${r}, ${g}, ${b}, ${highlightOpacity});`);
    }
    for (let shade in shades) {
      if (l > 0.50) {
        cssColorVariables.push(`${key}--shade--${shade}: ${darken(theme[key], shades[shade])};`);
      } else
      {
        cssColorVariables.push(`${key}--shade--${shade}: ${lighten(theme[key], shades[shade])};`);
      }
    }
  }

  return cssColorVariables.join('');
}

/**
   * This function iterates through JSON of themed dependent design tokens,
   * populate each key-value pair into a CSS variable syntax and join them
   * into a single string.
   *
   * @param {*} theme - Object of themed design tokens
   */
function populateThemedDesignTokens(theme) {
  let themedDesignTokens = [];
  for (let key in theme) {
    themedDesignTokens.push(`${key}: ${theme[key]};`);
  }
  return themedDesignTokens.join('');
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 6860:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalColors_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6861);
var _globalColors_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(6861, 1);
 /* TODO: Replace this with global colors imported from design-system package */

const lightTheme = {
  '--content-color-primary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-90'],
  '--content-color-secondary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-50'],
  '--content-color-tertiary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-40'],
  '--content-color-brand': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['orange-70'],
  '--content-color-info': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['blue-70'],
  '--content-color-error': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['red-70'],
  '--content-color-success': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['green-70'],
  '--content-color-warning': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['yellow-70'],
  '--content-color-link': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['blue-60'],
  '--background-color-primary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-00'],
  '--background-color-secondary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-05'],
  '--background-color-tertiary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-10'],
  '--background-color-info': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['blue-10'],
  '--background-color-brand': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['orange-10'],
  '--background-color-error': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['red-10'],
  '--background-color-success': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['green-10'],
  '--background-color-warning': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['yellow-10'],
  '--border-color-default': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-20'],
  '--border-color-strong': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-30'],
  '--highlight-background-color-primary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-20'],
  '--highlight-background-color-secondary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-20'],
  '--highlight-background-color-tertiary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-30'],
  '--highlight-background-color-transparent': 'rgba(26, 26, 26, 0.1)',
  '--background-modal-backdrop': 'rgba(26, 26, 26, 0.5)',
  '--shadow-default': '0px 2px 8px 0px rgba(0, 0, 0, 0.2)',
  '--illustration-stroke-color': '#686868',

  '--color-content-primary': '#1A1A1A',
  '--color-content-secondary': '#666666',
  '--color-content-tertiary': '#999999',
  '--color-content-brand-primary': '#C73500',
  '--color-content-brand-secondary': '#145EA9',
  '--color-content-accent-error': '#B32E14',
  '--color-content-accent-success': '#127347',
  '--color-content-accent-warning': '#7D6517',
  '--color-background-primary': '#FFFFFF',
  '--color-background-secondary': '#F7F7F7',
  '--color-background-tertiary': '#EBEBEB',
  '--color-border-regular': '#E3E3E3',
  '--color-border-strong': '#D9D9D9',
  '--color-highlight-background-primary': '#F2F2F2',
  '--color-highlight-background-secondary': '#EBEBEB',
  '--color-highlight-background-tertiary': '#E3E3E3' };


/* harmony default export */ __webpack_exports__["default"] = (lightTheme);

/***/ }),

/***/ 6861:
/***/ (function(module) {

module.exports = JSON.parse("{\"grey-00\":\"#FFFFFF\",\"grey-05\":\"#F9F9F9\",\"grey-10\":\"#F2F2F2\",\"grey-20\":\"#EDEDED\",\"grey-30\":\"#E6E6E6\",\"grey-40\":\"#A6A6A6\",\"grey-50\":\"#6B6B6B\",\"grey-60\":\"#3B3B3B\",\"grey-70\":\"#303030\",\"grey-80\":\"#2B2B2B\",\"grey-85\":\"#262626\",\"grey-90\":\"#212121\",\"orange-10\":\"#FFF1EB\",\"orange-20\":\"#FFD1BE\",\"orange-30\":\"#FFB091\",\"orange-40\":\"#FF8E64\",\"orange-50\":\"#FF6C37\",\"orange-60\":\"#E05320\",\"orange-70\":\"#D23F0E\",\"orange-80\":\"#A12700\",\"orange-90\":\"#553326\",\"blue-10\":\"#E7F0FF\",\"blue-20\":\"#ADCDFB\",\"blue-30\":\"#74AEF6\",\"blue-40\":\"#3E92F2\",\"blue-50\":\"#097BED\",\"blue-60\":\"#0265D2\",\"blue-70\":\"#0053B8\",\"blue-80\":\"#00439D\",\"blue-90\":\"#002D70\",\"red-10\":\"#FFEBE7\",\"red-20\":\"#FBC3BA\",\"red-30\":\"#F79A8E\",\"red-40\":\"#F37264\",\"red-50\":\"#EB2013\",\"red-60\":\"#D10D03\",\"red-70\":\"#B70700\",\"red-80\":\"#9C0400\",\"red-90\":\"#591B08\",\"green-10\":\"#E5FFF1\",\"green-20\":\"#A4EEC4\",\"green-30\":\"#6BDD9A\",\"green-40\":\"#38CC74\",\"green-50\":\"#0CBB52\",\"green-60\":\"#049D40\",\"green-70\":\"#007F31\",\"green-80\":\"#006024\",\"green-90\":\"#013614\",\"yellow-10\":\"#FFF9E0\",\"yellow-20\":\"#FFF4BE\",\"yellow-30\":\"#FFE47E\",\"yellow-40\":\"#FFCF3F\",\"yellow-50\":\"#FFB400\",\"yellow-60\":\"#DE9C00\",\"yellow-70\":\"#AD7A03\",\"yellow-80\":\"#886000\",\"yellow-90\":\"#523A03\"}");

/***/ }),

/***/ 6862:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalColors_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6861);
var _globalColors_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(6861, 1);
 /* TODO: Replace this with global colors imported from design-system package */

const darkTheme = {
  '--content-color-primary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-00'],
  '--content-color-secondary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-40'],
  '--content-color-tertiary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-50'],
  '--content-color-brand': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['orange-40'],
  '--content-color-info': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['blue-30'],
  '--content-color-error': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['red-30'],
  '--content-color-success': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['green-30'],
  '--content-color-warning': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['yellow-30'],
  '--content-color-link': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['blue-40'],
  '--background-color-primary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-90'],
  '--background-color-secondary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-85'],
  '--background-color-tertiary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-80'],
  '--background-color-info': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['blue-90'],
  '--background-color-brand': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['orange-90'],
  '--background-color-error': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['red-90'],
  '--background-color-success': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['green-90'],
  '--background-color-warning': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['yellow-90'],
  '--border-color-default': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-70'],
  '--border-color-strong': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-60'],
  '--highlight-background-color-primary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-70'],
  '--highlight-background-color-secondary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-70'],
  '--highlight-background-color-tertiary': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-60'],
  '--highlight-background-color-transparent': 'rgba(255, 255, 255, 0.1)',
  '--background-modal-backdrop': 'rgba(0, 0, 0, 0.7)',
  '--shadow-default': '0px 2px 8px 0px rgba(0, 0, 0, 0.65)',
  '--illustration-stroke-color': _globalColors_json__WEBPACK_IMPORTED_MODULE_0__['grey-50'],

  '--color-content-primary': '#FFFFFF',
  '--color-content-secondary': '#BFBFBF',
  '--color-content-tertiary': '#808080',
  '--color-content-brand-primary': '#FF7542',
  '--color-content-brand-secondary': '#80BFFF',
  '--color-content-accent-error': '#EEA495',
  '--color-content-accent-success': '#41E199',
  '--color-content-accent-warning': '#FFBD1F',
  '--color-background-primary': '#262626',
  '--color-background-secondary': '#2E2E2E',
  '--color-background-tertiary': '#363636',
  '--color-border-regular': '#3D3D3D',
  '--color-border-strong': '#4D4D4D',
  '--color-highlight-background-primary': '#333333',
  '--color-highlight-background-secondary': '#3B3B3B',
  '--color-highlight-background-tertiary': '#424242' };


/* harmony default export */ __webpack_exports__["default"] = (darkTheme);

/***/ }),

/***/ 6863:
/***/ (function(module) {

module.exports = JSON.parse("{\"--bg-primary\":\"#FFFFFF\",\"--bg-secondary\":\"#FAFAFA\",\"--bg-tertiary\":\"#ECECEC\",\"--hairline-regular\":\"#EAEAEA\",\"--hairline-strong\":\"#D4D4D4\",\"--content-primary\":\"#505050\",\"--content-secondary\":\"#808080\",\"--content-tertiary\":\"#A9A9A9\",\"--brand-primary\":\"#F26B3A\",\"--brand-secondary\":\"#097BED\",\"--accent-error\":\"#ED4B48\",\"--accent-warning\":\"#FFB400\",\"--accent-success\":\"#26B47F\",\"--color-darkest\":\"#282828\",\"--dark-bg-primary\":\"#303030\",\"--dark-bg-secondary\":\"#333333\",\"--dark-bg-tertiary\":\"#464646\",\"--dark-hairline-regular\":\"#3C3C3C\",\"--dark-hairline-strong\":\"#5A5A5A\",\"--dark-content-primary\":\"#F0F0F0\",\"--dark-content-secondary\":\"#808080\",\"--dark-color-lightest\":\"#FFFFFF\"}");

/***/ }),

/***/ 6864:
/***/ (function(module) {

module.exports = JSON.parse("{\"--bg-primary\":\"#282828\",\"--bg-secondary\":\"#303030\",\"--bg-tertiary\":\"#404040\",\"--hairline-regular\":\"#434343\",\"--hairline-strong\":\"#464646\",\"--content-primary\":\"#f0f0f0\",\"--content-secondary\":\"#969696\",\"--content-tertiary\":\"#6F6F6F\",\"--brand-primary\":\"#F26B3A\",\"--brand-secondary\":\"#097BED\",\"--accent-error\":\"#ED4B48\",\"--accent-warning\":\"#FFB400\",\"--accent-success\":\"#26B47F\",\"--color-darkest\":\"#ffffff\",\"--dark-bg-primary\":\"#303030\",\"--dark-bg-secondary\":\"#333333\",\"--dark-bg-tertiary\":\"#464646\",\"--dark-hairline-regular\":\"#3C3C3C\",\"--dark-hairline-strong\":\"#5A5A5A\",\"--dark-content-primary\":\"#F0F0F0\",\"--dark-content-secondary\":\"#808080\",\"--dark-color-lightest\":\"#FFFFFF\"}");

/***/ }),

/***/ 6967:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Theme", function() { return Theme; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(82);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_2__);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};



const theme = {
  light: _postman_aether__WEBPACK_IMPORTED_MODULE_2__["light"], dark: _postman_aether__WEBPACK_IMPORTED_MODULE_2__["dark"] };


// wrapper Theme component which exposes the design tokens based on current theme
const Theme = ({ children }) => {

  const [currentTheme, setCurrentTheme] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(_extends({
    name: pm.settings.getSetting('postmanTheme') },
  theme[pm.settings.getSetting('postmanTheme')]));

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    pm.settings.on('setSetting:postmanTheme', () => {
      setCurrentTheme(_extends({
        name: pm.settings.getSetting('postmanTheme') },
      theme[pm.settings.getSetting('postmanTheme')]));

    });
  }, []);
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(styled_components__WEBPACK_IMPORTED_MODULE_1__["ThemeProvider"], { theme: currentTheme }, children);
};

/***/ })

}]);