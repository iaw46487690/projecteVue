(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[38],{

/***/ 15395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionChangelog; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_collections_browser_CollectionBrowserActivityFeed__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15396);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(420);
/* harmony import */ var _js_utils_util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(448);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3159);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2304);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(80);
var _class;








/**
                                        * Collection Changelog
                                        *
                                        * @param {Object} props
                                        */let

CollectionChangelog = Object(mobx_react__WEBPACK_IMPORTED_MODULE_6__["observer"])(_class = class CollectionChangelog extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  componentDidMount() {
    _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_5__["default"].addEventV2({
      category: 'collection',
      action: 'view_changelog',
      label: 'collection_context_bar',
      entityId: _.get(this.props, 'contextData.collectionUid') });

  }

  render() {
    const { id } = this.props.contextData,
    collection = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CollectionStore').find(id),
    currentUser = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore'),
    canRestore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('PermissionStore').can('restore', 'collection', id),
    isProUser = _js_utils_util__WEBPACK_IMPORTED_MODULE_3__["default"].isProUser(currentUser),
    isSocketConnected = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('SyncStatusStore').isSocketConnected;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__["default"], { identifier: 'activityFeed' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_collections_browser_CollectionBrowserActivityFeed__WEBPACK_IMPORTED_MODULE_1__["default"], {
          canAddVersionTag: collection && collection.isEditable && currentUser.isLoggedIn,
          collectionUid: collection.uid,
          enableRestore: isProUser && canRestore,
          key: collection.uid,
          title: this.props.title,
          isOffline: !isSocketConnected,
          isLoggedIn: currentUser.isLoggedIn,
          onClose: this.props.onClose })));



  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CollectionBrowserActivityFeed; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3659);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_date_helper__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _activity_feed_ActivityFeed__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(15397);
/* harmony import */ var _modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3947);
/* harmony import */ var _activity_feed_AddVersionTagModal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15400);
/* harmony import */ var _stores_CollectionActivityFeedStore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3051);
/* harmony import */ var _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2304);
/* harmony import */ var _modules_model_event__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(429);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(420);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_9__);










let

CollectionBrowserActivityFeed = class CollectionBrowserActivityFeed extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      collectionRestoreTarget: null,
      openVersionTagModal: false,
      isRemovingTag: false,
      isRestored: false };


    this.handleLoadNew = this.handleLoadNew.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleRestore = this.handleRestore.bind(this);
    this.handleRetry = this.handleRetry.bind(this);
    this.handleAddVersionTag = this.handleAddVersionTag.bind(this);
    this.handleRemoveVersionTag = this.handleRemoveVersionTag.bind(this);
    this.handleVersionTagModalClose = this.handleVersionTagModalClose.bind(this);
  }

  UNSAFE_componentWillMount() {
    this.store = new _stores_CollectionActivityFeedStore__WEBPACK_IMPORTED_MODULE_5__["default"]();

    if (!this.props.isOffline) {
      this.initializeFeeds();
    }

    this.subscribeToCollectionRestore();
  }

  componentWillUnmount() {
    this.unsubscribe && typeof this.unsubscribe === 'function' && this.unsubscribe();
  }

  initializeFeeds() {
    let collectionUid = this.props.collectionUid;
    this.store.initialize(collectionUid);
  }

  handleLoadNew() {
    return this.store.loadNew().
    then(isNewFeedLoaded => {
      return Promise.resolve(isNewFeedLoaded);
    });
  }

  handleLoadMore() {
    this.store.loadMore();
  }

  handleSignIn() {
    _modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_3__["default"].initiateLogin();
  }

  isWorkspaceMember() {
    return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_8__["getStore"])('ActiveWorkspaceStore').isMember;
  }

  /**
     * Note: If user is not a member of active workspace, action will be blocked by a modal
     * asking user to join the workspace first.
     */
  handleAddVersionTag(revisionId) {
    // If the user is not a part of the workspace, then
    if (!this.isWorkspaceMember()) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    }

    // Add analytics event to track initial version tag create
    _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_6__["default"].addEventV2({
      category: 'collection',
      action: 'initiate_version_tag_create',
      label: 'collection_changelog',
      entityId: this.props.collectionUid });


    this.setState({
      openVersionTagModal: !this.state.openVersionTagModal,
      revisionId: revisionId });

  }

  /**
     * Note: If user is not a member of active workspace, action will be blocked by a modal
     * asking user to join the workspace first.
     */
  handleRemoveVersionTag(revisionId, tagId) {
    if (!this.isWorkspaceMember()) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    }

    this.setState({ isRemovingTag: !this.state.isRemovingTag });

    let criteria = {
      collectionUid: this.props.collectionUid,
      tagId: tagId,
      revisionId: revisionId };


    // Add analytics event to track version tag delete
    _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_6__["default"].addEventV2({
      category: 'collection',
      action: 'version_tag_delete',
      label: 'collection_changelog',
      entityId: this.props.collectionUid });


    this.store.removeVersionTag(criteria).
    then(() => {
      this.setState({ isRemovingTag: !this.state.isRemovingTag });
    }).
    catch(() => {
      this.setState({ isRemovingTag: !this.state.isRemovingTag });
    });
  }

  handleVersionTagModalClose() {
    this.setState({ openVersionTagModal: !this.state.openVersionTagModal });
  }

  handleRestore(maxId, createdAt) {
    let restoreTarget = {
      collectionUid: this.props.collectionUid,
      maxId: maxId };


    this.restoreDate = _postman_date_helper__WEBPACK_IMPORTED_MODULE_1___default.a.getFormattedDateAndTime(createdAt);

    // Add analytics event to show that collection has been restored
    _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_6__["default"].addEventV2({
      category: 'collection',
      action: 'restore_collection',
      label: 'collection_changelog',
      entityId: this.props.collectionUid });


    this.setState({ isRestored: false });
    this.setState({ collectionRestoreTarget: restoreTarget }, () => {
      pm.syncManager.restoreCollection(restoreTarget, (error, res) => {

        // Since artemis uses restore collection from Sync Manager New directly, we need to handle the callback response here
        // and set respective restored and collectionRestoreTarget states.
        if (error || res && res.error) {
          // when socket connection is not available
          this.setState({ isRestored: false, collectionRestoreTarget: null });

          pm.toasts.error('We couldn\'t restore collection to a previous state. Please try again later.', { noIcon: true, title: 'Unable to restore collection' });
        } else
        if (res && res.data) {
          this.setState({ isRestored: true, collectionRestoreTarget: null });

          pm.toasts.success(`Your collection has been successfully restored to ${this.restoreDate || 'a previous state'}.`, { noIcon: true, title: 'Collection restored' });
        }
      });
    });
  }

  subscribeToCollectionRestore() {
    this.unsubscribe = pm.eventBus.channel('sync-manager-internal').subscribe(event => {
      if (!event) {
        return;
      }

      const eventNamespace = Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_7__["getEventNamespace"])(event),
      eventName = Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_7__["getEventName"])(event),
      eventData = Object(_modules_model_event__WEBPACK_IMPORTED_MODULE_7__["getEventData"])(event) || {};

      if (eventName === 'collectionRestored' && eventNamespace === 'command') {
        this.setState({ isRestored: true, collectionRestoreTarget: null });

        if (!eventData.error) {
          pm.toasts.success(`Your collection has been successfully restored to ${this.restoreDate || 'a previous state'}.`, { noIcon: true, title: 'Collection restored' });
        } else
        {
          pm.toasts.error('We couldn\'t restore collection to a previous state. Please try again later.', { noIcon: true, title: 'Unable to restore collection' });
        }
      }
    });
  }

  /**
     * @param {Boolean} - options.initialRetry - retry initializing change-logs
     * @param {Boolean} - options.loadMoreRetry - retry loading more change-logs
     */
  handleRetry(options) {
    if (options.initialRetry) {
      this.initializeFeeds();
    } else
    if (options.loadMoreRetry) {
      this.handleLoadMore();
    }
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_9___default()({ 'collection-browser-activity-feed-lists': true }, this.props.className);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getClasses(), onClickCapture: e => {console.log('|| clicked', e.target);} },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_activity_feed_ActivityFeed__WEBPACK_IMPORTED_MODULE_2__["default"], {
          activityFeed: this.store,
          title: this.props.title,
          canAddVersionTag: this.props.canAddVersionTag,
          collectionRestoreTarget: this.state.collectionRestoreTarget,
          enableRestore: this.props.enableRestore,
          isLoggedIn: this.props.isLoggedIn,
          isRemovingTag: this.state.isRemovingTag,
          isRestored: this.state.isRestored,
          isVisible: this.props.isVisible,
          isOffline: this.props.isOffline,
          onLoadNew: this.handleLoadNew,
          onLoadMore: this.handleLoadMore,
          onSignIn: this.handleSignIn,
          onRestore: this.handleRestore,
          onRetry: this.handleRetry,
          onAddVersionTag: this.handleAddVersionTag,
          onRemoveVersionTag: this.handleRemoveVersionTag,
          onClose: this.props.onClose }),


        this.state.openVersionTagModal &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_activity_feed_AddVersionTagModal__WEBPACK_IMPORTED_MODULE_4__["default"], {
          activityFeed: this.store,
          revisionId: this.state.revisionId,
          collectionUid: this.props.collectionUid,
          isOffline: this.props.isOffline,
          isOpen: this.state.openVersionTagModal,
          onRequestClose: this.handleVersionTagModalClose })));




  }};

/***/ }),

/***/ 15397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ActivityFeed; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Activity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15398);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3659);
/* harmony import */ var _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_date_helper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pure_render_decorator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3212);
/* harmony import */ var pure_render_decorator__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pure_render_decorator__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2800);
/* harmony import */ var _external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2301);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3667);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3170);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(80);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _version_control_pull_request_components_PullRequestListing__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(15399);
var _class;











let


ActivityFeed = Object(mobx_react__WEBPACK_IMPORTED_MODULE_10__["observer"])(_class = class ActivityFeed extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {

  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
    this.handleScrollDebounced = _.debounce(this.handleScroll, 100);
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleRefreshFeed = this.handleRefreshFeed.bind(this);
    this.handleChangeLogLearn = this.handleChangeLogLearn.bind(this);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.activityFeed &&
    this.props.activityFeed &&
    nextProps.activityFeed.collectionUid &&
    nextProps.activityFeed.collectionUid !== this.props.activityFeed.collectionUid) {
      this.refs.activity_feed && (this.refs.activity_feed.scrollTop = 0);
    }

    if (nextProps.isRestored && nextProps.isRestored !== this.props.isRestored) {
      this.handleRefreshFeed();
    }
  }

  componentDidUpdate(prevProps) {
    // Refetch the changelog once back from offline state if it is not yet fetched
    const activities = _.get(prevProps, 'activityFeed.feeds', []),
    meta = _.get(prevProps, 'activityFeed.meta', {}),
    filteredActivities = _.filter(activities, activity => {
      return !(meta.model === 'collection' && _.includes(['subscribe', 'unsubscribe'], activity.action));
    }),
    wasOffline = prevProps.isOffline;

    if (wasOffline && wasOffline !== this.props.isOffline && _.isEmpty(filteredActivities)) {
      this.props.onRetry({ initialRetry: true });
    }
  }

  handleChangeLogLearn() {
    Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_6__["openExternalLink"])(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_5__["ACTIVITY_FEED_AND_RESTORING_COLLECTION"]);
  }

  getRefreshTextClass() {
    return classnames__WEBPACK_IMPORTED_MODULE_9___default()({
      'changelog-learn-more-header__refresh-container': true,
      'is-loading-new': this.props.activityFeed.isLoadingNew });

  }

  handleSignIn() {
    this.props.onSignIn && this.props.onSignIn();
  }

  handleScroll() {
    let node = this.refs.activity_feed;
    if (node.scrollHeight - (node.scrollTop + node.offsetHeight) <= 5) {
      _.isEmpty(this.props.activityFeed.error) && this.props.onLoadMore && this.props.onLoadMore();
    }
  }

  handleRefreshFeed() {
    this.props.onLoadNew && this.props.onLoadNew().
    then(isNewFeedLoaded => {
      if (isNewFeedLoaded) {
        this.refs.activity_feed.scrollTop = 0;
      }
    });
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_9___default()({ 'activity-feed-container': true }, this.props.className);
  }

  render() {
    let props = this.props,
    activities = _.get(props, 'activityFeed.feeds', []),
    meta = _.get(props, 'activityFeed.meta', {}),
    error = _.get(props, 'activityFeed.error', {}),
    initialError = error && error.type === 'initial',
    loadMoreError = error && error.type === 'loadMore';

    // Don't show subscribe / Unsubscribe event in collection activity feed
    let filteredActivities = _.filter(activities, activity => {
      return !(meta.model === 'collection' && _.includes(['subscribe', 'unsubscribe'], activity.action));
    }),
    groupedActivities = _.isEmpty(filteredActivities) ? [] : _postman_date_helper__WEBPACK_IMPORTED_MODULE_2___default.a.getDateGroups(filteredActivities, 'createdAt', 'MMMM D, YYYY'),
    latestActivity = _.maxBy(filteredActivities, 'id');

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getClasses() },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_7__["ContextBarViewHeader"], { title: 'Changelog', onClose: this.props.onClose },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_8__["Button"], {
              type: 'tertiary',
              className: this.getRefreshTextClass(),
              onClick: this.handleRefreshFeed,
              disabled: props.activityFeed.isLoading || props.isOffline,
              tooltip: props.isOffline ? 'You can perform this action once you\'re back online' : 'Fetch newer feeds' },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_11__["Icon"], { name: 'icon-action-refresh-stroke', className: 'changelog-learn-more-header__refresh-icon pm-icon pm-icon-normal' }))),



        props.isOffline && _.isEmpty(error) && _.isEmpty(groupedActivities) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_version_control_pull_request_components_PullRequestListing__WEBPACK_IMPORTED_MODULE_12__["EmptyState"], {
          icon: 'icon-state-offline-stroke',
          title: 'Check your connection',
          message: 'Get online to view Changelog' }),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: 'activity-feed',
            ref: 'activity_feed',
            onScroll: this.handleScrollDebounced },


          (props.activityFeed.isLoading || initialError) &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Activity__WEBPACK_IMPORTED_MODULE_1__["LoadFeed"], {
            error: error,
            key: 'loading',
            isLoading: props.activityFeed.isLoading,
            onRetry: this.props.onRetry }),



          _.map(groupedActivities, subActivities => {
            if (_.isEmpty(subActivities)) {
              return false;
            }

            return (
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'activity-feed-date-group-wrapper',
                  key: subActivities.name },

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-feed-date-group' }, ' ', subActivities.name, ' '),

                _.map(subActivities.items, activity => {
                  return (
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Activity__WEBPACK_IMPORTED_MODULE_1__["Activity"], {
                      activity: activity,
                      canAddVersionTag: this.props.canAddVersionTag,
                      enableRestore: !!(this.props.enableRestore && latestActivity && latestActivity.id !== activity.id),
                      isRestoring: _.get(this.props, 'collectionRestoreTarget.maxId') === activity.id,
                      isRemovingTag: this.props.isRemovingTag,
                      isVisible: this.props.isVisible,
                      isOffline: this.props.isOffline,
                      key: activity.id,
                      meta: meta,
                      onAddVersionTag: this.props.onAddVersionTag,
                      onRemoveVersionTag: this.props.onRemoveVersionTag,
                      onRestore: this.props.onRestore }));


                })));



          }),


          !props.activityFeed.isLoading && _.isEmpty(groupedActivities) && !error &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: props.className },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-feed-loading-container ' }, 'No activities yet'))),






        (props.activityFeed.isLoadingMore || loadMoreError) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Activity__WEBPACK_IMPORTED_MODULE_1__["LoadFeed"], {
          error: error,
          key: 'loading-more',
          isLoading: props.activityFeed.isLoadingMore,
          isOffline: props.isOffline,
          onRetry: this.props.onRetry })));




  }}) || _class;


ActivityFeed.propTypes = {
  activityFeed: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.object.isRequired,
  activityName: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.string,
  onLoadMore: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.func.isRequired };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Activity", function() { return Activity; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadFeed", function() { return LoadFeed; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7130);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _base_Icons_WarningIcon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3161);
/* harmony import */ var _utils_ActivityFeedHelper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5341);
/* harmony import */ var _utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2793);
/* harmony import */ var _base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3192);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3170);
/* harmony import */ var _version_control_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5101);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;










const COLLECTION_ACTIVITY_MODEL = 'collection';
const OFFLINE_TOOLTIP_TEXT = 'You can perform this action once you\'re back online';let


Activity = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class Activity extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
  }

  findCurrentLabel(activity) {
    return _.find(activity.tags, ['system', true]);
  }

  shouldLoadCustomComponents(user) {
    return ['isPublic', 'isAccessible', 'publicProfileUrl'].every(function (key) {
      return user.hasOwnProperty(key);
    });
  }

  getProfilePicComponent(user) {
    if (this.shouldLoadCustomComponents(user)) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_version_control_common__WEBPACK_IMPORTED_MODULE_9__["CustomProfilePic"], user);
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["ProfilePic"], user);
  }

  getUserComponent(user) {
    if (this.shouldLoadCustomComponents(user)) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_version_control_common__WEBPACK_IMPORTED_MODULE_9__["CustomUser"], user);
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["User"], user);
  }

  render() {
    let activity = this.props.activity,
    meta = this.props.meta,
    destination = null,
    trigger = activity.trigger,
    changesCount = _utils_ActivityFeedHelper__WEBPACK_IMPORTED_MODULE_5__["default"].getWhiteListedActivitiesCount(activity),
    pluralizedChangeText = _utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_6__["default"].pluralize({
      count: changesCount,
      singular: 'change',
      plural: 'changes' }),

    headingText,
    activityDetails = null,
    currentLabel = this.findCurrentLabel(activity);

    if (activity.to && activity.to.model) {
      destination =
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' to the ', activity.to.model, ' '),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'activity-item-model' }, ' ', activity.to.instance.name, ' '));


    }

    switch (`${activity.model}:${activity.action}`) {
      case 'collection:create':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' created this collection');
        break;
      case 'collection:update':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item-head-content' },
            changesCount > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' made ', changesCount, ' ', pluralizedChangeText, ' to '),
            changesCount === 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' ', trigger === 'restore' ? 'restored' : 'modified', ' ')), 'this collection');



        activityDetails = changesCount > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Details"], _extends({ isVisible: this.props.isVisible }, activity));
        break;
      case 'collection:destroy':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' deleted this collection');
        break;
      case 'folder:create':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' created the folder ');
        break;
      case 'folder:update':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item-head-content' },
            changesCount > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' made ', changesCount, ' ', pluralizedChangeText, ' to '),
            changesCount === 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' modified ')), 'the folder');



        activityDetails = !_.isEmpty(activity.input) && !_.isEmpty(activity.rollback) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Details"], _extends({ isVisible: this.props.isVisible }, activity));
        break;
      case 'folder:destroy':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' deleted the folder ');
        break;
      case 'folder:transfer':
        headingText = headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' moved the folder ');
        break;
      case 'request:create':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' added the request ');
        break;
      case 'request:update':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item-head-content' },
            changesCount > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' made ', changesCount, ' ', pluralizedChangeText, ' to '),
            changesCount === 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' modified ')), 'the request');



        activityDetails = !_.isEmpty(activity.input) && !_.isEmpty(activity.rollback) &&
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Details"], _extends({ isVisible: this.props.isVisible }, activity));
        break;
      case 'request:destroy':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' deleted the request ');
        break;
      case 'request:transfer':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' moved the request ');
        break;
      case 'response:create':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' created the example ');
        break;
      case 'response:update':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' modified the example ');
        break;
      case 'response:destroy':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' deleted the example ');
        break;
      case 'response:transfer':
        headingText = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, ' moved the response ');
        break;
      default:null;}


    const canRestore = this.props.enableRestore,
    hasVersionTag = !_.isEmpty(activity.tags) && !_.head(activity.tags).system,
    canAddVersionTag = _.get(activity, 'allowedActions.versionTagCreate') && this.props.canAddVersionTag,
    hasActivityActions = canRestore || hasVersionTag || canAddVersionTag;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-feed-connect-line' }),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item-content' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Header"], null,
            this.getProfilePicComponent(activity.user),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item-head' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Meta"], null,
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Time"], { timestamp: activity.createdAt }),


                !_.isEmpty(activity.tags) &&
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'version-label-wrapper' },

                  !_.head(activity.tags).system &&
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', {
                      className: 'activity-item-tagged_version__label is-tag',
                      title: _.head(activity.tags).apiVersion.name },

                    _.head(activity.tags).apiVersion.name),



                  currentLabel &&
                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'activity-item-tagged_version__label' },
                    'CURRENT'))),






              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Heading"], null,
                this.getUserComponent(activity.user),
                headingText,
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'activity-item-model' }, ' ', activity.model !== COLLECTION_ACTIVITY_MODEL && activity.instance.name, ' '),
                destination))),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item-details-wrapper' },
            activityDetails,
            hasActivityActions &&
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-item__action' },

              hasVersionTag ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["RemoveTag"], {
                tagId: _.head(activity.tags).id,
                revisionId: activity.id,
                onRemoveVersionTag: this.props.onRemoveVersionTag,
                isRemovingTag: this.props.isRemovingTag,
                isOffline: this.props.isOffline }) :


              // Purpose of conditional rendering of <TagVersion> component is to
              // disallow free users to create version tag on non-latest revision of
              // a collection. This flag `activity.allowedActions.versionTagCreate`
              // is sent by server. Added a safe check to ensure this doesn't break
              // the app.

              canAddVersionTag &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["TagVersion"], {
                revisionId: activity.id,
                isOffline: this.props.isOffline,
                onAddVersionTag: this.props.onAddVersionTag }),




              canRestore &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ActivityItemComponents__WEBPACK_IMPORTED_MODULE_2__["Restore"], {
                createdAt: activity.createdAt,
                id: activity.id,
                isRestoring: this.props.isRestoring,
                isOffline: this.props.isOffline,
                rollback_from: meta.rollback_from,
                onRestore: this.props.onRestore }),



              this.props.enableRestore && this.props.isRestoring &&
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__["default"], { className: 'activity-item-restore-loader' }))))));







  }}) || _class;let


LoadFeed = class LoadFeed extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
  }

  getClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_3___default()({
      'activity-feed-loader': true,
      'initial-fetch-error-banner': this.props.error.type === 'initial',
      'load-more-error-banner': this.props.error.type === 'loadMore' });

  }

  handleClick(options) {
    this.props.onRetry && this.props.onRetry(options);
  }

  renderInitialErrorBanner() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-feed-initial-fetch-error-container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'initial-fetch-error-title' }, 'Couldn\'t load changelog'),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'initial-fetch-error-subtitle' }, 'There was an unexpected error. Please try again.'),


        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_8__["Button"], {
            className: 'activity-feed-retry__btn',
            onClick: this.handleClick.bind(this, { initialRetry: true }),
            disabled: this.props.isLoading || this.props.isOffline,
            tooltip: this.props.isOffline && OFFLINE_TOOLTIP_TEXT,
            tooltipPlacement: 'left' },


          this.props.isLoading && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__["default"], null) || 'Try again')));




  }

  renderLoadMoreErrorBanner() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'activity-feed-load-more-error-container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'load-more-error-info' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'load-more-error-icon' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Icons_WarningIcon__WEBPACK_IMPORTED_MODULE_4__["default"], { size: 'sm' })),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'load-more-error-subtitle' },

            this.props.isOffline ?
            'Get online to view more changes.' :
            'Something went wrong while retrieving more changelogs. Try again.')),



        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_8__["Button"], {
            className: 'activity-feed-retry__btn',
            onClick: this.handleClick.bind(this, { loadMoreRetry: true }),
            disabled: this.props.isLoading || this.props.isOffline,
            tooltip: this.props.isOffline && OFFLINE_TOOLTIP_TEXT,
            tooltipPlacement: 'left' }, 'Retry')));





  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: this.getClasses() },

        this.props.error && this.props.error.type === 'initial' && this.renderInitialErrorBanner() ||
        this.props.error && this.props.error.type === 'loadMore' && this.renderLoadMoreErrorBanner() ||
        this.props.isLoading && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_7__["default"], null)));



  }};



/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AddVersionTagModal; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2301);
/* harmony import */ var _base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3192);
/* harmony import */ var _base_Buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3170);
/* harmony import */ var _base_Modals__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3636);
/* harmony import */ var _base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3211);
/* harmony import */ var _api_dev_components_api_editor_common_ErrorHandler_ErrorHandler__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5377);
/* harmony import */ var _modules_services_ActivityFeedService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3052);
/* harmony import */ var _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2304);
/* harmony import */ var _constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(2800);
/* harmony import */ var _api_dev_services_APIDevService__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2763);
var _class;













const CONTEXT_LIST = [
{ model: 'collection', type: 'documentation', name: 'Documentation' },
{ model: 'collection', type: 'testsuite', name: 'Test Suite' },
{ model: 'collection', type: 'integrationtest', name: 'Integration Test' },
{ model: 'collection', type: 'contracttest', name: 'Contract Test' }];let



AddVersionTagModal = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class AddVersionTagModal extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      items: null,
      selectedAPI: null,
      selectedAPIVersion: null,
      selectedAPIContext: '',
      isAPIVersionSelectEnable: false,
      isRequiredWarning: true,
      isModalLoading: false,
      isLoading: false,
      isOnlyOneAPI: false,
      modalOpenError: null };


    this.handleClose = this.handleClose.bind(this);
    this.handleAPISelect = this.handleAPISelect.bind(this);
    this.handleAPIVersionSelect = this.handleAPIVersionSelect.bind(this);
    this.handleAPIContextSelect = this.handleAPIContextSelect.bind(this);
    this.handleAddVersionTag = this.handleAddVersionTag.bind(this);
    this.handleActivityFeedTagsLearn = this.handleActivityFeedTagsLearn.bind(this);
  }

  componentDidMount() {
    let criteria = _.assign({}, {
      collectionUid: this.props.collectionUid });

    this.store = this.props.activityFeed;
    this.initialFetch(criteria);
  }

  getCustomStyles() {
    return {
      margin: 'auto',
      height: '384px',
      width: '350px' };

  }

  handleActivityFeedTagsLearn() {
    Object(_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_3__["openExternalLink"])(_constants_AppUrlConstants__WEBPACK_IMPORTED_MODULE_11__["ACTIVITY_FEED_VERSION_TAGS"]);
  }

  handleClose() {
    this.props.onRequestClose && this.props.onRequestClose();
  }

  initialFetch(criteria) {
    this.setState({
      isModalLoading: true });


    _modules_services_ActivityFeedService__WEBPACK_IMPORTED_MODULE_9__["default"].fetchPossibleAPIVersions(criteria).
    then(entities => {
      this.setState({
        isModalLoading: false,
        items: entities },
      () => {
        this.state.items.length === 1 &&
        this.setState({
          selectedAPI: _.head(this.state.items),
          isAPIVersionSelectEnable: true,
          isOnlyOneAPI: true });

      });
    }).
    catch(error => {
      this.setState({
        isModalLoading: false,
        modalOpenError: error });

      pm.logger.warn(error);
    });
  }

  handleAPISelect(id) {
    this.setState({ isAPIVersionSelectEnable: true });
    let item = _.find(this.state.items, ['id', id]);

    !_.isUndefined(item) ?
    this.setState({ selectedAPI: item }) :
    this.setState({ selectedAPI: _.head(this.state.items) });
  }

  handleAPIVersionSelect(id) {
    let item = _.find(this.state.selectedAPI.versions, ['id', id]);

    this.setState({
      selectedAPIVersion: item,

      // if API and API context are selected, enable `Add version tag` button by setting `isRequiredWarning` state to `false`
      isRequiredWarning: this.state.selectedAPI && this.state.selectedAPIContext ? false : true });

  }

  handleAPIContextSelect(contextType) {
    let context = _.find(CONTEXT_LIST, ['type', contextType]);

    this.setState({
      selectedAPIContext: context,

      // if API and API version are selected, enable `Add version tag` button by setting `isRequiredWarning` state to `false`
      isRequiredWarning: this.state.selectedAPI && this.state.selectedAPIVersion ? false : true });

  }

  handleAddVersionTag() {
    // Bail out if API or API version or API context is not selected
    if (!this.state.selectedAPI || !this.state.selectedAPIVersion || !this.state.selectedAPIContext) {
      return;
    } else
    {
      this.setState({ isLoading: true });
      let criteria = {
        collectionUid: this.props.collectionUid,
        revisionId: this.props.revisionId,
        apiVersionId: this.state.selectedAPIVersion.id,
        relationType: this.state.selectedAPIContext.type };


      this.store.addVersionTag(criteria).
      then(() => {
        this.setState({ isLoading: false });

        // Add analytics event to track initial version tag create
        _modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_10__["default"].addEventV2({
          category: 'collection',
          action: 'successful_version_tag_create',
          label: 'collection_changelog',
          entityId: this.props.collectionUid });


        this.handleClose();
      }).
      catch(() => {
        this.setState({ isLoading: false });
      });
    }
  }

  getModalContentClasses() {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'version-tag__modal-content': true,
      'is-modal-loading': this.state.isModalLoading });

  }

  getDropdownHeadLabelClasses(showPlaceholderText) {
    return classnames__WEBPACK_IMPORTED_MODULE_2___default()({
      'dropdown-head-label': true,
      'placeholder-text': showPlaceholderText });

  }

  renderModalContent() {
    if (this.state.isModalLoading) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_4__["default"], null);
    }

    if (this.state.modalOpenError) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'version-tag-modal-error' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_api_dev_components_api_editor_common_ErrorHandler_ErrorHandler__WEBPACK_IMPORTED_MODULE_8__["default"], {
          title: 'Something went wrong',
          description: 'There was a problem adding a version tag to this collection. Please try again after some time.' }));


    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'version-tag__modal-content-heading' }, 'Link this collection revision to a specific API version.',

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', {
              className: 'btn btn-text version-tag__learn-btn-text',
              onClick: this.handleActivityFeedTagsLearn }, 'Learn More')),




        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'version-tag__modal-content-dropdown' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-select__dropdown' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-label' }, 'Select API'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["Dropdown"], { onSelect: this.handleAPISelect },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropdownButton"], { type: 'secondary', size: 'small' },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
                    className: 'dropdown-head',
                    disabled: this.state.isOnlyOneAPI },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: this.getDropdownHeadLabelClasses(!this.state.selectedAPI) },
                    this.state.selectedAPI && this.state.selectedAPI.name || 'Select an API'))),



              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropdownMenu"], {
                  className: 'dropdown-menu',
                  fluid: true },


                !_.isEmpty(this.state.items) ?
                this.state.items.map(menuItem => {
                  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], { key: menuItem.id, refKey: menuItem.id }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, menuItem.name));
                }) :
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], { disabled: true }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, 'There are no APIs in this workspace'))))),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-version-select__dropdown' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-label' }, 'Select version'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["Dropdown"], {
                onSelect: this.handleAPIVersionSelect },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropdownButton"], { type: 'secondary', size: 'small' },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
                    className: 'dropdown-head',
                    disabled: !this.state.isAPIVersionSelectEnable },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: this.getDropdownHeadLabelClasses(!this.state.selectedAPIVersion) },
                    this.state.selectedAPIVersion && this.state.selectedAPIVersion.name || 'Select a version'))),



              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropdownMenu"], {
                  className: 'dropdown-menu-list',
                  fluid: true },


                this.state.selectedAPI &&
                !_.isEmpty(this.state.selectedAPI.versions) ?
                this.state.selectedAPI.versions.map(menuItem => {
                  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], { key: menuItem.id, refKey: menuItem.id }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, menuItem.name));
                }) :
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], { disabled: true }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, 'There are no API versions to tag this collection.'))))),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-relation-select__dropdown' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-label' }, 'Add to API as'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["Dropdown"], {
                onSelect: this.handleAPIContextSelect },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropdownButton"], { type: 'secondary', size: 'small' },
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
                    className: 'dropdown-head',
                    disabled: !this.state.isAPIVersionSelectEnable },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: this.getDropdownHeadLabelClasses(!this.state.selectedAPIContext) },
                    this.state.selectedAPIContext.name || 'Add this to your API as'))),



              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["DropdownMenu"], {
                  className: 'dropdown-menu-list',
                  fluid: true },


                CONTEXT_LIST.map(menuItem => {
                  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Dropdowns__WEBPACK_IMPORTED_MODULE_7__["MenuItem"], { key: menuItem.type, refKey: menuItem.type }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, menuItem.name));
                }))))),





        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'version-tag__modal-content-action' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
              type: 'secondary',
              onClick: this.props.onRequestClose,
              className: 'modal-cancel-btn' }, 'Cancel'),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Buttons__WEBPACK_IMPORTED_MODULE_5__["Button"], {
              type: 'primary',
              onClick: this.handleAddVersionTag,
              className: 'modal-version-tag-btn',
              disabled: this.state.isRequiredWarning || this.state.isLoading || this.props.isOffline,
              tooltip:
              this.props.isOffline ?
              'You can perform this action once you\'re back online' :
              'To add a version tag, select an API version and what you want to add the collection as.' },



            this.state.isLoading ?
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_4__["default"], null) :
            'Add Version Tag'))));




  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Modals__WEBPACK_IMPORTED_MODULE_6__["Modal"], {
          className: 'version-tag-modal',
          isOpen: this.props.isOpen,
          onRequestClose: this.props.onRequestClose,
          customStyles: this.getCustomStyles() },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Modals__WEBPACK_IMPORTED_MODULE_6__["ModalHeader"], null, ' Add a version tag '),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_Modals__WEBPACK_IMPORTED_MODULE_6__["ModalContent"], { className: this.getModalContentClasses() },
          this.renderModalContent())));



  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);