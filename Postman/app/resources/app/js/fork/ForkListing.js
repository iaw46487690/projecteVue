(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[39],{

/***/ 15401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ForkListing; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(82);
/* harmony import */ var querystring__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(588);
/* harmony import */ var querystring__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(querystring__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(420);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3667);
/* harmony import */ var _ForkListBody__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15402);
/* harmony import */ var _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2282);









const ForkList = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div`
  overflow-y: auto;
`;

const forkListConfig = {
  length: 12,
  sortBy: 'createdAt',
  sortType: 'desc' };


const storeMap = {
  'environment': 'EnvironmentStore',
  'collection': 'CollectionStore' };


/**
                                      * Fork Listing
                                      *
                                      * @param {Object} props
                                      */
function ForkListing(props) {
  const [forks, setForks] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]);
  const [forkInfo, setForkInfo] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({});
  const [loading, setLoading] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  const [isEmpty, setIsEmpty] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [error, setError] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
  const syncStatusStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore');
  const model = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])(storeMap[props.contextData.model]).find(props.contextData.id);
  const isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected;
  const isSignedOut = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore').isLoggedIn;
  const isVisitor = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('PermissionStore').can('addHistory', 'workspace', Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('ActiveWorkspaceStore').id);
  const queryObject = {
    populateUsers: true,
    pageSize: forkListConfig.length,
    sortBy: forkListConfig.sortBy,
    sortType: forkListConfig.sortType };

  const fetchForks = () => {
    setLoading(true);
    setError(null);

    syncStatusStore.
    onSyncAvailable({ timeout: 5000 }).
    then(() => _js_modules_services_RemoteSyncRequestService__WEBPACK_IMPORTED_MODULE_6__["default"].request(`/${props.contextData.model}/${model.uid}/fork-list?${querystring__WEBPACK_IMPORTED_MODULE_2___default.a.stringify(queryObject)}`)).
    then(res => {
      setForks(_.get(res, 'body.data.forks'));
      setForkInfo({
        publicForks: _.get(res, 'body.data.publicForkCount'),
        privateForks: _.get(res, 'body.data.privateForkCount'),
        hiddenForks: _.get(res, 'body.data.hiddenForkCount'),
        totalForks: _.get(res, 'body.data.totalForkCount') });

      setIsEmpty(!(_.get(res, 'body.data.forks') || []).length);
      setLoading(false);
    }).
    catch(error => {
      setError(error);
      setLoading(false);

      pm.logger.error('Unable to fetch fork list', error);
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    fetchForks();
  }, []);

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ForkList, null,
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_4__["ContextBarViewHeader"], {
        title: 'Forks',
        onClose: props.onClose }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ForkListBody__WEBPACK_IMPORTED_MODULE_5__["default"], {
        loading: loading,
        isOffline: isOffline,
        isEmpty: isEmpty,
        isForbidden: isVisitor || isSignedOut,
        error: error,
        forks: forks,
        fetchForks: fetchForks,
        forkInfo: forkInfo,
        model: model,
        modelName: props.contextData.model })));



}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ForkListBody; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(82);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment_timezone__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2776);
/* harmony import */ var moment_timezone__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment_timezone__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _common_TabLoader__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5192);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5101);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3199);
/* harmony import */ var _common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5197);
/* harmony import */ var _common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(15403);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(69);
/* harmony import */ var _js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(2301);
/* harmony import */ var _appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3200);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3170);
/* harmony import */ var _js_utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(2793);
/* harmony import */ var _common_ItemLink__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(5200);

















const ListItemWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div`
  display: inline-block;
`;

const ListItemAvatar = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["default"])(_common__WEBPACK_IMPORTED_MODULE_5__["CustomTooltip"])`
  float: right;
  margin-top: var(--spacing-s);
  margin-right: var(--spacing-xxl);
`;

const HelperText = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div`
  font-size: var(--text-size-m);
  color: var(--content-color-primary);
  font-weight: var(--text-weight-medium);
  margin-bottom: var(--spacing-m);
`;

const LinkText = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].span`
  color: var(--content-color-info);
  font-size: var(--text-size-m);
  margin-top: var(--spacing-l);
  cursor: pointer;
  padding-bottom: var(--spacing-xl);

  &:hover {
    text-decoration: underline
  }
`;

const ExternalLinkIcon = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["default"])(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Icon"])`
  position: relative;
  top: 2px;
`;

/**
    * Fork List Body
    *
    * @param {Object} props - React props
    */
function ForkListBody(props) {
  const FORK_LEARNING_CENTER_LINK = {
    collection: 'https://go.pstmn.io/docs-collection-forking',
    environment: 'https://go.pstmn.io/docs-environment-forking' };


  const EmptyStateAction =
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', null,
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkText, { onClick: () => Object(_js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_10__["openExternalLink"])(FORK_LEARNING_CENTER_LINK[props.modelName], '_blank') },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_2__["Text"], { type: 'link-primary', isExternal: true }, ' Learn more about forks')));



  const RetryAction =
  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_12__["Button"], {
      type: 'primary',
      size: 'small',
      onClick: props.fetchForks }, 'Retry');




  const forkText = _js_utils_PluralizeHelper__WEBPACK_IMPORTED_MODULE_13__["default"].pluralize({
    count: _.get(props, 'forkInfo.totalForks'),
    singular: 'fork',
    plural: 'forks' });


  if (props.loading) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabLoader__WEBPACK_IMPORTED_MODULE_4__["default"], null);
  } else
  if (pm.isScratchPad) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarEmptyStateContainer"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
          illustration: 'no-fork',
          title: 'Forks not available in scratchpad',
          message: 'Check to make sure you\'re online, then switch to a workspace to view forks.' })));



  } else
  if (props.isOffline) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarEmptyStateContainer"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
          showAction: true,
          illustration: 'check-internet-connection',
          title: 'Check your connection',
          message: 'Get online to view your list of forks.',
          action: RetryAction })));



  } else
  if (props.isForbidden || props.error) {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarEmptyStateContainer"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
          illustration: 'internal-server-error',
          title: 'Unable to load list of forks',
          message:
          props.isForbidden ?
          `You cannot view the list of forks for this ${props.modelName}.` :
          _.get(props, 'error.error.message') || 'Try refetching forks' })));




  } else
  if (props.isEmpty) {
    const hiddenForksExist = Boolean(_.get(props, 'forkInfo.hiddenForks'));

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarEmptyStateContainer"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_TabEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
          showAction: true,
          illustration: 'no-fork',
          title:
          hiddenForksExist ?
          `${_.get(props, 'forkInfo.totalForks')} ${forkText} of ${_.get(
          props,
          'model.name')
          }` :
          'No forks created yet',

          message:
          hiddenForksExist ?
          'They aren\'t visible because they\'re in workspaces you don\'t have access to.' :
          `All forks created from this ${props.modelName} will appear here.`,

          action: EmptyStateAction })));



  }

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null,
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarDescription"], null,
        _.get(props, 'forkInfo.totalForks'), ' ', forkText, ' of ', _.get(props, 'model.name'), '.',
        _.get(props, 'forkInfo.hiddenForks') > 0 ? ` ${_.get(props, 'forkInfo.hiddenForks')} of those aren't in this list because they're in workspaces you don't have access to.` : ''),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarContainer"], null,
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HelperText, null, 'Recently created:'),
        (props.forks || []).map(fork =>
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { key: fork.modelId },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ListItemWrapper, null,
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_11__["default"], {
                to: fork.modelId ? `${pm.artemisUrl}/${props.modelName}/${fork.modelId}` : '',
                disabled: !fork.modelId },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarListItem"], null,
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_ItemLink__WEBPACK_IMPORTED_MODULE_14__["default"], { text: fork.forkName }))),


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_context_bar_items__WEBPACK_IMPORTED_MODULE_8__["ContextBarSubtext"], null,
              `Created on: ${moment_timezone__WEBPACK_IMPORTED_MODULE_3___default()(fork.createdAt).format('DD MMM, YYYY')}`)),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ListItemAvatar, {
              align: 'bottom',
              body: _.get(fork, 'createdBy.name') },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_6__["default"], {
              size: 'medium',
              userId: _.get(fork, 'createdBy.id'),
              customPic: _.get(fork, 'createdBy.profilePicUrl') })))),




        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkText, { onClick: () => _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__["default"].transitionTo('build.forks', { model: props.modelName, id: _.get(props, 'model.uid') }) },
          _.get(props, 'forkInfo.totalForks') > (props.forks || []).length ? 'View all forks' : 'View details'))));



}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextBarDescription", function() { return ContextBarDescription; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextBarListItem", function() { return ContextBarListItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextBarSubtext", function() { return ContextBarSubtext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextBarContainer", function() { return ContextBarContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextBarLoading", function() { return ContextBarLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContextBarEmptyStateContainer", function() { return ContextBarEmptyStateContainer; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(82);


const ContextBarDescription = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].span`
  font-size: var(--text-size-m);
  color: var(--content-color-secondary);
  font-weight: var(--text-weight-regular);
  margin-left: var(--spacing-s);
  display: flex;
`;

const ContextBarListItem = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].span`
  color: var(--content-color-primary);
  font-size: var(--text-size-m);

  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`;

const ContextBarSubtext = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].p`
  margin-top: 0;
  margin-bottom: 0;
  color: var(--content-color-tertiary);
  font-size: var(--text-size-s);
`;

const ContextBarContainer = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  margin-left: var(--spacing-s);
  margin-top: var(--spacing-l);
  padding-bottom: var(--spacing-xl);

  div {
    margin-bottom: var(--spacing-s);
  }
`;

const ContextBarLoading = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  position: absolute;
  top: 50%;
  left: 50%;
`;

const ContextBarEmptyStateContainer = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  position: absolute;
  top: 30%;
  left: 15%;
  min-width: 300px;

  p {
    font-size: var(--text-size-m);
    color: var(--content-color-secondary);
    margin: 0;
  }
`;

/***/ })

}]);