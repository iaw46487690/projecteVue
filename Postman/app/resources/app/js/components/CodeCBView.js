(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[55],{

/***/ 16022:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CodeCBView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(74);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3667);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2304);
/* harmony import */ var _CodeControls__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(16023);
/* harmony import */ var _CodeSnippetEditor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(16025);
/* harmony import */ var _js_services_CodeGeneratorExperimental__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4466);
/* harmony import */ var _js_utils_ClipboardHelper_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3130);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(420);
var _class;









let


CodeCBView = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class CodeCBView extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeLanguage: { language: 'curl', variant: 'cURL' },
      optionsArray: [],
      optionsPreference: {},
      codeSnippet: '',
      originalSnippetHash: '',
      codeCopied: false,
      selectedLanguage: {} };

    this.handleCodeTypeSelect = this.handleCodeTypeSelect.bind(this);
    this.handleSnippetChange = this.handleSnippetChange.bind(this);
    this.handleCopyCode = this.handleCopyCode.bind(this);
    this.store = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('CodegenStore');
    this.throttledGenerateSnippet = _.throttle(this.handleCodeTypeSelect.bind(this), 500, { leading: true, trailing: true });
  }

  componentDidMount() {
    var activeLanguage = {
      id: this.store.languagePreference },

    { language, variant } = !_.isObject(activeLanguage.id) ? JSON.parse(activeLanguage.id) : activeLanguage;

    this.disposePreviewReaction = Object(mobx__WEBPACK_IMPORTED_MODULE_2__["reaction"])(() => {
      let editor = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_9__["getStore"])('EditorStore').find(this.props.contextData.editorId),
      editorModel = editor && editor.model,
      viewState = editorModel && editorModel.viewState;
      return viewState && Object(mobx__WEBPACK_IMPORTED_MODULE_2__["toJS"])(editorModel.viewState.previewRequest);
    }, () => {

      // Use throttled function to generate snippet because we don't want the
      // function to be fired on every small re-size or key stroke.
      this.throttledGenerateSnippet(this.state.selectedLanguage);
    });
    this.handleCodeTypeSelect(activeLanguage);

    _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_4__["default"].addEventV2({
      category: 'codegen',
      action: 'open_codegen_contextbar',
      label: `${language}_${variant}` });

  }

  componentWillUnmount() {
    this.disposePreviewReaction && this.disposePreviewReaction();
  }

  componentDidUpdate(prevProps) {
    const { id: prevId } = prevProps.contextData || {},
    id = _.get(this.props.contextData, 'id');

    if (prevId !== id) {
      this.handleCodeTypeSelect(this.state.selectedLanguage);
    }
  }

  /**
     *
     * @param {*} input - input string that needs to be hashed
     * @returns {*} sha1 hash of the string
     */
  hash(input) {
    let crypto = __webpack_require__(2084);
    return crypto.createHash('sha1').update(input).digest('base64');
  }

  handleCodeTypeSelect(value) {
    if (!_.isEmpty(value)) {
      this.setState({
        selectedLanguage: value });

    }

    let codegen = !_.isEmpty(value) ? JSON.parse(value.id) : this.store.languagePreference,
    { language, variant } = codegen,
    defaultOptions = {},
    optionsPreference;

    this.setState({
      activeLanguage: { language, variant } });


    _js_services_CodeGeneratorExperimental__WEBPACK_IMPORTED_MODULE_7__["default"].getOptions(language, variant).
    then(options => {
      if (options && options.length) {
        this.setState({
          optionsArray: options });

        options.forEach(option => {
          defaultOptions[option.id] = option.default;
        });
      }
    }).
    catch(err => {
      var options = [];
      this.setState({
        optionsArray: options });

      pm.logger.error(`CodeCBView~handleCodeTypeSelect - ${err}`);
    }).
    then(() => {
      optionsPreference = this.store.getSnippetOptionForLanguage(language, variant) || defaultOptions;
      this.setState({
        optionsPreference: optionsPreference });

    }).
    then(() => {
      let type = this.props.contextData && this.props.contextData.type;
      _js_services_CodeGeneratorExperimental__WEBPACK_IMPORTED_MODULE_7__["default"].getSnippet(this.props.contextData.editorId, { language, variant, optionsPreference }, type).
      then(snippetObject => {
        if (!snippetObject) {
          return;
        }

        // Hash the snippet to reduce the memory footprint.
        let newSnippetHash = this.hash(snippetObject.snippet);

        // Compare the hashes to make sure the local state is updated only
        // if there is a change in the request and the original code snippet
        if (newSnippetHash !== this.state.originalSnippetHash) {
          this.setState({
            codeSnippet: snippetObject.snippet,
            originalSnippetHash: newSnippetHash },
          () => {

            // update the snippetLanguage in the settings
            let snippetLanguage = JSON.stringify({ language, variant });
            this.store.updateLanguagePreference(snippetLanguage);
          });
        }
      });
    });
  }

  handleSnippetChange(content) {
    this.setState({ codeSnippet: content });
  }

  handleCopyCode() {
    _js_utils_ClipboardHelper_js__WEBPACK_IMPORTED_MODULE_8__["default"].copy(this.state.codeSnippet);
    this.setState({ codeCopied: true });
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({ codeCopied: false });
    }, 1500);
  }

  render() {
    let activeLanguage = _.get(this.state.activeLanguage, 'language', 'curl'),
    { controller = {} } = this.props || {},
    activeVariant = _.get(this.state.activeLanguage, 'variant', 'cURL'),
    editorMode = controller.labelEditorMap && controller.labelEditorMap[`${activeLanguage}_${activeVariant}`] ?
    controller.labelEditorMap[`${activeLanguage}_${activeVariant}`].editorMode : 'powershell',
    editorIndentation = {
      indentationType: this.state.optionsPreference.indentType ?
      _.toLower(this.state.optionsPreference.indentType) : null,
      indentationSize: this.state.optionsPreference.indentCount || null },

    snippetOptions = this.store.getSnippetOptions(),
    isDirty;
    if (controller.editor) {
      isDirty = controller.editor.isDirty;
    }

    // This is done because monaco editor doesn't support dart syntax highlighting
    // we are defaulting it to powershell for now till there is support for dart
    if (editorMode === 'dart') {
      editorMode = 'powershell';
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-code-snippet-view' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__["ContextBarViewHeader"], {
          title: 'Code snippet',
          onClose: this.props.onClose }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-code-snippet-body' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CodeControls__WEBPACK_IMPORTED_MODULE_5__["default"], {
            codeCopied: this.state.codeCopied,
            languageList: controller.languageList,
            onSelectLanguage: this.handleCodeTypeSelect,
            selectedLanguage: this.state.selectedLanguage,
            handleCopyCode: this.handleCopyCode,
            snippetOptions: snippetOptions,
            isDirty: isDirty }),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CodeSnippetEditor__WEBPACK_IMPORTED_MODULE_6__["default"], {
            indentation: editorIndentation,
            codeSnippet: this.state.codeSnippet,
            editorMode: editorMode,
            handleSnippetChange: this.handleSnippetChange }))));




  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 16023:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ContextBarCodegenDropdown_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(16024);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3170);
/* harmony import */ var _js_modules_controllers_ViewHierarchyController__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3008);




let

CodeControls = class CodeControls extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);this.




















    openLanguageSettingsModal = () => {
      _js_modules_controllers_ViewHierarchyController__WEBPACK_IMPORTED_MODULE_4__["ModalViewManager"].onMount(() => {
        pm.mediator.trigger('openLanguageSettingsModel', this.props.selectedLanguage);
      });
    };this.openLanguageSettingsModal = this.openLanguageSettingsModal.bind(this);}componentDidUpdate(prevProps) {let currentSnippetOptions = this.props.snippetOptions,prevSnippetOptions = prevProps.snippetOptions,selectedLanguage = JSON.parse(this.props.selectedLanguage.id),language = _.get(selectedLanguage, 'language', 'curl'),variant = _.get(selectedLanguage, 'variant', 'cURL'),optionPrev = JSON.stringify(prevSnippetOptions[`${language}_${variant}`]),optionCurrent = JSON.stringify(currentSnippetOptions[`${language}_${variant}`]);if (optionPrev !== optionCurrent) {this.props.onSelectLanguage(this.props.selectedLanguage);} else if (!this.props.isDirty && prevProps.isDirty) {this.props.onSelectLanguage(this.props.selectedLanguage);}}

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-codegen-controls' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-codegen-controls-left' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-codegen-controls-left__dropdown' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContextBarCodegenDropdown_js__WEBPACK_IMPORTED_MODULE_2__["default"], {
              selectedTarget: this.props.selectedLanguage,
              onTargetSelect: this.props.onSelectLanguage,
              menuClassName: 'context-bar-codegen-dropdown',
              filterItems: this.props.languageList })),


          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-codegen-controls-left__setting' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                type: 'tertiary',
                onClick: this.openLanguageSettingsModal },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
                name: 'icon-descriptive-setting-stroke',
                className: 'language-settings-icon',
                tooltip: 'Code settings' })))),




        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-codegen-controls-right' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-codegen-controls-right__copy' },

            this.props.codeCopied ?
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                onClick: this.props.handleCopyCode,
                tooltip: 'Copied' },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
                name: 'icon-state-success-stroke',
                className: 'code-copied-icon' })) :


            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_3__["Button"], {
                type: 'tertiary',
                onClick: this.props.handleCopyCode,
                tooltip: 'Copy snippet' },

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
                name: 'icon-action-copy-stroke',
                className: 'code-copy-icon' }))))));







  }};


/* harmony default export */ __webpack_exports__["default"] = (CodeControls);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 16024:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ContextBarCodegenDropdown; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_base_InputSelectV2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3635);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3159);
/* harmony import */ var _js_components_base_SearchHighlighter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3459);



let

ContextBarCodegenDropdown = class ContextBarCodegenDropdown extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);
    this.selectedTarget = this.selectedTarget.bind(this);
  }

  getFilteredList(list, query, options = {}) {

    if (!query || options.firstRender) {
      return list;
    }

    return _.filter(list, item => {
      return item.id && _.includes(item.name.toLowerCase(), query.toLowerCase());
    });
  }

  selectedTarget() {
    var filterItems = this.props.filterItems,
    selectedTarget = this.props.selectedTarget,
    modifiedSelectedTarget = _.find(filterItems, item => {return item.id === selectedTarget.id;});

    // handle renaming and deletion of selected item.

    if (modifiedSelectedTarget) {
      return modifiedSelectedTarget;
    } else
    {
      return {
        id: '{language:"curl",variant:"cURL"}',
        name: 'cURL' };

    }
  }

  getOption(item, search, option = {}) {
    const name = item.name,
    id = item.id;
    if (search && !option.firstRender) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__["default"], null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
              className: 'input-select-item',
              key: id },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_SearchHighlighter__WEBPACK_IMPORTED_MODULE_3__["default"], {
              source: name,
              query: search }))));




    }
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_2__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'input-select-item', key: id }, name));
  }

  render() {
    var { filterItems } = this.props;
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-dropdown-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_InputSelectV2__WEBPACK_IMPORTED_MODULE_1__["InputSelectV2"], {
            style: { height: '32px' },
            onSelect: this.props.onTargetSelect,
            getFilteredList: this.getFilteredList.bind(this, filterItems),
            optionRenderer: this.getOption,
            selectedItem: this.selectedTarget(),
            ref: 'inputSelect',
            menuClassName: this.props.menuClassName,
            getInputValue: listObj => {
              return listObj.name || '';
            } }))));




  }};
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 16025:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _appsdk_components_editors_texteditor_TextEditor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3677);

let

CodeSnippetEditor = class CodeSnippetEditor extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'context-bar-code-snippet-editor' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_editors_texteditor_TextEditor__WEBPACK_IMPORTED_MODULE_1__["default"], {
          ref: input => {return input && input.focus();},
          indentation: this.props.indentation,
          value: this.props.codeSnippet,
          language: this.props.editorMode,
          onChange: this.props.handleSnippetChange,
          wordWrap: true,
          ref: 'editor' })));



  }};


/* harmony default export */ __webpack_exports__["default"] = (CodeSnippetEditor);

/***/ })

}]);