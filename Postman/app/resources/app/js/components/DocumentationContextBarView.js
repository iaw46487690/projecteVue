(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[40],{

/***/ 15404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DocumentationContextBarView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _SkeletonLoader_ContextBarSkeletonLoader_ContextBarSkeletonLoader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(15405);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(420);
/* harmony import */ var _js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3648);
/* harmony import */ var _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2816);
/* harmony import */ var _runtime_api_CollectionInterface__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2802);
/* harmony import */ var _ContextBar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15406);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4152);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2304);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(5212);
/* harmony import */ var _DocumentationError__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(5216);
/* harmony import */ var _hocs_withErrorHandler__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(15409);
/* harmony import */ var _hocs_withContextBarTitle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(15410);
/* harmony import */ var _DocumentationIntersectionObserver__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(5219);
/* harmony import */ var _Entity__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(5227);
var _class;
















let




DocumentationContextBarView = Object(_hocs_withContextBarTitle__WEBPACK_IMPORTED_MODULE_13__["default"])(_class = Object(_hocs_withErrorHandler__WEBPACK_IMPORTED_MODULE_12__["default"])(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class DocumentationContextBarView extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {constructor(...args) {var _temp;return _temp = super(...args), this.
    state = {
      showFooter: true,
      collectionData: undefined }, this.


    unsubscribe = null, this.


















    openCollection = () => {
      let { contextData, controller } = this.props,
      { id: entityId, type: entityType } = contextData,
      { entityUid } = controller;

      _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_9__["default"].addEventV2({
        category: _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ANALYTICS"].CATEGORY,
        action: 'open_tab',
        label: Object(_utils_utils__WEBPACK_IMPORTED_MODULE_10__["isPublicWorkspace"])() ?
        _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ANALYTICS"].LABEL.PUBLIC_CONTEXTBAR :
        _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ANALYTICS"].LABEL.PRIVATE_CONTEXTBAR,
        entityType,
        entityId:
        entityType === _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ENTITY"].COLLECTION ? entityUid : entityId });

    }, this.

    requestEditAccess = () => {
      let collectionId =
      this.props.contextData.parentCollectionUid || this.props.contextData.uid;

      Object(_runtime_api_CollectionInterface__WEBPACK_IMPORTED_MODULE_6__["collectionActions"])(collectionId, _js_constants_CollectionActionsConstants__WEBPACK_IMPORTED_MODULE_5__["ACTION_REQUEST_ACCESS"]);
    }, this.

    handleEditModeChange = (entityType, entityId, editMode) => {
      return this.setState({ showFooter: !editMode });
    }, this.

    handleUpdateName = (entityType, entityId, title) => {
      this.props.controller.store.updateName(title);
    }, this.

    handleUpdateDescription = (entityType, entityId, description) => {
      this.props.controller.store.updateDescription(description);
    }, this.




    handleParentEntityClick = (parentType, parentId) => {
      return Object(_utils_utils__WEBPACK_IMPORTED_MODULE_10__["openEntityByResolvingUid"])(parentType, parentId);
    }, this.


















    renderEntity = ({
      data,
      id,
      type,
      parentCollectionData,
      parentCollectionUid,
      rootRef,
      isEditable,
      store }) =>
    {
      const controller = this.props.controller;

      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
              ref: rootRef,
              className: 'documentation-context-view__entity-body-container' },

            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Entity__WEBPACK_IMPORTED_MODULE_15__["default"], {
              type: type,
              entityData: data,
              parentCollectionData: parentCollectionData,
              store: store,
              onClose: this.props.onClose,
              source: _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ORIGIN"].CONTEXT_VIEW,
              onRequestAccess: this.requestEditAccess,
              editable: isEditable,
              updateName: this.handleUpdateName,
              updateDescription: this.handleUpdateDescription,
              onParentEntityClick: this.handleParentEntityClick,
              onEditModeChange: this.handleEditModeChange,
              updateUnsavedDescription: _.get(controller, 'updateUnsavedDescription'),
              unsavedDescription: _.get(controller, 'contextBarUnsavedDescription') })),


          this.state.showFooter ?
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ContextBar__WEBPACK_IMPORTED_MODULE_7__["ContextBarFooter"], {
            id: id,
            type: type,
            collectionUid: parentCollectionUid,
            onOpenCollection: this.openCollection }) :

          null));


    }, _temp;}componentDidMount() {let { contextData, controller } = this.props,{ entityUid } = controller,{ type: entityType, id: entityId } = contextData;_js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_9__["default"].addEventV2({ category: _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ANALYTICS"].CATEGORY, action: 'view', label: Object(_utils_utils__WEBPACK_IMPORTED_MODULE_10__["isPublicWorkspace"])() ? _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ANALYTICS"].LABEL.PUBLIC_CONTEXTBAR : _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ANALYTICS"].LABEL.PRIVATE_CONTEXTBAR, entityType, entityId: entityType === _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ENTITY"].COLLECTION ? entityUid : entityId });} /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         * Handle click on the parent entity while rendering inherited auth
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         */ /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * Render an entity of any type: collection, folder, request.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             *
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {Object} data - The data to be rendered within the component, with resolved variables/placeholders
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {String} id - UUID of the selected entity
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {String} type - The element type. One of collection, folder, or request
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {String} collectionUid - collectionUid of the parent collection of the selected entity
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {Object} [rawData] - Data with unresolved variables/placeholders. Necessary if inline editing is to be
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             *                              enabled.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {Object} parentCollectionData - Parent collection object of the selected entity.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             *                                         Required for inherited auth resolution.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {String} parentCollectionUid - Uid of the parent collection
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {Object} rootRef - Ref to the root element
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {Boolean} isEditable - Flag indicating if the entity is editable by the user
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @param {Object} store - The context bar store
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             * @return {JSX|null} - The rendered element-specific output, or null.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             */render() {const { contextData, controller, editorId } = this.props;let editorStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('EditorStore').find(editorId); // Forcefully un-mounting the documentation context bar
    // Otherwise the hash fragment conflicts if same documentation opened in 2 different tabs
    // @todo - properly unmount the parent tab itself when tab is not active
    if (!editorStore.isActive) {return null;}let { id, type } = contextData,{ store } = controller,{ isEditable, parentCollectionData, entityData } = store,{ isLoading, error, data } = entityData,parentCollectionUid = type === _constants__WEBPACK_IMPORTED_MODULE_8__["DOCUMENTATION_ENTITY"].COLLECTION ? contextData.collectionUid : contextData.parentCollectionUid;

    if (typeof id === 'undefined') {
      return null;
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
        isLoading ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SkeletonLoader_ContextBarSkeletonLoader_ContextBarSkeletonLoader__WEBPACK_IMPORTED_MODULE_2__["default"], null) :
        error ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocumentationError__WEBPACK_IMPORTED_MODULE_11__["default"], { title: error.title, message: error.message }) :

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_4__["default"], {
            message: `There was an error while fetching the documentation for this ${type}. If this problem persists, contact us at help@postman.com`

            // error should only shown only when there is an explict error, other wise we can show
            // empty screen (render null)
            , showError: error },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DocumentationIntersectionObserver__WEBPACK_IMPORTED_MODULE_14__["default"], null,
            ({ rootRef }) =>
            data ?
            this.renderEntity({
              data,
              id,
              type,
              parentCollectionData,
              parentCollectionUid,
              rootRef,
              isEditable,
              store }) :

            null))));






  }}) || _class) || _class) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ContextBarSkeletonLoader; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _SkeletonUnit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5222);



/**
                                                * Return skeleton loader for Context Bar View
                                                */
function ContextBarSkeletonLoader() {

  const contextBarSkeletonProps = [
  { height: 16, width: 293, top: 60, left: 60, borderRadius: 3 },
  { height: 16, width: 211, top: 88, left: 60, borderRadius: 3 },
  { height: 16, width: 144, top: 124, left: 60, borderRadius: 3 },
  { height: 16, width: 20, top: 156, left: 60, borderRadius: 3 },
  { height: 16, width: 20, top: 186, left: 60, borderRadius: 3 },
  { height: 16, width: 116, top: 156, left: 88, borderRadius: 3 },
  { height: 16, width: 116, top: 186, left: 88, borderRadius: 3 }];


  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'documentation-context-bar-loader' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'documentation-context-bar-body' },

        _.map(contextBarSkeletonProps, (contextBarSkeletonProp, index) => {
          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SkeletonUnit__WEBPACK_IMPORTED_MODULE_1__["default"], {
              key: index,
              height: contextBarSkeletonProp.height,
              width: contextBarSkeletonProp.width,
              top: contextBarSkeletonProp.top,
              left: contextBarSkeletonProp.left,
              borderRadius: contextBarSkeletonProp.borderRadius }));


        }))));




}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ContextBarTitle_ContextBarTitle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(15407);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ContextBarTitle", function() { return _ContextBarTitle_ContextBarTitle__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _ContextBarFooter_ContextBarFooter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(15408);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ContextBarFooter", function() { return _ContextBarFooter_ContextBarFooter__WEBPACK_IMPORTED_MODULE_1__["default"]; });






/***/ }),

/***/ 15407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ContextBarTitle; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


/**
                            * A static title to be displayed at the top of the context bar component
                            */
function ContextBarTitle() {
  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('h4', { className: 'context-bar-header__title' }, 'Documentation'));

}

/***/ }),

/***/ 15408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ContextBarFooter; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3200);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4152);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3159);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};





/**
                                                                  * A footer to be displayed at the bottom of the context bar, allowing the user to
                                                                  * access persistent capabilities, like the full documentation view.
                                                                  */
function ContextBarFooter(props) {
  let { id, type, collectionUid } = props,
  entity;

  if (type !== _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ENTITY"].COLLECTION) {
    entity = `${type}-${id}`;
  }

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__["default"], { identifier: 'viewFullDocumentation' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
          to: _extends({
            routeIdentifier: 'build.documentation',
            routeParams: { collectionUid } },
          entity && { queryParams: { entity } }),

          className: 'documentation-context-bar-footer',
          onClick: props.onOpenCollection },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'documentation-context-bar-footer__expand-cta' }, 'View complete collection documentation'),
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
          name: 'icon-direction-forward',
          color: 'content-color-link' }))));




}

/***/ }),

/***/ 15409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return withErrorHandler; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3648);



/**
                                                                           * Higher Order Component to wrap an existic component
                                                                           */
function withErrorHandler(WrappingComponent) {let
  WrappedComponent = class WrappedComponent extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
    render() {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_empty_states_CrashHandler__WEBPACK_IMPORTED_MODULE_1__["default"], null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(WrappingComponent, this.props)));


    }};


  WrappedComponent.displayName = `withErrorHandler(${WrappingComponent.displayName || WrappingComponent.name})`;

  return WrappedComponent;
}

/***/ }),

/***/ 15410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return withContextBarTitle; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2304);
/* harmony import */ var _components_DocumentationEntityTitle__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5275);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4152);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5212);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};





/**
                                                     * Higher Order Component to wrap the component with context bar title. If the
                                                     * underlying component throws an error, this the entity title would be visible
                                                     *
                                                     * @param {React.Component} WrappingComponent - The component to be wrapped
                                                     */
function withContextBarTitle(WrappingComponent) {let
  WrappedComponent = class WrappedComponent extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {constructor(...args) {var _temp;return _temp = super(...args), this.
      handleContextBarClose = () => {
        let { contextData, controller } = this.props,
        { entityUid } = controller,
        { type: entityType, id: entityId } = contextData;

        _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_1__["default"].addEventV2({
          category: _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ANALYTICS"].CATEGORY,
          action: 'close',
          label: Object(_utils_utils__WEBPACK_IMPORTED_MODULE_4__["isPublicWorkspace"])() ? _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ANALYTICS"].LABEL.PUBLIC_CONTEXTBAR : _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ANALYTICS"].LABEL.PRIVATE_CONTEXTBAR,
          entityType,
          entityId: entityType === _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ENTITY"].COLLECTION ? entityUid : entityId });


        this.props.onClose();
      }, _temp;}

    render() {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: 'documentation-context-view' },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_DocumentationEntityTitle__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({},
          this.props, {
            onClose: this.handleContextBarClose,
            source: _constants__WEBPACK_IMPORTED_MODULE_3__["DOCUMENTATION_ORIGIN"].CONTEXT_VIEW })),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(WrappingComponent, this.props)));


    }};


  WrappedComponent.displayName = `withContextBarTitle(${WrappingComponent.displayName || WrappingComponent.name})`;

  return WrappedComponent;
}

/***/ })

}]);