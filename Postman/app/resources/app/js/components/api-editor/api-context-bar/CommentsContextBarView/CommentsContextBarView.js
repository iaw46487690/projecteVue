(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[45],{

/***/ 15421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CommentsContextBarView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(420);
/* harmony import */ var _js_utils_util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(448);
/* harmony import */ var _collaboration_services_ContextBarCommentService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3220);
/* harmony import */ var _collaboration_components_comments_InlineCommentDrawer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3210);
/* harmony import */ var _js_models_services_DashboardService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2799);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3170);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3211);
/* harmony import */ var _js_components_base_Inputs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(3215);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _js_modules_services_InlineCommentTransformerService__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(3148);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3667);
/* harmony import */ var _common_APIOffline_APIOffline__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(5552);
/* harmony import */ var _collaboration_services_PermissionService__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(3668);
/* harmony import */ var _collaboration_constants_comments__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(3012);
var _class;















let


CommentsContextBarView = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class CommentsContextBarView extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      loadError: false,
      hasError: false,
      isOpenFilterSelected: true,
      isResolvedFilterSelected: false,
      isScrollable: true };


    this.rightOverlayModalRef = null;

    this.parseError = this.parseError.bind(this);
    this.handleRetry = this.handleRetry.bind(this);
    this.handleResolvedFilter = this.handleResolvedFilter.bind(this);
    this.handleOpenFilter = this.handleOpenFilter.bind(this);
    this.setCommentDrawerRef = this.setCommentDrawerRef.bind(this);
    this.stopScroll = this.stopScroll.bind(this);
  }

  async componentDidMount() {
    this.setState({ loading: true, loadError: false });

    _js_modules_services_InlineCommentTransformerService__WEBPACK_IMPORTED_MODULE_12__["default"].handleAnalytics('api', 'view', 'api');

    _collaboration_services_ContextBarCommentService__WEBPACK_IMPORTED_MODULE_4__["default"].getComments({
      model: this.props.contextData.commentModel,
      modelId: this.props.contextData.commentModelId }).

    then(() => {
      /**
                 * When ContextBarCommentService.getComments resolves it is not guaranteed that
                 * CommentStore is fully done populating
                 * hence we use requestIdleCallback to defer setting the loaded flag as true
                 */
      requestIdleCallback(() => {
        Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CommentStore').setLoaded(true);
      });

      this.setState({ loading: false, loadError: false });
    }).
    catch(e => {
      this.setState({ loading: false, loadError: true });
    });

    // Permissions
    let permissions = ['addComment', 'deleteComment', 'resolveComment'],
    apiId = this.props.contextData.commentModelId,
    { addComment, deleteComment, resolveComment } = await Object(_collaboration_services_PermissionService__WEBPACK_IMPORTED_MODULE_15__["fetchPermissions"])(permissions, apiId, _collaboration_constants_comments__WEBPACK_IMPORTED_MODULE_16__["MODEL_TYPE"].API);

    this.setState({
      addCommentPermission: addComment,
      deleteCommentPermission: deleteComment,
      resolveCommentPermission: resolveComment });

  }

  componentDidUpdate() {
    if (!this.state.isResolvedFilterSelected) {
      const commentStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CommentStore'),
      activeComment = commentStore.find(commentStore.activeCommentId),
      activeCommentAnnotation = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('AnnotationStore').find(_.get(activeComment, 'annotationId'));

      _.get(activeCommentAnnotation, 'status.resolved') && this.handleResolvedFilter();
    }
  }

  handleOpenFilter() {
    this.setState(prevState => {
      if (prevState.isOpenFilterSelected && !prevState.isResolvedFilterSelected) {
        return;
      }

      return {
        isOpenFilterSelected: !prevState.isOpenFilterSelected };

    });
  }

  handleResolvedFilter() {
    this.setState(prevState => {
      if (!prevState.isOpenFilterSelected && prevState.isResolvedFilterSelected) {
        return;
      }

      return {
        isResolvedFilterSelected: !prevState.isResolvedFilterSelected };

    });
  }

  parseError(e) {
    if (!e.isFriendly) {
      return '';
    }

    return e.message;
  }

  handleRetry() {
    this.setState({ loading: true, loadError: false });

    return _collaboration_services_ContextBarCommentService__WEBPACK_IMPORTED_MODULE_4__["default"].getComments({
      model: this.props.contextData.commentModel,
      modelId: this.props.contextData.commentModelId }).

    then(() => {
      this.setState({ loading: false, loadError: false });
    }).
    catch(e => {
      this.setState({ loading: false, loadError: true });
    });
  }

  setCommentDrawerRef(node) {
    this.commentDrawerRef = node;

    // Added as a hack to fix a bug where @ mentions
    // would crash the UI trying to create portal on `commentDrawerRef`
    // when it is not present
    this.commentDrawerRef && this.forceUpdate();
  }

  stopScroll(isScrollable) {
    this.setState({ isScrollable: !isScrollable });
  }

  render() {
    let allComments,
    { commentModel, commentModelId, schemaId } = this.props.contextData || {};

    allComments = _.filter(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CommentStore').values, comment => {
      return comment.model === commentModel && comment.modelId === commentModelId;
    });

    const inlineComments = _.filter(allComments, comment => comment.anchor &&
    comment.anchor.startsWith(`${commentModel}/${commentModelId}`) &&
    comment.anchor !== `${commentModel}/${commentModelId}`),
    comments = _.sortBy(_.filter(allComments, comment => !comment.anchor ||
    comment.anchor === `${commentModel}/${commentModelId}`), 'createdAt') || [],
    title = this.props.contextData.name,
    currentUser = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore') || {},
    teamUserStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('TeamUserStore'),
    teamUsersMap = _.reduce(teamUserStore.values, (res, user) => {
      res[user.id] = {
        name: user.name,
        id: user.id,
        username: user.username,
        email: user.email,
        profilePicUrl: user.profilePicUrl,
        roles: user.roles };


      return res;
    }, {}),
    teamUsers = Object.values(teamUsersMap),
    isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('SyncStatusStore').isSocketConnected,
    isAdmin = _js_utils_util__WEBPACK_IMPORTED_MODULE_3__["default"].isUserAdmin(currentUser),
    annotationOrder = _.reduce((allComments || []).reverse(), (result, comment) => {
      if (!result.includes(comment.annotationId)) {
        result.push(comment.annotationId);
      }

      return result;
    }, []),
    commentStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CommentStore'),
    activeComment = commentStore.find(commentStore.activeCommentId),
    setActiveComment = commentStore.setActiveCommentId;

    /**
                                                         * For comments belonging to multiple API versions appropriate
                                                         * versionId is to be sent as query param for comments link.
                                                         * Hence here we create a map of annotationId to versionId to be sent as commentLinkParam
                                                         */
    let commentLinkParams = _.reduce(allComments || [], (result, comment) => {
      let schemaId = comment && comment.anchor && _.get(comment.anchor.split('/'), '[3]'),
      version = (this.props.contextData.versions || []).filter(version => schemaId === _.get(version, 'schema[0]')),
      versionId = _.get(version, '[0].id');

      versionId && (result[comment.annotationId] = { version: versionId });

      return result;
    }, {});

    if (isOffline && this.state.loading) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_13__["ContextBarViewHeader"], { onClose: this.props.onClose, title: 'Comments' }),
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_APIOffline_APIOffline__WEBPACK_IMPORTED_MODULE_14__["default"], { origin: 'context-bar' })));


    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
          className: classnames__WEBPACK_IMPORTED_MODULE_11___default()({ 'comment-drawer-container': true, 'no-scroll': !this.state.isScrollable }),
          ref: this.setCommentDrawerRef },

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_13__["ContextBarViewHeader"], { onClose: this.props.onClose, title: 'Comments' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'comment-drawer-container__actions' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'comment-drawer-container__actions-filter' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__["Dropdown"], null,
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__["DropdownButton"], {
                    dropdownStyle: 'nocaret' },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_7__["Button"], {
                      className: 'comment-drawer-container__actions-filter-button',
                      type: 'tertiary',
                      tooltip: 'Filter comments',
                      disabled: !inlineComments.length },

                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_10__["Icon"], {
                      name: 'icon-action-filter-stroke' }))),



                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__["DropdownMenu"], {
                    className: 'comment-drawer-container__actions-filter-menu' },

                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'comment-drawer-container__actions-filter--action-header' }, 'Filter by'),


                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'comment-drawer-container__actions-filter--action-item' },
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Inputs__WEBPACK_IMPORTED_MODULE_9__["Checkbox"], {
                      className: 'comment-filter-dropdown-item-checkbox',
                      checked: this.state.isOpenFilterSelected,
                      onChange: this.handleOpenFilter }),

                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'comment-drawer-container__actions-filter--action-item-name' }, 'Open comments')),



                  react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'comment-drawer-container__actions-filter--action-item' },
                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Inputs__WEBPACK_IMPORTED_MODULE_9__["Checkbox"], {
                      className: 'comment-filter-dropdown-item-checkbox',
                      checked: this.state.isResolvedFilterSelected,
                      onChange: this.handleResolvedFilter }),

                    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'comment-drawer-container__actions-filter--action-item-name' }, 'Resolved comments'))))))),








        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'comment-drawer' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_collaboration_components_comments_InlineCommentDrawer__WEBPACK_IMPORTED_MODULE_5__["default"], {
            comments: allComments,
            requestComments: comments,
            annotationOrder: annotationOrder,
            isResolvedFilterSelected: this.state.isResolvedFilterSelected,
            isOpenFilterSelected: this.state.isOpenFilterSelected,
            addCommentPermission: this.state.addCommentPermission,
            deleteCommentPermission: this.state.deleteCommentPermission,
            resolveCommentPermission: this.state.resolveCommentPermission,
            isAdmin: isAdmin,
            isOffline: isOffline,
            loading: this.state.loading,
            loadError: this.state.loadError,
            hasError: this.state.hasError,
            teamUsers: teamUsers,
            teamUsersMap: teamUsersMap,
            onRetry: this.handleRetry,
            user: currentUser,
            title: title,
            stopScroll: this.stopScroll,
            commentRef: this.commentDrawerRef,
            commentLinkDisabled: _.get(this.props.contextData, 'commentLinkDisabled'),
            model: 'api',
            modelId: _.get(this.props.contextData, 'commentModelId'),
            supportsInlineComments: true,
            commentLinkParams: commentLinkParams,
            activeComment: activeComment,
            setActiveComment: setActiveComment,
            origin: 'contextBar',
            anchor: _.get(this.props.contextData, 'anchor') }))));




  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);