(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[44],{

/***/ 15419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return APIInfoContextBarView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3667);
/* harmony import */ var _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3130);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3170);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2778);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _common_UserProfile_UserProfile__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(15420);
/* harmony import */ var _common_APIOffline_APIOffline__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5552);
var _extends = Object.assign || function (target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i];for (var key in source) {if (Object.prototype.hasOwnProperty.call(source, key)) {target[key] = source[key];}}}return target;};var _class;









const COPIED_TICK_DURATION = 3000;let


APIInfoContextBarView = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class APIInfoContextBarView extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      entityIdCopied: {
        id: false,
        versionId: false,
        schemaId: false } };



    this.handleCopyToClipboard = this.handleCopyToClipboard.bind(this);
  }

  componentWillUnmount() {
    clearTimeout(this.copiedTimeout);
  }

  /**
     * Copies data to clipboard and changes icon state for meta info from copy to success tick, triggered when copy icon is clicked.
     * @param {String} path its the path to the key we want to copy from props.contextData, possible values are id, versionId and schemaId
     */
  handleCopyToClipboard(path) {
    if (_.get(this.state.entityIdCopied, path)) {
      return;
    }

    _js_utils_ClipboardHelper__WEBPACK_IMPORTED_MODULE_3__["default"].copy(_.get(this.props.contextData, path, ''));

    this.setState(prevState => {
      return { entityIdCopied: _extends({},
        prevState.entityIdCopied, {
          [path]: true }) };

    },
    () => {
      this.copiedTimeout = setTimeout(
      () => this.setState(prevState => {
        return { entityIdCopied: _extends({},
          prevState.entityIdCopied, {
            [path]: false }) };


      }),
      COPIED_TICK_DURATION);

    });
  }

  render() {
    const api = _.get(this.props, 'contextData', {}),
    userFriendlyAPICreatedAt = moment__WEBPACK_IMPORTED_MODULE_6___default()(api.createdAt).format('DD MMM YYYY, h:mm A'),
    userFriendlyVersionCreatedAt = moment__WEBPACK_IMPORTED_MODULE_6___default()(api.versionCreatedAt).format('DD MMM YYYY, h:mm A'),
    versionHeaderText = 'Version details';

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_2__["ContextBarViewHeader"], {
          title: 'API details',
          onClose: this.props.onClose }),

        api.isOffline ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_APIOffline_APIOffline__WEBPACK_IMPORTED_MODULE_8__["default"], { origin: 'context-bar' }) :
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'ID'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'api-info-context-view__entity__content__id--entity',
                  title: api.id },

                api.id),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                  tooltip: 'Copy',
                  type: 'icon',
                  onClick: () => this.handleCopyToClipboard('id') },

                _.get(this.state.entityIdCopied, 'id') ?
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                  name: 'icon-state-success-stroke',
                  className: 'pm-icon pm-icon-normal success-tick-icon' }) :

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                  name: 'icon-action-copy-stroke',
                  className: 'pm-icon pm-icon-normal' })))),





          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'Created by'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },

              _.get(api, 'createdBy.id') ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_UserProfile_UserProfile__WEBPACK_IMPORTED_MODULE_7__["default"], {
                id: _.get(api, 'createdBy.id'),
                name: _.get(api, 'createdBy.name'),
                username: _.get(api, 'createdBy.username'),
                profileLink: _.get(api, 'createdBy.profileLink'),
                isAccessible: _.get(api, 'createdBy.isAccessible') }) :


              'Deactivated User')),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'Created on'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'api-info-context-view__entity__content__id',
                  title: userFriendlyAPICreatedAt },

                userFriendlyAPICreatedAt))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('p', { className: 'api-info-context-view__version-header' },
            versionHeaderText),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'Version'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'api-info-context-view__entity__content__id',
                  title: api.versionName },

                api.versionName))),



          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'Version ID'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'api-info-context-view__entity__content__id--entity',
                  title: api.versionId },

                api.versionId),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                  tooltip: 'Copy',
                  type: 'icon',
                  onClick: () => this.handleCopyToClipboard('versionId') },

                _.get(this.state.entityIdCopied, 'versionId') ?
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                  name: 'icon-state-success-stroke',
                  className: 'pm-icon pm-icon-normal success-tick-icon' }) :

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                  name: 'icon-action-copy-stroke',
                  className: 'pm-icon pm-icon-normal' })))),






          this.props.contextData.schemaId &&
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'Schema ID'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'api-info-context-view__entity__content__id--entity',
                  title: this.props.contextData.schemaId },

                this.props.contextData.schemaId),

              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__["Button"], {
                  tooltip: 'Copy',
                  type: 'icon',
                  onClick: () => this.handleCopyToClipboard('schemaId') },

                _.get(this.state.entityIdCopied, 'schemaId') ?
                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                  name: 'icon-state-success-stroke',
                  className: 'pm-icon pm-icon-normal success-tick-icon' }) :

                react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_postman_aether__WEBPACK_IMPORTED_MODULE_5__["Icon"], {
                  name: 'icon-action-copy-stroke',
                  className: 'pm-icon pm-icon-normal' })))),






          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'Created by'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },

              _.get(api, 'versionCreatedBy.id') ?
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_UserProfile_UserProfile__WEBPACK_IMPORTED_MODULE_7__["default"], {
                id: _.get(api, 'versionCreatedBy.id'),
                name: _.get(api, 'versionCreatedBy.name'),
                username: _.get(api, 'versionCreatedBy.username'),
                profileLink: _.get(api, 'versionCreatedBy.profileLink'),
                isAccessible: _.get(api, 'versionCreatedBy.isAccessible') }) :


              'Deactivated User')),




          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__label' }, 'Created on'),
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-info-context-view__entity__content' },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
                  className: 'api-info-context-view__entity__content__id',
                  title: userFriendlyVersionCreatedAt },

                userFriendlyVersionCreatedAt))))));







  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UserProfile; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3199);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(420);
/* harmony import */ var _js_external_navigation_ExternalNavigationService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2301);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3200);
var _class;





let


UserProfile = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class UserProfile extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {

  constructor(props) {
    super(props);
  }

  getClasses(wrapperType) {
    return classnames__WEBPACK_IMPORTED_MODULE_5___default()(
    {
      [`${wrapperType}-wrapper`]: true,
      'is-not-action-link': !this.props.isAccessible });


  }

  render() {
    let currentUser = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore'),
    displayName = currentUser.id === this.props.id ? 'You' : this.props.name || this.props.username;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'user-profile-wrapper' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
            className: this.getClasses('user-profile-pic'),
            style: { pointerEvents: !this.props.isAccessible && 'none' } },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Avatar__WEBPACK_IMPORTED_MODULE_2__["default"], { type: 'user', userId: this.props.id })),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_components_link_Link__WEBPACK_IMPORTED_MODULE_6__["default"], {
            to: this.props.isAccessible && this.props.profileLink,
            className: this.getClasses('user-name') },

          displayName)));



  }}) || _class;

/***/ })

}]);