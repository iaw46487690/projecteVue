(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[42],{

/***/ 15413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return APISidebarContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3165);
/* harmony import */ var _APISidebarMenu_APISidebarMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15414);
/* harmony import */ var _APISidebarList_APISidebarList__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15415);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3159);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(420);
/* harmony import */ var _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4259);
/* harmony import */ var _common_APIOffline_APIOffline__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5552);
var _class;










let


APISidebarContainer = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class APISidebarContainer extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.apiListStore;
    this.apiSidebarStore;

    this.focus = this.focus.bind(this);
    this.focusNext = this.focusNext.bind(this);
    this.focusPrev = this.focusPrev.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.renameItem = this.renameItem.bind(this);
    this.handleDeleteShortcut = this.handleDeleteShortcut.bind(this);
    this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    this.toggleRemoveFromWSModal = this.toggleRemoveFromWSModal.bind(this);
  }

  focus() {
    let sidebar = Object(react_dom__WEBPACK_IMPORTED_MODULE_2__["findDOMNode"])(this.refs.sidebar);
    sidebar && sidebar.focus();
  }

  isWorkspaceMember() {
    return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('ActiveWorkspaceStore').isMember;
  }

  isLoggedIn() {
    return Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('CurrentUserStore').isLoggedIn;
  }

  getKeyMapHandlers() {
    return {
      nextItem: pm.shortcuts.handle('nextItem', this.focusNext),
      prevItem: pm.shortcuts.handle('prevItem', this.focusPrev),
      select: pm.shortcuts.handle('select', this.selectItem),
      delete: pm.shortcuts.handle('delete', this.handleDeleteShortcut),
      rename: pm.shortcuts.handle('rename', this.renameItem) };

  }

  focusNext(e) {
    e && e.preventDefault();
    this.apiSidebarStore.focusNext();
  }

  focusPrev(e) {
    e && e.preventDefault();
    this.apiSidebarStore.focusPrev();
  }

  selectItem() {
    this.apiSidebarStore.openEditor(this.apiSidebarStore.activeItem);
  }

  renameItem() {
    let apiId = this.apiSidebarStore.activeItem;

    // Show showSignInModal if user is not signed in
    if (!this.isLoggedIn()) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'rename_API',
        origin: 'API_sidebar',
        continueUrl: new URL(window.location.href) });

    }

    if (!this.isWorkspaceMember()) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    }

    apiId && this.refs.list.refs[apiId].handleEditName();
  }

  handleDeleteShortcut() {
    if (!this.isWorkspaceMember()) {
      return pm.mediator.trigger('openUnjoinedWorkspaceModal');
    }

    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('CurrentUserStore').isLoggedIn) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'delete_API',
        origin: 'API_sidebar',
        continueUrl: new URL(window.location.href) });

    }

    let id = this.apiSidebarStore.activeItem,
    name = id && _.find(this.apiSidebarStore.sortedAPIs, api => api.id === id).name;

    id && name && this.toggleDeleteModal(id, name);
  }

  toggleDeleteModal(apiId) {
    pm.mediator.trigger('showAPIDeleteModal', apiId);
  }

  toggleRemoveFromWSModal(apiId) {
    const canDeleteAPI = _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_8__["default"].hasPermission(this.apiListStore.permissions, 'delete', 'api', apiId),
    canShareAPI = _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_8__["default"].hasPermission(this.apiListStore.permissions, 'share', 'api', apiId);

    pm.mediator.trigger('showRemoveFromWorkspaceModal', apiId, {
      type: 'api',
      origin: 'sidebar',
      disableDelete: !canDeleteAPI,
      disableShare: !canShareAPI });

  }

  render() {
    this.apiSidebarStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('APISidebarStore');
    this.apiListStore = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('APIListStore');

    // The API isConsistentlyOffline is only supposed to be used to show the offline state view to the user when he has been consistently offline.
    // For disabling the actions on lack of connectivity, please rely on the socket connected state itself for now.
    // Otherwise, there is a chance of data loss if we let the user perform actions when we are attempting a connection.
    const isConsistentlyOffline = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('SyncStatusStore').isConsistentlyOffline;

    let searchQuery = this.apiSidebarStore.searchQuery;

    if (isConsistentlyOffline && !this.apiListStore.isLoaded) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_APIOffline_APIOffline__WEBPACK_IMPORTED_MODULE_9__["default"], { origin: 'sidebar' });
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_keymaps_KeyMaps__WEBPACK_IMPORTED_MODULE_3__["default"], { handlers: this.getKeyMapHandlers() },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-sidebar', ref: 'sidebar' },
          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_APISidebarMenu_APISidebarMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
            ref: 'menu',
            apiListStore: this.apiListStore }),

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_6__["default"], { identifier: 'api' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_APISidebarList_APISidebarList__WEBPACK_IMPORTED_MODULE_5__["default"], {
              ref: 'list',
              apiListStore: this.apiListStore,
              apiSidebarStore: this.apiSidebarStore,
              searchQuery: searchQuery,

              toggleDeleteModal: this.toggleDeleteModal,
              toggleRemoveFromWSModal: this.toggleRemoveFromWSModal })))));





  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return APISidebarMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(420);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3006);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3170);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2304);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(98);
/* harmony import */ var _postman_aether__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_postman_aether__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(7125);
/* harmony import */ var _constants_Permissions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4257);
/* harmony import */ var _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4259);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(69);
/* harmony import */ var _constants_APITooltips__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(5331);
var _class;












let


APISidebarMenu = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class APISidebarMenu extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      refreshDisabled: false,
      isCreateAPIModalOpen: false };


    this.getAddIconText = this.getAddIconText.bind(this);
    this.openCreateAPIModal = this.openCreateAPIModal.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);

    this.store = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('APISidebarStore');
  }

  openCreateAPIModal() {
    // Show showSignInModal if user is not signed in
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('CurrentUserStore').isLoggedIn) {
      return pm.mediator.trigger('showSignInModal', {
        type: 'createAPI',
        origin: 'API_sidebar',
        continueUrl: new URL(window.location.href),
        subtitle: 'You need an account to continue exploring Postman.' });

    }

    _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_5__["default"].addEventV2({
      category: 'api',
      action: 'start_create_flow',
      label: 'sidebar',
      value: 1 });


    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_10__["default"].transitionTo('build.api.new');
  }

  getAddIconText(isOffline, canCreateAPI) {
    if (isOffline) {
      return _constants_APITooltips__WEBPACK_IMPORTED_MODULE_11__["IS_OFFLINE_TOOLTIP_MSG"];
    }

    if (!canCreateAPI) {
      return _constants_Permissions__WEBPACK_IMPORTED_MODULE_8__["API_PERMISSION_ERROR"];
    }

    return 'Create new API';
  }

  componentWillUnmount() {
    clearTimeout(this.enableRefreshTimeout);
  }

  handleSearchChange(query) {
    this.store.setSearchQuery(query);
  }

  render() {
    const isOnline = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('SyncStatusStore').isSocketConnected,
    canAddAPI = isOnline && !this.props.apiListStore.isCreating,
    canRefresh = canAddAPI && !this.state.refreshDisabled && !this.props.apiListStore.isHydrating,
    currentWorkspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_2__["getStore"])('ActiveWorkspaceStore').id,
    canCreateAPI = _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_9__["default"].hasPermission(this.props.apiListStore.permissions, 'create', 'workspace', currentWorkspaceId);

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListActions__WEBPACK_IMPORTED_MODULE_7__["default"], {
        createNewConfig: {
          tooltip: this.getAddIconText(!isOnline, canCreateAPI),
          disabled: !canAddAPI || !canCreateAPI,
          onCreate: this.openCreateAPIModal,
          xPathIdentifier: 'addAPI' },

        onSearch: this.handleSearchChange,
        searchQuery: this.store.searchQuery }));


  }}) || _class;

/***/ }),

/***/ 15415:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return APISidebarList; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_modules_services_ShareModalService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2998);
/* harmony import */ var _js_modules_services_ManageRolesModalService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2999);
/* harmony import */ var _js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3159);
/* harmony import */ var _APISidebarListItem_APISidebarListItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(15416);
/* harmony import */ var _APISidebarListEmptyItem_APISidebarListEmptyItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15417);
/* harmony import */ var _appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5667);
var _class;








let


APISidebarList = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class APISidebarList extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleSelect = this.handleSelect.bind(this);
    this.handleRename = this.handleRename.bind(this);
    this.handleRemoveFromWS = this.handleRemoveFromWS.bind(this);
  }

  handleSelect(apiId) {
    if (!apiId) {
      return;
    }

    // set focus and open in editor
    this.props.apiSidebarStore.openEditor(apiId);
  }

  handleRemoveFromWS(api) {
    if (!api) {
      return;
    }

    let { apiSidebarStore } = this.props;

    this.props.apiListStore.removeFromWorkspace(api.id, api.name).
    then(() => {

      // Switch focus if selected item got removed
      if (api.id === apiSidebarStore.activeItem) {
        apiSidebarStore.focusPrev({ openEditor: false });
      }

      this.props.toggleRemoveFromWSModal();
    });
  }

  handleRename(apiId, oldName, newName) {
    this.props.apiListStore.rename(apiId, newName).
    catch(() => _.invoke(this.refs, `${apiId}.inlineInput.setState`, { value: oldName }));
  }

  renderEmptyList() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-sidebar-list is-empty' },

        this.props.apiListStore.isHydrating ?
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarLoadingState_SidebarLoadingState__WEBPACK_IMPORTED_MODULE_7__["default"], null) :
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_APISidebarListEmptyItem_APISidebarListEmptyItem__WEBPACK_IMPORTED_MODULE_6__["default"], {
          query: this.props.searchQuery,
          apiListStore: this.props.apiListStore })));




  }

  render() {
    let apiSidebarStore = this.props.apiSidebarStore,
    sortedApis = apiSidebarStore.sortedAPIs;

    if (_.isEmpty(sortedApis)) {
      return this.renderEmptyList();
    }

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'api-sidebar-list' },

        _.map(sortedApis, apiItem => {
          return (
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_XPaths_XPath__WEBPACK_IMPORTED_MODULE_4__["default"], { identifier: apiItem.id, key: apiItem.id },
              react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_APISidebarListItem_APISidebarListItem__WEBPACK_IMPORTED_MODULE_5__["default"], {
                ref: apiItem.id,
                key: apiItem.id,
                id: apiItem.id,

                name: apiItem.name,
                editable: apiItem.isEditable,
                isPublic: apiItem.isPublic,
                shared: Boolean(apiItem.team),
                selected: apiSidebarStore.activeItem === apiItem.id,

                onSelect: this.handleSelect,
                onRename: this.handleRename,
                onDelete: this.props.toggleDeleteModal,
                onShare: _js_modules_services_ShareModalService__WEBPACK_IMPORTED_MODULE_2__["shareAPI"],
                onManageRole: _js_modules_services_ManageRolesModalService__WEBPACK_IMPORTED_MODULE_3__["manageRolesOnAPI"],
                onRemoveFromWorkspace: this.props.toggleRemoveFromWSModal })));



        })));



  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return APISidebarListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5479);
/* harmony import */ var _js_services_UIEventService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2673);
/* harmony import */ var _js_constants_UIEventConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2792);
/* harmony import */ var _js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(7122);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(420);
/* harmony import */ var _js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3211);
/* harmony import */ var _api_editor_common_APIStatusIcon_APIStatusIcon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(5480);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(69);
/* harmony import */ var _util_TooltipText__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(5474);
/* harmony import */ var _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(4259);
/* harmony import */ var _js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(3756);
/* harmony import */ var _constants_Permissions__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(4257);
/* harmony import */ var _constants_APITooltips__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(5331);
var _class;















let


APISidebarListItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class APISidebarListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.handleEditName = this.handleEditName.bind(this);
    this.handleRenameSubmit = this.handleRenameSubmit.bind(this);
    this.handleDropdownActionSelect = this.handleDropdownActionSelect.bind(this);
    this.openAPIInNewTab = this.openAPIInNewTab.bind(this);
    this.getStatusIndicators = this.getStatusIndicators.bind(this);
    this.containerRef = this.containerRef.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.unsubscribeHandler = _js_services_UIEventService__WEBPACK_IMPORTED_MODULE_4__["default"].subscribe(_js_constants_UIEventConstants__WEBPACK_IMPORTED_MODULE_5__["SAMPLE_API_IMPORT"], api => {

      // id of the imported sample API in lessons
      const apiId = _.get(api, 'data.id');

      this.openAPIInNewTab(apiId);
    });
  }

  componentWillUnmount() {
    this.unsubscribeHandler && this.unsubscribeHandler();
  }

  openAPIInNewTab(apiId) {
    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_10__["default"].transitionTo('build.api', { apiId }, {}, { additionalContext: { isNew: true } });
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.selected && this.props.selected) {
      let $node = Object(react_dom__WEBPACK_IMPORTED_MODULE_2__["findDOMNode"])(this.listItem);
      $node && $node.scrollIntoViewIfNeeded && $node.scrollIntoViewIfNeeded();
    }
  }

  containerRef(ele) {
    this.listItem = ele;
  }

  handleClick() {
    this.props.onSelect && this.props.onSelect(this.props.id, this.props.name);
  }

  handleDropdownActionSelect(action) {
    if (this.shouldBlockAction(action)) {
      return;
    }

    switch (action) {

      case _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_SHARE"]:
        this.props.onShare(this.props.id);
        return;
      case _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_MANAGE_ROLES"]:
        this.props.onManageRole(this.props.id);
        return;
      case _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_DELETE"]:
        this.props.onDelete(this.props.id);
        return;
      case _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_REMOVE_FROM_WORKSPACE"]:
        this.props.onRemoveFromWorkspace(this.props.id);
        return;
      case _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_RENAME"]:
        this.handleEditName();
        return;}

  }

  handleEditName() {
    _.invoke(this.listItem, 'editText');
  }

  /**
     * Return true for action type 'Manage Roles', 'Delete', 'Remove From Workspace' and 'Rename'
     * based on whether the user is a member of active workspace or not.
     *
     * Return false if action type is 'Share API'.
     * @param {String} action - action type
     */
  shouldBlockAction(action) {
    // Check whether the user is signed in or not
    if (!Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('CurrentUserStore').isLoggedIn) {
      pm.mediator.trigger('showSignInModal', {
        type: `${action}_API`,
        origin: 'API_sidebar',
        continueUrl: new URL(window.location.href) });


      return true;
    }

    if (action !== _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_SHARE"]) {

      // Check whether the user is a member of active workspace or not
      let isWorkspaceMember = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('ActiveWorkspaceStore').isMember;
      if (!isWorkspaceMember) {
        pm.mediator.trigger('openUnjoinedWorkspaceModal');

        return true;
      }
    }

    // Don't block if user is a member or action type is 'Share API'
    return false;
  }

  handleRenameSubmit(newName) {
    const isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('SyncStatusStore').isSocketConnected;

    if (isOffline) {
      pm.toasts.error('Get online to perform this action');
      return;
    }

    this.props.onRename(this.props.id, this.props.name, newName);
  }

  getStatusIndicators() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_api_editor_common_APIStatusIcon_APIStatusIcon__WEBPACK_IMPORTED_MODULE_9__["default"], {
        isPublic: this.props.isPublic,
        isShared: this.props.shared,
        isEditable: this.props.editable }));


  }

  getActions() {
    let isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('SyncStatusStore').isSocketConnected;
    const permissions = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('APIListStore').permissions;
    const currentWorkspaceId = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('ActiveWorkspaceStore').id;
    const canManageRolesOnAPI = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_7__["getStore"])('CurrentUserStore').team;
    const canRenameAPI = _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_12__["default"].hasPermission(permissions, 'rename', 'api', this.props.id);
    const canRemoveAPIFromWorkspace = _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_12__["default"].hasPermission(permissions, 'removeFromWorkspace', 'workspace', currentWorkspaceId);
    const canDeleteAPI = _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_12__["default"].hasPermission(permissions, 'delete', 'api', this.props.id);
    const canShareAPI = _services_APIPermissionService__WEBPACK_IMPORTED_MODULE_12__["default"].hasPermission(permissions, 'share', 'api', this.props.id);

    return [
    {
      type: _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_SHARE"],
      label: 'Share API',
      isDisabled: isOffline || !canShareAPI,
      hasPermission: canShareAPI },

    {
      type: _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_MANAGE_ROLES"],
      label: 'Manage Roles',
      isDisabled: isOffline || !canManageRolesOnAPI,

      // permitting users based of whether they are a part of team or not and
      // disabling manage roles option accordingly.
      disabledText: isOffline && _constants_APITooltips__WEBPACK_IMPORTED_MODULE_15__["IS_OFFLINE_TOOLTIP_MSG"] || !canManageRolesOnAPI && _constants_Permissions__WEBPACK_IMPORTED_MODULE_14__["MANAGE_ROLES_DISABLED_TEXT"],
      hasPermission: canManageRolesOnAPI },

    {
      type: _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_RENAME"],
      label: 'Rename',
      isDisabled: isOffline || !canRenameAPI,
      hasPermission: canRenameAPI,
      shortcut: 'rename' },

    {
      type: _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_REMOVE_FROM_WORKSPACE"],
      label: 'Remove from workspace',
      isDisabled: isOffline || !canRemoveAPIFromWorkspace,
      hasPermission: canRemoveAPIFromWorkspace },

    {
      type: _constants_APIActionsConstants__WEBPACK_IMPORTED_MODULE_3__["ACTION_TYPE_DELETE"],
      label: 'Delete',
      isDisabled: isOffline || !canDeleteAPI,
      shortcut: 'delete' }];


  }

  getMenuItems() {
    return _.chain(this.getActions()).
    map(action => {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__["MenuItem"], {
            key: action.type,
            refKey: action.type,
            disabled: action.isDisabled,
            disabledText: action.disabledText || Object(_util_TooltipText__WEBPACK_IMPORTED_MODULE_11__["default"])(action.isDisabled, !action.hasPermission) },

          react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', { className: 'api-action-item' },
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-label' },
              action.label),


            action.shortcut &&
            react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'dropdown-menu-item-shortcut' }, Object(_js_controllers_ShortcutsList__WEBPACK_IMPORTED_MODULE_13__["getShortcutByName"])(action.shortcut)))));




    }).value();
  }

  getActionsMenuItems() {
    const menuItems = this.getMenuItems();

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_Dropdowns__WEBPACK_IMPORTED_MODULE_8__["DropdownMenu"], {
          className: 'api-dropdown-menu',
          'align-right': true },

        menuItems));


  }


  render() {
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_containers_apps_requester_sidebar_SidebarListItem__WEBPACK_IMPORTED_MODULE_6__["default"], {
        ref: this.containerRef,
        text: this.props.name,
        isSelected: this.props.selected,
        onClick: this.handleClick,
        onRename: this.handleRenameSubmit,
        statusIndicators: this.getStatusIndicators,
        moreActions: this.getActionsMenuItems(),
        onActionsDropdownSelect: this.handleDropdownActionSelect }));


  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ }),

/***/ 15417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return APISidebarListEmptyItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(80);
/* harmony import */ var _js_components_base_Buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3170);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(420);
/* harmony import */ var _js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3192);
/* harmony import */ var _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2304);
/* harmony import */ var _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3947);
/* harmony import */ var _appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(7128);
/* harmony import */ var _appsdk_sidebar_SidebarNoResultsFound_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(7127);
/* harmony import */ var _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(69);
/* harmony import */ var _constants_APITooltips__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(5331);
var _class;











let


APISidebarListEmptyItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_1__["observer"])(_class = class APISidebarListEmptyItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(props) {
    super(props);

    this.state = {
      isCreateAPIModalOpen: false };


    this.handleOffline = this.handleOffline.bind(this);
    this.handleLoggedOut = this.handleLoggedOut.bind(this);
    this.getComponent = this.getComponent.bind(this);
    this.openCreateAPIModal = this.openCreateAPIModal.bind(this);
  }

  openCreateAPIModal() {
    _js_modules_services_AnalyticsService__WEBPACK_IMPORTED_MODULE_5__["default"].addEventV2({
      category: 'api',
      action: 'start_create_flow',
      label: 'sidebar',
      value: 1 });


    _js_services_NavigationService__WEBPACK_IMPORTED_MODULE_9__["default"].transitionTo('build.api.new');
  }

  handleOffline() {
    this.props.apiListStore.reload();
  }

  handleLoggedOut() {
    _js_modules_services_AuthHandlerService__WEBPACK_IMPORTED_MODULE_6__["default"].initiateLogin();
  }

  getComponent(title, description, buttonText, handler) {
    const isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected;

    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarEmptyState_SidebarEmptyState__WEBPACK_IMPORTED_MODULE_7__["default"], {
        illustration: 'no-api',
        title: title,
        message: description,
        action: {
          label: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('span', null, this.props.apiListStore.isCreating ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_js_components_base_LoadingIndicator__WEBPACK_IMPORTED_MODULE_4__["default"], null) : buttonText),
          handler: handler,
          tooltip: isOffline && _constants_APITooltips__WEBPACK_IMPORTED_MODULE_10__["IS_OFFLINE_TOOLTIP_MSG"] },

        hasPermissions: !this.props.apiListStore.isCreating && !isOffline }));



  }

  getContent() {
    let isLoggedOut = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('CurrentUserStore').isLoggedIn,
    isOffline = !Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_3__["getStore"])('SyncStatusStore').isSocketConnected,
    title = '',
    description = '',
    buttonText = '';

    if (isLoggedOut) {
      title = 'Sign in to create APIs';
      description = 'APIs define related collections and environments under a consistent schema.';
      buttonText = 'Sign in to create APIs';

      return this.getComponent(title, description, buttonText, this.handleLoggedOut);
    }

    if (this.props.query && _.isEmpty(this.props.apiListStore.sortedAPIs)) {
      return (
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_sidebar_SidebarNoResultsFound_SidebarNoResultsFound__WEBPACK_IMPORTED_MODULE_8__["default"], { searchQuery: this.props.query, illustration: 'no-api' }));

    }

    title = 'No APIs yet';
    description = 'APIs define related collections and environments under a consistent schema.';
    buttonText = 'Create an API';
    return (
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null,
        this.getComponent(title, description, buttonText, this.openCreateAPIModal)));


  }

  render() {
    return (
      this.getContent());

  }}) || _class;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);