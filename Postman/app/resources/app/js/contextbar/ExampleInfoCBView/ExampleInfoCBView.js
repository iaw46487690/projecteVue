(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[33],{

/***/ 15375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ExampleInfoCBView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3470);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3667);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(420);
/* harmony import */ var _js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(444);









/**
                                                                  * Component for showing info about the example in the context bar
                                                                  *
                                                                  * @component
                                                                  */
function ExampleInfoCBView(props) {
  const { id } = props.contextData,
  example = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('ResponseStore').find(id),
  parentRequestId = _.get(example, 'parent.id'),
  owner = _.get(Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('RequestStore').find(parentRequestId), 'owner'),

  info = example ? {
    id: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_4__["getStore"])('CurrentUserStore').isLoggedIn ? Object(_js_utils_uid_helper__WEBPACK_IMPORTED_MODULE_5__["composeUID"])(id, owner) : id,
    createdOn: example.createdAt } :
  {};

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'example-info-cb' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__["ContextBarViewHeader"], {
        title: 'Example details',
        onClose: props.onClose }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'example-info-cb__container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_2__["EntityMetaInfoView"], {
          userFriendlyEntityName: 'example',
          info: info }))));




}

ExampleInfoCBView.propTypes = {
  contextData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired };


ExampleInfoCBView.defaultProps = {
  contextData: {} };
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(10)))

/***/ })

}]);