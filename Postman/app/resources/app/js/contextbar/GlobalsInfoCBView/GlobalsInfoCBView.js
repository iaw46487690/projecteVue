(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[28],{

/***/ 15363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GlobalsInfoCBView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3470);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3667);






/**
                                                                                                  * Component for showing info about the globals in the context bar
                                                                                                  *
                                                                                                  * @component
                                                                                                  */
function GlobalsInfoCBView(props) {
  const { id } = props.contextData,
  info = { id };

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'globals-info-cb' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__["ContextBarViewHeader"], {
        title: 'Details',
        onClose: props.onClose }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'globals-info-cb__container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_2__["EntityMetaInfoView"], {
          userFriendlyEntityName: 'globals',
          info: info }))));




}

GlobalsInfoCBView.propTypes = {
  contextData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired };


GlobalsInfoCBView.defaultProps = {
  contextData: {} };

/***/ })

}]);