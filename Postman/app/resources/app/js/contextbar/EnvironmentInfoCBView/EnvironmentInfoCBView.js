(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[31],{

/***/ 15373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EnvironmentInfoCBView; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(93);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_molecule__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3470);
/* harmony import */ var _appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3667);
/* harmony import */ var _monitors_components_context_bar_MonitorContextBarList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(15360);
/* harmony import */ var _js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(420);
/* harmony import */ var _mocks_components_InfoPaneMockListing__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(15361);










/**
                                                                                           * Component for showing info about the environment in the context bar
                                                                                           *
                                                                                           * @component
                                                                                           */
function EnvironmentInfoCBView(props) {
  const { id } = props.contextData,
  environment = Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('EnvironmentStore').find(id),

  // @todo[EntityInTabs]: Pipe data via the EnvironmentInfoCBController
  info = environment ? {
    id: Object(_js_stores_get_store__WEBPACK_IMPORTED_MODULE_5__["getStore"])('CurrentUserStore').isLoggedIn ? environment.uid : id,
    createdOn: environment.createdAt,
    owner: environment.owner } :
  {};

  return (
    react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'environment-info-cb' },
      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_appsdk_contextbar_ContextBarViewHeader__WEBPACK_IMPORTED_MODULE_3__["ContextBarViewHeader"], {
        title: 'Environment details',
        onClose: props.onClose }),

      react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', { className: 'environment-info-cb__container' },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_base_molecule__WEBPACK_IMPORTED_MODULE_2__["EntityMetaInfoView"], {
          userFriendlyEntityName: 'environment',
          info: info }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_mocks_components_InfoPaneMockListing__WEBPACK_IMPORTED_MODULE_6__["default"], {
          entity: 'environment',
          environmentUid: info.id }),

        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_monitors_components_context_bar_MonitorContextBarList__WEBPACK_IMPORTED_MODULE_4__["MonitorContextBarList"], {
          entity: 'environment',
          id: environment.uid }))));




}

EnvironmentInfoCBView.propTypes = {
  contextData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  onClose: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired };


EnvironmentInfoCBView.defaultProps = {
  contextData: {} };

/***/ })

}]);